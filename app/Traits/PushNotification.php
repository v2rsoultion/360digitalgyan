<?php

namespace App\Traits;
trait PushNotification{

	public function SendAndroidNotification($notificationTo,$msg)
    {
        $androidTopics  = config('custom.notification_topics_android');
        
        if ($notificationTo=='all'){
            /* for all apps */
            foreach ($androidTopics as $topic){
                if ($topic!='') {
                    $this->AndroidPushNotification($msg,$topic);
                }
            }
        }else{
            /* according to category */
            $topic = $androidTopics[$notificationTo];
            if(!empty($topic)){
                $this->AndroidPushNotification($msg,$topic);
            }
        }
    }


    public function SendIphoneNotification($notificationTo,$msg,$title,$desc)
    {
        $IOSTopics      = config('custom.notification_topics_ios');
        
        if ($notificationTo=='all'){
            foreach ($IOSTopics as $topic){
                if ($topic!=''){
                    $this->IphonePushNotification($msg,$title,$desc,$topic);
                }
            }
        }else{
            /* according to category */
            $topic = $IOSTopics[$notificationTo];
            if(!empty($topic)){
                $this->IphonePushNotification($msg,$title,$desc,$topic);
            }
        }
    }

    public function AndroidPushNotification($msg,$topic)
    {
        $path_to_firebase_cm = 'https://fcm.googleapis.com/fcm/send';
        $fields = array(
           'data' =>  array('title' => '', 'body' => $msg),
           'to'   =>  $topic, 
        );

        $headers = array(
          'Authorization:key= AIzaSyC-FuRTvdSTXnd5DAGmVIgcX05IAea9V1Y',
          'Content-Type:application/json'
        );

        $payload =json_encode($fields);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $path_to_firebase_cm); 
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 ); 
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    public function IphonePushNotification($msg,$title,$desc,$topic)
    {
        $path_to_firebase_cm = 'https://fcm.googleapis.com/fcm/send';   
        $fields = array(
            'notification' => [
                'title' =>  $title,
                'body'  =>  $desc,
            ],
            'data' => [
                'data' => $msg,
            ],
            'to'  => $topic,
        );

        $headers = array(
        'Authorization:key= AIzaSyC-FuRTvdSTXnd5DAGmVIgcX05IAea9V1Y',
        'Content-Type:application/json'
        );        
        $payload =json_encode($fields);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $path_to_firebase_cm); 
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 ); 
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        print_r($result);
        curl_close($ch);
        return $result;
    }
}  
