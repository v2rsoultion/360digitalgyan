<?php

//use File;

    use Illuminate\Support\Facades\Crypt;
    use App\Model\Pages\Pages;
    use Carbon\Carbon;
    use Session as NSESSION;
    use App\Model\Category\Category;
    use App\Model\SubCategory\SubCategory;
    use App\Model\VideoTutorials\VideoTutorials;
    use App\Model\Terms\Terms;
    use App\Model\InterviewQuestions\InterviewQuestions;
    use App\Model\QuesAndAns\QuesAndAns;
    use App\Model\PracticeQuiz\PracticeQuiz;
    use App\Model\StudyMaterial\StudyMaterial;
    use App\Model\LikeDislike\LikeDislike;
    use App\Model\Transaction\Transaction;
    use App\Model\SeoUser\SeoUser;
    use App\Model\SeoUser\SeoUserPoints;
    use App\Model\AllQuesLikes\AllQuesLikes;

    function p($p, $exit = 1)
    {
        echo '<pre>';
        print_r($p);
        echo '</pre>';
        if ($exit == 1){
            exit;
        }
    }

    function getAppCategory($type)
    {
        switch ($type) {
            case 'digital-marketing'    : $cat = 30 ;  break;
            case 'web-development'      : $cat = 52 ;  break;
            case 'software-testing'     : $cat = 50 ;  break;
            case 'plc-scada'            : $cat = 55 ;  break;
            case 'python'               : $cat = 53 ;  break;
            case 'java'                 : $cat = 49 ;  break;
            case 'android'              : $cat = 10 ;  break;
            default                     : $cat = '' ;  break;
        }
        return $cat;
    }


    function headerMenus($true)
    {
        if ($true==true){
            $headerMenus = array(
                'digital-marketing' => array(
                    'id'    => 30,
                    'slug'  => 'digital-marketing',
                    'title' => 'Digital Marketing'
                ),
                'web-development' => array(
                    'id'    => 52,
                    'slug'  => 'web-development',
                    'title' => 'Web Development'
                ),
                'software-testing' => array(
                    'id'    => 50,
                    'slug'  => 'software-testing',
                    'title' => 'Software Testing'
                ),
                'plc-scada' => array(
                    'id'    => 55,
                    'slug'  => 'plc-scada',
                    'title' => 'PLC-SACADA'
                ),
                'python' => array(
                    'id'    => 53,
                    'slug'  => 'python',
                    'title' => 'Python'
                ),
                'java' => array(
                    'id'    => 49,
                    'slug'  => 'java',
                    'title' => 'Java'
                ),
                'android' => array(
                    'id'    => 10,
                    'slug'  => 'android',
                    'title' => 'Android'
                )
            );

            return $headerMenus;
        }
    }


    function givePointsToUser($arr)
    {
        $points360  = config('custom.points');
        /* save transaction */
        $trans = new Transaction;
        $trans->txn_user_id     = $arr['user_id'];
        $trans->user_app_type   = $arr['app_type'];
        $trans->txn_question_id = $arr['ques_ans_id'];
        $trans->txn_points      = $arr['points'];
        $trans->txn_type        = 1; // 1 => add , 2 => less
        $trans->txn_flag        = $arr['point_for'];
        $trans->save();
        
        /* save points in usersPoints table */
        $checkUserPoint = SeoUserPoints::where([
                            'seo_point_user_id' => $arr['user_id'],
                            'seo_point_user_app_type'=> $arr['app_type']
                        ])->first();

        if (!empty($checkUserPoint)){
            
            SeoUserPoints::where([
                            'seo_points_id' => $checkUserPoint->seo_points_id
                            // 'seo_point_user_id' => $arr['user_id'],
                            // 'seo_point_user_app_type'=> $arr['app_type']
                        ])->update([
                            'seo_point_points' => ($checkUserPoint->seo_point_points+$arr['points'])
                        ]);

        }else{
            $userPoint                          = new SeoUserPoints;
            $userPoint->seo_point_user_id       = $arr['user_id'];
            $userPoint->seo_point_user_app_type = $arr['app_type'];
            $userPoint->seo_point_user_badges   = '';
            $userPoint->seo_point_points        = $arr['points'];
            $userPoint->save();
        }
    }

    function getSubCategories($cat_id,$type)
    {
        $query = SubCategory::Query();

        if ($type==1){
            $query = $query->whereHas('InterviewQuestions',function($q) use ($cat_id){
                                $q->where('iq_ques_ans_catid',$cat_id);
                            });
        }elseif($type==2){
            $query = $query->whereHas('Quiz',function($q) use ($cat_id){
                                $q->where('qustion_sec_id',$cat_id);
                            });
        }

        $data = $query->where('category_id',$cat_id)->where('status',1)->orderBy('category_order','ASC')->get()->toArray();
        return $data;
    }


    function getVideosCount($cat,$language)
    {
        $query = VideoTutorials::where('category_id',$cat)->where('status',1);
        if ($language!='all'){
            $query = $query->where('language',$language);
        }
        return $query->get()->count();
    }

    function getStudy($cat)
    {
        $query = StudyMaterial::where('study_material_catid',$cat)->where('study_ques_ans_status',1)->get();
        return $query->count();
    }

    function getTechTerm($cat)
    {
        $query = InterviewQuestions::where('question_type',2)->where('iq_ques_ans_catid',$cat)->where('iq_ques_ans_status',1)->get();
        return $query->count();
    }

    function getQuesAns($cat)
    {
        $query = QuesAndAns::where('ques_main_cat_id',$cat)->where('question_status',1)->get();
        return $query->count();
    }

    function getTerm($cat)
    {
        $query = Terms::where('term_cat',$cat)->where('term_status',1)->get();
        return $query->count();
    }

    function getPraQuiz($cat)
    {
        $query = PracticeQuiz::where('qustion_sec_id',$cat)->where('question_trial',0)->get();
        return $query->count();
    }

    function unSlugify($val)
    {
        return ucwords(str_replace('-',' ',$val));
    }

    function getTextTransform($text,$value)
    {
        if ($value!=0){
            $textTrans = substr(html_entity_decode($text),0,$value);    
        }else{
            $textTrans = html_entity_decode($text);
        }
        return $textTrans;
    }

    function getLikeDislike($user,$quesAns,$type)
    {
        $data = LikeDislike::where(['user_id'=>$user,'like_dislike_status'=>$type,'like_dislike_flag'=>$quesAns])->pluck('question_answer_id')->toArray();
        return $data;
    }

    function getIqLike($id)
    {
        $data = AllQuesLikes::where(['like_user_id'=>$id,'like_flag'=>1])->pluck('like_question_id')->toArray();
        return $data;
    }

    /**
     *  Get logged in user data
     *  @Shree on 22 Aug 2018
     * */
    function get_loggedin_user_data()
    {
        $user_data  = array();
        $admin_user = Auth::guard('admin')->user();
        if (!empty($admin_user['admin_id']))
        {
            $user_data = array(
                'admin_id'          => $admin_user['admin_id'],
                'admin_name'        => $admin_user['admin_name'],
                'email'             => $admin_user['email'],
                'type'              => $admin_user['type'],
                'admin_profile_img' => $admin_user['admin_profile_img'],
            );
        }
        return $user_data;
    }

    function get_loggedin_customer_data()
    {
        $user_data  = array();
        $admin_user = Auth::guard('web')->user();
        if (!empty($admin_user['customer_id']))
        {
            $user_data = array(
                'customer_id' => $admin_user['customer_id'],
                'fname'       => $admin_user['fname'],
                'email'       => $admin_user['email'],
            );
        }
        return $user_data;
    }

    /**
     *  Get last query
     *  @Shree on 22 Aug 2018
     * */
    function last_query($raw = false, $last = false)
    {
        DB::enableQueryLog();
        $log = DB::getQueryLog();

        if ($raw)
        {
            foreach ($log as $key => $query)
            {
                $log[$key] = vsprintf(str_replace('?', "'%s'", $query['query']), $query['bindings']);
            }
        }

        // return ($last) ? end($log) : $log; // return single table query
        return $log;  // return join tables queries 
    }

    /**
     * get website name
     * */
    function get_app_name()
    {
        return config('app.name');
    }

    /**
     *  Check string is empty or not
     *  @Shree on 22 Aug 2018
     * */
    function check_string_empty($value)
    {
        if (!empty($value))
        {
            return $value;
        }
        else
        {
            return '';
        }
    }

    /**
     *  Add default blank option to select box
     *  @Shree on 22 Aug 2018
     * */
    function add_blank_option($arr, $option)
    {
        $arr_option = array();
        if (!empty($option))
        {
            $arr_option[''] = $option;
        }
        else
        {
            $arr_option[''] = '';
        }
        // operator on array
        $result = $arr_option + $arr;

        return $result;
    }

    /**
     *  Get custom date formate
     *  @Shree on 22 Aug 2018
     * */
    function get_formatted_date($date)
    {
        $return      = array();
        $date_format = Config::get('custom.date_format');
        if (!empty($date))
        {
            $return = date($date_format, strtotime($date));
        }
        else
        {
            $return = '';
        }
        return $return;
    }

    /**
     *  Get encrypted value
     *  @Shree on 22 Aug 2018
     * */
    function get_encrypted_value($key, $encrypt = false)
    {

        $encrypted_key = null;
        if (!empty($key))
        {
            if ($encrypt == true)
            {
                $key = Crypt::encrypt($key);
            }
            $encrypted_key = $key;
        }
        return $encrypted_key;
    }

    /**
     *  Get decrypted value
     *  @Shree on 22 Aug 2018
     * */
    function get_decrypted_value($key, $decrypt = false)
    {
        $decrypted_key = null;
        if (!empty($key))
        {
            if ($decrypt == true)
            {
                $key = Crypt::decrypt($key);
            }
            $decrypted_key = $key;
        }
        return $decrypted_key;
    }

    /**
     *  Check file exist or not
     *  @Shree on 22 Aug 2018
     * */
    function check_file_exist($file_name, $custome_key)
    {
        $return_file        = '';
        $config_upload_path = \Config::get('custom.' . $custome_key);
        if (!empty($file_name))
        {
            if (is_file($config_upload_path['display_path'] . $file_name))
            {
                $return_file = $config_upload_path['display_path'] . $file_name;
            }
        }
        return $return_file;
    }

    /**
     *  Get all categories and sub categories
     *  @Shree on 23 Aug 2018
     * Type 0 = Root Categories
     * Type 1 = Sub categories
     * */
    function get_total_counter($modelname, $condition_arr = false)
    {

        $count = 0;
        if (!empty($condition_arr))
        {
            $modelname = $modelname->where($condition_arr);
            $count     = $modelname->count();
        }
        return $count;
    }

    function get_all_article_category($category_id = null, $request)
    {




        $loginInfo         = get_loggedin_user_data();
        $article_category  = [];
        $arr_category_list = ArticleCategory::where(function($query) use ($request)
            {

                if (!empty($category_id) && !empty($category_id) && $category_id != null)
                {
                    $query->where('category_id', "=", $category_id);
                }
                if (!empty($request) && !empty($request->has('category_id')) && $request->get('category_id') != null)
                {
                    $query->where('category_id', "=", $request->get('category_id'));
                }
                if (!empty($request) && !empty($request->has('category_title')) && $request->get('category_title') != null)
                {
                    $query->where('category_title', 'like', '%' . $request->get('category_title') . '%');
                }
            })->get();
        if (!empty($arr_category_list))
        {

            foreach ($arr_category_list as $value)
            {
                $article_category[] = array(
                    'article_category_id' => $value['article_category_id'],
                    'category_title'      => $value['category_title'],
                    'status'              => $value['status'],
                    'created_at'          => $value['created_at'],
                    'updated_at'          => $value['updated_at'],
                );
            }
        }

        return $article_category;
    }

    function masterpages($pages_id = '')
    {
        if ($pages_id == '')
        {
            $Pages = Pages::get();
            return $Pages;
        }
        else
        {


            $Pages = Pages::find($pages_id);



            return $Pages;
        }
    }

    function services($services_id = '')
    {
        if ($services_id == '')
        {
            $services = Services::get();
            return $services;
        }
        else
        {
            $services = Services::find($services_id);
            return $services;
        }
    }

    function get_customer_pan($customer_id)
    {
        $return_file = [];
        $pan         = CustomerDocuments::where('customer_id', $customer_id)
                ->where('type', 'pan')->orderBy('created_at', 'DESC')->first();
        if (!empty($pan->filename))
        {
            $file = check_file_exist($pan->filename, 'customer_pan');
            if ($file)
            {
                $return_file['file'] = $file;
            }
            $return_file['document_id'] = $pan->document_id;
        }
        return $return_file;
    }

    function get_customer_aadhaar($customer_id)
    {
        $return_file = [];
        $aadhaar     = CustomerDocuments::where('customer_id', $customer_id)
                ->where('type', 'aadhaar')->orderBy('created_at', 'DESC')->first();
        if (!empty($aadhaar->filename))
        {
            $file = check_file_exist($aadhaar->filename, 'customer_aadhaar');
            if ($file)
            {
                $return_file['file'] = $file;
            }
            $return_file['document_id'] = $aadhaar->document_id;
        }
        return $return_file;
    }

    function get_plan()
    {
        $return_plan = [];
        $plan        = DB::table('plan')->where('status', 1)->get()->toArray();
        if (!empty($plan))
        {
            $plan        = json_decode(json_encode($plan), true);
            $return_plan = array_column($plan, NULL, 'type');
        }
        return $return_plan;
    }

    function get_plan_detail($plan_type = '')
    {
        $return_plan = [];
        $plan_detail = DB::table('plan_details')->get()->toArray();
        if (!empty($plan_detail))
        {
            $return_plan = json_decode(json_encode($plan_detail), true);
        }
        return $return_plan;
    }

    function get_customer_payment($customer_payment_id)
    {
        $payment_data = [];
        if (!empty($customer_payment_id))
        {
            $payment_data = DB::table('customer_payments as cp')
                ->where('cp.customer_payment_id', $customer_payment_id)
                ->join('customer_questions as cq', 'cq.customer_question_id', '=', 'cp.customer_question_id')
                ->join('plan as p', 'cp.plan_id', '=', 'p.plan_id')
                ->orderBy('cp.payment_id', 'DESC')
                ->orderBy('cq.customer_question_id', 'DESC')
                ->where('cp.payment_status', 2)
                ->where('cq.status', 2)
                ->select('cq.ques_no', 'cq.customer_question_id', 'p.*', 'cp.*')
                ->first();

            if ($payment_data->payment_id)
            {
                $payment_data            = json_decode(json_encode($payment_data), true);
                $payment_data['ques_no'] = json_decode($payment_data['ques_no'], true);
            }
        }
        return $payment_data;
    }

    //get payment data with status
    function get_payment_data()
    {
        $payment_data = [];
        $customer     = Auth::guard("web")->user();
        if (!empty($customer->customer_id))
        {
            $payment_data = DB::table('customer_payments as cp')
                ->where('cp.customer_id', $customer->customer_id)
                ->join('customer_questions as cq', 'cq.customer_question_id', '=', 'cp.customer_question_id')
                ->join('plan as p', 'cp.plan_id', '=', 'p.plan_id')
                ->orderBy('cp.payment_id', 'DESC')
                ->orderBy('cq.customer_question_id', 'DESC')
                ->where('cp.payment_status', 2)
                ->where('cq.status', 2)
                ->select('cq.ques_no', 'cq.customer_question_id', 'p.*', 'cp.*')
                ->first();

            if ($payment_data->payment_id)
            {
                $payment_data            = (array) $payment_data;
                $payment_data['ques_no'] = json_decode($payment_data['ques_no'], true);
            }
        }
        return $payment_data;
    }

    function getUserRank($userid)
    {
        // $ranking = DB::table('seo_users as x')
        //             ->select('seo_users_id','user_points')
        //             ->select('COUNT(*)+1 AS rank_upper')
        //             ->where('user_points','>','x.user_points')
        //             ->select('COUNT(*) AS rank_lower')
        //             ->where('user_points','>=','x.user_points')
        //             ->select('COUNT(DISTINCT user_points)')
        //             ->where('user_points','>=','x.user_points')
        //             ->where('x.seo_users_id',$userid);
        // return $ranking;
        $ranking = DB::select( DB::raw("SELECT seo_users_id, user_points, (SELECT COUNT(*)+1 FROM seo_users WHERE user_points>x.user_points) AS rank_upper, (SELECT COUNT(*) FROM seo_users WHERE user_points>=x.user_points) AS rank_lower, (SELECT COUNT(DISTINCT user_points) FROM seo_users WHERE user_points>=x.user_points) AS rank_overall FROM `seo_users` x WHERE x.seo_users_id = '".$userid."'"));
        return $ranking;
    }

    function amount_format($money)
    {
        $amount = number_format($money);
        if ($money > 0)
        {
            $amount = ' &#x20B9; ' . $amount;
        }
        return $amount;
    }

    function display_date($date)
    {
        if ($date)
        {
            $date = date('d-m-Y', strtotime($date));
        }
        return $date;
    }

    function modify_file_name($file, $prefix = '', $file_type = '', $modify = true)
    {
        if ($modify)
        {
            $extension = $file->getClientOriginalExtension();
            $file      = $file_type . '-' . date('dmYHis');
            if ($prefix)
            {
                $file = $prefix . '-' . $file;
            }
            $filename = $file . '.' . $extension;
        }
        else
        {
            $filename = $file->getClientOriginalName();
        }
        return $filename;
    }

    function get_reserved_text()
    {
        return $text = '©' . '' . date('Y') . ' ' . get_app_name() . ' - All Rights Reserved.';
    }

    function get_support_email()
    {
        return \Config::get('custom.support_email');
    }

    function get_site_owner_info()
    {
        $info['email']   = \Config::get('custom.support_email');
        $info['phone']   = 9876543210;
        $info['website'] = url('/');
        $info['address'] = '425, Lorem ipsum dolor sit amet, consect-etur sed quis nostrud exercitation.';
        return $info;
    }

    function get_social_account()
    {
        return $arr_account = \Config::get('custom.social_account');
    }
?>