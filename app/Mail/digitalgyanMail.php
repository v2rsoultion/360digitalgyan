<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class digitalgyanMail extends Mailable
{
    use Queueable, SerializesModels;
    public $emailData;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($emailData,$view)
    {
        $this->emailData = $emailData;
        $this->view      = $view;
        if (!empty($emailData->subject)){
            $this->subject = $emailData->subject;
        }else{
            $this->subject = "360digitalgyan";
        }
        // $this->from('noreply@360digitalgyan.com');
        // $this->replyTo('no-reply@360digitalgyan.com','360digitalgyan');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view($this->view);
    }
}
