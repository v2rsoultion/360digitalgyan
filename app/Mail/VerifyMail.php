<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class VerifyMail extends Mailable
{

    use Queueable,SerializesModels;
    public $customer;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($customer, $view)
    {
        $this->customer = $customer;
        $this->view     = $view;
        if (!empty($customer->subject))
        {
            $this->subject = $customer->subject;
        }
        else
        {
            $this->subject = 'Account Verification';
        }
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('example@example.com')->view($this->view);
    }


    // $customer= (object) array(
    //    'admin_name' => $data['name'],
    //    'email' => $data['email'],
    //    'password' =>$data['password'],
    //    'subject'=>'Login Credentials'
    // );

    // $view           = 'email.welcome-subadmin';
    // Mail::to($customer->email)->send(new VerifyMail($customer, $view));
    // return view('admin-panel.auth.view_subadmins');
}
