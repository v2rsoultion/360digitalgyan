<?php

namespace App\Model\Terms;

use Illuminate\Database\Eloquent\Model;

class Terms extends Model
{
    Protected $table 	= "terms";
    Public $timestamps 	= false;
    Protected $primaryKey = "term_id";
}
