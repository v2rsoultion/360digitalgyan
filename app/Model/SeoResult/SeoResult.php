<?php

namespace App\Model\SeoResult;

use Illuminate\Database\Eloquent\Model;

class SeoResult extends Model
{
    Protected $table = "seo_results";
    Protected $primaryKey = "seo_result_id";
    Public $timestamps 	= false;
}
