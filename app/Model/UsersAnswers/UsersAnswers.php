<?php

namespace App\Model\UsersAnswers;

use Illuminate\Database\Eloquent\Model;

class UsersAnswers extends Model
{
	protected 	$table      = 'user_answers';
    protected 	$primaryKey = 'answer_id';
    public 		$timestamps = false;
    // protected 	$connection = 'mysqlsecond';

    public function getAnsUser()
    {
    	return $this->belongsTo('App\Model\SeoUser\SeoUser','ans_user_id');
    }

    public function getAnsQues()
    {
    	return $this->belongsTo('App\Model\QuesAndAns\QuesAndAns','ans_ques_id');
    }

    public function getAnsCat()
    {
        return $this->belongsTo('App\Model\Category\Category','main_ans_cat');
    }

    public function getAnsLikes()
    {
        return $this->hasMany('App\Model\LikeDislike\LikeDislike','question_answer_id')->where('like_dislike_flag',2)->where('like_dislike_status',1);
    }

    public function getAnsDisLikes()
    {
        return $this->hasMany('App\Model\LikeDislike\LikeDislike','question_answer_id')->where('like_dislike_flag',2)->where('like_dislike_status',2);
    }
}
