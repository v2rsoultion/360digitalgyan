<?php

namespace App\Model\StudyMaterial;

use Illuminate\Database\Eloquent\Model;

class StudyMaterial extends Model
{
	protected $table      = 'study_material';
	protected $primaryKey = 'study_material_id';
    public $timestamps = false;    
	// protected $connection = 'mysqlsecond';

	public function getCategory()
    {
        return $this->belongsTo('App\Model\Category\Category','study_material_catid');
    }

    public function getSubCategory()
    {
        return $this->belongsTo('App\Model\SubCategory\SubCategory','sub_cat_id');
    }
    public function getLikes()
    {
        return $this->hasMany('App\Model\AllQuesLikes\AllQuesLikes','like_question_id')->where('like_flag',2);
    }
    public function getViews()
    {
        return $this->hasMany('App\Model\AllQuesViews\AllQuesViews','view_question_id')->where('view_flag',2);
    }
    public function getComments()
    {
        return $this->hasMany('App\Model\AllQuesComments\AllQuesComments','comment_question_id')->where('comment_flag',2);
    }
}