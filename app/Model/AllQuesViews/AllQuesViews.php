<?php

namespace App\Model\AllQuesViews;

use Illuminate\Database\Eloquent\Model;

class AllQuesViews extends Model
{
    Protected $table = "view_questions_all";
    Protected $primaryKey ="view_id";
    public $timestamps = false;
}
