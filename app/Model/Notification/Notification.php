<?php

namespace App\Model\Notification;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
	protected $table      	= 'notification';
    protected $primaryKey 	= 'notification_id';
    public $timestamps 		= false;

	public function getCategory(){
	    return $this->belongsTo('App\Model\Category\Category','notification_flag');
	}
	public function getSubCat()
	{
		return $this->belongsTo('App\Model\SubCategory\SubCategory','sub_category_id');
	}
}
