<?php

namespace App\Model\NewsCategory;

use Illuminate\Database\Eloquent\Model;

class NewsCategory extends Model
{
	protected $table      = 'news_category';
    protected $primaryKey = 'news_category_id';
}
