<?php

namespace App\Model\QuizQuestions;

use Illuminate\Database\Eloquent\Model;

class QuizQuestions extends Model
{
	protected $table      = 'question_quiz';
    protected $primaryKey = 'qustion_id';
    Public $timestamps = false;

    public function getCategory()
    {
    	return $this->belongsTo('App\Model\Category\Category','qustion_sec_id');
    }

    public function getSubCategory()
    {
    	return $this->belongsTo('App\Model\SubCategory\SubCategory','qustion_sub_sec_id');
    }
}