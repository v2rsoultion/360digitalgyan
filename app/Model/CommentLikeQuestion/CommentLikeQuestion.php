<?php

namespace App\Model\CommentLikeQuestion;

use Illuminate\Database\Eloquent\Model;

class CommentLikeQuestion extends Model
{
	Protected $table = "comment_like_question";
    Protected $primaryKey ="comment_like_id";
    public $timestamps = false;
}