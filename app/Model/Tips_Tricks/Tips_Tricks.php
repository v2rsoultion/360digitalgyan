<?php

namespace App\Model\Tips_Tricks;

use Illuminate\Database\Eloquent\Model;

class Tips_Tricks extends Model
{
    Protected $table = "tips";
    Protected $primaryKey = "tip_id";
    Public $timestamps = false;

    public function getCategory(){
        return $this->belongsTo('App\Model\Category\Category','tip_cat');
    }
    public function getSubCat()
    {
    	return $this->belongsTo('App\Model\SubCategory\SubCategory','tip_subcat');
    }
}
