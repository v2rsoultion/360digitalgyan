<?php

namespace App\Model\Package;

use Illuminate\Database\Eloquent\Model;

class PackagePaperMap extends Model
{
    protected $table      = 'online_package_paper_mapp';
    protected $primaryKey = 'package_paper_mapp_id';

    public function getAdminInfo()
    {
        return $this->belongsTo('App\Admin', 'admin_id');
    }

    public function getPackageInfo()
    {
        return $this->belongsTo('App\Model\Package\Package', 'package_id');
    }

    public function getPaperInfo()
    {
        return $this->belongsTo('App\Model\Papers\Papers', 'paper_id');
    }
}
