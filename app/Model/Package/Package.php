<?php

namespace App\Model\Package;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    protected $table      = 'online_packages';
    protected $primaryKey = 'package_id';

    public function getAdminInfo()
    {
        return $this->belongsTo('App\Admin', 'admin_id');
    }

    public function getCategoryInfo()
    {
        return $this->belongsTo('App\Model\Category\Category', 'category_id');
    }
}
