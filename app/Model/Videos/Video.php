<?php

namespace App\Model\Videos;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    Protected $table	= 'course_video';
    protected $primaryKey = 'video_id';
    Public $timestamps	= false;

    public function getCategory()
    {
    	return $this->belongsTo('App\Model\Category\Category','category_id');
    }
    public function getSubCat()
    {
    	return $this->belongsTo('App\Model\SubCategory\SubCategory','sub_category_id');
    }
}
