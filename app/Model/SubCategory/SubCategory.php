<?php

namespace App\Model\SubCategory;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
	protected $table      = 'iq_sub_category';
	protected $primaryKey = 'sub_category_id';
    public $timestamps = false;

	public function InterviewQuestions()
    {
    	return $this->hasMany('App\Model\InterviewQuestions\InterviewQuestions','sub_cat_id')->where('iq_ques_ans_status',1)->where('question_type',1)->withCount(['getComment','getLikes','getViews']);
    }

    public function technicalTerms()
    {
        return $this->hasMany('App\Model\InterviewQuestions\InterviewQuestions','sub_cat_id')->where('iq_ques_ans_status',1)->where('question_type',2)->orderBy('iq_ques_ans_q','ASC');
    }

    public function Category()
    {
    	return $this->belongsTo('App\Model\Category\Category','category_id');
    }

    public function StudyMaterials()
    {
        return $this->hasMany('App\Model\StudyMaterial\StudyMaterial','sub_cat_id')->where('study_ques_ans_status',1)->orderBy('study_material_id','ASC');
    }

    public function PracticeQuiz()
    {
        return $this->hasMany('App\Model\PracticeQuiz\PracticeQuiz','qustion_sub_sec_id')->where('question_trial',0);
    }
    public function Quiz()
    {
        return $this->hasMany('App\Model\QuizQuestions\QuizQuestions','qustion_sub_sec_id')->limit(25);
    }

    public function getHindiVideos()
    {
        return $this->hasMany('App\Model\VideoTutorials\VideoTutorials','sub_category_id')->where('language',2)->where('status',1);
    }

    public function getEngVideos()
    {
        return $this->hasMany('App\Model\VideoTutorials\VideoTutorials','sub_category_id')->where('language',1)->where('status',1);
    }


    public function getVideos()
    {
        return $this->hasMany('App\Model\VideoTutorials\VideoTutorials','sub_category_id');
    }
}