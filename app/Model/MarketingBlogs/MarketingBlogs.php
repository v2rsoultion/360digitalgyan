<?php

namespace App\Model\MarketingBlogs;

use Illuminate\Database\Eloquent\Model;

class MarketingBlogs extends Model
{
	protected $table      = 'marketing_news';
    protected $primaryKey = 'marketing_id';
    public $timestamps = false;
    public function getLikes()
    {
    	return $this->hasMany('App\Model\AllQuesLikes\AllQuesLikes','like_question_id')->where('like_flag',3);
    }
    public function getComment()
    {
    	return $this->hasMany('App\Model\AllQuesComments\AllQuesComments','comment_question_id')->where('comment_flag',3);
    }
    public function getViews()
    {
    	return $this->hasMany('App\Model\AllQuesViews\AllQuesViews','view_question_id')->where('view_flag',3);
    }

    // public function getIsNewAttribute($value)
    // {
    // 	return $this->attributes['is_new'] = $value == 1 ? "New" : "Old";
    // }
}
