<?php

namespace App\Model\AllQuesLikes;

use Illuminate\Database\Eloquent\Model;

class AllQuesLikes extends Model
{
    Protected $table 	= "like_dislike_question_all";
    Protected $primaryKey = "like_id";
    public $timestamps = false;
}
