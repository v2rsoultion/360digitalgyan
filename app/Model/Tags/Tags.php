<?php

namespace App\Model\Tags;

use Illuminate\Database\Eloquent\Model;

class Tags extends Model
{
    Protected $table = "wscube_tags";
    Protected $primaryKey = "tags_id";
    Public $timestamps = false;
}
