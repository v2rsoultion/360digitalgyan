<?php

namespace App\Model\KeyTrends;

use Illuminate\Database\Eloquent\Model;

class KeyTrends extends Model
{
	protected $table      = 'terms';
    protected $primaryKey = 'term_id';
    public $timeStamps	  = false;
    // // protected $connection = 'mysqlsecond';
}
