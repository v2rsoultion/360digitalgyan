<?php

namespace App\Model\Transaction;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
	protected $table      = 'transactions';
    protected $primaryKey = 'txn_id';
    public $timestamps    = false;
}
