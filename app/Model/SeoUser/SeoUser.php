<?php

namespace App\Model\SeoUser;

use Illuminate\Database\Eloquent\Model;

class SeoUser extends Model
{
	protected $table      = 'seo_users';
    protected $primaryKey = 'seo_users_id';
    public    $timestamps = false;
    // protected $connection = 'mysqlsecond';

    public function getFlagAttribute($flag)
    {
    	if ($flag==1){
    		$value = "Login with Google";
    	}elseif ($flag==2){
    		$value = "Login with Facebook";
    	}elseif ($flag==3){
    		$value = "Login with Linkdin";
    	}else{
    		$value = "Normal Login";
    	}

    	return $this->attributes['flag'] = $value;
    }

    public function getDateOfRegistrationAttribute($value)
    {
    	return $this->attributes['date_of_registration'] = date("d M,Y", strtotime($value));
    }    
}
