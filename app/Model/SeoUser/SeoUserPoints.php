<?php

namespace App\Model\SeoUser;

use Illuminate\Database\Eloquent\Model;

class SeoUserPoints extends Model
{
	protected $table      = 'seo_users_points';
    protected $primaryKey = 'seo_points_id';
    public    $timestamps = false;
    // protected $connection = 'mysqlsecond';
    
}
