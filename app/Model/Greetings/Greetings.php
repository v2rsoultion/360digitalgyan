<?php

namespace App\Model\Greetings;

use Illuminate\Database\Eloquent\Model;

class Greetings extends Model
{
	Protected $table 	= "greetings";
    Public $primaryKey 	= "greetings_id";
    Public $timestamps 	= false;
}
