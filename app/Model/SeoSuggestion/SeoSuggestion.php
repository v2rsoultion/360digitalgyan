<?php

namespace App\Model\SeoSuggestion;

use Illuminate\Database\Eloquent\Model;

class SeoSuggestion extends Model
{
    Protected $table = "seo_suugestion";
    Protected $primaryKey = "suugestion_id";
    Public $timestamps 	= false;
}
