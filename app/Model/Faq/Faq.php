<?php

namespace App\Model\Faq;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
	protected $table      = 'faq';
    protected $primaryKey = 'faq_id';
    
}
