<?php

namespace App\Model\Category;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table      = 'iq_category';
    protected $primaryKey = 'iq_category_id';
    public $timestamps = false;
    // // protected $connection = 'mysqlsecond';

    public function SubCategories()
    {
    	return $this->hasMany('App\Model\SubCategory\SubCategory','category_id');
    }

    public function InterviewQuestions()
    {
    	return $this->hasMany('App\Model\InterviewQuestions\InterviewQuestions','iq_ques_ans_catid')->where('iq_ques_ans_status',1);
    }

    
}
