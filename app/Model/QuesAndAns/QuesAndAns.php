<?php

namespace App\Model\QuesAndAns;

use Illuminate\Database\Eloquent\Model;

class QuesAndAns extends Model
{
	protected $table      = 'user_questions';
    protected $primaryKey = 'ques_id';
    public    $timestamps = false;
    // protected $connection = 'mysqlsecond';
    
    public function getUser(){
    	return $this->belongsTo('App\Model\SeoUser\SeoUser','ques_user_id');
    }

    public function getCategory(){
        return $this->belongsTo('App\Model\Category\Category','ques_main_cat_id');
    }

    public function answers(){
    	return $this->hasMany('App\Model\UsersAnswers\UsersAnswers','ans_ques_id')->where('answer_status',1);
    }

    public function getLikes(){
        return $this->hasMany('App\Model\LikeDislike\LikeDislike','question_answer_id')->where('like_dislike_flag',1)->where('like_dislike_status',1);
    }

    public function getDisLikes(){
        return $this->hasMany('App\Model\LikeDislike\LikeDislike','question_answer_id')->where('like_dislike_flag',1)->where('like_dislike_status',2);
    }
}