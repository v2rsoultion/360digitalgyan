<?php

namespace App\Model\LikeDislike;

use Illuminate\Database\Eloquent\Model;

class LikeDislike extends Model
{
	protected $table        = 'like_dislike';
    protected $primaryKey   = 'like_dislike_id';
    public 	  $timestamps	= false;
    // // protected $connection = 'mysqlsecond';
}
