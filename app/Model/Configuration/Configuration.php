<?php

namespace App\Model\Configuration;

use Illuminate\Database\Eloquent\Model;

class Configuration extends Model
{
	protected $table      = 'configuration';
	protected $primaryKey = 'config_id';
    public $timestamps = false;
}