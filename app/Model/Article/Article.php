<?php

namespace App\Model\Article;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
	protected $table      = 'article';
    protected $primaryKey = 'articles_id';
    
    public function getCategoryInfo()
    {
         return $this->belongsTo('App\Model\ArticleCategory\ArticleCategory', 'article_category_id');
    }
    //
}
