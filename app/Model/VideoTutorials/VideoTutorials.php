<?php

namespace App\Model\VideoTutorials;

use Illuminate\Database\Eloquent\Model;

class VideoTutorials extends Model
{
	protected $table      = 'course_video';
	protected $primaryKey = 'video_id';
    public $timestamps = false;    
	// protected $connection = 'mysqlsecond';

    
    public function Category()
    {
    	return $this->belongsTo('App\Model\Category\Category','category_id');
    }
}