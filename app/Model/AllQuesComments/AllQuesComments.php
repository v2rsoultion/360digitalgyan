<?php

namespace App\Model\AllQuesComments;

use Illuminate\Database\Eloquent\Model;

class AllQuesComments extends Model
{
    Protected $table = "comment_questions";
    Protected $primaryKey ="comment_id";
    public $timestamps = false;

    public function getUser()
    {
    	return $this->belongsTo('App\Model\SeoUser\SeoUser','comment_user_id');
    }
    public function getStudyQues()
    {
    	return $this->belongsTo('App\Model\QuesAndAns\QuesAndAns','comment_question_id');
    }
    
    public function getInterviewQues()
    {
    	return  $this->belongsTo('App\Model\InterviewQuestions\InterviewQuestions','comment_question_id');
    }

    public function getBlogQues()
    {
    	return  $this->belongsTo('App\Model\MarketingBlogs\MarketingBlogs','comment_question_id');
    }

    public function likes()
    {
        return $this->hasMany('App\Model\CommentLikeQuestion\CommentLikeQuestion','comment_id')->with('comment_flag',1);   
    }
}
