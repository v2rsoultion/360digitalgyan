<?php

namespace App\Model\ShareBanner;

use Illuminate\Database\Eloquent\Model;

class ShareBanner extends Model
{
    Protected $table = "share_banner";
    Protected $primaryKey = "banner_id";
    Public $timestamps = false;

    public function getCount()
    {
    	return $this->hasMany('App\Model\BannerCount\BannerCount','banner_id');
    }

    public function getCategory()
    {
    	return $this->belongsTo('App\Model\Category\Category','banner_cat');
    }
}
