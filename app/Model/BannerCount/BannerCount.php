<?php

namespace App\Model\BannerCount;

use Illuminate\Database\Eloquent\Model;

class BannerCount extends Model
{
    Protected $table = "share_banner_count";
    Protected $primaryKey = "share_banner_count_id";
    Public $timestapms = false;
}
