<?php

namespace App\Model\PracticeQuiz;

use Illuminate\Database\Eloquent\Model;

class PracticeQuiz extends Model
{
	protected $table      = 'practice_question_quiz';
    protected $primaryKey = 'qustion_id';
    public $timestamps    = false;
    // protected $connection = 'mysqlsecond';

    
    public function getCategory()
    {
    	return $this->belongsTo('App\Model\Category\Category','qustion_sec_id');
    }

    public function getSubCategory()
    {
    	return $this->belongsTo('App\Model\SubCategory\SubCategory','qustion_sub_sec_id');
    }
}