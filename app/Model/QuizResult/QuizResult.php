<?php

namespace App\Model\QuizResult;

use Illuminate\Database\Eloquent\Model;

class QuizResult extends Model
{
	protected $table      = 'quiz_results';
    protected $primaryKey = 'quiz_result_id';
    public $timestamps    = false;

    public function getCategory()
    {
    	return $this->belongsTo('App\Model\Category\Category','quiz_category');
    }

    public function getSubCategory()
    {
    	return $this->belongsTo('App\Model\SubCategory\SubCategory','quiz_sub_cate_id');
    }
}