<?php

namespace App\Model\Pages;

use Illuminate\Database\Eloquent\Model;

class Pages extends Model
{
    //
    protected $table      = 'master_pages';
    protected $primaryKey = 'master_pages_id';
}
