<?php

namespace App\Model\InterviewQuestions;

use Illuminate\Database\Eloquent\Model;

class InterviewQuestions extends Model
{
	protected $table      = 'iq_ques_ans';
    protected $primaryKey = 'iq_ques_ans_id';
    public $timestamps = false;    
    // // protected $connection = 'mysqlsecond';

    public function getCategory()
    {
    	return $this->belongsTo('App\Model\Category\Category','iq_ques_ans_catid');
    }

    public function getSubCategory()
    {
    	return $this->belongsTo('App\Model\SubCategory\SubCategory','sub_cat_id');
    }

    public function getLikes()
    {
        return $this->hasMany('App\Model\AllQuesLikes\AllQuesLikes','like_question_id')->where('like_flag',1);
    }

    public function getViews()
    {
        return $this->hasMany('App\Model\AllQuesViews\AllQuesViews','view_question_id')->where('view_flag',1);
    }

    public function getComment()
    {
        return $this->hasMany('App\Model\AllQuesComments\AllQuesComments','comment_question_id')->where('comment_flag',1)->withCount('likes');
    }

}
