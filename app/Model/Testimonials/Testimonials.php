<?php

namespace App\Model\Testimonials;

use Illuminate\Database\Eloquent\Model;

class Testimonials extends Model
{
    Protected $table = "testimonial";
    Protected $primaryKey = "testimonial_id";
    Public $timestamps = false;
}
