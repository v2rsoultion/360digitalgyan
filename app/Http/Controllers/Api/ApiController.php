<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Category\Category;
use App\Model\SubCategory\SubCategory;

use DB;
use Hash;

class ApiController extends Controller
{
	/* Function view Categories that have questions	
	*  @Pankaj 2019
	*/
    public function get_sub_category(Request $request){

    	/*
	     * Variable initialization
	     */
	    $result = [];
	    $data 	= [];

	    $cat_id	= $request->input('category_id');
	    $type	= $request->input('question_type');
	 	$data 	= SubCategory::withCount(['Questions' , 'Questions AS new_question_count' => function ($query){
                                    $query->where('is_new',1);
                                }])->whereHas('Questions',function($q) use ($cat_id,$type){
                                        $q->where('iq_ques_ans_status','=',1)
                                          ->where('iq_ques_ans_catid',$cat_id)
                                          ->where('question_type',$type);
                                	})->where('category_id',$cat_id)
                                  	->where('status',1)
                                  	->orderBy('category_order','ASC')
                                  	->get()
                                  	->toArray();

	    $result['response'] 	= 1;
	    $result['message']  	= $data; 

    	return response()->json($result, 200);

    }   

    /* Function name : userRegistration
	*  User Registration
	*  @Pratyush on 02 Sept. 2018	
	*/ 
    public function userRegistration(Request $request)
    {
    	 /*
	     * Variable initialization
	     */
	    $result_data    = [];
	    $arr_response   = [];

	    /*
	    * input variables
	    */
	    $name		= $request->input('name');
	    $email		= $request->input('email');
	    $password	= $request->input('password');
	    $device_id	= $request->input('device_id');
    	
	    if(!empty($request->input())){
	    	
	    	if(!empty($name) && !empty($email) && !empty($password) && !empty($device_id)){

			    $previous_user = User::where('user_email',$email)->first();

			    if(empty($previous_user)){

			    	$user = new User;
			    	$user->user_name 			= $name;
			    	$user->user_email 			= $email;
			    	$user->user_device_token 	= $device_id;
			    	$user->user_password		= Hash::make($password);
			    	$user->save();

			    	$status 		= true; 
					$message 		= 'User inserted Successfully';
			    }else{

			    	$status 		= false; 
					$message 		= 'Email Address is aleady used';
			    }

	    	}else{
	    		$status 		= false; 
				$message 		= 'Parameters missmatch';	
	    	}
	    }else{
	    	$status 		= false; 
			$message 		= 'Parameters missing';
	    }

    	$result_response['data']    	= $result_data;
        $result_response['status']  	= $status;
        $result_response['message'] 	= $message;
        return response()->json($result_response, 200);
    }
}
