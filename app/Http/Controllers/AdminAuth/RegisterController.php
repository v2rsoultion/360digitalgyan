<?php

namespace App\Http\Controllers\AdminAuth;

use App\Admin;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use App\Mail\VerifyMail;
use Illuminate\Support\Facades\Mail;
class RegisterController extends Controller
{


    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */
    public function  __construct(){
        $this->middleware('subadmin');
    }
    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = 'admin-panel/view-sub-admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
   

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:admins',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return Admin
     */
    protected function create(array $data)
    {   
        
        // $Admin     = New Admin;
        // $Admin->admin_name         = $data['name'];
        // $Admin->email      = $data['email'];
        // $Admin->password   = bcrypt($data['password']);
        // $Admin->save();
        // die;

        Admin::create([
            'admin_name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
        
        $customer= (object) array(
           'admin_name' => $data['name'],
           'email' => $data['email'],
           'password' =>$data['password'],
           'subject'=>'Login Credentials'
        );
        // $customer = json_encode($customer);
        // echo $customer->email;

        
        // die;
        $view           = 'email.welcome-subadmin';
        Mail::to($customer->email)->send(new VerifyMail($customer, $view));
        return view('admin-panel.auth.view_subadmins');
    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($admins = $this->create($request->all())));

        // $this->guard()->login($user);

        return $this->registered($request, $admins)
                        ?: redirect($this->redirectPath());
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        $loginInfo                  = get_loggedin_user_data();
       
        $data = array(
            'page_title'    => 'Add Sub Admin',
            'redirect_url'  => url('admin-panel/view-sub-admin'),
            'login_info'    => $loginInfo,
            
        );
        return view('admin-panel.auth.register',$data);
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('admin');
    }
}
