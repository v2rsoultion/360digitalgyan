<?php

namespace App\Http\Controllers\AdminAuth;

use App\Model\Admin\AdminUser;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class ChangePasswordController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Get Admin Data
    	$loginInfo                      = get_loggedin_user_data();
        $encrypted_admin_id  = get_encrypted_value($loginInfo['admin_id'], true);

        $admin           	 = AdminUser::find($loginInfo['admin_id']);

        $page_title =   'Admin Panel | Change Password';

        // Return With Data Array
        $data = array( 
                        'page_title'                  => $page_title,
                        'admin_data'             => $admin,
                        'login_info'             => $loginInfo,
                        'update_password_url'    => url('admin-panel/change-password')
                    );

        return view('admin-panel.profile.change-password')->with($data);
    }

    protected function guard()
    {
        return auth()->guard('admin');
    }

    public function changePassword(Request $request)
    {
       $admin_id = Auth::guard('admin')->user()->admin_id;
       
       $adminDetails = AdminUser::where(['admin_id' => $admin_id])->first();

       if(Hash::check($request->get('current-password'), $adminDetails['password'])){
        $values=array(
              'password'          => Hash::make($request->get('new-password')),
        );

        $validatior = Validator::make($request->all(), [
            'current-password'          => 'required',
            'new-password'              => 'required|different:current-password',
            'new-password_confirmation' => 'required|same:new-password'
        ], [
            'new-password_confirmation.same' => 'The new password and new confirm password are not the same.',
            'new-password.different' => "New Password can't be same as current password",
        ]);

        if ($validatior->fails())
        {
                return redirect()->back()->withInput()->withErrors($validatior);

        } else {

            AdminUser::where('admin_id', $admin_id)->update($values);
            return redirect('/admin-panel/change-password')->with("message","Password changed successfully!");

        }

   } else {
        return redirect('/admin-panel/change-password')->withErrors("Incorrect current password. Please try again.");
   }
    }
}