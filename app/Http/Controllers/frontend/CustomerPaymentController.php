<?php

    namespace App\Http\Controllers\frontend;

    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use App\Model\Customer\CustomerPayment;
    use App\Model\Customer\CustomerQuestion;
    use Illuminate\Support\Facades\Input;
    use PaytmWallet;
    use Session;
    use Auth;
    use DB;
    use App\Mail\VerifyMail;
    use Illuminate\Support\Facades\Mail;

    class CustomerPaymentController extends Controller
    {

        public function payment(Request $request)
        {
            $customer_info        = Auth::guard('web')->user();
            $customer_question_id = Input::get('customer_question_id');
            $customer_question    = CustomerQuestion::Find($customer_question_id);
            if (!empty($customer_question['customer_question_id']) && !empty($customer_info->customer_id))
            {
                $plan_amount = $customer_question['plan_price'] - $customer_question['discount_amount'];
                $plan_amount = $plan_amount > 0 ? $plan_amount : 0;
                if ($plan_amount > 0)
                {
                    try
                    {
                        $payment                       = new CustomerPayment;
                        $customer_payment_id           = 'TXZEE' . $customer_info['customer_id'] . '_' . time();
                        $payment->customer_question_id = $customer_question['customer_question_id'];
                        $payment->customer_payment_id  = $customer_payment_id;
                        $payment->year_id              = $customer_question['year_id'];
                        $payment->plan_id              = $customer_question['plan_id'];
                        $payment->customer_id          = $customer_info['customer_id'];
                        $payment->plan_amount          = $plan_amount;
                        $payment->payment_status       = 1;
                        if ($payment->save())
                        {
                            $payment_paytm = PaytmWallet::with('receive');
                            $payment_paytm->prepare([
                                'order'        => $customer_payment_id,
                                'user'         => $customer_info['customer_id'] . 'C' . time(),
                                'email'        => $customer_info['email'],
                                'amount'       => $plan_amount,
                                'callback_url' => url('api/payment/status')
                            ]);
                            return $payment_paytm->receive();
                        }
                    }
                    catch (Exception $e)
                    {
                        DB::rollback();
                        $error_message = $e->getMessage();
                        return redirect()->back()->withErrors($error_message);
                    }
                    DB::commit();
                }
                return redirect('file-itr')->withErrors("Payment amount cann't be 0");
            }
            return redirect('file-itr')->withErrors('Something went wrong, please try again');
        }

        /**
         * Obtain the payment information.
         *
         * @return Object
         */
        public function paymentCallback()
        {
            $transaction  = PaytmWallet::with('receive');
            $response     = $transaction->response();
            $cus_payment  = CustomerPayment::where('customer_payment_id', $response['ORDERID'])->first();
            $cus_question = CustomerQuestion::Find($cus_payment['customer_question_id']);
            if ($response['STATUS'] == 'TXN_SUCCESS')
            {
                //update payment status
                $start_date            = date("Y-m-d");
                $expire_date           = date("Y-m-d", strtotime(date("Y-m-d", strtotime($start_date)) . " + 365 day"));
                CustomerPayment::where('customer_payment_id', $response['ORDERID'])
                    ->update(['payment_status'   => 2, 'transaction_id'   => $response['TXNID'],
                        'plan_start_date'  => $start_date, 'plan_expire_date' => $expire_date]);
                // update customer question table 
                $cus_question->status  = 2; // payment success
                $cus_question->save();
                // send success mail to customer
                $payment_data          = DB::table('customer_payments as cp')
                        ->join('customer as c', 'cp.customer_id', '=', 'c.customer_id')
                        ->join('plan as p', 'cp.plan_id', '=', 'p.plan_id')
                        ->join('financial_assesment_year as fay', 'cp.year_id', '=', 'fay.year_id')
                        ->where('customer_payment_id', $response['ORDERID'])->first();
                $payment_data->subject = trans('language.payment_success_subject');
                if (!empty($payment_data->email))
                {
                    $view = 'email.payment-success';
                    Mail::to($payment_data->email)->send(new VerifyMail($payment_data, $view));
                }
                return redirect('get-itr-file/' . $response['ORDERID'] . '/success');
                Log::info('PaymentStatus_' . date('Y-m-d') . ':' . $response);
            }
            else //if ($transaction->isFailed())
            {
                CustomerPayment::where('customer_payment_id', $response['ORDERID'])->update(['payment_status' => 3, 'transaction_id' => $response['TXNID']]);
                $cus_question->status = 3; // payment success
                $cus_question->save();
                return redirect('file-itr/failed');
                Log::info('PaymentFailed_' . date('Y-m-d') . ':' . $response);
            }
        }

    }
    