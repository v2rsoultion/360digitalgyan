<?php

    namespace App\Http\Controllers\frontend;

    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use App\Model\Customer\Customer;
    use App\Model\Customer\CustomerDocuments;
    use App\Model\Customer\Message;
    use App\Model\Customer\CustomerItr;
    use Illuminate\Support\Facades\Input;
    use Illuminate\Support\Facades\DB;
    use Validator;
    use Auth;
    use Hash;

    class CustomerController extends Controller
    {

        public function myProfile()
        {
            $data['customer']         = Auth::guard('web')->user();
            $data['arr_account_type'] = \Config::get('custom.bank_account_type');
            $data['arr_bank']         = get_bank_list();
            return view('frontend.my-profile')->with($data);
        }

        public function updateMyProfile(Request $request)
        {
            $customer_id      = Auth::guard('web')->user();
            $customer         = Customer::Find($customer_id->customer_id);
            $prefix           = $customer['fname'];
            $arr_valid_fields = [
                'fname' => 'required',
                'email' => 'required|unique:customer,email,' . $customer_id->customer_id . ',customer_id',
            ];

            if ($request->has('change-password'))
            {
                $arr_valid_fields['current_password'] = 'required';
                $arr_valid_fields['new_password']     = 'required|different:current_password';
                $arr_valid_fields['confirm_password'] = 'required|same:new_password';
            }

            $validatior       = Validator::make($request->all(), $arr_valid_fields);
            $password_message = '';
            if (!empty($request->get('new_password')))
            {
                if (Hash::check($request->get('current_password'), $customer['password']))
                {
                    $customer->password = Hash::make($request->get('new_password'));
                    $password_message   = 'Password has been changed successfully.';
                }
                else
                {
                    return redirect('my-profile')->withErrors('Invalid current password');
                }
            }
            if ($validatior->fails())
            {
                return redirect()->back()->withInput()->withErrors($validatior);
            }
            else
            {
                DB::beginTransaction();
                try
                {
                    $customer->fname          = Input::get('fname');
                    $customer->mname          = Input::get('mname');
                    $customer->lname          = Input::get('lname');
                    $customer->email          = Input::get('email');
                    $customer->phone          = Input::get('phone');
                    $customer->account_type   = Input::get('account_type');
                    $customer->bank_id        = Input::get('bank_id');
                    $customer->account_ifsc   = Input::get('account_ifsc');
                    $customer->account_number = Input::get('account_number');
                    $customer->save();
                    if ($request->hasFile('pan'))
                    {
                        if ($request->has('pan_id'))
                        {
                            $document_id       = Input::get('pan_id');
                            $customer_document = CustomerDocuments::Find($document_id);
                        }
                        else
                        {
                            $customer_document = new CustomerDocuments;
                        }
                        $file                        = $request->file('pan');
                        $customer_pan                = \Config::get('custom.customer_pan');
                        $destinationPath             = public_path() . $customer_pan['upload_path'];
                        $filename                    = modify_file_name($file, $prefix, 'pan', true);
                        $file->move($destinationPath, $filename);
                        $customer_document->filename = $filename;
                        $customer_document->type     = 'pan';
                        $customer->getDocuments()->save($customer_document);
                    }
                    if ($request->hasFile('aadhaar'))
                    {
                        if ($request->has('aadhaar_id'))
                        {
                            $document_id       = Input::get('aadhaar_id');
                            $customer_document = CustomerDocuments::Find($document_id);
                        }
                        else
                        {
                            $customer_document = new CustomerDocuments;
                        }

                        $file                        = $request->file('aadhaar');
                        $customer_aadhaar            = \Config::get('custom.customer_aadhaar');
                        $destinationPath             = public_path() . $customer_aadhaar['upload_path'];
                        $filename                    = modify_file_name($file, $prefix, 'aadhaar', true);
                        $file->move($destinationPath, $filename);
                        $customer_document->filename = $filename;
                        $customer_document->type     = 'aadhaar';
                        $customer->getDocuments()->save($customer_document);
                    }
                }
                catch (\Exception $e)
                {
                    DB::rollback();
                    $error_message = $e->getMessage();
                    return redirect()->back()->withErrors($error_message);
                }
                DB::commit();
            }
            $success_msg = $password_message != '' ? $password_message : 'Profile updated successfully';
            return redirect('my-profile')->withSuccess($success_msg);
        }

        public function myPlan()
        {
            $data         = [];
            $customer     = Auth::guard('web')->user();
            $customer_id  = $customer->customer_id;
            $data['plan'] = (array) DB::table('plan as p')
                    ->select('cp.plan_start_date', 'cp.plan_expire_date', 'cp.payment_id', 'p.plan_name', 'p.plan_description', 'p.plan_price', 'p.type', 'cp.plan_amount', 'fay.financial_year')
                    ->join('customer_payments as cp', 'p.plan_id', '=', 'cp.plan_id')
                    ->join('financial_assesment_year as fay', 'fay.year_id', '=', 'cp.year_id')
                    ->where('cp.payment_status', 2)
                    ->where('cp.customer_id', $customer_id)
                    ->orderBy('payment_id', 'DESC')
                    ->first();
            return view('frontend.my-plan')->with($data);
        }

        public function myDocuments()
        {

            $arr_doc       = [];
            $arr_directory = \Config::get('custom.document_folder');
            $arr_doc_type  = \Config::get('custom.document_type');
            $customer      = Auth::guard('web')->user();
            $customer_id   = $customer->customer_id;
            $arr_document  = DB::table('customer_itr as ci')
                    ->join('customer_documents as cd', 'ci.customer_itr_id', '=', 'cd.customer_itr_id')
                    ->join('financial_assesment_year as fay', 'ci.year_id', '=', 'fay.year_id')
                    ->where('ci.customer_id', $customer_id)
                    ->get()->toArray();
            foreach ($arr_document as $key => $document)
            {
                $document    = (array) $document;
                $file_name   = $document['filename'];
                $type        = $document['type'];
                $custome_key = $arr_directory[$type];
                $file        = check_file_exist($file_name, $custome_key);
                if ($file)
                {
                    $doc       = array(
                        'document_id'    => $document['document_id'],
                        'type'           => $arr_doc_type[$type],
                        'financial_year' => $document['financial_year'],
                        'file'           => $file,
                    );
                    $arr_doc[] = $doc;
                }
            }
            $data['arr_doc'] = $arr_doc;
            if (isset($arr_doc_type['aadhaar']))
            {
                unset($arr_doc_type['aadhaar']);
            }
            if (isset($arr_doc_type['pan']))
            {
                unset($arr_doc_type['pan']);
            }
            $data['arr_doc_type'] = $arr_doc_type;
            $year                 = get_financial_year();
            $data['arr_fy']       = $year['arr_fy'];
            $data['current_fy']   = $year['current_fy'];
            return view('frontend.my-documents')->with($data);
        }

        public function myItrReturn()
        {
            $data         = [];
            $arr_doc      = [];
            $arr_doc_type = \Config::get('custom.document_type');
            $customer     = Auth::guard('web')->user();
            $customer_id  = $customer->customer_id;
            $arr_document = CustomerItr::with(['getItrdocumnets' => function ($q)
                    {
                        $q->where('type', 'itr_return');
                    }])->with('getFAyear')->where('customer_id', $customer_id)->get();

            foreach ($arr_document as $key => $document)
            {
                $file_name = isset($document['getItrdocumnets'][0]['filename']) ? $document['getItrdocumnets'][0]['filename'] : '';
                $type      = isset($document['getItrdocumnets'][0]['type']) ? $document['getItrdocumnets'][0]['type'] : '';
                if ($type == 'itr_return')
                {
                    $custome_key = 'itr_return_final_document';
                }
                $file = check_file_exist($file_name, $custome_key);
                $doc  = array(
                    'type'            => $arr_doc_type[$type],
                    'customer_itr_id' => $document['customer_itr_id'],
                    'financial_year'  => $document['getFAyear']['financial_year'],
                    'assesment_year'  => $document['getFAyear']['assesment_year'],
                    'file'            => $file,
                    'detail_url'      => url('itr-return-detail/' . get_encrypted_value($document['customer_itr_id'], true)),
                    'message'         => url('message/' . get_encrypted_value($document['customer_itr_id'], true)),
                );

                $arr_doc[] = $doc;
            }
            $data['arr_itr_doc'] = $arr_doc;
            return view('frontend.my-itr-return')->with($data);
        }

        public function itrReturnDetail($customer_itr_id)
        {
            $data    = [];
            $arr_doc = [];
            $id      = get_decrypted_value($customer_itr_id, true);
            if ($id)
            {
                $arr_doc_type = \Config::get('custom.document_type');
                $customer     = Auth::guard('web')->user();
                $customer_id  = $customer->customer_id;

                $arr_itr_detail = CustomerItr::where('customer_id', $customer_id)
                        ->where('customer_itr_id', $id)
                        ->with('getItrdocumnets')
                        ->with('customerNri')
                        ->with('getSourcelincome')
                        ->with('getItrrentelincome')
                        ->with('getFAyear')
                        ->first()->toArray();
                if ($arr_itr_detail['get_itrdocumnets'])
                {
                    foreach ($arr_itr_detail['get_itrdocumnets'] as $key => $document)
                    {
                        $file_name = $document['filename'];
                        $type      = $document['type'];
                        if ($type == 'pan')
                        {
                            $custome_key = 'customer_pan';
                        }
                        elseif ($type == 'aadhaar')
                        {
                            $custome_key = 'customer_aadhaar';
                        }
                        elseif ($type == 'form16' || $type == 'salary_certificate')
                        {
                            $custome_key = 'customer_form16';
                        }
                        elseif ($type == 'nri_doc')
                        {
                            $custome_key = 'customer_nri';
                        }
                        elseif ($type == 'capital_share' || $type == 'capital_mutual' || $type == 'capital_other')
                        {
                            $custome_key = 'customer_capital_gain';
                        }
                        elseif ($type == 'profit_loss' || $type == 'balance_sheet' || $type == 'gst')
                        {
                            $custome_key = 'customer_business_income';
                        }
                        $file = check_file_exist($file_name, $custome_key);
                        if ($file)
                        {
                            $itr_return_doc = array(
                                'type'        => $arr_doc_type[$type],
                                'key'         => $type,
                                'file'        => $file,
                                'file_title'  => $document['filename'],
                                'document_id' => $document['document_id'],
                            );
                            $arr_doc[]      = $itr_return_doc;
                        }
                    }
                    $arr_itr_detail['get_itrdocumnets'] = $arr_doc;
                }
                $data['arr_itr_detail'] = $arr_itr_detail;
                $data['payment_data']   = get_payment_data();
                $data['arr_house_type'] = \Config::get('custom.house_type');
            }
//            p($data);
            return view('frontend.my-itr-return-detail')->with($data);
        }

        function checkPassword(Request $request)
        {
            $password    = $request->input('current_password');
            $customer_id = Auth::guard('web')->user();
            $customer    = Customer::Find($customer_id->customer_id);
            if (Hash::check($password, $customer['password']))
            {
                return response()->json(true);
            }
            else
            {
                return response()->json(false);
            }
        }

        public function taxPlanningTool()
        {
            $data                 = [];
            $arr_tax_ques         = DB::table('tax_plan_questions')->get();
            $arr_tax_ques         = json_decode(json_encode($arr_tax_ques), true);
            $data['arr_tax_ques'] = array_column($arr_tax_ques, null, 'question_no');
            return view('frontend.tax-planning-tool')->with($data);
        }

        public function getTaxPlanningData(Request $request)
        {
            $data             = [];
            $senior_citizen   = Input::get('taxpayer_age');
            $arr_tax_ques     = DB::table('tax_plan_questions')->get();
            $arr_input_amount = [];
            $total_tax_amount = 0;
            $status           = 'failed';
            $save_tax_message = '';
            foreach ($arr_tax_ques as $value)
            {
                $value = (array) $value;

                $arr_input_amount[$value['question_key']] = $request->has($value['question_key']) ? Input::get($value['question_key']) : 0;
            }
            if (!empty($arr_input_amount))
            {
                //  entered gross income amount
                $total_income_amount = $arr_input_amount['gross_pension_income'];
                // get tax config amount
                $tax_config          = DB::table('tax_configs')->get();
                $arr_tax_config      = json_decode(json_encode($tax_config), true);
                // key of tax_config data
                $arr_tax_config_data = array_column($arr_tax_config, null, 'tax_config_id');
                // set primary key on node
                $config_key          = array_column($arr_tax_config, 'tax_key', 'tax_config_id');

                // get exemption limit based on age for senior or youngster
                $exemption_limit = 0;
                if ($senior_citizen == 'yes')
                {
                    $tax_config_id   = array_search('senior_citizen', $config_key);
                    $exemption_limit = $arr_tax_config_data[$tax_config_id]['slab_amount_max'] ? $arr_tax_config_data[$tax_config_id]['slab_amount_max'] : 0;
                }
                else
                {
                    $tax_config_id   = array_search('youngster', $config_key);
                    $exemption_limit = $arr_tax_config_data[$tax_config_id]['slab_amount_max'] ? $arr_tax_config_data[$tax_config_id]['slab_amount_max'] : 0;
                }

                // get tax limit data
                $tax_max_limit             = DB::table('tax_max_limits')->get();
                $arr_tax_max_limit         = json_decode(json_encode($tax_max_limit), true);
                // set primary key on node
                $tax_max_limit_data        = array_column($arr_tax_max_limit, null, 'tax_max_limit_id');
                // array for keys
                $tax_max_limit_key         = array_column($arr_tax_max_limit, 'tax_key', 'tax_max_limit_id');
                // id of std_deduction
                $tax_max_limit_id          = array_search('std_deduction', $tax_max_limit_key);
                // standard deduction amount
                // subtract std deduction amount
                $standard_deduction_amount = $tax_max_limit_data[$tax_max_limit_id]['max_amount'];
                $net_salary_income         = $total_income_amount - $standard_deduction_amount;
                // subtract property rent received
                // house rent paid is equalt to subtract 30% of received amount
                $house_rent_paid           = 0;
                if ($arr_input_amount['property_rent_received'] > 0)
                {
                    $house_rent_paid = round(($arr_input_amount['property_rent_received'] * 30) / 100);
                }
                // net income from house
                $net_income_from_house   = $arr_input_amount['property_rent_received'] - $house_rent_paid;
                $fd_interest             = $arr_input_amount['other_fd_interest'];
                $home_loan_interest_paid = $arr_input_amount['home_loan_interest_paid'];

//                 gross income

                $gross_total_income    = ($net_salary_income + $net_income_from_house + $fd_interest ) - $home_loan_interest_paid;
                // 80 rule deduction
                $fd_interest_deduction = 0;
                $tax_max_limit_id      = array_search('fd_interest_deduction', $tax_max_limit_key);
//                isset($tax_max_limit_key['fd_interest_deduction']) ? $tax_max_limit_key['fd_interest_deduction'] : null;
                if (!empty($tax_max_limit_id))
                {
                    $fd_max_amount         = $tax_max_limit_data[$tax_max_limit_id]['max_amount'];
                    $fd_interest_deduction = $fd_interest > $fd_max_amount ? $fd_interest - $fd_max_amount : $fd_interest;
                    $fd_interest_deduction = $fd_interest_deduction > 0 ? $fd_interest_deduction : 0;
                }

                // get max amount of deduction
                $tax_max_home_loan_id   = array_search('home_loan_interest', $tax_max_limit_key);
                $tax_max_80c_id         = array_search('80_c_deduction', $tax_max_limit_key);
                $tax_max_fd_interest_id = array_search('fd_interest_deduction', $tax_max_limit_key);
                $tax_max_80d_id         = array_search('80_d_deduction', $tax_max_limit_key);

                $total_max_deduction = $tax_max_limit_data[$tax_max_home_loan_id]['max_amount'] + $tax_max_limit_data[$tax_max_80c_id]['max_amount'] + $tax_max_limit_data[$tax_max_fd_interest_id]['max_amount'] + $tax_max_limit_data[$tax_max_80d_id]['max_amount'];

                $actual_saver_taxable_amount = $gross_total_income - $total_max_deduction;
                // standard deduction amount
                // subtract std deduction amount

                $c_deduction           = ($arr_input_amount['80c_deduction'] + $arr_input_amount['80d_medical_insurance'] + $fd_interest_deduction);
                $actual_taxable_amount = $gross_total_income - $c_deduction;

                // relaxation according to age
                if ($actual_taxable_amount > 0)
                {
                    $arr_config_tax_range = [];
                    $arr_tax_after_tax    = [];
                    foreach ($arr_tax_config as $key => $value)
                    {
                        if ($value['tax_key'] == 'on_amount')
                        {
                            if (($actual_taxable_amount >= $value['slab_amount_min']))
                            {
                                $arr_config_tax_range[$value['tax_config_id']] = $value;
                            }
                        }
                        elseif ($value['tax_key'] == 'after_tax')
                        {
                            if ($value['tax_rate'] > 0)
                            {
                                $arr_tax_after_tax[$value['tax_config_id']] = $value;
                            }
                        }
                    }
                    if (!empty($arr_config_tax_range))
                    {
//                        p($actual_taxable_amount,0);
//                        p($actual_saver_taxable_amount);
                        $arr_slab_tax = [];
                        foreach ($arr_config_tax_range as $key => $tax_slab)
                        {
                            $slab_amount_tax = 0;

                            $slab_max_amount   = $tax_slab['slab_amount_max'] - ($tax_slab['slab_amount_min'] - 1);
                            $slab_amount       = $slab_max_amount > 0 && $actual_taxable_amount > $slab_max_amount ? $slab_max_amount : $actual_taxable_amount;
                            //saver slab amount 
                            $slab_saver_amount = $slab_max_amount > 0 && $actual_saver_taxable_amount > $slab_max_amount ? $slab_max_amount : $actual_saver_taxable_amount;
                            if ($key == 1)
                            {
                                $slab_amount                 = $slab_amount - $exemption_limit;
                                $slab_amount                 = $slab_amount > 0 && $actual_taxable_amount > $slab_amount ? $slab_amount : $actual_taxable_amount;
                                $actual_taxable_amount       = $actual_taxable_amount - $tax_slab['slab_amount_max'];
                                // slab amount
                                $slab_saver_amount           = $slab_saver_amount - $exemption_limit;
                                $slab_saver_amount           = $slab_saver_amount > 0 && $actual_saver_taxable_amount > $slab_saver_amount ? $slab_saver_amount : $actual_saver_taxable_amount;
                                $actual_saver_taxable_amount = $actual_saver_taxable_amount - $tax_slab['slab_amount_max'];
                            }
                            else
                            {
                                $slab_amount           = $slab_amount > 0 && $actual_taxable_amount > $slab_amount ? $slab_amount : $actual_taxable_amount;
                                $actual_taxable_amount = $actual_taxable_amount - $slab_amount;

                                // savable taxable amount
                                $slab_saver_amount           = $slab_saver_amount > 0 && $actual_saver_taxable_amount > $slab_saver_amount ? $slab_saver_amount : $actual_saver_taxable_amount;
                                $actual_saver_taxable_amount = $actual_saver_taxable_amount - $slab_saver_amount;
                            }

                            if ($slab_amount > 0)
                            {
                                $slab_amount_tax = round($slab_amount * $tax_slab['tax_rate'] / 100);

                                $arr_slab_tax[$tax_slab['tax_config_id']] = array(
                                    'slab_amount'     => $slab_amount,
                                    'slab_amount_tax' => $slab_amount_tax,
                                );
                            }
                            else
                            {
                                break;
                            }
                            if ($slab_saver_amount > 0)
                            {
                                $slab_saver_amount_tax                          = round($slab_saver_amount * $tax_slab['tax_rate'] / 100);
                                $arr_slab_tax_saver[$tax_slab['tax_config_id']] = array(
                                    'slab_saver_amount'     => $slab_saver_amount,
                                    'slab_saver_amount_tax' => $slab_saver_amount_tax,
                                );
                            }
                        }
//p($arr_slab_tax,0);
//p($arr_slab_tax_saver);
                        if (!empty($arr_slab_tax))
                        {
                            $cess_tax_amount        = 0;
                            $total_tax_amount       = array_sum(array_column($arr_slab_tax, 'slab_amount_tax'));
                            $total_tax_saver_amount = array_sum(array_column($arr_slab_tax_saver, 'slab_saver_amount_tax'));
                            if ($total_tax_amount > 0)
                            {
                                // for current it will be cess tax
                                foreach ($arr_tax_after_tax as $key => $after_tax)
                                {
                                    $cess_tax_amount = round($total_tax_amount * $after_tax['tax_rate'] / 100);
                                }
//                                apply cess tax
                                if ($total_tax_saver_amount > 0)
                                {
                                    foreach ($arr_tax_after_tax as $key => $after_tax)
                                    {
                                        $cess_tax_saver_amount = round($total_tax_saver_amount * $after_tax['tax_rate'] / 100);
                                    }
                                }
                            }
                            $total_tax_amount = $total_tax_amount + $cess_tax_amount;

                            $total_tax_saver_amount = $cess_tax_saver_amount + $total_tax_saver_amount;
                            $savable_amount         = $total_tax_amount - $total_tax_saver_amount;
                            if ($savable_amount > 0)
                            {
                                $tax_saver_message = 'You can save upto <strong>Rs. ' . $savable_amount . '</strong> with proper tax planning. <a href="' . url('contact-us') . '" class="contact" target="_blank"> <strong>Contact</strong></a> to get free consultation.';
                            }
                            $message = 'Your estimated tax amount is :';
                            $status  = 'success';
                        }
                    }
                }
                else
                {
                    $message = 'Your income is in range of exemption limit';
                }
            }
            else
            {
                $message = 'Income can not be 0';
            }
            return response()->json(array(
                    'status'           => $status,
                    'total_tax_amount' => $total_tax_amount,
                    'message'          => $message,
                    'save_tax_message' => $tax_saver_message
            ));
        }

        /*
         * Customer Itr message 
         */

        public function customerItrMessage($customer_itr_id)
        {
            $data = [];
            if (!empty($customer_itr_id))
            {
                $customer_itr_id         = get_decrypted_value($customer_itr_id, true);
                $customer                = Auth::guard('web')->user();
                $customer_id             = $customer->customer_id;
                $arr_document            = Message::with('messageCustomer')
                    ->with('CustomerItr')
                    ->with('AdminUser')
                    ->where('customer_id', $customer_id)
                    ->where('customer_itr_id', $customer_itr_id)
                    ->orderBy('created_at', 'DESC')
                    ->get();
                $data['message']         = $arr_document;
                $data['customer_itr_id'] = $customer_itr_id;
//            p($arr_document);
//            foreach ($arr_document as $key => $document)
//            {
//                $file_name = isset($document['getItrdocumnets'][0]['filename']) ? $document['getItrdocumnets'][0]['filename'] : '';
//                $type      = isset($document['getItrdocumnets'][0]['type']) ? $document['getItrdocumnets'][0]['type'] : '';
//                if ($type == 'itr_return')
//                {
//                    $custome_key = 'itr_return_final_document';
//                }
//                $file = check_file_exist($file_name, $custome_key);
//                $doc  = array(
//                    'type'            => $arr_doc_type[$type],
//                    'customer_itr_id' => $document['customer_itr_id'],
//                    'financial_year'  => $document['getFAyear']['financial_year'],
//                    'assesment_year'  => $document['getFAyear']['assesment_year'],
//                    'file'            => $file,
//                    'detail_url'      => url('itr-return-detail/' . get_encrypted_value($document['customer_itr_id'], true)),
//                );
//
//                $arr_doc[] = $doc;
//            }
//            $data['arr_itr_doc'] = $arr_doc;
            }
            return view('frontend.message')->with($data);
        }

        function saveMessage(Request $request)
        {
            
        }

    }
    