<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\KeyTrends\KeyTrends;

class KeyTrendsController extends Controller
{
	public function index(Request $request,$char=false)
	{
		$title 	= "Digital Marketing & SEO Quiz | Interview Questions and technical Terms";

        $query  = KeyTrends::Query();

        if($char!=''){
            $query->where('term_title','LIKE',substr($char,0,1).'%');
        }

        if($request['tag']!=''){
            $query->where('term_title','LIKE','%'.$request['tag'].'%');   
        }

        $appends = array('tag'=>$request['tag']);
        $trends = $query->where('term_status',1)->orderBy('term_title','ASC')->paginate(10)->appends($appends);

        $all = KeyTrends::orderBy('term_title')->pluck('term_title');
        $grouped = $all->groupBy(function ($item, $key) {
            return  strtolower(substr($item,0,1));
        })->toArray();
        
        $data = array(
            'title' 		=> $title,
            'page_title' 	=> "Key Terms & Concepts",
            'trends'		=> $trends,
            'alphabts'		=> $grouped,
            'keyUrl'        => url('digital-marketing/key-terms')
        );
        return view('frontend/key-trends')->with($data);
	}
}