<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\QuesAndAns\QuesAndAns;
use App\Model\UsersAnswers\UsersAnswers;
use App\Model\QuizQuestions\QuizQuestions;
use App\Model\SubCategory\SubCategory;

class QuizController extends Controller
{

	/*
	** for app type according to url
	*/
	public $appCat,$url;

	public function __construct()
	{
		$url  = str_replace(url('/'), '', url()->current());
		$ar = explode('/',ltrim($url, $url[0]));
		$this->appCat = getAppCategory($ar[0]);
		$this->url    = $ar[0].'/'.$ar[1];
	}

    public function index($slug=false)
    {
    	$appCat = $this->appCat;
    	$appUrl = $this->url;
    	
    	$title 	= "Digital Marketing & SEO Quiz | Interview Questions and technical Terms";

		$query             = SubCategory::Query();
		if($slug!='')
		{
		$query = $query->where('cat_slug',$slug);
		}

		$questions = $query->whereHas('Quiz')
						   	->with('Quiz')
		                   	->with('Category')
		                   	->where('status',1)
		                   	->where('category_id',$appCat)
		                   	->first();
		
		$subcategories  = SubCategory::whereHas('Quiz')
										->withCount('Quiz')
										->where('status',1)
										->where('category_id',$appCat)
										->orderBy('category_order','ASC')
										->get();

		$data = array(
		'title' 		   => $title,
		'page_title' 	   => "Quiz",
		'questions'		   => $questions,
		'subcategories'    => $subcategories,
		'practiceUrl'      => url($appUrl)
		);
		// p($data);

		return view('frontend/quiz')->with($data);
    }
}
