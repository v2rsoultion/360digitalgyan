<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CompetitionController extends Controller
{
	public function index()
    {
        $title = "Digital Marketing & SEO Quiz | Interview Questions and technical Terms";
        $data = array(
            'title' => $title,
        );
        return view('frontend/all-competition')->with($data);
    }

    public function finderrors()
    {
    	$title = "Digital Marketing & SEO Quiz | Interview Questions and technical Terms";
        $data = array(
            'title' => $title,
        );
        return view('frontend/findingspotting-errors')->with($data);
    }
}
