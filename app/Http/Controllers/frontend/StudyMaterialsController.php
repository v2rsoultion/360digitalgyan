<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\StudyMaterial\StudyMaterial;
use App\Model\SubCategory\SubCategory;

class StudyMaterialsController extends Controller
{

  /*
  ** for app type according to url
  */
  public $appCat,$url;

  public function __construct()
  {
    $url  = str_replace(url('/'), '', url()->current());
    $ar = explode('/',ltrim($url, $url[0]));
    $this->appCat = getAppCategory($ar[0]);
    $this->url    = $ar[0].'/'.$ar[1];
  }

	public function index($slug=false)
	{
    $appCat     = $this->appCat;
    $appUrl     = $this->url;
    
		$title 			= "Digital Marketing & SEO Quiz | Interview Questions and technical Terms";
    $subcategories  = SubCategory::whereHas('StudyMaterials')
                                  ->with('StudyMaterials')
                                  ->where('category_id',$appCat)
                                  ->where('status',1)
                                  ->orderBy('category_order','ASC')
                                  ->get();
    
    if($slug!=''){
    	$materialSingle = StudyMaterial::where('study_material_slug',$slug)->first();
    }else{
    	$materialSingle = StudyMaterial::where('study_material_catid',$appCat)->first();
    }

    $Next     = StudyMaterial::where('study_ques_ans_status',1)
    							  ->where('study_material_id','>',$materialSingle->study_material_id)
    							  // ->where('study_material_catid',$appCat)
    							  // ->where('sub_cat_id',$materialSingle->sub_cat_id)
    							  ->orderBy('study_material_id','ASC')->first();

    $Previous = StudyMaterial::where('study_ques_ans_status',1)
    							  ->where('study_material_id','<',$materialSingle->study_material_id)
    							  // ->where('study_material_catid',$appCat)
    							  // ->where('sub_cat_id',$materialSingle->sub_cat_id)
    							  ->orderBy('study_material_id','DESC')->first();

    $data = array(
      'title' 		        => $title,
      'page_title'	      => "Study Material",
      'subcategories'	    => $subcategories,
      'materialSingle'    => $materialSingle,
      'previous'		      => $Previous,
      'next'			        => $Next,
      'material_url'	    => url($appUrl)
    );
    
    return view('frontend/study-material')->with($data);
	}

	public function getstudy(Request $request)
	{
		// $id 		= explode(',',$request->get('material_id'));
		// $material 	= StudyMaterial::where('study_material_id',$id)->first();
		// $Next 		= StudyMaterial::where('study_material_id','>',$id)->where('study_material_catid',$appCat)->where('study_ques_ans_status',1)->min('study_material_id');
  //       $Previous 	= StudyMaterial::where('study_material_id','<',$id)->where('study_material_catid',$appCat)->where('study_ques_ans_status',1)->max('study_material_id');
  //       $data = array(
  //           'materialSingle'=> $material,
  //           'previous'		=> $Previous,
  //           'next'			=> $Next,
  //       );
  //       return view('frontend/ajax-study-material')->with($data);
	}
}
