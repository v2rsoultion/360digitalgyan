<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\PracticeQuiz\PracticeQuiz;
Use App\Model\Category\Category;
use App\Model\SubCategory\SubCategory;

class PracticeQuizController extends Controller
  {
  /*
  ** for app type according to url
  */
  public $appCat,$url;

  public function __construct()
  {
    $url  = str_replace(url('/'), '', url()->current());
    $ar = explode('/',ltrim($url, $url[0]));
    $this->appCat = getAppCategory($ar[0]);
    $this->url    = $ar[0].'/'.$ar[1];
  }

	public function index(Request $request,$slug=false)
	{
    $appCat = $this->appCat;
    $appUrl = $this->url;

    $title 	= "Digital Marketing & SEO Quiz | Interview Questions and technical Terms";
    $query  = SubCategory::Query();
    if($slug!=''){
      $query = $query->where('cat_slug',$slug);
    }
    $questions = $query->whereHas('PracticeQuiz')
                         ->with('Category')
                         ->where('status',1)
                         ->where('category_id',$appCat)
                         ->first();
    if(!$questions){
      return redirect('digital-marketing/videos-tutorials');
    } 
    $subcategories     = SubCategory::whereHas('PracticeQuiz')
                                        ->withCount('PracticeQuiz')
                                        ->where('status',1)
                                        ->where('category_id',$appCat)
                                        ->get();

    $data = array(
      'title' 		    => $title,
      'page_title' 	  => "Practice Quiz",
      'questions'		  => $questions,
      'subcategories' => $subcategories,
      'practiceUrl'   => url($appUrl)
    );
    return view('frontend/practice_quiz')->with($data);
	}
}