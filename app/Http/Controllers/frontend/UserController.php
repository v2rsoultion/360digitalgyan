<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Model\SeoUser\SeoUser;
use App\Model\SeoUser\SeoUserPoints;
use App\Model\PracticeQuiz\PracticeQuiz;
use App\Model\QuesAndAns\QuesAndAns;
use App\Model\QuizQuestions\QuizQuestions;
use App\Model\UsersAnswers\UsersAnswers;
use App\Model\QuizResult\QuizResult;
use App\Model\Transaction\Transaction;
use App\Model\LikeDislike\LikeDislike;
use App\Model\AllQuesLikes\AllQuesLikes;
use App\Model\AllQuesComments\AllQuesComments;
use App\Model\CommentLikeQuestion\CommentLikeQuestion;
use Validator;
use DB;
use Mail;
use App\Mail\digitalgyanMail;
use Socialite;

class UserController extends Controller
{
	public function login(Request $request)
	{

		$mail	= $request['email'];
		$pass 	= $request['password'];
		$user 	= SeoUser::where([
							'seo_users_email'		=> $mail,
							'seo_users_password'	=> md5($pass)
						])->first();

		$url  = str_replace(url('/'), '', url()->previous());
		
		if(!empty($user)){
			$name = explode(' ',$user->seo_users_name);
		  	session([
		  		'digital_user_id' 	=> $user->seo_users_id ,
		  		'digital_user_name' => $name[0]
		  	]);

			return response()->json([
				'status'	=> 'true',
				'message'	=> 'You Are successfully login',
				'url'		=> $url
			]);
		}else{
			return response()->json([
				'status'	=> 'false',
				'message'	=> 'Oops! That email / password combination is not valid..!!',
				'url'		=> $url
			]);
		}
	}

	public function profile()
	{
		$user = SeoUser::find(session('digital_user_id'));
		
		$data = array(
			'user' => $user
		);

		return view('frontend.user-profile')->with($data);
	}

	public function update_profile(Request $request)
	{
		try {
			$user 					= SeoUser::find(session('digital_user_id'));
		    $user->seo_users_name	= $request['name'];
		    $user->seo_user_phone	= $request['mobilenumber'];
		    $user->save();
			
			$success_msg = '<strong>Success!</strong> profile uploaded successfully..!';
			return redirect()->back()->withSuccess($success_msg);
		} catch (\Exception $e) {
			DB::rollback();
    		$error_msg	= $e->getMessage();
    		return redirect()->back()->withErrors($error_msg);
		}
	}

	public function change_password(Request $request)
	{

		try {
			$user 	= SeoUser::find(session('digital_user_id'));
			if (!empty($user) && ($user->seo_users_password==md5($request['cur_password']))){
				$user->seo_users_password = md5($request['new_password']);
			}
			$user->save();
			$success_msg = '<strong>Success!</strong> password updated successfully..!';
			return redirect()->back()->withSuccess($success_msg);
		
		} catch (\Exception $e) {
			DB::rollback();
    		$error_msg	= $e->getMessage();
    		return redirect()->back()->withErrors($error_msg);
		}
	}

	public function forgot_password(Request $request)
	{
		$mail	= $request['email'];
		$user 	= SeoUser::where([
							'seo_users_email' => $mail
						])->first();

		$url  = str_replace(url('/'), '', url()->previous());
		
		if(!empty($user)){
			
			$user2 = SeoUser::find($user->seo_users_id);
			$user2->user_reset_password = 1;
			$user2->save();

			$emailData = (object) array(
        		'mail' 		=> $user->seo_users_email,
        		'name' 		=> $user->seo_users_name,
        		'subject' 	=> "Reset password mail",
        		'user_id' 	=> get_encrypted_value($user->seo_users_id,true)
        	);

        	$view = 'email.360.reset-password';
        	Mail::to($emailData->mail)->send(new digitalgyanMail($emailData,$view));

			return response()->json([
				'status'	=> 'true',
				'message'	=> 'We have e-mailed your password reset link!',
				'url'		=> $url
			]);

		}else{
			return response()->json([
				'status'	=> 'false',
				'message'	=> "We can't find a user with that e-mail address.",
				'url'		=> $url
			]);
		}
	}

	public function reset_password($id=false)
	{	
		$userId = get_decrypted_value($id,true);
		$user 	= SeoUser::find($userId);
		if(!empty($user)){
            if($user->user_reset_password == 1){
                $uid   = get_encrypted_value($user->seo_users_id,true);
                $data = compact('uid');
                return view('frontend.reset-password',$data);
            }else{
            	$msg = "You already reset your password, this link does't exist";
                $data = compact('msg');
                return view('frontend.reset-password',$data);
            } 
        }
        return redirect('/');
	}

	public function change_reset_password(Request $request,$id)
	{
		$userId = get_decrypted_value($id,true);
		$user = SeoUser::find($userId);
		if (!empty($user)){
			$user->seo_users_password 	= md5($request['new_password']);
			$user->user_reset_password 	= 0;
			$user->save();

	        return redirect('/');
		}

	    return redirect('/');
	}

	public function logout()
	{
		session()->forget('digital_user_id');
		session()->forget('digital_user_name');
		return redirect('/');
	}
	
	/*
	** by hameed on 25/05/19
	*/
	public function register(Request $request)
	{
		$url    = str_replace(url('/'),'', url()->previous());
		$user = SeoUser::where('seo_users_email',$request['email'])->first();
		$validator = Validator::make($request->all(), [
			'agree' 			=> 'required',
			'name'			 	=> 'required',
			'email' 			=> 'required|email',
		]);
		if($validator->fails())
		{
		   return response()->json(['status'=>'false','message'=>$validator->errors()->first(),'url'=>$url]);
		}
		else
		{
			if(empty($user))
			{
				$user = New SeoUser;
				DB::beginTransaction();
	        	try{
	        		$user->seo_users_name 	= Input::get('name');
	        		$user->seo_users_email 	= Input::get('email');
	        		$user->seo_users_password = md5(Input::get('pwd'));
	        		$user->seo_user_type	= 3;
	        		$user->user_points 		= 0;
	        		$user->app_user 		= 30;
	        		$user->flag 			= 0;
	        		$user->save();
	        	}catch (\Exception $e){
	        		DB::rollback();
	        		$error_msg	= $e->getMessage();
	        		return response()->json($error_msg);
	        	}
	        	DB::commit();
	        	
	        	session(['digital_user_id' => $user->seo_users_id ,'digital_user_name' => $user->seo_users_name]);

	        	$emailData = (object) array(
	        		'mail' 		=> Input::get('email'),
	        		'name' 		=> Input::get('name'),
	        		'subject' 	=> "Welcome to 360digitalgyan"
	        	);
	        	$view = 'email.360.welcome-user';
	        	Mail::to($emailData->mail)->send(new digitalgyanMail($emailData,$view));
	        	
	        	return response()->json(['status'=>'true','message'=>'You Are successfully sign-up','url'=>$url]);
			}
		} 
	}

	public function email_check(Request $request)
	{
		$url    = str_replace(url('/'),'', url()->previous());
		$user 	= SeoUser::where('seo_users_email',$request->get('email'))->first();
		if(!empty($user)){
			return response()->json(['status'=>'false','message'=>'Oops, Email id already exiests!','url'=>$url]);
		}
	}

	public function social($driver)
	{
		return Socialite::driver($driver)->redirect();
	}
	public function callback($driver)
	{
		$user 		= Socialite::driver($driver)->user();

		if($driver=='google'){
			$userFlag = 1;
		}elseif($driver=='facebook') {
			$userFlag = 2;
		}elseif ($driver=='linkedin') {
			$userFlag =	3;
		}else{
			$userFlag = 0;
		}

		$userMail 	= $user->getEmail();
		if (empty($userMail)) {
			return redirect('/')->withErrors("You do not have an email in your social account");	
		}
		$seoUser 	= SeoUser::where('seo_users_email',$userMail)->first();
		$socialId 	= $user->getId();
		$name 		= $user->getName();
		$image 		= $user->getAvatar();
		if(!empty($seoUser)){
			session([
				'digital_user_id' 	=> $seoUser->seo_users_id,
				'digital_user_name' => $name
			]);
		}else{
			$newUser = New SeoUser;
			DB::beginTransaction();
        	try{
	    		$newUser->seo_users_name 		= $name;
	    		$newUser->seo_users_email 		= $userMail;
	    		$newUser->seo_users_password 	= '';
	    		$newUser->seo_users_fb_linked_in_id = $socialId;
	    		$newUser->seo_users_image 		= $image;
	    		$newUser->seo_user_type			= 3;
	    		$newUser->user_points 			= 0;
	    		$newUser->app_user 				= 30;
	    		$newUser->flag 					= $userFlag;
	    		$newUser->save();
        	}catch (\Exception $e){
        		DB::rollback();
        		$error_msg	= $e->getMessage();
        		return redirect('/')->withErrors($error_msg);
        	}
        	DB::commit();

        	session([
				'digital_user_id' 	=> $newUser->seo_users_id,
				'digital_user_name' => $name
			]);
			$emailData = (object) array(
        		'mail' 		=> $userMail,
        		'name' 		=> $name,
        		'subject' 	=> "Welcome to 360digitalgyan",
        	);

        	$view = 'email.360.welcome-user';
        	Mail::to($emailData->mail)->send(new digitalgyanMail($emailData,$view));
		}
		return redirect('/');
	}

	public function practice_quiz(Request $request)
	{
		if ($request['subcategory']!=''){
			$questions = PracticeQuiz::where('qustion_sub_sec_id',$request['subcategory'])->where('question_trial',0)->get();
			$totalQuestions = count($questions);
			$corrects		= 0;
			$inCorrects		= 0;
			$attempted		= 0;
			$notAttempted	= 0;
			foreach ($questions as $key => $ques){
				$i = $key+1;
				$name = 'radio'.$i;
				if ($request[$name]!=''){
					if ($ques->qustion_correct_answer==$request[$name]){
						$corrects++;
						$attempted++;
					}else if($ques->qustion_correct_answer!=$request[$name]){
						$inCorrects++;
						$attempted++;
					}
				}else{
					$notAttempted++;
				}
			}

			$msg = array(
				'total'			=> $totalQuestions,
				'correct'		=> $corrects,
				'incorrect'		=> $inCorrects,
				'attemped'		=> $attempted,
				'net' 			=> $corrects
			);
			return response()->json(['status'=>true,'message'=>$msg]);
		}else{
			$msg = "We found an error in your data. Please return to quiz page and try again.";
			return response()->json(['status'=>false,'message'=>$msg]);
		}
		
	}

	public function quiz_result(Request $request)
    {

    	if ($request['subcategory']!='') {
    		
    		DB::beginTransaction();
    		try {
    			$appCat		= getAppCategory($request['app-type']);
    			$DigiUser 	= session('digital_user_id');
				$user 		= SeoUser::find($DigiUser);
				$questions 	= QuizQuestions::where('qustion_sub_sec_id',$request['subcategory'])->where('question_trial',0)->get();
				$quizQues 	= array();
				/* result calulation */
				$totalQuestions = count($questions);
				$corrects		= 0;
				$inCorrects		= 0;
				$attempted		= 0;
				$notAttempted	= 0;
				foreach ($questions as $key => $ques){
					$i = $key+1;
					$name = 'radio'.$i;
					if ($request[$name]!=''){
						if ($ques->qustion_correct_answer==$request[$name]){
							$corrects++;
							$attempted++;
						}else if($ques->qustion_correct_answer!=$request[$name]){
							$inCorrects++;
							$attempted++;
						}
					}else{
						$notAttempted++;
					}
					$quizQues = $ques;
				}

				$pointsResults = array(
					'total'			=> $totalQuestions,
					'correct'		=> $corrects,
					'incorrect'		=> $inCorrects,
					'attemped'		=> $attempted,
					'notattemped'	=> $notAttempted,
					'net' 			=> $corrects
				);

				/* calculate result points */
				$resultPercent  = ($corrects/$totalQuestions)*100;
				if($resultPercent >= 80.0) {
	                $pointsEarned = 30;
	            }else if ($resultPercent >= 60.0 && $resultPercent < 80.0) {
	                $pointsEarned = 20;
	            }else if ($resultPercent >= 50.0 && $resultPercent < 60.0) {
	                $pointsEarned = 10;
	            }else{
	            	$pointsEarned = 0;
	            }
	            
	            $pointsResults['percentage'] 	= $resultPercent;
	            $pointsResults['pointsearned'] 	= $pointsEarned;
	            // p($pointsResults);
	            /* save quiz result for user */
	            // p($quizQues);
	            $quizRes 					= new QuizResult;
	            $quizRes->user_id 			= $DigiUser;
	            $quizRes->quiz_category 	= $quizQues->qustion_sec_id;
	            $quizRes->quiz_sub_cate_id 	= $request['subcategory'];
	            $quizRes->question_attempted= $pointsResults['attemped'];
	            $quizRes->question_skipped 	= $pointsResults['notattemped'];
	            $quizRes->correct_answers 	= $pointsResults['correct'];
	            $quizRes->wrong_answers 	= $pointsResults['incorrect'];
	            $quizRes->points_earned 	= $pointsResults['pointsearned'];
	            $quizRes->result_percentage = $pointsResults['percentage'];
	            $quizRes->save();

	            /* save user's points in seo_users table */
	            $user->user_points 	= $user->user_points+$pointsResults['pointsearned'];
	            $user->save();

	            /* save transaction for Quiz */
	            $transaction 					= new Transaction;
	            $transaction->txn_user_id 		= $DigiUser;
	            $transaction->user_app_type 	= $appCat;
	            $transaction->txn_question_id 	= $quizRes->quiz_result_id;
	            $transaction->txn_points 		= $pointsResults['pointsearned'];
	            $transaction->txn_type 			= 1; // 1 => Add, 2 => Less
	            $transaction->txn_flag 			= 7; // 7 => Quiz Points
	            $transaction->save();

	            $userPoints = SeoUserPoints::where(['seo_point_user_app_type'=>$appCat,'seo_point_user_id'=>$DigiUser])->first();
	            if(!empty($userPoints)){
	            	/* points add for app and user if exist */
	            	$userPointsOld 	= SeoUserPoints::find($userPoints->seo_points_id);
	            	$userPointsOld->seo_point_points = $userPointsOld->seo_point_points+$pointsResults['pointsearned'];
	            	$userPointsOld->save();
	            }else{
	            	/* points add for app and user if not exist */
	            	$userPointsNew = new SeoUserPoints;
	            	$userPointsNew->seo_point_user_id 		= $DigiUser;
	            	$userPointsNew->seo_point_user_app_type = $appCat;
	            	$userPointsNew->seo_point_user_badges	= '';
	            	$userPointsNew->seo_point_points 		= $pointsResults['pointsearned'];
	            	$userPointsNew->save();
	            }
    		} catch (\Exception $e) {
    			DB::rollback();
    			$errorMsg = $e->getMessage();
    			// $errorMsg = "We found an error in your data. Please return to quiz page and try again.";
				return response()->json(['status'=>false,'message'=>$errorMsg]);
    		}
    		DB::commit();

    		return response()->json(['status'=>true,'message'=>$pointsResults]);
		}else{
			$errorMsg ="We found an error in your data. Please return to quiz page and try again.";
			return response()->json(['status'=>false,'message'=>$errorMsg]);
		}
    }


	public function save_answer(Request $request)
	{
		$appCat		= getAppCategory($request['category_name']);
		$validator 	= Validator::make($request->all(), [
			'user_answer' 	=> 'required',
			'quetion_name' 	=> 'required',
			'category_name' => 'required',
		]);

		if($validator->fails())
		{
		   return redirect()->back()->withInput()->withErrors($validator);
		}
		else
		{
			if ($appCat!=''){
				DB::beginTransaction();
				try {
					$answer 	= $request['user_answer'];
					$user 		= session('digital_user_id');
					$quesId 	= $request['quetion_name'];
					
					$userAns = new UsersAnswers;
					$userAns->main_ans_cat 	= $appCat;
					$userAns->ques_answer  	= $answer;
					$userAns->ans_user_id   = $user;
					$userAns->ans_ques_id 	= $quesId;
					$userAns->ans_time 		= date('Y-m-d h:i:s');
					$userAns->answer_status = 0;
					$userAns->save();

				} catch (\Exception $e){
					DB::rollback();
					$error_message = $e->getMessage();
					return redirect()->back()->withErrors($error_message);				
				}
				DB::commit();	
			}else{
				$error_message = "<strong>Error!</strong> A problem has been occurred while submitting your data.";
				return redirect()->back()->withInput()->withErrors($error_message);
			}
		}

		// $success_msg = '<strong>Well done!</strong> You successfully posted answer..!';
		$success_msg = '<strong>Well done!</strong> Your answer is under review, It will be uploaded soon..!';
		return redirect()->back()->withSuccess($success_msg);
	}


	public function save_question(Request $request)
	{
		$appCat		= getAppCategory($request['category_name']);
		$validator 	= Validator::make($request->all(), [
			'user_question' 	=> 'required',
			'category_name' 	=> 'required',
		]);
		if($validator->fails())
		{
		   return redirect()->back()->withInput()->withErrors($validator);
		}
		else
		{
			if ($appCat!=''){
				DB::beginTransaction();
				try {
					$question 	= $request['user_question'];
					$user 		= session('digital_user_id');
					$tags 		= $request['questions_tags'];

					$userQues = new QuesAndAns;
					$userQues->ques_main_cat_id = $appCat;
					$userQues->user_question  	= $question;
					$userQues->ques_user_id   	= $user;
					$userQues->question_tags 	= $tags;
					$userQues->ques_time 		= date('Y-m-d h:i:s');
					$userQues->view_question_count 	= 0;
					$userQues->trending 			= 1;
					$userQues->question_status 		= 0;
					$userQues->save();

				} catch (\Exception $e){
					DB::rollback();
					$error_message = $e->getMessage();
					return redirect()->back()->withErrors($error_message);				
				}
				DB::commit();	
			}else{
				$error_message = "<strong>Error!</strong> A problem has been occurred while submitting your data.";
				return redirect()->back()->withInput()->withErrors($error_message);
			}
		}

		$success_msg = '<strong>Well done!</strong> Your question is under review, It will be uploaded soon..!'; 
		return redirect()->back()->withSuccess($success_msg);
	}


	protected function getQuestion($quesid,$type)
	{
		$question 	= QuesAndAns::where('ques_id',$quesid)
						->withCount(['getLikes','getDisLikes'])
						->first();

		$likes 	 	= $question->get_likes_count!=''?$question->get_likes_count:0;
		$dislikes = $question->get_dis_likes_count!=''?$question->get_dis_likes_count:0;
		
		if ($type=='like'){
			$classs1 = 'like';
			$classs2 = 'like-dislike-question';
		}else{
			$classs1 = 'like-dislike-question';
			$classs2 = 'dislike';
		}
		$msg = '<span class="'.$classs1.'"><i class="fas fa-thumbs-up"></i>'.$likes.'</span> <span class="'.$classs2.'"><i class="fas fa-thumbs-down"></i>'.$dislikes.'</span>';
		return 	$msg;
	}

	protected function pointsQuestionLike($quesId,$type,$user=false)
	{
		$question = QuesAndAns::where('ques_id',$quesId)->first();
		$points360	= config('custom.points');
		if ($user!=''){
			$userId 		= $user;
			$txnFlag 		= 3;
			$rwrdPoint 	= $points360['like_question'];
		}else{
			$userId 		= $question->ques_user_id;
			$txnFlag 		= 5;
			$rwrdPoint 	= $points360['user_like_question'];
		}
		$appType 			= $question->ques_main_cat_id;

		$traxn 		= Transaction::where([
									'txn_question_id' => $quesId,
									'txn_user_id'			=> $userId,
									'txn_flag'				=> $txnFlag
								])->first();
		
		if (empty($traxn)){
			$trans 									= new Transaction;
      $trans->txn_user_id     = $userId;
      $trans->user_app_type   = $appType;
      $trans->txn_question_id = $quesId;
      $trans->txn_points      = $rwrdPoint;
      $trans->txn_type        = $type=='add' ? 1 : 2 ; // 1 => add , 2 => less
      $trans->txn_flag        = $txnFlag;
      $trans->save();
		}else{
			$OldTraxn = Transaction::find($traxn->txn_id);
			$OldTraxn->delete();
		}

		$userPoint 	= SeoUserPoints::where([
    							'seo_point_user_id' 			=> $userId,
    							'seo_point_user_app_type'	=> $appType
    						])->first();

    if(!empty($userPoint)){
    	/* old user points = OUP */
    	$OUP = SeoUserPoints::Query();
    	$OUP->where([
						'seo_point_user_id' 			=> $userId,
						'seo_point_user_app_type'	=> $appType
					]);
			
			if ($type=='add'){
    		$OUP->update([
						'seo_point_points' => $userPoint->seo_point_points+$rwrdPoint
    		]);
    	}else{
    		$OUP->update([
						'seo_point_points' => $userPoint->seo_point_points-$rwrdPoint
    		]);
    	}
    }else{
    	if ($type=='add'){
	    	$NUP 													= new SeoUserPoints;
	    	$NUP->seo_point_user_id 			= $userId;
	    	$NUP->seo_point_user_app_type = $appType;
	    	$NUP->seo_point_user_badges 	= '';
	    	$NUP->seo_point_points 				= $rwrdPoint;
	    	$NUP->creatted_at 						= date('Y-m-d H:i:s');
	    	$NUP->save();
	    }
    }
		return true;
	}

	public function like_dislike_ques(Request $request)
	{
		// dd($request);
		$quesId 	= $request['data_id'];
		$userId 	= session('digital_user_id');
		$likeType = $request['liketype']=='like' ? 1 : 2; // 1 = like , 2 = dislike
		$quesType = 1; // 1 = question , 2 = answer

		$checkLike 	= LikeDislike::where([
			'question_answer_id' 	=> $quesId,
			'user_id' 			 			=> $userId,
			'like_dislike_flag'  	=> $quesType,
		])->first();

		if(empty($checkLike)){
			/* first time like or dislike */
			$likeDislike 											= new LikeDislike;
			$likeDislike->question_answer_id 	= $quesId;
			$likeDislike->user_id 						= $userId;
			$likeDislike->like_dislike_status = $likeType;
			$likeDislike->like_dislike_flag 	= $quesType;
			$likeDislike->save();

			if ($likeType==1){
				/* when like by user */
				$msg = $this->getQuestion($quesId,'like');
				$this->pointsQuestionLike($quesId,'add');
				$this->pointsQuestionLike($quesId,'add',$userId);	
			}else{
				/* when dislike by user */
				$msg = $this->getQuestion($quesId,'dislike');
			}

		}else{
			/* second time like or dislike */
			$query = LikeDislike::Query();
			$query->where([
					'question_answer_id' 	=> $quesId,
					'user_id' 			 			=> $userId,
					'like_dislike_flag'  	=> $quesType,
				]);

			if($checkLike->like_dislike_status==2){
				/* when like by user */
				$query->update([
					'like_dislike_status'	=> 1
				]);
				$msg = $this->getQuestion($quesId,'like');
				$this->pointsQuestionLike($quesId,'add');
				$this->pointsQuestionLike($quesId,'add',$userId);
			}else{
				/* when dislike by user */
				$query->update([
					'like_dislike_status'	=> 2
				]);

				$msg = $this->getQuestion($quesId,'dislike');
				$this->pointsQuestionLike($quesId,'less');
				$this->pointsQuestionLike($quesId,'less',$userId);
			} 
		}

		return response()->json(['status'=>1,'data'=>$msg]);
	}

	protected function getAnswers($ansId,$type)
	{
		$answer = UsersAnswers::where('answer_id',$ansId)
										->withCount(['getAnsLikes','getAnsDisLikes'])
										->first();

		$likes 	= $answer->get_ans_likes_count!=''?$answer->get_ans_likes_count:0;
		$dLikes = $answer->get_ans_dis_likes_count!=''?$answer->get_ans_dis_likes_count:0;
		
		if ($type=='like'){
			$classs1 = 'like';
			$classs2 = 'like-dislike-answer';
		}else{
			$classs1 = 'like-dislike-answer';
			$classs2 = 'dislike';
		}
		$msg = '<span class="'.$classs1.'"><i class="fas fa-thumbs-up"></i>'.$likes.'</span> <span class="'.$classs2.'"><i class="fas fa-thumbs-down"></i>'.$dLikes.'</span>';
		return 	$msg;
	}

	protected function pointsAnswerLike($ansId,$type,$user=false)
	{
		$answer 		= UsersAnswers::where('answer_id',$ansId)->first();
		$points360	= config('custom.points');
		if ($user!=''){
			$userId 		= $user;
			$txnFlag 		= 4;
			$rwrdPoint 	= $points360['like_answer'];
		}else{
			$userId 		= $answer->ans_user_id;
			$txnFlag 		= 6;
			$rwrdPoint 	= $points360['user_like_answer'];
		}

		$appType 			= $answer->main_ans_cat;
		$traxn 	= Transaction::where([
									'txn_question_id' => $ansId,
									'txn_user_id'			=> $userId,
									'txn_flag'				=> $txnFlag
								])->first();
		
		if (empty($traxn))
		{
			$trans 									= new Transaction;
      $trans->txn_user_id     = $userId;
      $trans->user_app_type   = $appType;
      $trans->txn_question_id = $ansId;
      $trans->txn_points      = $rwrdPoint;
      $trans->txn_type        = $type=='add' ? 1 : 2 ; // 1 => add , 2 => less
      $trans->txn_flag        = $txnFlag;
      $trans->save();
		}else{
			$OldTraxn = Transaction::find($traxn->txn_id);
			$OldTraxn->delete();
		}

		$userPoint 	= SeoUserPoints::where([
    							'seo_point_user_id' 			=> $userId,
    							'seo_point_user_app_type'	=> $appType
    						])->first();

    if(!empty($userPoint)){
    	/* old user points = OUP */
    	$OUP = SeoUserPoints::Query();
    	$OUP->where([
						'seo_point_user_id' 			=> $userId,
						'seo_point_user_app_type'	=> $appType
					]);
			
			if ($type=='add'){
    		$OUP->update([
						'seo_point_points' => $userPoint->seo_point_points+$rwrdPoint
    		]);
    	}else{
    		$OUP->update([
						'seo_point_points' => $userPoint->seo_point_points-$rwrdPoint
    		]);
    	}
    }else{
    	if ($type=='add'){
	    	$NUP 													= new SeoUserPoints;
	    	$NUP->seo_point_user_id 			= $userId;
	    	$NUP->seo_point_user_app_type = $appType;
	    	$NUP->seo_point_user_badges 	= '';
	    	$NUP->seo_point_points 				= $rwrdPoint;
	    	$NUP->creatted_at 						= date('Y-m-d H:i:s');
	    	$NUP->save();
	    }
    }
		return true;
	}

	public function like_dislike_ans(Request $request)
	{
		$ansId 		= $request['data_id'];
		$userId 	= session('digital_user_id');
		$likeType = $request['liketype']=='like' ? 1 : 2; // 1 = like , 2 = dislike
		$ansType  = 2; // 1 = question , 2 = answer

		$checkLike 	= LikeDislike::where([
			'question_answer_id' 	=> $ansId,
			'user_id' 			 			=> $userId,
			'like_dislike_flag'  	=> $ansType,
		])->first();

		if(empty($checkLike)){
			$likeDislike 											= new LikeDislike;
			$likeDislike->question_answer_id 	= $ansId;
			$likeDislike->user_id 						= $userId;
			$likeDislike->like_dislike_status = $likeType;
			$likeDislike->like_dislike_flag 	= $ansType;
			$likeDislike->save();

			if ($likeType==1){
				$msg = $this->getAnswers($ansId,'like');
				$this->pointsAnswerLike($ansId,'add');
				$this->pointsAnswerLike($ansId,'add',$userId);
			}else{
				$msg = $this->getAnswers($ansId,'dislike');
			}
		}else{

			/* second time like or dislike */
			$query = LikeDislike::Query();
			$query->where([
					'question_answer_id' 	=> $ansId,
					'user_id' 			 			=> $userId,
					'like_dislike_flag'  	=> $ansType,
				]);
			if($checkLike->like_dislike_status==2){
				/* when like by user */
				$query->update([
					'like_dislike_status'	=> 1
				]);
				$msg = $this->getAnswers($ansId,'like');
				$this->pointsAnswerLike($ansId,'add');
				$this->pointsAnswerLike($ansId,'add',$userId);
			}else{
				/* when dislike by user */
				$query->update([
					'like_dislike_status'	=> 2
				]);
				$msg = $this->getAnswers($ansId,'dislike');
				$this->pointsAnswerLike($ansId,'less');
				$this->pointsAnswerLike($ansId,'less',$userId);
			}
		}
		return response()->json(['status'=>1,'data'=>$msg]);
	}

	public function iq_like(Request $request)
	{
		$userId 	= session('digital_user_id');
		$iqId 		= $request['data_id'];
		$iqLiked 	= AllQuesLikes::where([
							'like_user_id'		=> $userId,
							'like_question_id'	=> $iqId,
							'like_flag' 		=> 1
						])->first();
		
		if (!empty($iqLiked)){
			$oldIqLiked = AllQuesLikes::find($iqLiked->like_id);
			$oldIqLiked->delete();
			
		}else{
			$allLikes 						= new AllQuesLikes;
			$allLikes->like_user_id 		= $userId;
			$allLikes->like_question_id 	= $iqId;
			$allLikes->like_flag 			= 1;
			$allLikes->save();
		}

		$result['likes'] = AllQuesLikes::where('like_question_id',$iqId)->get()->count();
		$result['status']= 1;
		return response()->json($result,200);
	}


	public function iq_commnt_like(Request $request)
	{
		$userId 	= session('digital_user_id');
		$cmnId 		= $request['data_id'];
		$iqCmnt 	= CommentLikeQuestion::where([
							'comment_user_id'		=> $userId,
							'comment_id'			=> $cmnId,
							'comment_flag' 			=> 1
						])->first();
		
		if (!empty($iqCmnt)){
			$oldIqLiked = CommentLikeQuestion::find($iqCmnt->comment_like_id);
			$oldIqLiked->delete();
			
		}else{
			$allLikes 						= new CommentLikeQuestion;
			$allLikes->comment_user_id 		= $userId;
			$allLikes->comment_id 			= $cmnId;
			$allLikes->comment_flag 		= 1;
			$allLikes->save();
		}		

		$result['likes'] = CommentLikeQuestion::where('comment_id',$cmnId)->get()->count();
		$result['status']= 1;
		return response()->json($result,200);
	}

	public function save_iq_comment(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'user_comment' 			=> 'required',
			'quetion_name'			=> 'required'
		]);
		if($validator->fails())
		{
		   return redirect()->back()->withInput()->withErrors($validator);
		}
		else
		{
			$cmnt = New AllQuesComments;
			DB::beginTransaction();
        	try{
        		$cmnt->comment_question_id 	= Input::get('quetion_name');
        		$cmnt->comment_user_id 		= session('digital_user_id');
        		$cmnt->comment_comment		= Input::get('user_comment');
        		$cmnt->comment_flag 		= 1;
        		$cmnt->save();
        	}catch (\Exception $e){
        		DB::rollback();
        		$error_msg	= $e->getMessage();
        		return redirect()->back()->withErrors($error_msg);
        	}

        	DB::commit();	        	
	        
	        $success_msg = '<strong>Well done!</strong> Your comment is posted successfully..!';
			return redirect()->back()->withSuccess($success_msg);
		}
	}
}
