<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
Use App\Model\Category\Category;
use App\Model\SubCategory\SubCategory;
use Route;
class TechnicalTermsController extends Controller
{
  /*
  ** for app type according to url
  */
  public $appCat,$url;

  public function __construct()
  {
    $url  = str_replace(url('/'), '', url()->current());
    $ar = explode('/',ltrim($url, $url[0]));
    $this->appCat = getAppCategory($ar[0]);
    $this->url    = $ar[0].'/'.$ar[1];
  }

	public function index(Request $request,$subcat=false)
  {
    $appCat = $this->appCat;
    $appUrl = $this->url;
    $subcategories  = SubCategory::whereHas('technicalTerms')
                                  ->withCount('technicalTerms')
                                  ->where('status',1)
                                  ->where('category_id',$appCat)
                                  ->get();

  	$query          = SubCategory::Query();
    
    if($subcat!=''){
      $query = $query->where('cat_slug',$subcat);
    }

    // if ($request['category']!=''){
    //   $query  = $query->where('sub_category_id',$request['category']);
    // }

    $questions= $query->with([
                          'technicalTerms' => function($q) use ($request){
                        if ($request['title']!=''){
                          $q->where('iq_ques_ans_q','LIKE','%'.$request['title'].'%');
                        }
                      }])
                      ->with(['Category'])
                      ->where('status',1)
                      ->where('category_id',$appCat)
                      ->first();
    // p($questions);
    // dd($query->toSql()); 
    $data = array(
  		'questions'       => $questions,
  		'subcategories'	  => $subcategories,
      'termUrl'         => url($appUrl)
  	);
    return view('frontend/technical-terms')->with($data);
  }
}
