<?php

    namespace App\Http\Controllers\frontend;

    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use App\Model\Customer\CustomerDocuments;
    use App\Model\Customer\CustomerItr;
    use App\Model\Customer\CustomerIncomenri;
    use App\Model\Customer\CustomerSourceincome;
    use App\Model\Customer\CustomerRentalincome;
    use App\Model\Customer\CustomerQuestion;
    use App\Model\Customer\CustomerPayment;
    use App\Model\Customer\Customer;
    use Illuminate\Support\Facades\Input;
    use Illuminate\Support\Facades\DB;
    use Validator;
    use Auth;
    use View;
    use Session as NSESSION;
    use App\Mail\VerifyMail;
    use Illuminate\Support\Facades\Mail;

    class FileItrController extends Controller
    {

        public function __construct()
        {
            
        }

        public function index($paytm_status = '')
        {
            $session_url = NSESSION::get('new_itr_request');
            $url         = where_to_go($session_url);
            if ($url == 'file-itr')
            {
                $data                      = [];
                $data['payment_data']      = get_customer_question_data();
                $data['payment_status']    = isset($data['payment_data']['status']) ? $data['payment_data']['status'] : null;
                $data['paytm_status']      = $paytm_status == 'failed' ? $paytm_status : '';
                $data['ques_data']         = $this->getQuestion();
                $data['arr_year']          = get_financial_year();
                $step2_data['data']        = $data['payment_data'];
                $data['selected']          = $step2_data;
                $data['refresh_url']       = url('file-itr');
                $data['itr_step2_content'] = View::make('frontend.file-itr-step-2')->with($step2_data)->render();
                return view('frontend.file-itr')->with($data);
            }
            else
            {
                return redirect($url);
            }
        }

        public function getQuestion()
        {
            $return_ques = [];
            $ques        = DB::table('questionnaire')->where('status', 1)->get()->toArray();
            if (!empty($ques))
            {
                $plan        = json_decode(json_encode($ques), true);
                $return_ques = array_column($plan, NULL, 'ques_no');
            }
            return $return_ques;
        }

        function fileItrData(Request $request)
        {
            $return = [];
            $status = 'failed';
            $input  = $request->input();
            if (!empty($input))
            {
                $step    = $input['step'];
                $year_id = $input['year_id'];
                if ($step == 'one')
                {
                    unset($input['_token']);
                    unset($input['step']);
                    unset($input['year_id']);
                    $question_no = DB::table('questionnaire')->select('ques_no')->where('status', 1)->get();
                    $question_no = json_decode($question_no, true);
                    $ques_result = [];
                    foreach ($question_no as $key => $ques)
                    {
                        $input_quest                                 = [];
                        $input_quest['question_' . $ques['ques_no']] = 'no';
                        if (isset($input['question_' . $ques['ques_no']]))
                        {
                            $input_quest['question_' . $ques['ques_no']] = $input['question_' . $ques['ques_no']];
                        }
                        $ques_result = array_merge($ques_result, $input_quest);
                    }
                    if ($ques_result['question_10'] == 'no')
                    {
                        $ques_result['question_1'] = '';
                        $ques_result['question_2'] = '';
                    }
                }
                $ques_1  = $ques_result['question_1'] == 'yes' ? true : false;
                $ques_2  = $ques_result['question_2'] == 'yes' ? true : false;
                $ques_3  = $ques_result['question_3'] == 'yes' ? true : false;
                $ques_4  = $ques_result['question_4'] == 'yes' ? true : false;
                $ques_5  = $ques_result['question_5'] == 'yes' ? true : false;
                $ques_6  = $ques_result['question_6'] == 'yes' ? true : false;
                $ques_7  = $ques_result['question_7'] == 'yes' ? true : false;
                $ques_8  = $ques_result['question_8'] == 'yes' ? true : false;
                $ques_9  = $ques_result['question_9'] == 'yes' ? true : false;
                $ques_10 = $ques_result['question_10'] == 'yes' ? true : false;
                $ques_11 = $ques_result['question_11'] == 'yes' ? true : false;
                if ($ques_8)
                {
                    $plan_type = 'diamond';
                }
                elseif ($ques_7 == true && $ques_8 == false)
                {
                    $plan_type = 'gold';
                }
                //&& ($ques_2 == true || $ques_3 == true || $ques_4 == true || $ques_5 == true || $ques_6 == true)
                elseif ($ques_10 == true && $ques_1 == false)
                {
                    $plan_type = 'silver';
                }
                else
                {
                    $plan_type = 'bronze';
                }
                $discount_amount = 0;
                if ($ques_11 == true && $ques_10 == true && $ques_1 == false && $plan_type == 'silver')
                {
                    $discount_amount = get_plan_coupn();
                }
//                elseif ($ques_1 == true && ($ques_2 == true || $ques_3 == true || $ques_4 == true || $ques_5 == true || $ques_6 == true))
//                {
//                    $plan_type = 'bronze';
//                }
//                elseif ($ques_1 == true && ($ques_2 == false && $ques_3 == false && $ques_4 == false && $ques_5 == false && $ques_6 == false && $ques_7 == false && $ques_8 == false))
//                {
//                    $plan_type = 'bronze';
//                }
                $plan_data = DB::table('plan')->where('type', $plan_type)->first();
                $plan_data = json_decode(json_encode($plan_data), true);

                // save payment data  for step 1
                $customer_info       = Auth::guard('web')->user();
                $customer_payment_id = 'TXZEE' . $customer_info['customer_id'] . '_' . time();

                try
                {
                    $payment_question_data = CustomerQuestion::where('customer_id', $customer_info['customer_id'])->where('status', 1)->first();
                    if (!empty($payment_question_data->customer_question_id))
                    {
                        // if user fille 2 steps in last login then will able to do only payment 
                        $cus_question = $payment_question_data;
                    }
                    else
                    {
                        // new entry of questionanaire
                        $cus_question = new CustomerQuestion;
                    }
                    $cus_question->customer_id     = $customer_info['customer_id'];
                    $cus_question->plan_id         = $plan_data['plan_id'];
                    $cus_question->plan_price      = $plan_data['plan_price'];
                    $cus_question->discount_amount = $discount_amount;
                    $cus_question->ques_no         = json_encode($ques_result);
                    $cus_question->year_id         = $year_id;
                    $cus_question->status          = 1;
                    if ($cus_question->save())
                    {
                        // update payment id in customer table
//                        $customer_update             = Customer::find($customer_info['customer_id']);
//                        $customer_update->payment_id = $cus_payment['payment_id'];
//                        $customer_update->save();
                        $arr_ques                                  = [];
                        $arr_ques['ques_no']                       = $ques_result;
                        $step_data['data']                         = array_merge($arr_ques, $plan_data);
                        $step_data['data']['customer_question_id'] = $cus_question->customer_question_id;
                        $step_data['data']['discount_amount']      = $discount_amount;
                        $return['itr_step2_content']               = View::make('frontend.file-itr-step-2')->with($step_data)->render();
                        $status                                    = 'success';
                    }
                }
                catch (Exception $e)
                {
                    DB::rollback();
                    $error_message = $e->getMessage();
                    p($error_message);
                    return redirect()->back()->withErrors($error_message);
                }
                DB::commit();
                return response()->json(array('status' => $status, 'data' => $return));
            }
        }

        function getItrFile($customer_payment_id, $status = '')
        {
            $url = where_to_go();
            if ($url == 'get-itr-file/' . $customer_payment_id)
            {
                $payment_data = get_customer_payment($customer_payment_id);
                if (!empty($payment_data))
                {
                    $data                 = [];
                    $data['paytm_status'] = $status == 'success' ? 'success' : '';
                    $data['refresh_url']  = url('get-itr-file/' . $customer_payment_id);
                    $data['payment_data'] = get_customer_payment($customer_payment_id);
                    return view('frontend/file-itr-step-4')->with($data);
                }
                else
                {
                    return redirect('/');
                }
            }
            else
            {
                return redirect($url);
            }
        }

        public function saveFileItr(Request $request)
        {
            $customer_detail         = Auth::guard('web')->user();
            $customer_id             = $customer_detail->customer_id;
            $prefix                  = $customer_detail->fname;
            $customer_payment_id     = Input::get('customer_payment_id');
            $customer_payment        = get_customer_payment($customer_payment_id);
            $arr_selected_ques       = $customer_payment['ques_no'];
            $input_validate['phone'] = 'required';
            if ($arr_selected_ques['question_1'] == 'yes')
            {
                $input_validate['form_16.*'] = 'required';
            }
//            if ($arr_selected_ques['question_2'] == 'yes')
//            {
//                $input_validate['interest_saving_bank']       = 'required';
//                $input_validate['interest_fixed_deposits']    = 'required';
//                $input_validate['interest_recurring_deposit'] = 'required';
//                $input_validate['interest_bonds']             = 'required';
//                $input_validate['other_interest']             = 'required';
//                $input_validate['agriculture_income']         = 'required';
//                $input_validate['dividend_income']            = 'required';
//                $input_validate['ppf_interest']               = 'required';
//                $input_validate['family_pension']             = 'required';
//                $input_validate['ppf_interest']               = 'required';
//                $input_validate['commission_income']          = 'required';
//                $input_validate['parttime_income']            = 'required';
//            }
//            if ($arr_selected_ques['question_3'] == 'yes')
//            {
//                $input_validate['address']            = 'required';
//                $input_validate['house_type']         = 'required';
//                $input_validate['lettable_value']     = 'required';
//                $input_validate['rent_receivable']    = 'required';
//                $input_validate['unrealized_rent']    = 'required';
//                $input_validate['house_tax']          = 'required';
//                $input_validate['homeloan_interest']  = 'required';
//                $input_validate['FY_year_completion'] = 'required';
//                $input_validate['FY_loantaken']       = 'required';
//            }
            if ($arr_selected_ques['question_4'] == 'yes' || $arr_selected_ques['question_5'] == 'yes')
            {
                
            }
            if ($arr_selected_ques['question_7'] == 'yes')
            {
                
            }
            if ($arr_selected_ques['question_8'] == 'yes')
            {
                $input_validate['country_residence'] = 'required';
                $input_validate['address_india']     = 'required';
            }

            $validatior = Validator::make($request->all(), $input_validate);
            if ($validatior->fails())
            {
                return redirect()->back()->withInput()->withErrors($validatior);
            }
            else
            {
                DB::beginTransaction();
                try
                {
                    $customer_itr                           = new CustomerItr;
                    $customer_itr->plan_id                  = Input::get('plan_id');
                    $customer_itr->year_id                  = $customer_payment['year_id'];
                    $customer_itr->payment_id               = $customer_payment['payment_id'];
                    $customer_itr->additional_info          = Input::get('additional_info');
                    $customer_itr->home_loan_principal_paid = Input::get('home_loan_principal_paid');
                    $customer_itr->life_insurance           = Input::get('life_insurance');
                    $customer_itr->contribution_pf_ppf      = Input::get('contribution_pf_ppf');
                    $customer_itr->tuition_fee              = Input::get('tuition_fee');
                    $customer_itr->sukanya_deposit          = Input::get('sukanya_deposit');
                    $customer_itr->tax_fixed_deposit        = Input::get('tax_fixed_deposit');
                    $customer_itr->deduction_80c            = Input::get('deduction_80c');
                    $customer_itr->deduction_80d            = Input::get('deduction_80d');
                    $customer_itr->charitable_contribution  = Input::get('charitable_contribution');
                    $customer_itr->customer_id              = $customer_id;
                    if ($customer_itr->save())
                    {

                        // update phone no of customer
                        $customer_update        = Customer::Find($customer_id);
                        $customer_update->phone = Input::get('phone');
                        $customer_update->save();
                        // if customer is nri (question_8 is yes)

                        if ($arr_selected_ques['question_10'] == 'yes')
                        {
                            if ($request->hasFile('form_16'))
                            {
                                $files     = $request->file('form_16');
                                $file_type = 'form16';
                                foreach ($files as $key => $file)
                                {
                                    $customer_form16              = new CustomerDocuments;
                                    $form16                       = \Config::get('custom.customer_form16');
                                    $destinationPath              = public_path() . $form16['upload_path'];
                                    $filename                     = modify_file_name($file, $prefix, $file_type, true);
                                    $file->move($destinationPath, $filename);
                                    $customer_form16->filename    = $filename;
                                    $customer_form16->customer_id = $customer_id;
                                    $customer_form16->type        = 'form16';
                                    $arr_customer_form16[]        = $customer_form16;
                                }
                                $customer_itr->getItrdocumnets()->saveMany($arr_customer_form16);
                            }
                            $arr_customer_salary = [];
                            if ($request->hasFile('salary_certificate'))
                            {
                                $files     = $request->file('salary_certificate');
                                $file_type = 'salary_certificate';
                                foreach ($files as $key => $file)
                                {
                                    $customer_salary_certificate              = new CustomerDocuments;
                                    $salary_certificate                       = \Config::get('custom.customer_form16');
                                    $destinationPath                          = public_path() . $salary_certificate['upload_path'];
                                    $filename                                 = modify_file_name($file, $prefix, $file_type, true);
                                    $file->move($destinationPath, $filename);
                                    $customer_salary_certificate->filename    = $filename;
                                    $customer_salary_certificate->customer_id = $customer_id;
                                    $customer_salary_certificate->type        = 'salary_certificate';
                                    $arr_customer_salary[]                    = $customer_salary_certificate;
                                }
                                $customer_itr->getItrdocumnets()->saveMany($arr_customer_salary);
                            }
                        }
//                        if ($arr_selected_ques['question_2'] == 'yes' || $arr_selected_ques['question_6'] == 'yes' || $arr_selected_ques['question_9'] == 'yes')
//                        {
                        $customer_income_source                             = new CustomerSourceincome;
                        $customer_income_source->customer_id                = $customer_id;
                        $customer_income_source->interest_saving_bank       = Input::get('interest_saving_bank');
                        $customer_income_source->interest_fixed_deposits    = Input::get('interest_fixed_deposits');
                        $customer_income_source->interest_recurring_deposit = Input::get('interest_recurring_deposit');
                        $customer_income_source->interest_bonds             = Input::get('interest_bonds');
                        $customer_income_source->other_interest             = Input::get('other_interest');
                        $customer_income_source->agriculture_income         = Input::get('agriculture_income');
                        $customer_income_source->dividend_income            = Input::get('dividend_income');
                        $customer_income_source->ppf_interest               = Input::get('ppf_interest');
                        $customer_income_source->family_pension             = Input::get('family_pension');
                        $customer_income_source->commission_income          = Input::get('commission_income');
                        $customer_income_source->parttime_income            = Input::get('parttime_income');

                        if (!empty($customer_income_source))
                        {
                            $customer_itr->getSourcelincome()->save($customer_income_source);
                        }
//                        }
                        if ($arr_selected_ques['question_3'] == 'yes')
                        {
                            $customer_rental_income                     = new CustomerRentalincome;
                            $customer_rental_income->customer_id        = $customer_id;
                            $customer_rental_income->address            = Input::get('address');
                            $customer_rental_income->type               = Input::get('house_type');
                            $customer_rental_income->lettable_value     = Input::get('lettable_value');
                            $customer_rental_income->rent_receivable    = Input::get('rent_receivable');
                            $customer_rental_income->unrealized_rent    = Input::get('unrealized_rent');
                            $customer_rental_income->house_tax          = Input::get('house_tax');
                            $customer_rental_income->homeloan_interest  = Input::get('homeloan_interest');
                            $customer_rental_income->FY_year_completion = Input::get('FY_year_completion');
                            $customer_rental_income->FY_loantaken       = Input::get('FY_loantaken');

                            $customer_itr->getItrrentelincome()->save($customer_rental_income);
                        }
                        if ($arr_selected_ques['question_4'] == 'yes' || $arr_selected_ques['question_5'] == 'yes')
                        {
                            if ($request->hasFile('capital_gain_share'))
                            {
                                $files     = $request->file('capital_gain_share');
                                $file_type = 'capital_share';
                                foreach ($files as $key => $file)
                                {
                                    $capital_share              = new CustomerDocuments;
                                    $share_gain                 = \Config::get('custom.customer_capital_gain');
                                    $destinationPath            = public_path() . $share_gain['upload_path'];
                                    $filename                   = modify_file_name($file, $prefix, $file_type, true);
                                    $file->move($destinationPath, $filename);
                                    $capital_share->filename    = $filename;
                                    $capital_share->customer_id = $customer_id;
                                    $capital_share->type        = 'capital_share';
                                    $arr_capital_share[]        = $capital_share;
                                }

                                $customer_itr->getItrdocumnets()->saveMany($arr_capital_share);
                            }
                            if ($request->hasFile('capital_gain_mutual_fund'))
                            {
                                $files     = $request->file('capital_gain_mutual_fund');
                                $file_type = 'capital_mutual';
                                foreach ($files as $key => $file)
                                {
                                    $capital_mutual              = new CustomerDocuments;
                                    $mutual_gain                 = \Config::get('custom.customer_capital_gain');
                                    $destinationPath             = public_path() . $mutual_gain['upload_path'];
                                    $filename                    = modify_file_name($file, $prefix, $file_type, true);
                                    $file->move($destinationPath, $filename);
                                    $capital_mutual->filename    = $filename;
                                    $capital_mutual->customer_id = $customer_id;
                                    $capital_mutual->type        = 'capital_mutual';
                                    $arr_capital_mutual[]        = $capital_mutual;
                                }
                                $customer_itr->getItrdocumnets()->saveMany($arr_capital_mutual);
                            }
                            if ($request->hasFile('capital_gain_other'))
                            {
                                $files     = $request->file('capital_gain_other');
                                $file_type = 'capital_gain';
                                foreach ($files as $key => $file)
                                {
                                    $capital_other              = new CustomerDocuments;
                                    $other_gain                 = \Config::get('custom.customer_capital_gain');
                                    $destinationPath            = public_path() . $other_gain['upload_path'];
                                    $filename                   = modify_file_name($file, $prefix, $file_type, true);
                                    $file->move($destinationPath, $filename);
                                    $capital_other->filename    = $filename;
                                    $capital_other->customer_id = $customer_id;
                                    $capital_other->type        = 'capital_other';
                                    $arr_capital_other[]        = $capital_other;
                                }
                                $customer_itr->getItrdocumnets()->saveMany($arr_capital_other);
                            }
                        }
                        if ($arr_selected_ques['question_7'] == 'yes')
                        {
                            if ($request->hasFile('profit_loss'))
                            {
                                $files     = $request->file('profit_loss');
                                $file_type = 'profit_loss';
                                foreach ($files as $key => $file)
                                {
                                    $profit_loss              = new CustomerDocuments;
                                    $business_income          = \Config::get('custom.customer_business_income');
                                    $destinationPath          = public_path() . $business_income['upload_path'];
                                    $filename                 = modify_file_name($file, $prefix, $file_type, true);
                                    $file->move($destinationPath, $filename);
                                    $profit_loss->filename    = $filename;
                                    $profit_loss->customer_id = $customer_id;
                                    $profit_loss->type        = 'profit_loss';
                                    $arr_profit_loss[]        = $profit_loss;
                                }
                                $customer_itr->getItrdocumnets()->saveMany($arr_profit_loss);
                            }
                            if ($request->hasFile('balance_sheet'))
                            {
                                $file_type = 'balance_sheet';
                                $files     = $request->file('balance_sheet');
                                foreach ($files as $key => $file)
                                {
                                    $balance_sheet              = new CustomerDocuments;
                                    $business_income            = \Config::get('custom.customer_business_income');
                                    $destinationPath            = public_path() . $business_income['upload_path'];
                                    $filename                   = modify_file_name($file, $prefix, $file_type, true);
                                    $file->move($destinationPath, $filename);
                                    $balance_sheet->filename    = $filename;
                                    $balance_sheet->customer_id = $customer_id;
                                    $balance_sheet->type        = 'balance_sheet';
                                    $arr_balance_sheet[]        = $balance_sheet;
                                }
                                $customer_itr->getItrdocumnets()->saveMany($arr_balance_sheet);
                            }
                            if ($request->hasFile('gst_return'))
                            {
                                $files     = $request->file('gst_return');
                                $file_type = 'gst';
                                foreach ($files as $key => $file)
                                {
                                    $gst_return              = new CustomerDocuments;
                                    $business_income         = \Config::get('custom.customer_business_income');
                                    $destinationPath         = public_path() . $business_income['upload_path'];
                                    $filename                = modify_file_name($file, $prefix, $file_type, true);
                                    $file->move($destinationPath, $filename);
                                    $gst_return->filename    = $filename;
                                    $gst_return->customer_id = $customer_id;
                                    $gst_return->type        = 'gst';
                                    $arr_gst_return[]        = $gst_return;
                                }
                                $customer_itr->getItrdocumnets()->saveMany($arr_gst_return);
                            }
                        }
                        if ($arr_selected_ques['question_8'] == 'yes')
                        {
                            if ($request->has('country_residence') || $request->has('address_india'))
                            {
                                $customer_nri                    = new CustomerIncomenri;
                                $customer_nri->country_residence = Input::get('country_residence');
                                $customer_nri->address_india     = Input::get('address_india');
                                $customer_nri->customer_id       = $customer_id;
                                $customer_itr->customerNri()->save($customer_nri);

                                if ($request->hasFile('nri_doc'))
                                {
                                    $file_type            = 'nri_doc';
                                    $files                = $request->file('nri_doc');
                                    $arr_customer_nri_doc = [];
                                    foreach ($files as $key => $file)
                                    {
                                        $customer_nri_doc              = new CustomerDocuments;
                                        $nri_doc                       = \Config::get('custom.customer_nri');
                                        $destinationPath               = public_path() . $nri_doc['upload_path'];
                                        $filename                      = modify_file_name($file, $prefix, $file_type, true);
                                        $file->move($destinationPath, $filename);
                                        $customer_nri_doc->filename    = $filename;
                                        $customer_nri_doc->type        = 'nri_doc';
                                        $customer_nri_doc->customer_id = $customer_id;
                                        $arr_customer_nri_doc[]        = $customer_nri_doc;
                                    }
                                    if (!empty($arr_customer_nri_doc))
                                    {
                                        $customer_itr->getItrdocumnets()->saveMany($arr_customer_nri_doc);
                                    }
                                }
                            }
//                            p('endd');
                        }
                    }

                    if ($request->hasFile('pan'))
                    {
                        $file_type = 'pan';
                        if ($request->has('pan_id'))
                        {
                            $document_id       = Input::get('pan_id');
                            $customer_document = CustomerDocuments::Find($document_id);
                        }
                        else
                        {
                            $customer_document = new CustomerDocuments;
                        }
                        $file                           = $request->file('pan');
                        $customer_pan                   = \Config::get('custom.customer_pan');
                        $destinationPath                = public_path() . $customer_pan['upload_path'];
                        $filename                       = modify_file_name($file, $prefix, $file_type, true);
                        $file->move($destinationPath, $filename);
                        $customer_document->filename    = $filename;
                        $customer_document->customer_id = $customer_id;
                        $customer_document->type        = 'pan';
                        $customer_itr->getItrdocumnets()->save($customer_document);
                    }
                    if ($request->hasFile('aadhaar'))
                    {
                        if ($request->has('aadhaar_id'))
                        {
                            $document_id       = Input::get('aadhaar_id');
                            $customer_document = CustomerDocuments::Find($document_id);
                        }
                        else
                        {
                            $customer_document = new CustomerDocuments;
                        }
                        $file_type                      = 'aadhaar';
                        $file                           = $request->file('aadhaar');
                        $customer_aadhaar               = \Config::get('custom.customer_aadhaar');
                        $destinationPath                = public_path() . $customer_aadhaar['upload_path'];
                        $filename                       = modify_file_name($file, $prefix, $file_type, true);
                        $file->move($destinationPath, $filename);
                        $customer_document->filename    = $filename;
                        $customer_document->customer_id = $customer_id;
                        $customer_document->type        = $file_type;
                        $customer_itr->getItrdocumnets()->save($customer_document);
                    }
                    $success_msg = 'Your Itr file has been submitted successfully';
                }
                catch (\Exception $e)
                {
                    DB::rollback();
                    $error_message = $e->getMessage();
                    return redirect()->back()->withErrors($error_message);
                }
                DB::commit();
            }
            return redirect('dashboard')->withSuccess($success_msg);
        }

        function uploadItrDocument(Request $request)
        {
            if ($request->hasFile('upload_file'))
            {
                $arr_doc    = [];
                $validatior = Validator::make($request->all(), [
                        'type'    => 'required',
                        'year_id' => 'required',
                ]);
                if ($validatior->fails())
                {
                    return redirect()->back()->withInput()->withErrors($validatior);
                }
                else
                {
                    DB::beginTransaction();
                    try
                    {
                        $customer     = Auth::guard('web')->user();
                        $prefix       = $customer['fname'];
                        $customer_id  = $customer['customer_id'];
                        $year_id      = Input::get('year_id');
                        $customer_itr = CustomerItr::where('customer_id', $customer_id)->where('year_id', $year_id)->first();
                        if ($customer_itr->customer_itr_id)
                        {
                            $files           = $request->file('upload_file');
                            $file_type       = Input::get('type');
                            $arr_directory   = \Config::get('custom.document_folder');
                            $directory_name  = $arr_directory[$file_type];
                            $folder          = \Config::get('custom.' . $directory_name);
                            $destinationPath = public_path() . $folder['upload_path'];
                            foreach ($files as $key => $file)
                            {
                                $customer_doc              = new CustomerDocuments;
                                $filename                  = modify_file_name($file, $prefix, $file_type, true);
                                $file->move($destinationPath, $filename);
                                $customer_doc->filename    = $filename;
                                $customer_doc->customer_id = $customer_id;
                                $customer_doc->type        = $file_type;
                                $arr_doc[]                 = $customer_doc;
                            }
                            if (!empty($arr_doc))
                            {
                                $customer_itr->getItrdocumnets()->saveMany($arr_doc);
                            }
                        }
                        else
                        {
                            return redirect('my-documents')->withErrors("Itr of selected year doesn't exist, So please select valid year");
                        }
                    }
                    catch (\Exception $e)
                    {
                        DB::rollback();
                        $error_message = $e->getMessage();
                        return redirect()->back()->withErrors($error_message);
                    }
                    DB::commit();
                    return redirect('my-documents')->withSuccess('Document uploaded successfully');
                }
            }
            else
            {
                return redirect('my-documents')->withErrors("Please select file");
            }
        }

        function updateItrDocument(Request $request)
        {
            if ($request->hasFile('update_file'))
            {
                $customer    = Auth::guard('web')->user();
                $prefix      = $customer['fname'];
                $document_id = Input::get('document_id');
                DB::beginTransaction();
                try
                {
                    $customer_doc           = CustomerDocuments::Find($document_id);
                    $arr_directory          = \Config::get('custom.document_folder');
                    $directory_name         = $arr_directory[$customer_doc['type']];
                    $folder                 = \Config::get('custom.' . $directory_name);
                    $destinationPath        = public_path() . $folder['upload_path'];
                    $file                   = $request->file('update_file');
                    $file_type              = $customer_doc['type'];
                    $filename               = modify_file_name($file, $prefix, $file_type, true);
                    $file->move($destinationPath, $filename);
                    $customer_doc->filename = $filename;
                    $customer_doc->save();
                }
                catch (\Exception $e)
                {
                    DB::rollback();
                    $error_message = $e->getMessage();
                    return redirect()->back()->withErrors($error_message);
                }
                DB::commit();
                return redirect('my-documents')->withSuccess('File updated successfully');
            }
            else
            {
                return redirect('my-documents')->withErrors("Please select file");
            }
        }

        function checkPlanValidity(Request $request)
        {
            $year_id = $request->input('year_id');
            $return  = check_plan_validity($year_id);
            return response()->json($return);
        }

    }
    