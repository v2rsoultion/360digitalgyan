<?php

namespace App\Http\Controllers\frontend;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\QuesAndAns\QuesAndAns;
use App\Model\UsersAnswers\UsersAnswers;
use App\Model\LikeDislike\LikeDislike;
class QuestionsAnswerController extends Controller
{
    /*
    ** for app type according to url
    */
    public $appCat,$url;
    
    public function __construct()
    {
        $url  = str_replace(url('/'), '', url()->current());
        $ar = explode('/',ltrim($url, $url[0]));
        $this->appCat = getAppCategory($ar[0]);
        $this->url    = $ar[0];
    }

	public function answer($id=false)
    {
        $appCat     = $this->appCat;
        $appUrl     = $this->url;
        
        $title      = "Digital Marketing & SEO Quiz | Interview Questions and technical Terms";
        $question   = QuesAndAns::where('ques_id',$id)
                                ->with(['answers','answers.getAnsUser','getUser'])
                                ->withCount(['getLikes','getDisLikes'])
                                ->with([
                                    'answers'   => function($query){
                                        $query->withCount([
                                            'getAnsDisLikes',
                                            'getAnsLikes'
                                        ]);
                                    }])->first();
        
        $relQuestions   = QuesAndAns::where('question_status',1)
                                        ->where('ques_main_cat_id',$appCat)
                                        ->orderBy('trending','DESC')
                                        ->orderBy('ques_id','DESC')
                                        ->get()
                                        ->take(5);

        $unAnswerd      = QuesAndAns::has('answers','<',1)
                                        ->where('question_status',1)
                                        ->where('ques_main_cat_id',$appCat)
                                        ->orderBy('trending','DESC')
                                        ->orderBy('ques_id','DESC')
                                        ->get()
                                        ->take(5);

        if (!empty(session('digital_user_id'))){
            /* for get Question that liked by user */
            $quesLikedByUser    = getLikeDislike(session('digital_user_id'),1,1);
            /* for get Question that disliked by user */
            $quesDislikedByUser = getLikeDislike(session('digital_user_id'),1,2);

            /* for get Question that liked by user */
            $ansLikedByUser    = getLikeDislike(session('digital_user_id'),2,1);
            /* for get Question that disliked by user */
            $ansDislikedByUser = getLikeDislike(session('digital_user_id'),2,2);
        }

        $data = array(
            'title'             => $title,
            'question'          => $question,
            'relQuestions'      => $relQuestions,
            'unAnswerd'         => $unAnswerd,
            'questionUrl'       => url($appUrl),
            'quesLikedByUser'   => $quesLikedByUser,
            'quesDislikedByUser'=> $quesDislikedByUser,
            'ansLikedByUser'    => $ansLikedByUser,
            'ansDislikedByUser' => $ansDislikedByUser,
        );
        return view('frontend/answers')->with($data);
    }

    public function questions_answers(Request $request)
    {
        $appCat     = $this->appCat;
        $appUrl     = $this->url;
        $query      = QuesAndAns::Query();

        //1 -> Trending (Default)
        //2 -> Most Popular
        //3 -> Most Recent
        //4 -> Most Answered
        //5 -> Unanswered
        //6 -> My Questions (user_id)
        //7 -> My Answers (user_id)
        if($request['question_type']!=''){

            if($request['question_type']==1){
                $query = $query->orderBy('trending','DESC');
            }

            if($request['question_type']==2){
                $query = $query->orderBy('popular','DESC');
            }

            if($request['question_type']==3){
                $query = $query->orderBy('ques_id','DESC');
            }

            if($request['question_type']==4){
                $query = $query->whereHas('answers')->orderBy('answers_count','DESC');
            }

            if($request['question_type']==5){
                $query = $query->has('answers','<',1);
            }

            if($request['question_type']==6){
                $query = $query->where('ques_user_id',session('digital_user_id'));
            }
            if ($request['question_type']==7){
                $userId = session('digital_user_id');
                $query = $query->whereHas('answers',function($q) use ($userId){
                    $q->where('ans_user_id',$userId)
                      ->where('answer_status',1);
                });
            }
        }else{
            $query = $query->orderBy('trending','DESC')->orderBy('ques_id','DESC');
        }

        if ($request['tag']!=''){
            $query = $query->whereRaw('FIND_IN_SET(?,question_tags)',$request['tag']);
        }

        $appends    = array(
            'question_type'  => $request['question_type'],
            'tag'            => $request['tag']
        );

        $questions  = $query->with('getUser')
                            ->withCount(['getLikes','getDisLikes','answers'])
                            ->where('question_status',1)
                            ->where('ques_main_cat_id',$appCat)
                            ->paginate(10)
                            ->appends($appends);
        // p($questions);
        $title  = "Digital Marketing & SEO Quiz | Interview Questions and technical Terms";
        $relQuestions   = QuesAndAns::where('question_status',1)
                                        ->where('ques_main_cat_id',$appCat)
                                        ->orderBy('trending','DESC')
                                        ->orderBy('ques_id','DESC')
                                        ->get()
                                        ->take(5);
                                        
        $unAnswerd      = QuesAndAns::has('answers','<',1)
                                        ->where('question_status',1)
                                        ->where('ques_main_cat_id',$appCat)
                                        ->orderBy('trending','DESC')
                                        ->orderBy('ques_id','DESC')
                                        ->get()
                                        ->take(5);
        if (!empty(session('digital_user_id'))){
            /* for get Question that liked by user */
            $likedByUser    = getLikeDislike(session('digital_user_id'),1,1);
            /* for get Question that disliked by user */
            $dislikedByUser = getLikeDislike(session('digital_user_id'),1,2);
        }

        $questionUrl = url($appUrl);
        $data = array(
            'title'         => $title,
            'questions'     => $questions,
            'relQuestions'  => $relQuestions,
            'unAnswerd'     => $unAnswerd,
            'questionUrl'   => $questionUrl,
            'likedByUser'   => $likedByUser,
            'dislikedByUser'=> $dislikedByUser,
        );

        if($request->ajax()) {
            return view('frontend.ques', compact('questions','questionUrl','appends','likedByUser','dislikedByUser'));
        }

        return view('frontend/QuesAndAns')->with($data);
    }
}