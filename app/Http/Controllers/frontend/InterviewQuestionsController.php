<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
Use App\Model\Category\Category;
use App\Model\SubCategory\SubCategory;
use App\Model\InterviewQuestions\InterviewQuestions;
use App\Model\CommentLikeQuestion\CommentLikeQuestion;
class InterviewQuestionsController extends Controller
{
  /*
  ** for app type according to url
  */
  public $appCat,$url;

  public function __construct()
  {
    $url  = str_replace(url('/'), '', url()->current());
    $ar = explode('/',ltrim($url, $url[0]));
    $this->appCat = getAppCategory($ar[0]);
    $this->url    = $ar[0].'/'.$ar[1];
  }

	public function index($subcat=false)
  {
    $appCat    = $this->appCat;
    $appUrl    = $this->url;

    $subcategories = SubCategory::whereHas('InterviewQuestions')
                                  ->withCount('InterviewQuestions')
                                  ->where('status',1)
                                  ->where('category_id',$appCat)
                                  ->orderBy('category_order','ASC')
                                  ->get();

    $query    = SubCategory::Query();
    $userId   = session('digital_user_id');

    if (!empty($userId)){
        /* for get IQ Question that liked by user */
        $likedByUser    = getIqLike($userId,1,1);
    }

    if(is_numeric($subcat))
    {
      $ques   = InterviewQuestions::where('iq_ques_ans_id',$subcat)
                                    ->with([
                                      'getSubCategory',
                                      'getComment',
                                      'getComment.getUser'
                                    ])
                                    ->withCount([
                                      'getComment',
                                      'getViews',
                                      'getLikes'
                                    ])
                                    ->first();

      $data = array(
          'ques'            => $ques,
          'subcategories'   => $subcategories,
          'interUrl'        => url($appUrl),
          'likedByUser'     => $likedByUser,
          'cmntLikedByUser' => CommentLikeQuestion::where('comment_user_id',$userId)
                                                    ->pluck('comment_id')
                                                    ->toArray()
      );
      return view('frontend/interview-questions-details')->with($data);
    }else{
      $query = $query->where('cat_slug',$subcat);
    }
    
    $questions         = $query->with('InterviewQuestions')
                                ->with('Category')
                                ->where('category_id',$appCat)
                                ->where('status',1)
                                ->orderBy('category_order','ASC')
                                ->first();

    
    // p($questions);
    $data = array(
        'questions'     => $questions,
        'subcategories' => $subcategories,
        'interUrl'      => url($appUrl),
        'likedByUser'   => $likedByUser
    );
    return view('frontend/interview-questions')->with($data);
  }
}
