<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Contact;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use App\Model\Customer\CustomerItr;
use Validator;
use Auth;
    
class HomeController extends Controller
{

    public function index()
    {
        $title  = "Digital Marketing & SEO Quiz | Interview Questions and technical Terms";
        $data   = array(
            'title' => $title,
        );
        return view('frontend/home')->with($data);
    }

    public function dashboard()
    {
        $data = [];
        $url  = where_to_go();
        if ($url == 'dashboard')
        {
            $user              = Auth::guard('web')->user();
            $data['total_itr'] = CustomerItr::where('customer_id', $user->customer_id)->count();
            return view('frontend.dashboard')->with($data);
        }
        else
        {
            return redirect($url);
        }
    }

    public function thank_you()
    {
        $data = [];
        return view('frontend.thank-you')->with($data);
    }

    public function test()
    {
        $data = [];
        return view('frontend.test')->with($data);
    }

    public function termsConditions()
    {
        $data = [];
        return view('frontend.terms-conditions')->with($data);
    }

    public function privacyPolicy()
    {
        $data = [];
        return view('frontend.privacy-policy')->with($data);
    }

    function customLogin()
    {
        return redirect('/')->withErrors('Login');
    }

    /*
     * * Contact us
     */

    public function contact()
    {
        $data['info'] = get_site_owner_info();
        return view('frontend.contact-us')->with($data);
    }

    public function saveContact(Request $request)
    {
        $arr_valid_fields = [
            'name'  => 'required',
            'email' => 'required|email',
        ];

        $validatior = Validator::make($request->all(), $arr_valid_fields);
        if ($validatior->fails())
        {
            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
            DB::beginTransaction();
            try
            {
                $contact                    = New Contact;
                $contact->name              = Input::get('name');
                $contact->message           = Input::get('message');
                $contact->email             = Input::get('email');
                $contact->contact_number    = Input::get('contact_number');
                $contact->save();
            }
            catch (\Exception $e)
            {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }
        return redirect('contact-us')->withSuccess('Request submitted successfully');
    }

}
    