<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\VideoTutorials\VideoTutorials;
Use App\Model\Category\Category;
use App\Model\SubCategory\SubCategory;
use DB;

class VideosTutorialsController extends Controller
{
   /*
   ** for app type according to url
   */
   public $appCat,$url;

   public function __construct()
   {
      $url  = str_replace(url('/'), '', url()->current());
      $ar = explode('/',ltrim($url, $url[0]));
      $this->appCat = getAppCategory($ar[0]);
      $this->url    = $ar[0].'/'.$ar[1];
   }

   public function index(Request $request,$slug=null)
   {
      $appCat = $this->appCat;
      $appUrl = $this->url;

      $videoConfig = Category::where('iq_category_id',$appCat)
                                    ->pluck('show_category_video')
                                    ->first();
      $videoConfig = explode(',',$videoConfig);
      
      $title  = "Digital Marketing | Videos Tutorials";
      $query  = VideoTutorials::where('status',1)->where('category_id',$appCat);


      if ($slug=='hindi' || $request['language']==2){
         $language   = 2;
         $query->where('language',$language);
         $videoCounts= getVideosCount($appCat,$language);
      }elseif ($slug=='english' || $request['language']==1){
         $language   = 1;
         $query->where('language',$language);
         $videoCounts= getVideosCount($appCat,$language);
      }else{
         if($slug!='' || $request['cat_slug']!='' )
         {

            $video = VideoTutorials::where('status',1)
                                    ->where('video_slug',$slug)
                                    ->first();
            if(!empty($video)){
               $data = array(
                  'title'     => $video->title,
                  'video'     => $video,
                  'videos_url'=> url($appUrl),
                  'relVideos' => VideoTutorials::
                                 where('sub_category_id',$video->sub_category_id)
                                 ->limit(10)
                                 ->orderBy('video_id','ASC')
                                 ->get()
               );
               
               return view('frontend/video-detail')->with($data);
            }else{

               /* videos by category */
               $catVideoQuery = SubCategory::Query();
               $title         = $request['video_title'];
               $catVideo = $catVideoQuery->with(['getHindiVideos'=> function($query) use ($request){
                     if ($request['video_title']!=''){
                        $query->where('title','LIKE','%'.$request['video_title'].'%');
                     }

                     if ($request['sort']!=''){
                        if ($request['sort']==2){
                           $query->orderBy('video_id','ASC');
                        }elseif($request['sort']==3){
                           $query->orderBy('video_views','DESC');
                        }else{
                           $query->orderBy('video_id','DESC');
                        }
                     }

                  }])->where('cat_slug',$slug)
                     ->withCount('getHindiVideos')
                     ->withCount('getEngVideos')
                     ->first();
               
               // p($catVideo);
               if(!empty($catVideo)){
                  $data       = array(
                     'title'             => $title,
                     'videos'            => $catVideo['getHindiVideos'],
                     'language'          => $language,
                     'cat_slug'          => $catVideo->cat_slug,
                     'video_category'    => $catVideo->title,
                     'engVideoCounts'    => $catVideo->get_eng_videos_count,
                     'hindiVideoCounts'  => $catVideo->get_hindi_videos_count,
                     'videos_url'        => url($appUrl),
                  );
                  
                  return view('frontend/videos-tutorials')->with($data);
               }
            }
         }else{
            $language = 'all';
         }
      }

      if (in_array($language!='all' ? $language : 2,$videoConfig))
      {

         $catQuery = SubCategory::Query();
         if ($request['category_title']!=''){
            $catQuery->where('title','LIKE','%'.$request['category_title'].'%');
         }
      
         if ($language==2){
            $catQuery->whereHas('getHindiVideos')
                        ->withCount([
                           'getHindiVideos as video_count' => function ($query) use ($language){
                              $query->where('language',$language);
                           }])
                        ->where('category_id',$appCat);
         }else{
            $catQuery->whereHas('getEngVideos')
                        ->withCount([
                           'getEngVideos as video_count' => function ($query) use ($language){
                             $query->where('language',$language);
                           }])
                        ->where('category_id',$appCat);
         }
         
         $categories = $catQuery->get();
         // p($categories);
         $data       = array(
            'title'             => $title,
            'language'          => $language,
            'engVideoCounts'    => getVideosCount($appCat,1),
            'hindiVideoCounts'  => getVideosCount($appCat,2),
            'videos_url'        => url($appUrl),
            'categories'        => $categories
         );
         return view('frontend/videos-category-tutorials')->with($data);
      }
        // p($categories);
      $videos     = $query->get();
      $data       = array(
         'title'             => $title,
         'videos'            => $videos,
         'language'          => $language,
         // 'AllVideoCounts'    => getVideosCount($appCat,'all'),
         'engVideoCounts'    => getVideosCount($appCat,1),
         'hindiVideoCounts'  => getVideosCount($appCat,2),
         'videos_url'        => url($appUrl),
         'categories'        => $categories,
      );
      return view('frontend/videos-tutorials')->with($data);   
   }
}