<?php

    namespace App\Http\Controllers\frontend\Auth;

    use App\Http\Controllers\Controller;
    use Illuminate\Foundation\Auth\AuthenticatesUsers;
    use Illuminate\Support\Facades\Hash;
    use Illuminate\Http\Request;
    use Auth;
    use Socialite;
    use App\Model\Customer\Customer;

    class LoginController extends Controller
    {
        /*
          |--------------------------------------------------------------------------
          | Login Controller
          |--------------------------------------------------------------------------
          |
          | This controller handles authenticating users for the application and
          | redirecting them to your home screen. The controller uses a trait
          | to conveniently provide its functionality to your applications.
          |
         */

use AuthenticatesUsers;

        /**
         * Where to redirect users after login.
         *
         * @var string
         */
        protected $redirectTo  = '/';
        protected $maxAttempts = 100;

        public function __construct()
        {

//         $this->middleware('guest')->except('logout');
        }

        protected function guard()
        {
            return auth()->guard('web');
        }

        public function email()
        {
            return 'email';
        }

        /**
         * Validate the user login.
         * @param Request $request
         */
        protected function validateLogin(Request $request)
        {
            $this->validate(
                $request, [
                'email'    => 'required|string',
                'password' => 'required|string',
                ], [
                'email.required'    => 'Email is required',
                'password.required' => 'Password is required',
                ]
            );
        }

        /**
         * Get the failed login response instance.
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\RedirectResponse
         */
        protected function sendFailedLoginResponse(Request $request)
        {

            $errors = [$this->email() => trans('auth.failed')];
            return response()->json(['status' => 'error', 'message' => $errors]);
        }

        /**
         * The user has been authenticated.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  mixed  $customer
         * @return mixed
         */
        protected function authenticated(Request $request, $customer)
        {
            if ($customer->email_verification != 1)
            {
                $this->logout($request);
                return response()->json(['status' => 'inactive', 'message' => 'Please verify your email address by clicking on the link to access your account.']);
            }
            elseif ($customer->status != 1)
            {
                $this->logout($request);
                return response()->json(['status' => 'inactive', 'message' => 'Your account is inactive']);
            }
            else
            {
                return response()->json(['status' => 'success', 'message' => 'You have logged in successfully.']);
            }
        }

        public function redirectToGoogle()
        {
            return Socialite::driver('google')->redirect();
        }

        public function handleGoogleCallback()
        {
            try
            {
                $user                         = Socialite::driver('google')->stateless()->user();
                $create['fname']              = $user->getName();
                $create['email']              = $user->getEmail();
                $create['google_id']          = $user->getId();
                $create['email_verification'] = $user->user['verified_email'];
                $customerModel                = new Customer;
                $createdCustomer              = $customerModel->addNew($create);
                if ($createdCustomer->status == 1)
                {
                    $this->guard()->login($createdCustomer);
                    $url = where_to_go('google_login');
                    if (!empty($url))
                    {
                        return redirect($url);
                    }
                    else
                    {
                        return redirect('/');
                    }
                }
                else
                {
                    return redirect('/')->withErrors('account_inactive');
                }
            }
            catch (Exception $e)
            {
                return redirect('auth/google');
            }
        }

        public function logout()
        {
            auth()->guard('web')->logout();
            return redirect()->intended('/');
        }

    }
    