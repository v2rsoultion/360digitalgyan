<?php

    namespace App\Http\Controllers\frontend\Auth;

    use App\Http\Controllers\Controller;
//    use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
//    use Illuminate\Support\Facades\Password;
    use Illuminate\Http\Request;
    use App\Model\Customer\Customer;
    use DB;
    use Hash;
    use App\Mail\VerifyMail;
    use Illuminate\Support\Facades\Mail;

    class ForgotPasswordController extends Controller
    {
        /*
          |--------------------------------------------------------------------------
          | Password Reset Controller
          |--------------------------------------------------------------------------
          |
          | This controller is responsible for handling password reset emails and
          | includes a trait which assists in sending these notifications from
          | your application to your users. Feel free to explore this trait.
          |
         */

//use SendsPasswordResetEmails;

        /**
         * Create a new controller instance.
         *
         * @return void
         */
        public function __construct()
        {
//        $this->middleware('admin.guest');
        }

        /**
         * Display the form to request a password reset link.
         *
         * @return \Illuminate\Http\Response
         */
//    public function showLinkRequestForm()
//    {
//        return view('admin-panel.auth.passwords.email');
//    }

        public function sendPasswordResetLink(Request $request)
        {
            $status   = '';
            $message  = '';
            $email    = $request->get('email');
            $customer = Customer::where('email', $email)->first();
            if ($customer->email)
            {
                // delete if any record of this mail
                DB::table('password_resets')->where('email', $email)->delete();
                // insert new token for reset
                $token             = str_random(64);
                DB::table('password_resets')->insert(
                    ['email'      => $email,
                        'token'      => Hash::make($token),
                        'created_at' => date('Y-m-d H:i:s'),
                    ]
                );
                $customer->subject = trans('language.reset_password');
                $customer->token   = $token;
                $view              = 'email.reset-password-email';
                Mail::to($email)->send(new VerifyMail($customer, $view));
                $status            = 'success';
                $message           = trans('language.reset_password_message');
            }
            else
            {
                $status  = 'invalid_email';
                $message = "Sorry, we can't find a customer with that e-mail address.";
            }
            return response()->json(['status' => $status, 'message' => $message]);
        }

        /**
         * Get the broker to be used during password reset.
         *
         * @return \Illuminate\Contracts\Auth\PasswordBroker
         */
//    public function broker()
//    {
//        return Password::broker('web');
//    }
    }
    