<?php

    namespace App\Http\Controllers\frontend\Auth;

    use App\Http\Controllers\Controller;
    use Illuminate\Foundation\Auth\ResetsPasswords;
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Facades\Password;
    use Illuminate\Http\Request;
    use DB;
    use Hash;
    use App\Model\Customer\Customer;
    use Validator;

    class ResetPasswordController extends Controller
    {
        /*
          |--------------------------------------------------------------------------
          | Password Reset Controller
          |--------------------------------------------------------------------------
          |
          | This controller is responsible for handling password reset requests
          | and uses a simple trait to include this behavior. You're free to
          | explore this trait and override any methods you wish to tweak.
          |
         */

//    use ResetsPasswords;

        /**
         * Where to redirect users after login / registration.
         *
         * @var string
         */
        public $redirectTo = '/';

        /**
         * Create a new controller instance.
         *
         * @return void
         */
        public function __construct()
        {
//        $this->middleware('admin.guest');
        }

        /**
         * Display the password reset view for the given token.
         *
         * If no token is present, display the link request form.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  string|null  $token
         * @return \Illuminate\Http\Response
         */
        public function showResetForm(Request $request, $email = null, $token = null)
        {
            if (!empty($token) && !empty($email))
            {
                $email        = get_decrypted_value($email, true);
                $current_time = time();
                $data         = DB::table('password_resets')->where('email', $email)->first();
                if (Hash::check($token, $data->token))
                {
                    $requested_time = strtotime($data->created_at);
                    $diff           = $current_time - $requested_time;
                    $time_taken     = round($diff/60);
                    $set_time       = \Config::get('custom.reset_password_token_expire');
                    if ($time_taken > $set_time)
                    {
                        return redirect('/')->withErrors('reset_token_expire');
                    }
                    return view('frontend.reset-password')->with(
                            ['token' => $token, 'email' => $email]
                    );
                }
                else
                {
                    return redirect('/')->withErrors('invalid_reset_request');
                }
            }
            else
            {
                return redirect('/')->withErrors('invalid_reset_request');
            }
        }

        public function resetPassword(Request $request)
        {
            $token = trim($request->get('token'));
            $email = $request->get('email');
            $data  = DB::table('password_resets')->where('email', $email)->first();
            if (Hash::check($token, $data->token))
            {
                $arr_valid_fields = [
                    'password'         => 'required',
                    'email'            => 'required',
                    'confirm_password' => 'required|same:password',
                ];

                $validatior = Validator::make($request->all(), $arr_valid_fields);
                if ($validatior->fails())
                {
                    return redirect()->back()->withInput()->withErrors($validatior);
                }
                else
                {
                    DB::beginTransaction();
                    try
                    {
                        $password           = $request->get('password');
//                        $confirm_password = $request->get('confirm_password');
                        $customer           = Customer::where('email', $email)->first();
                        $customer->password = Hash::make($password);
                        $customer->save();
                        // delete link
                        DB::table('password_resets')->where('email', $email)->delete();
                    }
                    catch (\Exception $e)
                    {
                        DB::rollback();
                        $error_message = $e->getMessage();
                        return redirect()->back()->withErrors($error_message);
                    }
                    DB::commit();
                    return redirect('/')->withSuccess('reset_password_success');
                }
            }
            else
            {
                return redirect()->back()->withErrors('Invalid Token');
            }
        }

    }
    