<?php

    namespace App\Http\Controllers\frontend\Auth;

    use App\Model\Customer\Customer;
    use App\Model\Customer\VerifyCustomer;
    use Illuminate\Http\Request;
    use Validator;
    use App\Http\Controllers\Controller;
    use Illuminate\Foundation\Auth\RegistersUsers;
    use Illuminate\Support\Facades\Auth;
    use App\Mail\VerifyMail;
    use Illuminate\Support\Facades\Mail;
    use Illuminate\Support\Facades\Hash;

    class RegisterController extends Controller
    {
        /*
          |--------------------------------------------------------------------------
          | Register Controller
          |--------------------------------------------------------------------------
          |
          | This controller handles the registration of new users as well as their
          | validation and creation. By default this controller uses a trait to
          | provide this functionality without requiring any additional code.
          |
         */

use RegistersUsers;

        /**
         * Where to redirect users after login / registration.
         *
         * @var string
         */
        protected $redirectTo = '/';

        /**
         * Create a new controller instance.
         *
         * @return void
         */

        /**
         * Get a validator for an incoming registration request.
         *
         * @param  array  $data
         * @return \Illuminate\Contracts\Validation\Validator
         */
        protected function validator(array $data)
        {
            return Validator::make($data, [
                    'fname'    => 'required|max:255',
                    'lname'    => 'required|max:255',
                    'email'    => 'required|email|max:255|unique:customer',
                    'password' => 'required|min:6|confirmed',
            ]);
//            if ($validator->fails())
//            {
//                return response()->json(['status' => 'error', 'message' => 'We sent you an activation code. Check your email and click on the link to verify']);
////    return Redirect::route('settings')->withErrors($validator)->withInput();
//            }
//            else
//            {
//                return $validator;
//            }
        }

        protected function guard()
        {
            return Auth::guard('web');
        }

        /**
         * Create a new user instance after a valid registration.
         *
         * @param  array  $data
         * @return Admin
         */
        protected function create(array $data)
        {
            $customer       = Customer::create([
                    'fname'    => $data['fname'],
                    'lname'    => $data['lname'],
                    'email'    => $data['email'],
                    'password' => Hash::make($data['password']),
            ]);
            $verifyCustomer = VerifyCustomer::create([
                    'customer_id' => $customer->customer_id,
                    'token'       => str_random(40)
            ]);
            $view           = 'email.verify-customer';
            Mail::to($customer->email)->send(new VerifyMail($customer, $view));
            return $customer;
        }

        protected function registered(Request $request, $user)
        {
            $this->guard()->logout();
            return response()->json(['status' => 'success', 'message' => trans('language.account_verification_link')]);
        }

        public function checkEmail(Request $request)
        {
            $email          = $request->input('email');
            $customer_email = Customer::where('email', $email)->first();
            if (!empty($customer_email->email))
            {
                return response()->json(false);
            }
            else
            {
                return response()->json(true);
            }
        }

        public function verifyCustomer($token)
        {

            $verifyCustomer = VerifyCustomer::where('token', $token)->first();
            if (isset($verifyCustomer))
            {
                $login_info = Auth::guard('web')->user();
                if ($login_info->customer_id)
                {
                  return redirect('/');  
                }
                else
                {
                    $customer = Customer::find($verifyCustomer->customer_id);
                    if (!$customer->email_verification)
                    {
                        $customer->email_verification = 1;
                        $customer->save();

                        $status = "Your e-mail is <b>verified</b>. You can now";
                    }
                    else
                    {
                        $status = "Your e-mail is <b>already verified</b>. You can now";
                    }
                    return view('frontend.email-verification')->with('success', $status);
                }
//                auto login after verification
//                $this->guard()->login($customer);
            }
            else
            {
                return redirect('/')->withErrors('invalid_verification_token');
            }
        }

    }
    
