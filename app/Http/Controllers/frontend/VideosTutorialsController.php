<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\VideoTutorials\VideoTutorials;
Use App\Model\Category\Category;
use App\Model\SubCategory\SubCategory;

class VideosTutorialsController extends Controller
{
   /*
   ** for app type according to url
   */
   public $appCat,$url;

   public function __construct()
   {
      $url  = str_replace(url('/'), '', url()->current());
      $ar = explode('/',ltrim($url, $url[0]));
      $this->appCat = getAppCategory($ar[0]);
      $this->url    = $ar[0].'/'.$ar[1];
   }

   public function index(Request $request,$slug=null)
   {
      $appCat = $this->appCat;
      $appUrl = $this->url;

      $title  = "Digital Marketing | Videos Tutorials";
      $query  = VideoTutorials::where('status',1)->where('category_id',$appCat);

      $videoConfig = Category::where('iq_category_id',$appCat)
                                    ->pluck('show_category_video')
                                    ->first();
      $videoConfig = explode(',',$videoConfig);

      if ($slug=='hindi' || $slug==''){
         $language = 2;
      }elseif ($slug=='english'){
         $language = 1;
      }else{
         $video = VideoTutorials::where('status',1)
                                    ->where('video_slug',$slug)
                                    ->first();
         if(!empty($video)){
            $data = array(
               'title'     => $video->title,
               'video'     => $video,
               'videos_url'=> url($appUrl),
               'relVideos' => VideoTutorials::
                              where('sub_category_id',$video->sub_category_id)
                              ->limit(10)
                              ->orderBy('video_id','ASC')
                              ->get()
            );
            
            return view('frontend/video-detail')->with($data);
         }
      }

         
      if (in_array($language,$videoConfig))
      {
         $catQuery = SubCategory::Query();
         
         if ($request['category_title']!=''){
            $catQuery->where('title','LIKE','%'.$request['category_title'].'%');
         }

         $catQuery->whereHas('getVideos', function ($query) use ($language) {
                       $query->where(function ($q) use ($language) {
                           $q->where('language',$language);
                       });
                   })
                  ->withCount([
                     'getVideos as videos_count' => function ($query) use ($language)
                     {
                        $query->where('language',$language);
                     }])
                  ->where('category_id',$appCat);

         $categories = $catQuery->get();
         $data       = array(
            'title'             => $title,
            'language'          => $language,
            'engVideoCounts'    => getVideosCount($appCat,1),
            'hindiVideoCounts'  => getVideosCount($appCat,2),
            'videos_url'        => url($appUrl),
            'categories'        => $categories
         );
         return view('frontend/videos-category-tutorials')->with($data);
      }

      if ($request['language']!=''){
         $query->where('language',$request['language']);
         $language = $request['language'];
      }

      if ($request['video_title']!=''){
         $query->where('title','LIKE','%'.$request['video_title'].'%');
      }

      if ($request['categorytype']!=''){
         $catgory = SubCategory::where('cat_slug',$request['categorytype'])->first();
         if (!empty($catgory)){
            $query->where('sub_category_id',$catgory->sub_category_id);
            $cat_slug = $request['categorytype'];
         }
      }

      if ($request['sort']!=''){
         if ($request['sort']==2){
            $query->orderBy('video_id','ASC');
         }else if($request['sort']==3){
            $query->orderBy('video_views','DESC');
         }else{
            $query->orderBy('video_id','DESC');
         }
      }

      if ($language!='') {
         $query->where('language',$language);
      }

      $videos     = $query->get();
      // dd($query->toSql(),$query->getBindings());   
      $data       = array(
         'title'             => $title,
         'videos'            => $videos,
         'cat_slug'          => $cat_slug,
         'language'          => $language,
         'engVideoCounts'    => getVideosCount($appCat,1),
         'hindiVideoCounts'  => getVideosCount($appCat,2),
         'videos_url'        => url($appUrl),
         'categories'        => $categories,
      );
      return view('frontend/videos-tutorials')->with($data);   
   }
}