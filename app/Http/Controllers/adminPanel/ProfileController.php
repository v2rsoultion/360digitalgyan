<?php

namespace App\Http\Controllers\adminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\AdminUser;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use File;
use Image;
use Validator;



class ProfileController extends Controller
{
    public function __construct()
    {
        // Admin login or not
        $this->middleware('admin');
        
    }

    /**
     * Profile Page
     *
     * @return view
    */
    public function index(){
        
        $loginInfo       = get_loggedin_user_data();
        $admin_data     = AdminUser::find($loginInfo['admin_id']);

        $page_title     =  '360 Digital Gyan | Profile';

        // Return With Data Array
        $data = array( 
                        'page_title'         => $page_title,
                        'login_info'    => $loginInfo,
                        'admin_data'    => $admin_data,
                        'edit_url'		=> url('admin-panel/profile/')
                    );

        return view('admin-panel.profile.index')->with($data);
    }

    /**
     * Update Profile
     *
     * @return Edit Profile Input Data
     */
    public function update_profile(Request $request)
    {
        if( !empty($request) ){
            $admin          = get_loggedin_user_data();

            $validatior = Validator::make($request->all(), [
                'admin_name'  => 'required|string|max:255',
                'email'     => 'required|string|email|max:255',
                'phone'     => 'required',
            ]);

            if ($validatior->fails())
            {
                    return redirect()->back()->withInput()->withErrors($validatior);
            }
            else
            {
                DB::beginTransaction();
                try
                {
                    $admin     = AdminUser::find($admin['admin_id']);

                    $admin->admin_name    = Input::get('admin_name');
                    $admin->email       = Input::get('email');
                    $admin->phone       = Input::get('phone');

                    $image              = $request->file('profile_picture');

                    if ( !empty($image) ){
                        $ext                           = substr($image->getClientOriginalName(),-4);
                        $name                          = substr($image->getClientOriginalName(),0,-4);
                        $filename                      = $name.mt_rand(0,100000).time().$ext;
                        $config_admin_upload_path      = \Config::get('custom.admin_pic');
                        $destinationPath               = public_path() . $config_admin_upload_path['upload_path'];
                        $destinationThumbPath          = public_path() . $config_admin_upload_path['upload_thumb_path'];

                        $input['imagename'] = $filename;

                        $img = Image::make($image->getRealPath(),array(
                            'width' => 100,
                            'height' => 100,
                            'grayscale' => false
                        ));
                        
                        if (! File::exists($destinationThumbPath)) {
                            File::makeDirectory($destinationThumbPath,0777,true);
                        }

                        if (! File::exists($destinationPath)) {
                            File::makeDirectory($destinationPath,0777,true);
                        }

                        $img->save($destinationThumbPath.$filename);
                        $image->move($destinationPath, $filename);
                        $admin->admin_profile_img = $filename;
                    }
                    $admin->save();
                } 
                catch (\Exception $e)
                {
                    DB::rollback();
                    $error_message = $e->getMessage();
                    return redirect()->back()->withErrors($error_message);
                }
                DB::commit();
                return redirect('/admin-panel/profile')->with('message', 'Your profile updated successfully');
            }
        }
    }
}
