<?php

namespace App\Http\Controllers\adminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Tips_Tricks\Tips_Tricks;
use DB;
use Validator;
use Illuminate\Support\Facades\Input;
use Yajra\Datatables\Datatables;
use App\Model\SubCategory\SubCategory;
use App\Model\Category\Category;

class TipsTricksController extends Controller
{
    public function index()
    {
    	$data = array(
    		'page_title' 	=> trans('language.view_tricks'),
    		'redirect_url' 	=> url('admin-panel/tips_tricks/view_tricks'),
            'Categories'        => Category::where('iq_category_status',1)->get(),
            'subCategories'     => SubCategory::where('status',1)->get(),
    	);
    	return view('admin-panel.tips_tricks.index')->with($data);
    }
    public function add($id=false)
    {
    	if (!empty($id)) 
    	{
    		$decrypted_trick_id = get_decrypted_value($id, true);
            $tricks              = Tips_Tricks::Find($decrypted_trick_id);
            if (!$tricks)
            {
                return redirect('admin-panel/tips-tricks/add-video')->withError('Tip-Trick not found!');
            }

            $tricks = Tips_Tricks::where('tip_id', "=", $decrypted_trick_id)->first();
            $page_title 	= trans('language.edit_tricks');
            $save_url 		= url('admin-panel/tips-tricks/save/'.$id);
            $submit_button 	= "update";
    	}
    	else
    	{
    		$tricks 		= [];
    		$page_title 	= trans('language.add_tricks');
    		$submit_button 	= "save";
    		$save_url 		= url('admin-panel/tips-tricks/save');
    	}
    	$data 	= array(
    		'page_title' 		=> $page_title,
    		'tricks' 			=> $tricks,
    		'submit_button' 	=> $submit_button,
    		'categories'		=> Category::where('iq_category_status',1)->get(),
            'subCategories'		=> SubCategory::where('status',1)->get(),
    		'save_url'			=> $save_url,
    		'redirect_url' 		=> url('admin-panel/tips-tricks/view-tricks'),
    	);
    	return view('admin-panel.tips_tricks.add')->with($data);
    }
    public function save(Request $request,$id=false)
    {
    	$decrypted_trick_id = get_decrypted_value($id, true);
    	if(!empty($id)) 
    	{
    		$tricks = tips_tricks::find($decrypted_trick_id);
            if (!$tricks)
            {
                return redirect('admin-panel/tips-tricks/view-tricks/')->withError('Trick not found!');
            }
            $success_msg = 'Trick updated successfully!';
            $nameValidator = 'required|unique:tips,tip_title,' . $decrypted_trick_id . ',tip_id';
            
    	}
    	else
    	{
    		$tricks 	= New Tips_Tricks;
    		$success_msg = 'Tips & Tricks saved successfully';
			$nameValidator = 'required|unique:tips,tip_title';
    	}
    	$validator = Validator::make($request->all(), [
            'title'        => $nameValidator,
        ]);
    	if ($validator->fails())
        {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        else
        {
        	DB::beginTransaction();
        	try
        	{
        		$file = $request->file('trick_image');
                if($file != "")
                {
                    if(file_exists('uploads/'.$tricks->tip_image))
                    {
                        unlink('uploads/'.$tricks->tip_image);
                    }

                    $destinationPath = 'uploads/tricks_image/';
                    $imageName = 'tricks_image/'.rand().time().'.'.$file->getClientOriginalExtension();
                    $file->move($destinationPath,$imageName);
                    $tricks->tip_image = $imageName;
                }
        		$tricks->tip_title 	= Input::get('title');
        		$tricks->tip_cat	= Input::get('trick_cat_id');
        		$tricks->tip_subcat	= Input::get('trick_sub_id');
        		$tricks->tip_desc 	= Input::get('trick_des');
        		$tricks->is_new 		= Input::get('is_new');
        		$tricks->save();
        	}
        	catch(\ Exception $e)
        	{
        		DB::rollback();
        		$error_msg = $e->getMessage();
        		return redirect()->back()->withErrors($error_msg);
        	}
        	DB::commit();
        }
        return redirect('admin-panel/tips-tricks/view-tricks')->withSuccess($success_msg);
    }
    public function data(Request $request)
    {
    	$trick 	= [];
    	$trick 	= Tips_Tricks::orderBy('tip_id','DESC')->where(function($query) use ($request){
            if ($request->get('title')!= null){
                $query->where('tip_title','LIKE','%'.$request->get('title').'%');
            }
            if($request->get('category_id')!=null){
                $query->where('tip_cat',$request->get('category_id'));
            }
            if($request->get('sub_category_id')!=null){
                $query->where('tip_subcat',$request->get('sub_category_id'));
            }
        })->get();
    	return Datatables::of($trick)->addColumn('description', function ($trick){
                return "<div style='max-height:100px;overflow:scroll'>".getTextTransform($trick->tip_desc,0)."</div>";
            })->addColumn('new',function($trick){
            	if($trick->is_new==1){
            		$new = "New";
            	}
            	else
            	{
            		$new = "-";
            	}
            	return $new;
            })->addColumn('category',function($trick){
                return $trick['getCategory']->iq_category_name;
            })->addColumn('sub_category',function($trick){
                return $trick['getSubCat']->title;
            })->addColumn('image', function ($trick)
            {
                if ($trick->tip_image!=''){
                    $img  = "<img src='".url('uploads/'.$trick->tip_image)."' height='50' width='50' />";    
                }else{
                    $img  = "<img src='".url('public/admin/nopreview-available.jpg')."' height='50' width='50' />";
                }
                return "<div class='bg-info text-center'>".$img."</div>";
               
            })->addColumn('action',function($trick){
			$encrypted_trick_id = get_encrypted_value($trick->tip_id, true);
			return "
                &nbsp;
                <a href='add-tricks/".$encrypted_trick_id."'><i class='fas fa-pencil-alt'></i></a>
                &nbsp;&nbsp; 
                <a href='tricks-delete/".$encrypted_trick_id."' onclick='return confirm(".'"Are you sure?"'.")'><i class='fas fa-trash'></i></a>";
		})->addColumn('status',function($trick){
                if ($trick->tip_status==1){
                    $status = 0;
                    $radio_cls = 'radio-success';
                }
                else
                {
                    $status = 1;
                    $radio_cls = 'radio-danger';
                }
                return "<div class='radio $radio_cls custom_radiobox'><input type='radio' onClick='changeStatus(".$trick['tip_id'].",".$status.")' name='vradio".$trick['tip_id']."' id='vradio".$trick['tip_id']."' value='option4' checked=''></a>
                    <label for='vradio".$trick['tip_id']."'> </label>
                </div>";
		})->rawColumns(['description'=>'description','sub_category'=>'sub_category','category'=>'category','image'=>'image','action'=>'action','status'=>'status'])->addIndexColumn()->make(true);
    }
    public function status(Request $request)
    {
    	$id = $request['trick_id'];
        $status = $request['trick_status'];
        
        $trick    = tips_tricks::find($id);
        if (!empty($trick))
        {
            $trick->tip_status  = $status;
            $trick->save();
            echo "Success";
        }
        else
        {
            echo "Fail";
        }
    }
    public function destroy($id)
    {
    	$trick_id = get_decrypted_value($id, true);
        $trick    = tips_tricks::find($trick_id);
        if ($trick)
        {
            if (file_exists($trick->tip_image)){
                unlink($trick->tip_image);
            }
            $trick->delete();
            $success_msg = "Tip-Trick deleted successfully!";
            return redirect('admin-panel/tips-tricks/view-tricks')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Tip-Trick not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }
}
