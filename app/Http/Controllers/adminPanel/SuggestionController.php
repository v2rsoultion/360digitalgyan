<?php

namespace App\Http\Controllers\adminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Validator;
use App\Model\Category\Category;
use Yajra\Datatables\Datatables;
use DB;
use App\Model\SeoSuggestion\SeoSuggestion;

class SuggestionController extends Controller
{
    public function index()
    {
    	$data = array(
    		"page_title" => trans('language.view_seo_suggestion'),
    		"redirect_url" => url('admin-panel/seo-suggestion/view-seo-suggestion'),
    	);
    	return view('admin-panel.seo_suggestion.index')->with($data);
    }
    public function data(Request $request)
    {
    	$suggestion = [];
        $suggestion = SeoSuggestion::where(function($query) use ($request){
            if ($request->get('user_name')!= null){
                $query->where('user_name','LIKE','%'.$request->get('user_name').'%');
            }
            if ($request->get('user_email')!=null) {
            	$query->where('email','LIKE','%'.$request->get('user_email').'%');
            }
        })->orderBy('suugestion_id','DESC')->get();
        return Datatables::of($suggestion)->addColumn('action',function($suggestion){
                $encrypted_id = get_encrypted_value($suggestion->suugestion_id, true);
                return "<a href='suggestion-delete/".$encrypted_id."' onclick='return confirm(".'"Are you sure?"'.")'><i class='fas fa-trash'></i></a>";
               
            })->addColumn('message',function($suggestion){
            	return "<div style='max-width:200px;max-height:80px;overflow-y:scroll'>".getTextTransform($suggestion->suggestion_message,0)."</div>";
            })->addColumn('date',function($suggestion){
                return  date("d M,Y", strtotime($suggestion->created_at));
            })->rawColumns(['message'=>'message','action' => 'action','date'=>'date'])->addIndexColumn()->make(true);
    }
    public function destroy($id)
    {
    	$suggestion_id = get_decrypted_value($id, true);
        $suggestion    = SeoSuggestion::find($suggestion_id);
        if ($suggestion)
        {
            
           $suggestion->delete();
           $success_msg = "Seo Suggestion deleted successfully!";
           return redirect('admin-panel/seo-suggestion/view-seo-suggestion')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Seo Suggestion not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }
}
