<?php

namespace App\Http\Controllers\adminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Category\Category;
use App\Model\SubCategory\SubCategory;
use Yajra\Datatables\Datatables;

class SubCategoryController extends Controller
{
    public function  __construct(){
        $this->middleware('subadmin');
    }

    public function index()
    {   
        $data = array(
            'page_title'    => trans('language.view_subcategories'),
            'redirect_url'  => url('admin-panel/sub-/view-sub-'),
            'categories'    => Category::where('iq_category_status',1)->get(),
        );
        return view('admin-panel.sub-category.index')->with($data);
    }

    /**
     *  Add page for SubCategory
     *  @Pankaj on 11th March 2019
    **/
    public function add(Request $request, $id = NULL)
    {
        if (!empty($id))
        {
            $decrypted_category_id = get_decrypted_value($id, true);
            $subcategory      		   = SubCategory::Find($decrypted_category_id);
            if (!$subcategory)
            {
                return redirect('admin-panel/sub-category/add-sub-category')->withError('sub category not found!');
            }

            $subcategory = SubCategory::where('sub_category_id', "=", $decrypted_category_id)->first();
            $page_title             	= trans('language.edit_subcategory');
            $encrypted_category_id      = get_encrypted_value($subcategory->sub_category_id,true);
            $save_url               	= url('admin-panel/sub-category/save/'.$encrypted_category_id);
            $submit_button          	= 'Update';
        }
        else
        {
            $subcategory  = [];
            $page_title    = trans('language.add_subcategory');
            $save_url      = url('admin-panel/sub-category/save/');
            $submit_button = 'Save';
        }

        $data   = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'subcategory' 		=> $subcategory,
            'categories'		=> Category::where('iq_category_status',1)->get(),
            'redirect_url'  	=> url('admin-panel/sub-category/view-sub-categories'),
        );
        return view('admin-panel.sub-category.add')->with($data);
    }
    
    /**
     *  Save category data
     *  @pankaj on 11th March 2019
    **/
    public function save(Request $request, $id = NULL)
    {
        $decrypted_category_id = get_decrypted_value($id, true);
        if (!empty($id))
        {
            $subcategory = SubCategory::find($decrypted_category_id);
            if (!$subcategory)
            {
                return redirect('admin-panel/sub-category/view-sub-categories/')->withError('Sub Category not found!');
            }
            $success_msg = 'Sub Category updated successfully!';
            $nameValidator = 'required|unique:iq_sub_category,title,' . $decrypted_category_id . ',sub_category_id';
        }
        else
        {
            $subcategory     = New SubCategory;
            $success_msg = 'Category saved successfully!';
            $nameValidator = 'required|unique:iq_sub_category';
        }
        
        $validator = Validator::make($request->all(), [
                'title'        => $nameValidator,
        ]);

        if ($validator->fails())
        {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        else
        {
            DB::beginTransaction();
            try
            {
                $file = $request->file('category_icon');
                if($file != ""){
                    if(file_exists('uploads/'.$subcategory->image))
                    {
                        unlink('uploads/'.$subcategory->image);
                    }
                    $destinationPath = 'uploads/sub_category/';
                    $imageName = 'sub_category/'.rand().time().'.'.$file->getClientOriginalExtension();
                    $file->move($destinationPath,$imageName);
                    $subcategory->image = $imageName;
                }
                $subcategory->title          = Input::get('title');
                $subcategory->cat_slug		 = str_slug(Input::get('title'));
                $subcategory->category_id    = Input::get('category_id');
                $subcategory->quiz_time      = Input::get('quiz_time');
                $subcategory->category_order = Input::get('category_order');
                $subcategory->save();
            }
            catch (\Exception $e)
            {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }
        return redirect('admin-panel/sub-category/view-sub-categories')->withSuccess($success_msg);
    }

    /**
     *  Get Data for view page(Datatables)
     *  @pankaj on 11th March 2019
    **/
    public function anyData(Request $request)
    {
        $subcategory     = [];
    	$subcategory = SubCategory::orderBy('sub_category_id','DESC')->where(function($query) use ($request)
    	{
	        if ($request->get('sub_category_name')!= null){
    	        $query->where('title','LIKE','%'.$request->get('sub_category_name').'%');
        	}
            if ($request->get('category_id')!= null){
                $query->where('category_id',$request->get('category_id'));
            }
        })->get();

        return Datatables::of($subcategory)
            
            ->addColumn('action', function ($subcategory)
            {
                $encrypted_sub_category_id = get_encrypted_value($subcategory['sub_category_id'], true);
                return '
                &nbsp;
                <a href="add-sub-category/' . $encrypted_sub_category_id . '" "><i class="fas fa-pencil-alt"></i></a>
                &nbsp;&nbsp; 
                <a href="sub-category-delete/' . $encrypted_sub_category_id . '" onclick="return confirm('."'Are you sure?'".')" "><i class="fas fa-trash"></i></a> ';
               
            })->addColumn('status', function ($subcategory)
            {
                if($subcategory['status'] == 0) {
                    $status = 1;
                    $statusValIq = '<div class="radio radio-danger custom_radiobox">
                    <input type="radio" name="iqradio'.$subcategory['sub_category_id'].'" onClick="changeStatusCategory('.$subcategory['sub_category_id'].','.$status.')" id="iqradio'.$subcategory['sub_category_id'].'" value="option4" checked="">
                    <label for="iqradio'.$subcategory['sub_category_id'].'"> </label>
                </div>';
                }
                else {
                    $status = 0;
                    $statusValIq = '<div class="radio radio-success custom_radiobox">
                        <input type="radio" name="iqradio'.$subcategory['sub_category_id'].'" onClick="changeStatusCategory('.$subcategory['sub_category_id'].','.$status.')" id="iqradio'.$subcategory['sub_category_id'].'" value="option4" checked="">
                        <label for="iqradio'.$subcategory['sub_category_id'].'"> </label>
                    </div>';    
                }
                return $statusValIq;
               
            })->addColumn('icon', function ($subcategory)
            {
                if ($subcategory->image!=''){
                    $img  = "<img src='".url('uploads/'.$subcategory->image)."' height='50' width='50' />";    
                }else{
                    $img  = "<img src='".url('public/admin/nopreview-available.jpg')."' height='50' width='50' />";
                }
                return "<div class='bg-info text-center'>".$img."</div>";

            })->addColumn('category_order', function ($subcategory)
            {
                $order  = "<input class='nums' name='order[]' data-id='".$subcategory->sub_category_id."' onchange='changeCategoryOrder(this)' min='0' type='number' value='".$subcategory->category_order."' type='number' /> ";
                return $order;
            })->rawColumns(['action' => 'action','status'=>'status','icon'=>'icon','category_order'=>'category_order'])->addIndexColumn()->make(true);
    }


    /**
     *  Change Category's status
     *  @pankaj on 11th March 2019
    **/
    public function changeStatus(Request $request)
    {
        $id = $request->get('category_id');
        $status = $request->get('category_status');

        $category    = SubCategory::find($id);
        if ($category)
        {
            $category->status  = $status;
            $category->save();
            echo "Success";
        }
        else
        {
            echo "Fail";
        }
    }

    /**
     *  Destroy category data
     *  @Pankaj on 11th March 2019
    **/
    public function destroy($id)
    {
        $category_id 	= get_decrypted_value($id, true);
        $SubCategory    = SubCategory::find($category_id);
        if ($SubCategory)
        {
            if (file_exists($SubCategory->image)){
                unlink($SubCategory->image);
            }
            $SubCategory->delete();
            $success_msg = "Sub Category deleted successfully!";
            return redirect('admin-panel/sub-category/view-sub-categories')->withSuccess('success', $success_msg);
        }
        else
        {
            $error_message = "Sub Category not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }


    /**
     *  Change Sub Category's Order
     *  @pankaj on 11th March 2019
    **/
    public function changeOrder(Request $request)
    {
        $id     		= $request->get('category_id');
        $order  		= $request->get('category_order');
        $subcategory 	= SubCategory::find($id);
        if($subcategory)
        {
            $subcategory->category_order  = $order;
            $subcategory->save();
            echo "Success";
        }
        else
        {
            echo "Fail";
        }
    }
}
