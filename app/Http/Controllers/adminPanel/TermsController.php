<?php

namespace App\Http\Controllers\adminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Terms\Terms;
use DB;
use Validator;
use Illuminate\Support\Facades\Input;
use Yajra\Datatables\Datatables;

class TermsController extends Controller
{
    public function add($id=false)
    {
    	if (!empty($id)) 
    	{
    		$decrypted_terms_id = get_decrypted_value($id,true);
    		$terms = Terms::Find($decrypted_terms_id);
    		if (!$terms) 
    		{
    			return redirect('admin-panel/terms/add-terms')->withError('Term not found!');
    		}
    		$terms 	= Terms::where('term_id',$decrypted_terms_id)->first();
    		$page_title = trans('language.edit_terms');
    		$save_url 	= url('admin-panel/terms/save/'.$id);
    		$submit_button = 'update'; 
    	}	
    	else
    	{
    		$terms 		= [];
    		$page_title = trans('language.add_terms');
    		$submit_button 	= 'save';
    		$save_url 		= url('admin-panel/terms/save'); 
    	}
    	$data 	= array(
    		'page_title' 	=> $page_title,
    		'terms' 		=> $terms,
    		'submit_button' => $submit_button,
    		'save_url'		=> $save_url,
    		'redirect_url'	=> url('admin-panel/terms/view-terms'),  
    	);
    	return view('admin-panel.terms.add')->with($data);
    }
    public function index()
    {
    	$data = array(
    		'page_title'	=> trans('language.view_terms'),
    		'redirect_url' 	=> url('admin-panel/terms/view-terms'),
    	);
    	return view('admin-panel.terms.index')->with($data);
    }
    public function data(Request $request)
    {
    	$term 	= [];
    	$term 	= Terms::orderBy('term_id','DESC')->where(function($query) use ($request){
            if ($request->get('title')!= null){
                $query->where('term_title','LIKE','%'.$request->get('title').'%');
            }
        })->get();
    	return Datatables::of($term)->addColumn('description',function($term){
    			return "<div style='max-height:100px;overflow:scroll'>".getTextTransform($term->term_description,0)."</div>";
    	})->addColumn('new',function($term){
    		if($term->is_new==1)
    		{
    			$new = "New";
    		}
    		else
    		{
    			$new = "---";
    		}
    		return $new;
    	})->addColumn('image',function($term){
    		if ($term->term_image!=''){
                    $img  = "<img src='".url('uploads/'.$term->term_image)."' height='50' width='50' />";    
                }else{
                    $img  = "<img src='".url('public/admin/nopreview-available.jpg')."' height='50' width='50' />";
                }
                return "<div class='bg-info text-center'>".$img."</div>";
    	})->addColumn('action',function($term){
    		$encrypted_term_id = get_encrypted_value($term->term_id, true);
			return "
                &nbsp;
                <a href='add-terms/".$encrypted_term_id."'><i class='fas fa-pencil-alt'></i></a>
                &nbsp;&nbsp; 
                <a href='terms-delete/".$encrypted_term_id."' onclick='return confirm(".'"Are you sure?"'.")'><i class='fas fa-trash'></i></a>";
    	})->addColumn('status',function($term){
    		if ($term->term_status==1){
                    $status = 0;
                    $radio_cls = 'radio-success';
            }
            else
            {
                $status = 1;
                $radio_cls = 'radio-danger';
            }
            return "<div class='radio $radio_cls custom_radiobox'><input type='radio' onClick='changeStatus(".$term['term_id'].",".$status.")' name='vradio".$term['term_id']."' id='vradio".$term['term_id']."' value='option4' checked=''></a>
                <label for='vradio".$term['term_id']."'> </label>
            </div>";
    	})->rawColumns(['description'=>'description','new'=>'new','image'=>'image','action'=>'action','status'=>'status'])->addIndexColumn()->make(true);
    }
    public function save(Request $request,$id=false)
    {
    	$decrypted_terms_id = get_decrypted_value($id,true);
    	if (!empty($id)) 
    	{
    		$terms = Terms::Find($decrypted_terms_id);
    		if (!$terms) 
    		{
    			return redirect('admin-panel/terms/view-terms/')->withError('Term not found!');
    		}
    		$success_msg = 'Term updated successfully!';
            $nameValidator = 'required|unique:terms,term_title,' . $decrypted_terms_id . ',term_id';
    	}
    	else
    	{
    		$terms 	= New Terms;
    		$success_msg = 'Terms saved successfully';
			$nameValidator = 'required|unique:terms,term_title';
    	}
    	$validator = Validator::make($request->all(), [
            'title'        => $nameValidator,
        ]);
        if ($validator->fails())
        {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        else
        {
        	DB::beginTransaction();
        	try
        	{
        		$file = $request->file('term_image');
                if($file != "")
                {
                    if(file_exists('uploads/'.$terms->term_image))
                    {
                        unlink('uploads/'.$terms->term_image);
                    }
                    $destinationPath = 'uploads/terms_image/';
                    $imageName = 'terms_image/'.rand().time().'.'.$file->getClientOriginalExtension();
                    $file->move($destinationPath,$imageName);
                    $terms->term_image = $imageName;
                }
        		$terms->term_title 	      = Input::get('title');
        		$terms->term_description  = Input::get('term_des');
        		$terms->is_new 		      = Input::get('is_new');
        		$terms->save();
        	}
        	catch(\ Exception $e)
        	{
        		DB::rollback();
        		$error_msg = $e->getMessage();
        		return redirect()->back()->withErrors($error_msg);
        	}
        	DB::commit();
        }
        return redirect('admin-panel/terms/view-terms')->withSuccess($success_msg);
    }
    public function destroy($id)
    {
    	$term_id = get_decrypted_value($id,true);
    	$term = Terms::Find($term_id);
    	if ($term) 
    	{
    	 	if(file_exists($term->term_image))
    	 	{
                unlink($term->term_image);
            }
            $term->delete();
            $success_msg = "Term deleted successfully!";
            return redirect('admin-panel/terms/view-terms')->withSuccess($success_msg);
    	}
    	else
    	{
    		$error_message = "Term not found!";
            return redirect()->back()->withErrors($error_message);
    	} 
    }
    public function status(Request $request)
    {
    	$id = $request['term_id'];
        $status = $request['term_status'];
        
        $term    = Terms::find($id);
        if (!empty($term))
        {
            $term->term_status  = $status;
            $term->save();
            echo "Success";
        }
        else
        {
            echo "Fail";
        }
    }
}
