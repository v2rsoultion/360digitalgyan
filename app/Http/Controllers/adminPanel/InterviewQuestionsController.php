<?php

namespace App\Http\Controllers\adminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\InterviewQuestions\InterviewQuestions;
use App\Model\Category\Category;
use App\Model\SubCategory\SubCategory;
use Yajra\Datatables\Datatables;
use App\Model\Notification\Notification;
use App\Traits\PushNotification;

class InterviewQuestionsController extends Controller 
{
    use PushNotification;

    public function  __construct(){
        $this->middleware('subadmin');
    }

    public function index()
    {   

        // $p=0;
        // $value = str_split('153');
        // $tot=count($value);
        // foreach ($value as $v) {
        //     $p+=$v**$tot;
        // }
        // p($p);

        $data = array(
            'page_title'    => trans('language.view_interview_questions'),
            'redirect_url'  => url('admin-panel/interview-questions/view-interview-questions'),
            'Categories'		=> Category::where('iq_category_status',1)->get(),
            'subCategories'		=> SubCategory::where('status',1)->get(),
        );
        return view('admin-panel.interview_questions.index')->with($data);
    }

    /*
    **  Add page for interview questions
    **  @Pankaj on 19th March 2019
    */
    public function add(Request $request, $id = NULL)
    {
        if (!empty($id))
        {
            $decrypted_iq_id 	= get_decrypted_value($id, true);
            $interviewQues      = InterviewQuestions::Find($decrypted_iq_id);
            if(!$interviewQues)
            {
            	return redirect('admin-panel/interview-questions/add-interview-questions')->withError('Interview Question not found!');
            }

            $interviewQues 		= InterviewQuestions::where('iq_ques_ans_id',$decrypted_iq_id)->first();
            $page_title   		= trans('language.edit_interview_questions');
            $encrypted_iq_id 	= get_encrypted_value($interviewQues->iq_ques_ans_id,true);
            $save_url          	= url('admin-panel/interview-questions/save/'.$encrypted_iq_id);
            $submit_button     	= 'Update';
        }
        else
        {
            $interviewQues  = [];
            $page_title    = trans('language.add_interview_questions');
            $save_url      = url('admin-panel/interview-questions/save/');
            $submit_button = 'Save';
        }

        $data   = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'interviewQues' 	=> $interviewQues,
            'categories'		=> Category::where('iq_category_status',1)->get(),
            'subCategories'		=> SubCategory::where('status',1)->get(),
            'redirect_url'  	=> url('admin-panel/interview-questions/view-interview-questions'),
        );
        return view('admin-panel.interview_questions.add')->with($data);
    }
    
    /**
     *  Save Interview Question data
     *  @pankaj on 19th March 2019
    **/
    public function save(Request $request, $id = NULL)
    {
        $decrypted_iq_id = get_decrypted_value($id, true);
        if (!empty($id))
        {
            $interviewQues = InterviewQuestions::find($decrypted_iq_id);
            if (!$interviewQues)
            {
                return redirect('admin-panel/interview-questions/view-interview-questions/')->withError('Interview Question not found!');
            }
            $success_msg = 'Interview Question updated successfully!';
            $nameValidator = 'required';
        }
        else
        {
            $interviewQues     = New InterviewQuestions;
            $success_msg = 'Interview Question  saved successfully!';
            $nameValidator = 'required';
        }
        
        $validator = Validator::make($request->all(), [
        	'iq_ques_ans_q'  => $nameValidator,
        ]);

        if ($validator->fails())
        {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        else
        {
            DB::beginTransaction();
            try
            {
                $interviewQues->question_type      	= Input::get('question_type');
                $interviewQues->iq_ques_ans_catid   = Input::get('category_id');
                $interviewQues->sub_cat_id   		= Input::get('sub_category_id');
				$interviewQues->iq_ques_ans_q		= Input::get('iq_ques_ans_q');
                $interviewQues->question_coding		= Input::get('question_coding');
                $interviewQues->iq_ques_ans_a 		= Input::get('iq_ques_ans_a');
                $interviewQues->coding 				= Input::get('coding');
               	$interviewQues->iq_ques_ans_metatitle= Input::get('iq_ques_ans_metatitle');
               	$interviewQues->iq_ques_ans_metakey = Input::get('iq_ques_ans_metakey');
               	$interviewQues->iq_ques_ans_metadesc= Input::get('iq_ques_ans_metadesc');
				$interviewQues->is_new				= Input::get('is_new') ? Input::get('is_new') : 0 ;
                $interviewQues->save();


                /* only in insert case */
                if(empty($id))
                {
                    $quesType     = Input::get('question_type');
                    $notification = new Notification;
                    $notification->notification_title   = strip_tags(Input::get('iq_ques_ans_q'));
                    $notification->sub_category_id      = Input::get('sub_category_id');
                    $notification->notification_for     = $quesType==1 ? 2 : 4 ; //2=IQ,4=Technical
                    $notification->notification_flag    = Input::get('category_id');
                    $notification->save();
                    $notifyId     = $notification->notification_id;
                    
                    $notifyCat = Input::get('category_id');
                    /*
                    ** Push notification for all apps in Android
                    ** by pankaj : 29th august 2019
                    */
                    $notificationMsgArray = array(
                        "msg"               => "New interview question available : ".strip_tags(Input::get('iq_ques_ans_q')),
                        "image"             => $imageName!=''? $imageName : "",
                        "notification_id"   => $notifyId,
                        "notification_for"  => 2
                    );

                    $notifyMsg = json_encode($notificationMsgArray);
                    $this->SendAndroidNotification($notifyCat,$notifyMsg);

                    /*
                    ** Push notification for all apps in IOS
                    ** by pankaj : 29th august 2019
                    */

                    $notifyTitle = "New interview question available";
                    $notifyDesc  = strip_tags(Input::get('iq_ques_ans_q'));
                    $this->SendIphoneNotification($notifyCat,$notifyMsg,$notifyTitle,$notifyDesc);
                }
            }
            catch (\Exception $e)
            {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }
        return redirect('admin-panel/interview-questions/view-interview-questions')->withSuccess($success_msg);
    }

    /**
     *  Get Data for view page(Datatables)
     *  @pankaj on 18th March 2019
    **/
    public function anyData(Request $request)
    {

        $interviewQues = [];
        $interviewQues = InterviewQuestions::orderBy('iq_ques_ans_id','DESC')->where(function($query) use ($request){
            if ($request->get('category_id')!= null){
                $query->where('iq_ques_ans_catid',$request->get('category_id'));
            }

            if ($request->get('sub_category_id')!= null){
                $query->where('sub_cat_id',$request->get('sub_category_id'));
            }

            if ($request->get('question_type')!= null){
                $query->where('question_type',$request->get('question_type'));
            }
        })->with('getCategory')->with('getSubCategory')->get();
        

        return Datatables::of($interviewQues)
            ->addColumn('action', function ($interviewQues){
                 $encrypted_iq_id = get_encrypted_value($interviewQues['iq_ques_ans_id'], true);
                return '&nbsp;
                <a href="add-interview-questions/' . $encrypted_iq_id . '" "><i class="fas fa-pencil-alt"></i></a>
                &nbsp;&nbsp; 
                <a href="interview-questions-delete/' . $encrypted_iq_id . '" onclick="return confirm('."'Are you sure?'".')" "><i class="fas fa-trash"></i></a> ';
               
            })->addColumn('status', function ($interviewQues){
                if($interviewQues['iq_ques_ans_status'] == 0) {
                    $status = 1;
                    $statusValIq = '<div class="radio radio-danger custom_radiobox">
                    <input type="radio" name="iqradio'.$interviewQues['iq_ques_ans_id'].'" onClick="changeStatusInterview('.$interviewQues['iq_ques_ans_id'].','.$status.')" id="iqradio'.$interviewQues['iq_ques_ans_id'].'" value="option4" checked="">
                    <label for="iqradio'.$interviewQues['iq_ques_ans_id'].'"> </label>
                </div>';
                }
                else {
                    $status = 0;
                    $statusValIq = '<div class="radio radio-success custom_radiobox">
                        <input type="radio" name="iqradio'.$interviewQues['iq_ques_ans_id'].'" onClick="changeStatusInterview('.$interviewQues['iq_ques_ans_id'].','.$status.')" id="iqradio'.$interviewQues['iq_ques_ans_id'].'" value="option4" checked="">
                        <label for="iqradio'.$interviewQues['iq_ques_ans_id'].'"> </label>
                    </div>';    
                }
                return $statusValIq;
               
            })->addColumn('new', function ($interviewQues){
            	return $interviewQues->is_new==1 ? "New" : "Old";
            })->addColumn('order', function ($interviewQues)
            {
                $order  = "<input class='nums' name='order[]' data-id='".$interviewQues->iq_ques_ans_id."' onchange='changeOrder(this)' min='0' type='number' value='".$interviewQues->iq_ques_ans_order."' type='number' /> ";
                return $order;
            })->addColumn('category',function($interviewQues){
            	return $interviewQues['getCategory']->iq_category_name;
            })->addColumn('subcategory',function($interviewQues){
            	return $interviewQues['getSubCategory']->title;
            })->addColumn('question',function($interviewQues){
            	return getTextTransform($interviewQues->iq_ques_ans_q,0);
            })->addColumn('answer',function($interviewQues){
            	return "<div style='max-height:100px;overflow:scroll'>".getTextTransform($interviewQues->iq_ques_ans_a,0)."</div>";
            })->rawColumns(['action' => 'action','status'=>'status','question'=>'question','order'=>'order','answer'=>'answer'])->addIndexColumn()->make(true);
    }

    public function detailData($id)
    {
        $detail = InterviewQuestions::where('iq_ques_ans_id',$id)->first();
        return view('admin-panel.interview_questions.detail',compact('detail'));
    }


    /**
     *  Change InterviewQuestions status
     *  @pankaj on 19th March 2019
    **/
    public function changeStatus(Request $request)
    {
        $id 			= $request->get('interview_id');
        $status 		= $request->get('interview_status');        
        $interviewQues  = InterviewQuestions::find($id);
        if($interviewQues)
        {
            $interviewQues->iq_ques_ans_status  = $status;           
            $interviewQues->save();
            echo "Success";
        }else{
            echo "Fail";
        }
    }

    /**
     *  Destroy interview data
     *  @Pankaj on 19th March 2019
    **/
    public function destroy($id)
    {
        $interviewQues_id = get_decrypted_value($id, true);
        $interviewQues    = InterviewQuestions::find($interviewQues_id);
        if ($interviewQues)
        {
            $interviewQues->delete();
            $success_msg = "Interview Question deleted successfully!";
            return redirect('admin-panel/interview-questions/view-interview-questions')->withSuccess($success_msg);
        }else{

            $error_message = "Interview Question not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    public function getsubcats($id)
    {
    	$options = "<option value=''>Select Sub Category</option>";
    	$subCategories = SubCategory::where('category_id',$id)->get();

    	foreach ($subCategories as $cat) {
    		$options.= "<option value='".$cat->sub_category_id."'> $cat->title </option>";
    	}

    	return response()->json(['data'=>$options]);
    }


    /**
     *  Change inter's Order
     *  @pankaj on 8th March 2019
    **/
    public function changeOrder(Request $request)
    {
        $id     = $request->get('interview_id');
        $order  = $request->get('interview_order');
        $interviewQues   = InterviewQuestions::find($id);
        if($interviewQues)
        {
            $interviewQues->iq_ques_ans_order  = $order;
            $interviewQues->save();
            echo "Success";
        }
        else
        {
            echo "Fail";
        }
    }
}