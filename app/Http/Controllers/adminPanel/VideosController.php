<?php

namespace App\Http\Controllers\adminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Category\Category;
use App\Model\Videos\Video;
use Illuminate\Support\Facades\Input;
use Validator;
use App\Model\SubCategory\SubCategory;
use Yajra\Datatables\Datatables;
use DB;
use App\Traits\PushNotification;

class VideosController extends Controller
{
    use PushNotification;
    public function index()
    {
    	$data=array(
    		'page_title'	=> trans('language.view_videos'),
            'redirect_url'  => url('admin-panel/videos/view-videos'),
            'Categories'        => Category::where('iq_category_status',1)->get(),
            'subCategories'     => SubCategory::where('status',1)->get(), 
    	);
    	return view('admin-panel.videos.index')->with($data);
    }
    public function add(Request $request,$id=false)
    {

        if (!empty($id))
        {
            $decrypted_video_id = get_decrypted_value($id, true);
            $video              = Video::find($decrypted_video_id);
            if (!$video)
            {
                return redirect('admin-panel/videos/add-video')->withError('video not found!');
            }

            $video = video::where('video_id', "=", $decrypted_video_id)->first();
            $page_title                 = trans('language.edit_video');
            $encrypted_video_id      = get_encrypted_value($video->video_id,true);
            $save_url                   = url('admin-panel/videos/save/'.$encrypted_video_id);
            $submit_button              = 'Update';
        }
        else
        {
            $video      = [];
            $page_title    = trans('language.add_video');
        	$submit_button	="save";
            $save_url       = url('admin-panel/videos/save/');
        }
        	$data=array(
        		'page_title'	=> $page_title,
        		'submit_button' 	=> $submit_button,
                'categories'		=> Category::where('iq_category_status',1)->get(),
                'subCategories'		=> SubCategory::where('status',1)->get(),
                'video'     => $video,
                'save_url'      => $save_url,
                'redirect_url'      => url('admin-panel/videos/view-videos'),
        	);
    	return view('admin-panel.videos.add')->with($data);
    }
    public function save(Request $request,$id = false)
    {
        $decrypted_video_id = get_decrypted_value($id, true);
        if(!empty($id))
        {
            $videos = video::find($decrypted_video_id);
            if (!$videos)
            {
                return redirect('admin-panel/videos/view-videos/')->withError('Video not found!');
            }
            $success_msg = 'Video updated successfully!';
            $nameValidator = 'required|unique:course_video,title,' . $decrypted_video_id . ',video_id';
        }
        else
        {
            $videos     = New Video;
            $success_msg = 'video saved successfully!';
            $nameValidator = 'required|unique:course_video,title';
        }
        $validator = Validator::make($request->all(), [
            'video_title'        => $nameValidator,
        ]);
        if ($validator->fails())
        {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        else
        {
            DB::beginTransaction();
            try
            {
                $file = $request->file('video_image');
                if($file != "")
                {
                    if(file_exists('uploads/'.$videos->image))
                    {
                        unlink('uploads/'.$videos->image);
                    }
                    $destinationPath = 'uploads/video_image/';
                    $imageName = 'video_image/'.rand().time().'.'.$file->getClientOriginalExtension();
                    $file->move($destinationPath,$imageName);
                    $videos->image = $imageName;
                }
                $videos->category_id        = Input::get('videos_cat_id'); 
                $videos->sub_category_id    = Input::get('videos_sub_id');
                $videos->title              = Input::get('video_title');
                $videos->video_slug         = str_slug(Input::get('video_title'));
                $videos->description        = Input::get('video_des');
                $videos->link               = Input::get('video_link');
                $videos->duration           = Input::get('video_dur');
                $videos->language           = Input::get('videos_lang');
                $videos->save();

                /* only in insert case */
                if(empty($id))
                {
                    $notification = new Notification;
                    $notification->notification_title   = strip_tags(Input::get('video_title'));
                    $notification->sub_category_id      = Input::get('videos_sub_id');
                    $notification->notification_for     = 1; //1=video
                    $notification->notification_flag    = Input::get('videos_cat_id');
                    $notification->save();
                    $notifyId     = $notification->notification_id;
                    
                    $notifyCat = Input::get('videos_cat_id');
                    /*
                    ** Push notification for all apps in Android
                    ** by pankaj : 29th august 2019
                    */
                    $notificationMsgArray = array(
                        "msg"               => "New video available : ".strip_tags(Input::get('video_title')),
                        "image"             => $imageName!=''? $imageName : "",
                        "notification_id"   => $notifyId,
                        "notification_for"  => 2
                    );

                    $notifyMsg = json_encode($notificationMsgArray);
                    $this->SendAndroidNotification($notifyCat,$notifyMsg);

                    /*
                    ** Push notification for all apps in IOS
                    ** by pankaj : 29th august 2019
                    */

                    $notifyTitle = "New video available";
                    $notifyDesc  = strip_tags(Input::get('video_title'));
                    $this->SendIphoneNotification($notifyCat,$notifyMsg,$notifyTitle,$notifyDesc);
                }

            }
            catch (\Exception $e)
            {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }

        return redirect('admin-panel/videos/view-videos')->withSuccess($success_msg);
    }
    public function data(Request $request)
    {
        $video = [];
        $video = video::orderBy('video_id','DESC')->where(function($query) use ($request){
            if ($request->get('category_id')!= null){
                $query->where('category_id',$request->get('category_id'));
            }
            if ($request->get('sub_category_id')!= null){
                $query->where('sub_category_id',$request->get('sub_category_id'));
            }
            if ($request->get('title')!= null){
                $query->where('title','LIKE','%'.$request->get('title').'%');
            }
        })->get();
        return Datatables::of($video)
            ->addColumn('description', function ($video){
                return "<div style='max-height:100px;max-width:200px;overflow-y:scroll;text-align:justify'>".getTextTransform($video->description,0)."</div>";

            })->addColumn('action',function($video){
                $encrypted_video_id = get_encrypted_value($video->video_id, true);
                return "
                &nbsp;
                <a href='add-videos/".$encrypted_video_id."'><i class='fas fa-pencil-alt'></i></a>
                &nbsp;&nbsp; 
                <a href='video-delete/".$encrypted_video_id."' onclick='return confirm(".'"Are you sure?"'.")'><i class='fas fa-trash'></i></a>";
               
            })->addColumn('status',function($video){
                $encrypted_video_id = get_encrypted_value($video->video_id, true);
                if ($video->status==1){
                    $status = 0;
                    $radio_cls = 'radio-success';
                }
                else
                {
                    $status = 1;
                    $radio_cls = 'radio-danger';
                }
                return "<div class='radio $radio_cls custom_radiobox'><input type='radio' onClick='changeStatus(".$video['video_id'].",".$status.")' name='vradio".$video['video_id']."' id='vradio".$video['video_id']."' value='option4' checked=''></a>
                    <label for='vradio".$video['video_id']."'> </label>
                </div>";
            })->addColumn('image', function ($video)
            {
                if ($video->image!=''){
                    $img  = "<img src='".url('uploads/'.$video->image)."' height='50' width='50' />";    
                }else{
                    $img  = "<img src='".url('public/admin/nopreview-available.jpg')."' height='50' width='50' />";
                }
                return "<div class='bg-info text-center'>".$img."</div>";
               
            })->addColumn('category',function($video){
                return $video['getCategory']->iq_category_name;
            })->addColumn('SubCategory',function($video){
                return $video['getSubCat']->title;
            })->rawColumns(['description'=>'description','image'=>'image','action' => 'action','status' => 'status','category'=>'category','SubCategory'=>'SubCategory'])->addIndexColumn()->make(true);
    }


    public function destroy($id)
    {
        $video_id = get_decrypted_value($id, true);
        $video    = video::find($video_id);
        if ($video)
        {
            if (file_exists('uploads/'.$video->image)){
                unlink('uploads/'.$video->image);
            }
            $video->delete();
            $success_msg = "video deleted successfully!";
            return redirect('admin-panel/videos/view-videos')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "video not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    
    public function changeStatus(Request $request)
    {
        $id = $request['video_id'];
        $status = $request['video_status'];
        
        $video    = video::find($id);
        if (!empty($video))
        {
            $video->status  = $status;
            $video->save();
            echo "Success";
        }
        else
        {
            echo "Fail";
        }
    }
}
