<?php

namespace App\Http\Controllers\adminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Tags\Tags;
use DB;
use Validator;
use Illuminate\Support\Facades\Input;
use Yajra\Datatables\Datatables;

class TagsController extends Controller
{
    public function index()
	   {
	   	 $data = array(
	   	 	'page_title'	=> trans('language.view_tags'),
	   	 	'redirect_url'	=> url('admin-panel/tags/view_tags'),
	   	 );
	   	 return view('admin-panel.tags.index')->with($data);
	   }
	public function add(Request $request,$id=false)
	{
		if (!empty($id)) 
		{	
			$decrypted_tag_id = get_decrypted_value($id, true);
            $tag 		      = Tags::Find($decrypted_tag_id);
            
            if (!$tag)
            {
                return redirect('admin-panel/tags/add-tag')->withError('tag not found!');
            }
            $tag = Tags::where('tags_id', "=", $decrypted_tag_id)->first();
			$page_title 	= trans('language.edit_tag');
			$encrypted_tag_id      = get_encrypted_value($tag->tags_id,true);
            $save_url       = url('admin-panel/tags/save/'.$encrypted_tag_id);
			$submit_button 	= 'update';
		}	
		else
		{
			$tag 	= [];
			$page_title	= trans('language.add_tag');
			$submit_button	= 'save';
			$save_url	= url('admin-panel/tags/save/');
		}

		$data 	= array(
			'page_title'	=> $page_title,
			'tag'			=> $tag,
			'submit_button'	=> $submit_button,
			'save_url'		=> $save_url,
			'rediect_url'	=> url('admin-panel/tags/view_tags'),
		);
		return view('admin-panel.tags.add')->with($data);
	}
	public function save(Request $request,$id=false)
	{
		$decrypted_tag_id = get_decrypted_value($id, true);
		if (!empty($id)) 
		{
			$tag 	= Tags::find($decrypted_tag_id);
			if (!$tag) 
			{
				return redirect('admin-panel/tags/view_tags')->withErrors('Tag not found!');
			}
			$success_msg = 'Tag updated successfully!';
            $nameValidator = 'required|unique:wscube_tags,tags,'.$decrypted_tag_id.',tags_id';
		}
		else
		{
			$tag = New Tags;
			$success_msg = 'Tag saved successfully';
			$nameValidator = 'required|unique:wscube_tags,tags';
		}
		$validator = Validator::make($request->all(), [
            'tag_name'        => $nameValidator,
        ]);
        if ($validator->fails())
        {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        else
        {
        	DB::beginTransaction();
        	try
        	{
        		$tag->tags = Input::get('tag_name');
        		$tag->save();
        	}
        	catch (\ Exception $e)
        	{
        		DB::rollback();
        		$error_msg	= $e->getMessage();
        		return redirect()->back()->withErrors($error_msg);
        	}
        	DB::commit();
        }
        return redirect('admin-panel/tags/view-tags')->withSuccess($success_msg);
	}
	public function data(Request $request)
	{
		$tag = [];
		$tag = Tags::orderBy('tags_id','DESC')->where(function($query) use ($request){
            if ($request->get('title')!= null)
            {
                $query->where('tags','LIKE','%'.$request->get('title').'%');
            }
            })->get();
		return Datatables::of($tag)->addColumn('action',function($tag){
			$encrypted_tag_id = get_encrypted_value($tag->tags_id, true);
			return "
                &nbsp;
                <a href='add-tag/".$encrypted_tag_id."'><i class='fas fa-pencil-alt'></i></a>
                &nbsp;&nbsp; 
                <a href='tag-delete/".$encrypted_tag_id."' onclick='return confirm(".'"Are you sure?"'.")'><i class='fas fa-trash'></i></a>";
		})->rawColumns(['action'=>'action'])->addIndexColumn()->make(true);
	}
    public function destroy($id)
    {
    	$tag_id 	= get_decrypted_value($id,true);
    	$tag 		= Tags::find($tag_id);
    	if($tag)
    	{
    		$tag->delete();
    		$success_msg = "Tag deleted successfully!";
            return redirect('admin-panel/tags/view-tags')->withSuccess($success_msg);
    	}
    	else
    	{
    		$error_msg = "Tag not found";
    		return redirect()->back()->withErrors($error_msg);
    	}
    }
}
