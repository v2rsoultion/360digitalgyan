<?php

namespace App\Http\Controllers\adminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\MarketingBlogs\MarketingBlogs;
use Yajra\Datatables\Datatables;
use App\Model\Notification\Notification;
use App\Traits\PushNotification;

class MarketingBlogController extends Controller
{
    use PushNotification;
    public function  __construct(){
        $this->middleware('subadmin');
    }


    public function index()
    {
        // $notificationMsgArray = array(
        //     "msg"   => "New blog added : Test Blog",
        //     "image" => 'hello_image',
        //     "notification_id"   => 0,
        //     "notification_for"  => 11
        // );

        // $notifyMsg = json_encode($notificationMsgArray);

        // AndroidPushNotification($notifyMsg,'/topics/360DigitalGyan-Dev');
        // die;

        $data = array(
            'page_title'    => trans('language.view_marketing_blog'),
            'redirect_url'  => url('admin-panel/marketing-blog/view-marketing-blog'),
        );
        return view('admin-panel.marketing_blogs.index')->with($data);
    }

    /**
     *  Add page for Marketing Blog
     *  @Pankaj on 18th March 2019
    **/
    public function add(Request $request, $id = NULL)
    {
        if (!empty($id))
        {
            $decrypted_mb_id 	= get_decrypted_value($id, true);
            $markeingBlog      	= MarketingBlogs::Find($decrypted_mb_id);
            if (!$markeingBlog)
            {
                return redirect('admin-panel/marketing-blog/add-marketing-blog')->withError('marketing blog not found!');
            }

            $markeingBlog 		= MarketingBlogs::where('marketing_id',$decrypted_mb_id)->first();
            $page_title   		= trans('language.edit_marketing_blog');
            $encrypted_news_id 	= get_encrypted_value($markeingBlog->marketing_id,true);
            $save_url          	= url('admin-panel/marketing-blog/save/'.$encrypted_news_id);
            $submit_button     	= 'Update';
        }
        else
        {
            $markeingBlog  = [];
            $page_title    = trans('language.add_marketing_blog');
            $save_url      = url('admin-panel/marketing-blog/save/');
            $submit_button = 'Save';
        }

        $data   = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'blog' 		    	=> $markeingBlog,
            'redirect_url'  	=> url('admin-panel/marketing-blog/view-marketing-blog'),
        );
        return view('admin-panel.marketing_blogs.add')->with($data);
    }
    
    /**
     *  Save marketing blog data
     *  @pankaj on 18th March 2019
    **/
    public function save(Request $request, $id = NULL)
    {
        $decrypted_blog_id = get_decrypted_value($id, true);
        if (!empty($id))
        {
            $blog = MarketingBlogs::find($decrypted_blog_id);
            if (!$blog)
            {
                return redirect('admin-panel/marketing-blog/view-marketing-blog/')->withError('Marketing Blog not found!');
            }
            $success_msg = 'Marketing Blog updated successfully!';
            $nameValidator = 'required';
        }
        else
        {
            $blog     = New MarketingBlogs;
            $success_msg = 'Marketing Blog  saved successfully!';
            $nameValidator = 'required';
        }
        
        $validator = Validator::make($request->all(), [
                'marketing_title'  => $nameValidator,
        ]);

        if ($validator->fails())
        {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        else
        {
            DB::beginTransaction();
            try
            {
            	$file = $request->file('marketing_image');
                if($file != ""){
                    if(file_exists('uploads/'.$blog->marketing_image))
                    {
                        unlink('uploads/'.$blog->marketing_image);
                    }
                    $destinationPath = 'uploads/marketing_images/';
                    $imageName = 'marketing_images/'.rand().time().'.'.$file->getClientOriginalExtension();
                    $file->move($destinationPath,$imageName);
                    $blog->marketing_image = $imageName;
                }

                $blog->marketing_title      = Input::get('marketing_title');
                $blog->marketing_slug   	= str_slug(Input::get('marketing_title'));
                $blog->marketing_slug_web   = str_slug(Input::get('marketing_title'));
                $blog->marketing_description= Input::get('marketing_description');
                $blog->is_new				= Input::get('is_new') ? Input::get('is_new') : 0 ;
                $blog->save();

                /* only in insert case */
                if (empty($id))
                {
                    /*
                    ** Push notification for all apps in Android
                    ** by pankaj : 29th august 2019
                    */
                    $notificationMsgArray = array(
                        "msg"   => "New blog added : ".Input::get('marketing_title'),
                        "image" => $imageName!=''? $imageName : "",
                        "notification_id"   => 0,
                        "notification_for"  => 11
                    );

                    $notifyMsg = json_encode($notificationMsgArray);
                    $this->SendAndroidNotification('all',$notifyMsg);

                    /*
                    ** Push notification for all apps in IOS
                    ** by pankaj : 29th august 2019
                    */

                    $notifyTitle = "New blog added";
                    $notifyDesc   = Input::get('marketing_title');
                    $this->SendIphoneNotification('all',$notifyMsg,$notifyTitle,$notifyDesc);
                }

            }
            catch (\Exception $e)
            {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }
        return redirect('admin-panel/marketing-blog/view-marketing-blog')->withSuccess($success_msg);
    }

    /**
     *  Get Data for view page(Datatables)
     *  @pankaj on 18th March 2019
    **/
    public function anyData(Request $request)
    {

        $blog = [];
        $blog = MarketingBlogs::orderBy('marketing_id','DESC')->where(function($query) use ($request){
            if ($request->get('marketing_title')!= null){
                $query->where('marketing_title','LIKE','%'.$request->get('marketing_title').'%');
            }
        })->get();
        
        return Datatables::of($blog)
            ->addColumn('action', function ($blog){
                 $encrypted_marketing_id = get_encrypted_value($blog['marketing_id'], true);
                return '
                &nbsp;
                <a href="add-marketing-blog/' . $encrypted_marketing_id . '" "><i class="fas fa-pencil-alt"></i></a>
                &nbsp;&nbsp; 
                <a href="marketing-blog-delete/' . $encrypted_marketing_id . '" onclick="return confirm('."'Are you sure?'".')" "><i class="fas fa-trash"></i></a> ';
               
            })->addColumn('status', function ($blog){
                if($blog['marketing_status'] == 0) {
                    $status = 1;
                    $statusValIq = '<div class="radio radio-danger custom_radiobox">
                    <input type="radio" name="iqradio'.$blog['marketing_id'].'" onClick="changeStatusBlog('.$blog['marketing_id'].','.$status.')" id="iqradio'.$blog['marketing_id'].'" value="option4" checked="">
                    <label for="iqradio'.$blog['marketing_id'].'"> </label>
                </div>';
                }
                else {
                    $status = 0;
                    $statusValIq = '<div class="radio radio-success custom_radiobox">
                        <input type="radio" name="iqradio'.$blog['marketing_id'].'" onClick="changeStatusBlog('.$blog['marketing_id'].','.$status.')" id="iqradio'.$blog['marketing_id'].'" value="option4" checked="">
                        <label for="iqradio'.$blog['marketing_id'].'"> </label>
                    </div>';    
                }
                return $statusValIq;
               
            })->addColumn('image', function ($blog){

                if ($blog->marketing_image!=''){
                    $img  = "<img src='".url('uploads/'.$blog->marketing_image)."' height='50' width='50' />";    
                }else{
                    $img  = "<img src='".url('public/admin/nopreview-available.jpg')."' height='50' width='50' />";
                }
                return "<div class='text-center'>".$img."</div>";

            })->addColumn('description', function ($blog){

            	return "<div style='max-height:100px;overflow:scroll'>".getTextTransform($blog->marketing_description,0)."</div>";

            })->addColumn('new', function ($blog){

            	return $blog->is_new==1 ? "New" : "Old";

            })->rawColumns(['action' => 'action','status'=>'status','image'=>'image','description'=>'description'])->addIndexColumn()->make(true);
    }

    public function detailData($id)
    {
        $detail = MarketingBlogs::where('marketing_id',$id)->first();
        return view('admin-panel.marketing_blogs.detail',compact('detail'));
    }


    /**
     *  Change News status
     *  @pankaj on 18th March 2019
    **/
    public function changeStatus(Request $request)
    {
        $id 		= $request->get('blog_id');
        $status 	= $request->get('blog_status');        
        $blog   = MarketingBlogs::find($id);
        if ($blog)
        {
            $blog->marketing_status  = $status;           
            $blog->save();
            echo "Success";
        }
        else
        {
            echo "Fail";
        }
    }

    /**
     *  Destroy blog data
     *  @Pankaj on 8th March 2019
    **/
    public function destroy($id)
    {
        $blog_id = get_decrypted_value($id, true);
        $blog    = MarketingBlogs::find($blog_id);
        if ($blog)
        {
            $blog->delete();
            $success_msg = "Marketing blog deleted successfully!";
            return redirect('admin-panel/marketing-blog/view-marketing-blog')->withSuccess($success_msg);
        }else{

            $error_message = "Marketing blog not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }
}
