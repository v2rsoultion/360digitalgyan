<?php

namespace App\Http\Controllers\adminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\NewsCategory\NewsCategory;
use Yajra\Datatables\Datatables;

class NewsCategoryController extends Controller
{
    public function  __construct(){
        $this->middleware('subadmin');
    }

    public function index()
    {   
        $data = array(
            'page_title'    => trans('language.view_news_categories'),
            'redirect_url'  => url('admin-panel/news-category/view-news-category'),
        );
        return view('admin-panel.news_category.index')->with($data);
    }

    /**
     *  Add page for News Category
     *  @Pankaj on 18th March 2019
    **/
    public function add(Request $request, $id = NULL)
    {
        if (!empty($id))
        {
            $decrypted_news_category_id = get_decrypted_value($id, true);
            $newsCategory      		   = NewsCategory::Find($decrypted_news_category_id);
            if (!$newsCategory)
            {
                return redirect('admin-panel/news-category/add-news-category')->withError('news category not found!');
            }

            $newsCategory = NewsCategory::where('news_category_id',$decrypted_news_category_id)->first();
            $page_title   = trans('language.edit_category');
            $encrypted_news_category_id = get_encrypted_value($newsCategory->news_category_id,true);
            $save_url               	= url('admin-panel/news-category/save/'.$encrypted_news_category_id);
            $submit_button          	= 'Update';
        }
        else
        {
            $newsCategory  = [];
            $page_title    = trans('language.add_news_category');
            $save_url      = url('admin-panel/news-category/save/');
            $submit_button = 'Save';
        }

        $data   = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'category' 		    => $newsCategory,
            'redirect_url'  	=> url('admin-panel/news-category/view-news-categories'),
        );
        return view('admin-panel.news_category.add')->with($data);
    }
    
    /**
     *  Save news category data
     *  @pankaj on 18th March 2019
    **/
    public function save(Request $request, $id = NULL)
    {
        $decrypted_news_category_id = get_decrypted_value($id, true);
        if (!empty($id))
        {
            $newsCategory = NewsCategory::find($decrypted_news_category_id);
            if (!$newsCategory)
            {
                return redirect('admin-panel/news-category/view-categories/')->withError('News category not found!');
            }
            $success_msg = 'News category updated successfully!';
            $nameValidator = 'required|unique:news_categories,news_category_name,' . $decrypted_news_category_id . ',news_category_id';
        }
        else
        {
            $newsCategory     = New NewsCategory;
            $success_msg = 'News category saved successfully!';
            $nameValidator = 'required|unique:news_categories';
        }
        
        $validator = Validator::make($request->all(), [
                'news_category_name'        => $nameValidator,
        ]);

        if ($validator->fails())
        {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        else
        {
            DB::beginTransaction();
            try
            {
                $newsCategory->news_category_name         = Input::get('news_category_name');
                $newsCategory->save();
            }
            catch (\Exception $e)
            {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }
        return redirect('admin-panel/news-category/view-news-categories')->withSuccess($success_msg);
    }

    /**
     *  Get Data for view page(Datatables)
     *  @pankaj on 18th March 2019
    **/
    public function anyData(Request $request)
    {

        $newsCategory     = [];
        $newsCategory = NewsCategory::orderBy('news_category_id','DESC')->where(function($query) use ($request){
            if ($request->get('news_category_name')!= null){
                $query->where('news_category_name','LIKE','%'.$request->get('news_category_name').'%');
            }
        })->get();
        
        return Datatables::of($newsCategory)
            
            ->addColumn('action', function ($newsCategory)
            {
                 $encrypted_news_category_id = get_encrypted_value($newsCategory['news_category_id'], true);
                return '
                &nbsp;
                <a href="add-news-category/' . $encrypted_news_category_id . '" "><i class="fas fa-pencil-alt"></i></a>
                &nbsp;&nbsp; 
                <a href="news-category-delete/' . $encrypted_news_category_id . '" onclick="return confirm('."'Are you sure?'".')" "><i class="fas fa-trash"></i></a> ';
               
            })->addColumn('news_status', function ($newsCategory)
            {
                if($newsCategory['news_category_status'] == 0) {
                    $status = 1;
                    $statusValIq = '<div class="radio radio-danger custom_radiobox">
                    <input type="radio" name="iqradio'.$newsCategory['news_category_id'].'" onClick="changeStatusNewsCategory('.$newsCategory['news_category_id'].','.$status.')" id="iqradio'.$newsCategory['news_category_id'].'" value="option4" checked="">
                    <label for="iqradio'.$newsCategory['news_category_id'].'"> </label>
                </div>';
                }
                else {
                    $status = 0;
                    $statusValIq = '<div class="radio radio-success custom_radiobox">
                        <input type="radio" name="iqradio'.$newsCategory['news_category_id'].'" onClick="changeStatusNewsCategory('.$newsCategory['news_category_id'].','.$status.')" id="iqradio'.$newsCategory['news_category_id'].'" value="option4" checked="">
                        <label for="iqradio'.$newsCategory['news_category_id'].'"> </label>
                    </div>';    
                }
                return $statusValIq;
               
            })->rawColumns(['action' => 'action','news_status'=>'news_status'])->addIndexColumn()->make(true);
    }


    /**
     *  Change News Category's status
     *  @pankaj on 18th March 2019
    **/
    public function changeStatus(Request $request)
    {
        $id 		= $request->get('news_category_id');
        $status 	= $request->get('news_category_status');        
        $newsCategory   = NewsCategory::find($id);
        if ($newsCategory)
        {
            $newsCategory->news_category_status  = $status;           
            $newsCategory->save();
            echo "Success";
        }
        else
        {
            echo "Fail";
        }
    }

    /**
     *  Destroy category data
     *  @Pankaj on 8th March 2019
    **/
    public function destroy($id)
    {
        $category_id = get_decrypted_value($id, true);
        $category    = NewsCategory::find($category_id);
        if ($category)
        {
            $category->delete();
            $success_msg = "News category deleted successfully!";
            return redirect('admin-panel/news-category/view-news-categories')->withSuccess($success_msg);
        }else{

            $error_message = "News category not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }
}
