<?php

namespace App\Http\Controllers\adminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\SeoResult\SeoResult;
use Illuminate\Support\Facades\Input;
use Validator;
use App\Model\Category\Category;
use Yajra\Datatables\Datatables;
use DB;

class SeoResultController extends Controller
{
    public function index()
    {
    	$data = array(
    		"page_title" => trans('language.view_seo_result'),
    		"redirect_url" => url('admin-panel/seo-results/view-seo-results')
    	);
    	return view('admin-panel.seo_results.index')->with($data);
    }
    public function data(Request $request)
    {
    	$result = [];
        $result = SeoResult::where(function($query) use ($request){
            if ($request->get('paper_name')!= null){
                $query->where('paper_title','LIKE','%'.$request->get('paper_name').'%');
            }
            if ($request->get('user_email')!= null){
                $query->where('email_id','LIKE','%'.$request->get('user_email').'%');
            }
        })->get();
        return Datatables::of($result)->addColumn('action',function($result){
                $encrypted_id = get_encrypted_value($result->seo_result_id, true);
                return "<a href='result-delete/".$encrypted_id."' onclick='return confirm(".'"Are you sure?"'.")'><i class='fas fa-trash'></i></a>";
               
            })->rawColumns(['action' => 'action'])->addIndexColumn()->make(true);
    }
    public function destroy($id)
    {
    	$result_id = get_decrypted_value($id, true);
        $result    = SeoResult::find($result_id);
        if ($result)
        {
            $result->delete();
            $success_msg = "Seo result deleted successfully!";
            return redirect('admin-panel/seo-results/view-seo-results')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Seo result not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }
}
