<?php

namespace App\Http\Controllers\adminPanel;

use App\Admin;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Model\ArticleCategory\ArticleCategory;

use App\Model\Pages\Pages;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

use DB;
use Validator;
use Illuminate\Support\Facades\Input;
use App\Model\SubCategory\SubCategory;
use App\Model\AllQuesComments\AllQuesComments;

class DashboardController extends Controller
{
    public function index()
    {
        $loginInfo          = get_loggedin_user_data();

        $data = array(
            'page_title'        => trans('language.dashboard'),
            'login_info'        => $loginInfo,
        );
        
        return view('admin-panel.dashboard.index')->with($data);
    }

   public function showsubadmin(){
        $loginInfo          = get_loggedin_user_data();
        $data = array(
            'page_title'        => trans('language.view_subadmin'),
            'login_info'        => $loginInfo,
            
        );
        return view('admin-panel.dashboard.subadmins')->with($data);
    }
    public function anyData(Request $request)
    {

        $subadmins     = [];
      
        $subadmins = Admin::where(function($query) use ($request) 
        {
           
            if (!empty($request) && !empty($request->has('username')) && $request->get('username') != null )
            {
                $query->where('admin_name', 'like', '%' . $request->get('username'). '%');
            }
        })->where('type', '=' ,'0')->get();

        $subadmins=$subadmins;
        return Datatables::of($subadmins)
             ->addColumn('type', function ($subadmins)
            {
                if ($subadmins->type==1) {
                   return "Admin";
                }
                else{
                    return "Subadmin";
                }
                
            })
              ->addColumn('admin_status', function ($subadmins)
            {
                if ($subadmins->admin_status==1) {
                   return "Active";
                }
                else{
                    return "Deactive";
                }
                
            })
            ->addColumn('action', function ($subadmins)
            {
                 $encrypted_subadmin_id = get_encrypted_value($subadmins['admin_id'], true);
                 if($subadmins['admin_status'] == 0) {
                    $status = 1;
                    $statusVal = '<div class="radio radio-danger custom_radiobox">
                    <input type="radio" name="radio'.$subadmins['admin_id'].'" onClick="changeStatus('.$subadmins['admin_id'].','.$status.')" id="radio'.$subadmins['admin_id'].'" value="option4" checked="">
                    <label for="radio'.$subadmins['admin_id'].'"> </label>
                </div>';
                }
                else {
                    $status = 0;
                    $statusVal = '<div class="radio radio-success custom_radiobox">
                        <input type="radio" name="radio'.$subadmins['admin_id'].'" onClick="changeStatus('.$subadmins['admin_id'].','.$status.')" id="radio'.$subadmins['admin_id'].'" value="option4" checked="">
                        <label for="radio'.$subadmins['admin_id'].'"> </label>
                    </div>';
                }
                return '
                '.$statusVal.'
                &nbsp;
            
                &nbsp;&nbsp; 
                <a href="subadmins-delete/' . $encrypted_subadmin_id . '" onclick="return confirm('."'Are you sure?'".')" "><i class="fas fa-trash"></i></a> ';
               
            })->rawColumns(['action' => 'action'])->addIndexColumn()->make(true);
    }
    public function changeStatus(Request $request)
    {
        $id = $request->get('admin_id');
        $status = $request->get('status');
        $admin    = Admin::find($id);
        if ($admin)
        {
            $admin->admin_status  = $status;
            $admin->save();
            echo "Success";
        }
        else
        {
            echo "Fail";
        }
    }


    public function destroy($id)
    {
        $subadmin_id = get_decrypted_value($id, true);
        $subadmin    = Admin::find($subadmin_id);
        if ($subadmin)
        {
            $subadmin->delete();
            $success_msg = "Subadmin deleted successfully!";
            return redirect('admin-panel/view-sub-admin')->withSuccess('success', $success_msg);
        }
        else
        {
            $error_message = "Subadmin not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }
    public function comments($slug,$id=false)
    {
        if ($slug=="study") 
        {
            $slug = 2;
            $page_title = trans('language.view_study_comments');
            $redirect_url  = url('admin-panel/study-material/view-comment/study');
        }
        elseif ($slug=="interview") 
        {
            $slug = 1;
            $page_title = trans('language.view_interview_comments');
            $redirect_url  = url('admin-panel/study-material/view-comment/interview');
        }
        else
        {
            $slug = 3;
            $page_title = trans('language.view_blog_comments');
            $redirect_url  = url('admin-panel/study-material/view-comment/blog');
        }
        $data = array(
            'slug_id'       => $id,
            'slug'          => $slug,
            'page_title'    => $page_title,
            'redirect_url'  => $redirect_url,
        );
        return view('admin-panel.study_material.view_comments')->with($data);
    }

    public function comments_data(Request $request,$slug,$id=false)
    {
        $comment    = [];
        if ($id!="") 
        {
            $id = get_decrypted_value($id,true);
            $comment    = AllQuesComments::where(function($query) use ($request){
                if ($request->get('title')!= null){
                    $query->where('comment_comment','LIKE','%'.$request->get('title').'%');
                }
            })->where('comment_flag',$slug)->where('comment_question_id',$id)->get();
        }
        else
        {
            $comment    = AllQuesComments::where(function($query) use ($request){
                if ($request->get('title')!= null){
                    $query->where('comment_comment','LIKE','%'.$request->get('title').'%');
                }
            })->where('comment_flag',$slug)->get();
        }
        return Datatables::of($comment)->addColumn('user',function($comment){
            return $comment['getUser']->seo_users_name;
        })->addColumn('cmt-for',function($comment){
        
        if($comment->comment_flag==1) 
        {
            $cmt_for  = $comment['getInterviewQues']->iq_ques_ans_q;
        }
        elseif($comment->comment_flag==2) 
        {
            $cmt_for  = $comment['getStudyQues']->user_question;
        }
        else
        {
            $cmt_for  = $comment['getBlogQues']->markeing_title;
        }
            return "<div style='max-width:200px;max-height:100px;overflow:scroll'>".getTextTransform($cmt_for,0)."</div>";
        
        })->addColumn('date',function($comment){
            return $comment->comment_created;
        })->addColumn('cmt', function ($comment){
        return "<div style='max-width:200px;max-height:100px;overflow:scroll'>".getTextTransform($comment->comment_comment,0)."</div>";
            })->rawColumns(['date'=>'date','cmt'=>'cmt','user'=>'user','cmt-for'=>'cmt-for'])->addIndexColumn()->make(true);
    }

}
