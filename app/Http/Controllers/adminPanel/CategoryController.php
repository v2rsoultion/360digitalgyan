<?php

namespace App\Http\Controllers\adminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Category\Category;

use Yajra\Datatables\Datatables;

class CategoryController extends Controller
{
    public function  __construct(){
        $this->middleware('subadmin');
    }

    public function index()
    {   
        $data = array(
            'page_title'    => trans('language.view_categories'),
            'redirect_url'  => url('admin-panel/category/view-category'),
        );
        return view('admin-panel.category.index')->with($data);
    }

    /**
     *  Add page for Category
     *  @Pankaj on 8th March 2019
    **/
    public function add(Request $request, $id = NULL)
    {
        if (!empty($id))
        {
            $decrypted_category_id = get_decrypted_value($id, true);
            $category      		   = Category::Find($decrypted_category_id);
            if (!$category)
            {
                return redirect('admin-panel/category/add-category')->withError('category not found!');
            }

            $category = Category::where('iq_category_id', "=", $decrypted_category_id)->first();
            $page_title             	= trans('language.edit_category');
            $encrypted_category_id      = get_encrypted_value($category->iq_category_id,true);
            $save_url               	= url('admin-panel/category/save/'.$encrypted_category_id);
            $submit_button          	= 'Update';
        }
        else
        {
            $category  = [];
            $page_title    = trans('language.add_category');
            $save_url      = url('admin-panel/category/save/');
            $submit_button = 'Save';
        }

        $data   = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'category' 		    => $category,
            'redirect_url'  	=> url('admin-panel/category/view-categories'),
        );
        return view('admin-panel.category.add')->with($data);
    }
    
    /**
     *  Save category data
     *  @pankaj on 8th March 2019
    **/
    public function save(Request $request, $id = NULL)
    {
        echo implode(",",$request['video_lan']);

        $decrypted_category_id = get_decrypted_value($id, true);
        if (!empty($id))
        {
            $category = Category::find($decrypted_category_id);
            if (!$category)
            {
                return redirect('admin-panel/category/view-categories/')->withError('Category not found!');
            }
            $success_msg = 'Category updated successfully!';
            $nameValidator = 'required|unique:iq_category,iq_category_name,' . $decrypted_category_id . ',iq_category_id';
        }
        else
        {
            $category     = New Category;
            $success_msg = 'Category saved successfully!';
            $nameValidator = 'required|unique:iq_category';
        }
        
        $validator = Validator::make($request->all(), [
            'iq_category_name' => $nameValidator,
        ]);

        if ($validator->fails())
        {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        else
        {
            DB::beginTransaction();
            try
            {
                $file = $request->file('category_icon');
                if($file != ""){
                    if(file_exists('uploads/'.$category->iq_category_icon))
                    {
                        unlink('uploads/'.$category->iq_category_icon);
                    }

                    $destinationPath = 'uploads/category_app_icon/';
                    $imageName = 'category_app_icon/'.rand().time().'.'.$file->getClientOriginalExtension();
                    $file->move($destinationPath,$imageName);
                    $category->iq_category_icon = $imageName;
                }
                $category->iq_category_name         = Input::get('iq_category_name');
                $category->iq_category_order        = Input::get('iq_category_order');
                $category->quiz_time                = Input::get('quiz_time');
                $category->iq_category_metatitle    = Input::get('iq_category_metatitle');
                $category->quiz_meta_title          = Input::get('quiz_meta_title');
                $category->iq_category_metakey      = Input::get('iq_category_metakey');
                $category->quiz_meta_key            = Input::get('quiz_meta_key');
                $category->iq_category_metadesc     = Input::get('iq_category_metadesc');
                $category->quiz_meta_desp           = Input::get('quiz_meta_desp');
                $category->show_category_video      = implode(",",$request['video_lan']);
                $category->save();
            }
            catch (\Exception $e)
            {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }
        return redirect('admin-panel/category/view-categories')->withSuccess($success_msg);
    }

    /**
     *  Get Data for view page(Datatables)
     *  @pankaj on 8th March 2019
    **/
    public function anyData(Request $request)
    {

        $category     = [];

        $category = Category::orderBy('iq_category_id','DESC')->where(function($query) use ($request){

            if ($request->get('iq_category_name')!= null){
                $query->where('iq_category_name','LIKE','%'.$request->get('iq_category_name').'%');
            }

        })->get();
        
        return Datatables::of($category)
            
            ->addColumn('action', function ($category)
            {
                 $encrypted_category_id = get_encrypted_value($category['iq_category_id'], true);
                return '
                &nbsp;
                <a href="add-category/' . $encrypted_category_id . '" "><i class="fas fa-pencil-alt"></i></a>
                &nbsp;&nbsp; 
                <a href="category-delete/' . $encrypted_category_id . '" onclick="return confirm('."'Are you sure?'".')" "><i class="fas fa-trash"></i></a> ';
               
            })->addColumn('iq_status', function ($category)
            {
                if($category['iq_category_status'] == 0) {
                    $status = 1;
                    $statusValIq = '<div class="radio radio-danger custom_radiobox">
                    <input type="radio" name="iqradio'.$category['iq_category_id'].'" onClick="changeStatusIQCategory('.$category['iq_category_id'].','.$status.')" id="iqradio'.$category['iq_category_id'].'" value="option4" checked="">
                    <label for="iqradio'.$category['iq_category_id'].'"> </label>
                </div>';
                }
                else {
                    $status = 0;
                    $statusValIq = '<div class="radio radio-success custom_radiobox">
                        <input type="radio" name="iqradio'.$category['iq_category_id'].'" onClick="changeStatusIQCategory('.$category['iq_category_id'].','.$status.')" id="iqradio'.$category['iq_category_id'].'" value="option4" checked="">
                        <label for="iqradio'.$category['iq_category_id'].'"> </label>
                    </div>';    
                }
                return $statusValIq;
               
            })->addColumn('quiz_status', function ($category)
            {
                if($category['iq_quiz_status'] == 0)
                {
                    $status = 1;
                    $statusValquiz = '<div class="radio radio-danger custom_radiobox">
                    <input type="radio" name="quizradio'.$category['iq_category_id'].'" onClick="changeStatusQuizCategory('.$category['iq_category_id'].','.$status.')" id="quizradio'.$category['iq_category_id'].'" value="option4" checked="">
                    <label for="quizradio'.$category['iq_category_id'].'"> </label>
                </div>';
                }
                else
                {
                    $status = 0;
                    $statusValquiz = '<div class="radio radio-success custom_radiobox">
                        <input type="radio" name="quizradio'.$category['iq_category_id'].'" onClick="changeStatusQuizCategory('.$category['iq_category_id'].','.$status.')" id="quizradio'.$category['iq_category_id'].'" value="option4" checked="">
                        <label for="quizradio'.$category['iq_category_id'].'"> </label>
                    </div>';
                }
                return $statusValquiz;
               
            })->addColumn('icon', function ($category)
            {
                if ($category->iq_category_icon!=''){
                    $img  = "<img src='".url('uploads/'.$category->iq_category_icon)."' height='50' width='50' />";    
                }else{
                    $img  = "<img src='".url('public/admin/nopreview-available.jpg')."' height='50' width='50' />";
                }
                return "<div class='bg-info text-center'>".$img."</div>";
               
            })->addColumn('category_order', function ($category)
            {
                $order  = "<input class='nums' name='order[]' data-id='".$category->iq_category_id."' onchange='changeCategoryOrder(this)' min='0' type='number' value='".$category->iq_category_order."' type='number' /> ";
                return $order;
            })->rawColumns(['action' => 'action','iq_status'=>'iq_status','quiz_status'=>'quiz_status','icon'=>'icon','category_order'=>'category_order'])->addIndexColumn()->make(true);
    }


    /**
     *  Change Category's status
     *  @pankaj on 8th March 2019
    **/
    public function changeStatus(Request $request)
    {
        $id = $request->get('category_id');
        $status = $request->get('category_status');
        $type = $request->get('category_type');
        
        $category    = Category::find($id);
        if ($category)
        {
            if ($type=='iq'){
                $category->iq_category_status  = $status;
            }else{
                $category->iq_quiz_status  = $status;
            }
            $category->save();
            echo "Success";
        }
        else
        {
            echo "Fail";
        }
    }

    /**
     *  Destroy category data
     *  @Pankaj on 8th March 2019
    **/
    public function destroy($id)
    {
        $category_id = get_decrypted_value($id, true);
        $category    = Category::find($category_id);
        if ($category)
        {
            if (file_exists($category->iq_category_icon)){
                unlink($category->iq_category_icon);
            }
            $category->delete();
            $success_msg = "Category deleted successfully!";
            return redirect('admin-panel/category/view-categories')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Category not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }


    /**
     *  Change Category's Order
     *  @pankaj on 8th March 2019
    **/
    public function changeOrder(Request $request)
    {
        $id     = $request->get('category_id');
        $order  = $request->get('category_order');
        $category   = Category::find($id);
        if($category)
        {
            $category->iq_category_order  = $order;
            $category->save();
            echo "Success";
        }
        else
        {
            echo "Fail";
        }
    }
}
