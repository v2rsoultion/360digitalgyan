<?php

namespace App\Http\Controllers\adminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Greetings\Greetings;
use DB;
use Validator;
use Illuminate\Support\Facades\Input;
use Yajra\Datatables\Datatables;

class GreetingsController extends Controller
{
    public function add($id=false)
    {
    	if (!empty($id)) 
    	{
    		$decrypted_greetings_id = get_decrypted_value($id,true);
    		$greetings 		= Greetings::find($decrypted_greetings_id);
    		if (!$greetings) 
    		{
    			return redirect('admin-panel/greetings/add-greetings')->withError('Greetings not found');
    		}
    		$greetings 		= Greetings::where('greetings_id',$decrypted_greetings_id)->first();
    		$page_title 	= trans('language.edit_greetings');
    		$submit_button	= 'update';
    		$save_url 		= url('admin-panel/greetings/save/'.$id);	
    	}
    	else
    	{
    		$greetings 		= [];
    		$page_title 	= trans('language.add_greetings');
    		$submit_button	= 'save';
    		$save_url 		= url('admin-panel/greetings/save');
    	}
    	$data 	= array(
    		'greetings'		=> $greetings,
    		'page_title' 	=> $page_title,
    		'submit_button'	=> $submit_button,
    		'redirect_url' 	=> url('admin-panel/greetings/view-greetings'),
    		'save_url'		=> $save_url,
    	);
    	return view('admin-panel.greetings.add')->with($data);
    }
    public function index()
    {
    	$data 	= array(
    		'page_title'	=> trans('language.view_greetings'),
    		'redirect_url'	=> url('admin-panel/greetings/view-greetings'),
    	);
    	return view('admin-panel.greetings.index')->with($data);
    }
    public function save(Request $request,$id=false)
    {
    	if (!empty($id)) 
    	{
    		$decrypted_greetings_id = get_decrypted_value($id,true);
    		$greetings 	= Greetings::find($decrypted_greetings_id);
    		if (!$greetings) 
    		{
    			return redirect('admin-panel/greetings/view-greetings')->withError('Greeting not found!');
    		}
    		$success_msg 	= 'Greeting updated successfully';
    		$nameValidator 	= ' required|unique:greetings,greetings_image,'.$decrypted_greetings_id.',greetings_id';
     	}
     	else
     	{
     		$greetings 	= New Greetings;
     		$success_msg = 'Greetings saved successfully';
     		$nameValidator = ' required|unique:greetings,greetings_image';
     	}
     	$validator = Validator::make($request->all(), [
            'greetings_image'        => $nameValidator,
        ]);
        if ($validator->fails())
        {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        else
        {
        	DB::beginTransaction();
        	try
        	{
                
        		$file = $request->file('greetings_image');
    			if ($file!=''){
                    if(file_exists('uploads/'.$greetings->greetings_image))
                    {
                        unlink('uploads/'.$greetings->greetings_image);
                    }

                    $destinationPath = 'uploads/greetings_image/';
                    $imageName = 'greetings_image/'.rand().time().'.'.$file->getClientOriginalExtension();
                    $file->move($destinationPath,$imageName);
                    $greetings->greetings_image = $imageName;
                    
                }
                $greetings->save();
        	}
        	catch(\ Exception $e)
        	{
        		DB::rollback();
        		$error_msg = $e->getMessage();
        		return redirect()->back()->withError($error_msg);
        	}
        	DB::commit();
        }
        return redirect('admin-panel/greetings/view-greetings')->withSuccess($success_msg); 
    }
    public function data()
    {
    	$greetings 	= [];
    	$greetings 	= Greetings::orderBy('greetings_id','DESC')->get();
    	return Datatables::of($greetings)->addColumn('image',function($greetings){
    		if ($greetings->greetings_image!=''){
                    $img  = "<img src='".url('uploads/'.$greetings->greetings_image)."' height='50' width='50' />";    
                }else{
                    $img  = "<img src='".url('public/admin/nopreview-available.jpg')."' height='50' width='50' />";
                }
                return "<div class='bg-info text-center'>".$img."</div>";
    	})->addColumn('action',function($greetings){
    		$encrypted_greetings_id = get_encrypted_value($greetings->greetings_id, true);
			return "
                &nbsp;
                <a href='add-greetings/".$encrypted_greetings_id."'><i class='fas fa-pencil-alt'></i></a>
                &nbsp;&nbsp; 
                <a href='greetings-delete/".$encrypted_greetings_id."' onclick='return confirm(".'"Are you sure?"'.")'><i class='fas fa-trash'></i></a>";
    	})->addColumn('status',function($greetings){
    		if ($greetings->greetings_status==1){
                    $status = 0;
                    $radio_cls = 'radio-success';
            }
            else
            {
                $status = 1;
                $radio_cls = 'radio-danger';
            }
            return "<div class='radio $radio_cls custom_radiobox'><input type='radio' onClick='changeStatus(".$greetings['greetings_id'].",".$status.")' name='vradio".$greetings['greetings_id']."' id='vradio".$greetings['greetings_id']."' value='option4' checked=''></a>
                <label for='vradio".$greetings['greetings_id']."'> </label>
            </div>";
    	})->rawColumns(['image'=>'image','action'=>'action','status'=>'status'])->addIndexColumn()->make(true);
    }
    public function destroy($id)
    {
    	$decrypted_greetings_id = get_decrypted_value($id,true);
    	$greetings = Greetings::find($decrypted_greetings_id);
    	if($greetings) 
    	{
    		unlink($greetings->greetings_image);
    		$greetings->delete();
    		$success_msg = 'Greeting deleted successfully';
    		return redirect('admin-panel/greetings/view-greetings')->withSuccess($success_msg);
    	}
    	else
    	{
    		return redirect()->back()->withError('Greeting not found!');
    	}
    }
    public function status(Request $request)
    {
    	$id = $request['greetings_id'];
        $status = $request['greetings_status'];
        
        $greetings    = Greetings::find($id);
        if (!empty($greetings))
        {
            $greetings->greetings_status  = $status;
            $greetings->save();
            echo "Success";
        }
        else
        {
            echo "Fail";
        }
    }
}
