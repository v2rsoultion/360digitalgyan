<?php

namespace App\Http\Controllers\adminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\ShareBanner\ShareBanner;
use Illuminate\Support\Facades\Input;
use Validator;
use App\Model\Category\Category;
use Yajra\Datatables\Datatables;
use DB;

class shareBannerController extends Controller
{
    public function index()
    {
    	$data =	array(
    		"page_title" => trans('language.view_share_banner'),
    		"redirect_url" => url('admin-panel/share-banner/view-banner')
    	);
    	return view('admin-panel.share_banner.index')->with($data);
    }
    public function add($id=false)
    {
    	if ($id!="") 
    	{
    		$encrypted_id 	= get_decrypted_value($id,true);
    		$banner 	= ShareBanner::where('banner_id',$encrypted_id)->first();
    		$page_title = trans('language.edit_share_banner');
    		$save_url = url('admin-panel/share-banner/save/'.$id);
    		$submit_button = "update";
    	}
    	else
    	{
    		$banner  = [];
            $page_title    = trans('language.add_share_banner');
            $save_url      = url('admin-panel/share-banner/save/');
            $submit_button = 'save';
    	}
    	$data = array(
    		'page_title' 	=> $page_title,
    		'save_url' 		=> $save_url,
    		'banner' 		=> $banner,
    		'categories'        => Category::where('iq_category_status',1)->get(),
    		'submit_button' => $submit_button,
    		'redirect_url'  => url('admin-panel/share-banner/view-share-banner'),
    	);
    	return view('admin-panel.share_banner.add')->with($data);
    }
    public function save(Request $request,$id=false)
    {
    	if ($id!="") 
    	{
    		$decrypted_id = get_decrypted_value($id,true);
    		$banner = ShareBanner::find($decrypted_id);
    		if (!$banner)
            {
                return redirect('admin-panel/share-banner/view-banner/')->withError('Share banner not found!');
            }
            $success_msg = 'Share banner updated successfully!';
            $nameValidator = 'required|unique:share_banner,banner_title,' . $decrypted_id . ',banner_id';
    	}
    	else
    	{
    		$banner = new ShareBanner;
    		$success_msg = "Banner saved successfully";
    		$nameValidator = 'required|unique:share_banner,banner_title';
    	}
    	$validator = Validator::make($request->all(), [
            'banner_title'        => $nameValidator,
        ]);
        if ($validator->fails())
        {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        else
        {
        	DB::beginTransaction();
        	try
        	{
        		$file = $request->file('banner_image');
                if($file != "")
                {
                    $destinationPath = 'uploads/share_banner_images/';
                    $imageName = 'share_banner_images/'.rand().time().'.'.$file->getClientOriginalExtension();
                    $file->move($destinationPath,$imageName);
                    $banner->banner_image = $imageName;
                }
                $banner->banner_title           = Input::get('banner_title');
                $banner->banner_cat 			= Input::get('cat_id');
                $banner->save();
        	}
        	catch(\ Exception $e)
        	{
        		DB::rollback();
        		$error_msg = $e->getMessage();
        		return redirect()->back()->withErrors($error_msg);
        	}
        	DB::commit();
        }
        return redirect('admin-panel/share-banner/view-banner')->withSuccess($success_msgs);
    }
    public function data(Request $request)
    {
    	$banner = [];
        $banner = ShareBanner::where(function($query) use ($request){
            if ($request->get('title')!= null){
                $query->where('banner_title','LIKE','%'.$request->get('title').'%');
            }
        })->get();
        return Datatables::of($banner)->addColumn('action',function($banner){
                $encrypted_id = get_encrypted_value($banner->banner_id, true);
                return "
                &nbsp;
                <a href='add-banner/".$encrypted_id."'><i class='fas fa-pencil-alt'></i></a>
                &nbsp;&nbsp; 
                <a href='banner-delete/".$encrypted_id."' onclick='return confirm(".'"Are you sure?"'.")'><i class='fas fa-trash'></i></a>";
               
            })->addColumn('status',function($banner){
                $encrypted_id = get_encrypted_value($banner->banner_id, true);
                if ($banner->banner_status==1){
                    $status = 0;
                    $radio_cls = 'radio-success';
                }
                else
                {
                    $status = 1;
                    $radio_cls = 'radio-danger';
                }
                return "<div class='radio $radio_cls custom_radiobox'><input type='radio' onClick='changeStatus(".$banner['banner_id'].",".$status.")' name='vradio".$banner['banner_id']."' id='vradio".$banner['banner_id']."' value='' checked=''></a>
                    <label for='vradio".$banner['banner_id']."'> </label>
                </div>";
            })->addColumn('image', function ($banner)
            {
                if ($banner->banner_image!=''){
                    $img  = "<img src='".url('uploads/'.$banner->banner_image)."' height='50' width='50' />";    
                }else{
                    $img  = "<img src='".url('public/admin/nopreview-available.jpg')."' height='50' width='50' />";
                }
                return "<div class='bg-info text-center'>".$img."</div>";
               
            })->addColumn('category',function($banner){
                return $banner['getCategory']->iq_category_name;
            })->rawColumns(['image'=>'image','action' => 'action','status' => 'status','category'=>'category'])->addIndexColumn()->make(true);
    }
    public function destroy($id)
    {
    	$banner_id = get_decrypted_value($id, true);
        $banner    = ShareBanner::find($banner_id);
        if ($banner)
        {
            if (file_exists($banner->banner_image)){
                unlink($banner->banner_image);
            }
            $banner->delete();
            $success_msg = "Share banner deleted successfully!";
            return redirect('admin-panel/share-banner/view-banner')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Share banner not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }
    public function status(Request $request)
    {
    	$id = $request['banner_id'];
        $status = $request['banner_status'];
        
        $banner    = ShareBanner::find($id);
        if (!empty($banner))
        {
            $banner->banner_status  = $status;
            $banner->save();
            echo "Success";
        }
        else
        {
            echo "Fail";
        }
    }
}
