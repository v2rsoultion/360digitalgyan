<?php

namespace App\Http\Controllers\adminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Notification\Notification;
use DB;
use Validator;
use Illuminate\Support\Facades\Input;
use Yajra\Datatables\Datatables;
use App\Model\SubCategory\SubCategory;
use App\Model\Category\Category;
use App\Traits\PushNotification;

class NotificationController extends Controller
{
    use PushNotification;

    public function index()
    {
    	$data = array(
    		'page_title' 	=> trans('language.view_notification'),
    		'redirect_url' 	=> url('admin-panel/notification/view_tricks'),
            'Categories'        => Category::where('iq_category_status',1)->get(),
            'subCategories'     => SubCategory::where('status',1)->get(),
    	);
    	return view('admin-panel.notification.index')->with($data);
    }
    public function add($id=false)
    {
    	if (!empty($id)) 
    	{
    		$decrypted_notificartion_id = get_decrypted_value($id, true);
            $notification              = Notification::Find($decrypted_notificartion_id);
            if (!$notification)
            {
                return redirect('admin-panel/notification/add-notification')->withError('Notification not found!!');
            }

            $notification = Notification::where('notification_id', "=", $decrypted_notificartion_id)->first();
            $page_title 	= trans('language.edit_notification');
            $save_url 		= url('admin-panel/notification/save-notification/'.$id);
            $submit_button 	= "update";
    	}
    	else
    	{
    		$notification 		= [];
    		$page_title 	= trans('language.add_notification');
    		$submit_button 	= "save";
    		$save_url 		= url('admin-panel/notification/save-notification');
    	}
    	$data 	= array(
    		'page_title' 		=> $page_title,
            'notificationsFor'  => config('custom.notifications_for'),
    		'notification' 		=> $notification,
    		'submit_button' 	=> $submit_button,
    		'categories'		=> Category::where('iq_category_status',1)->get(),
            'subCategories'		=> SubCategory::where('status',1)->get(),
    		'save_url'			=> $save_url,
    		'redirect_url' 		=> url('admin-panel/notification/view-notification'),
    	);
    	return view('admin-panel.notification.add')->with($data);
    }
    public function save(Request $request,$id=false)
    {
    	$decrypted_notification_id = get_decrypted_value($id, true);
    	if(!empty($id)) 
    	{
    		$notification = Notification::find($decrypted_notification_id);
            if (!$notification)
            {
                return redirect('admin-panel/notification/view-notification/')->withError('notification not found!');
            }
            $success_msg = 'notification updated successfully!';
            $nameValidator = 'required';
    	}
    	else
    	{
    		$notification 	= New Notification;
    		$success_msg = 'notification saved successfully';
			$nameValidator = 'required';
    	}
    	$validator = Validator::make($request->all(), [
            'title'        => $nameValidator,
        ]);
    	if ($validator->fails())
        {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        else
        {
        	DB::beginTransaction();
        	try
        	{
        		$file = $request->file('image');
                if($file != "")
                {
                    if(file_exists('uploads/'.$notification->notification_image))
                    {
                        unlink('uploads/'.$notification->notification_image);
                    }

                    $destinationPath = 'uploads/notification_image/';
                    $imageName='notification_image/'.rand().time().'.'.$file->getClientOriginalExtension();
                    $file->move($destinationPath,$imageName);
                    $notification->notification_image = $imageName;
                }
        		$notification->notification_title  = Input::get('title');
        		$notification->notification_flag   = Input::get('cat_id');
        		$notification->sub_category_id	   = Input::get('sub_cat_id');
        		$notification->notification_for    = Input::get('notification_for');
        		$notification->save();

                /* only in insert case */
                if(empty($id))
                {
                    switch (Input::get('notification_for')){
                        case 1:  $titles = 'Video';              break;
                        case 2:  $titles = 'Interview Question'; break;
                        case 3:  $titles = 'Quiz Question';      break;
                        case 4:  $titles = 'Technical Terms';    break;
                        case 5:  $titles = 'Study Material';     break;
                        case 6:  $titles = 'Question Answer';    break;
                        case 7:  $titles = 'Question';           break;
                        default: $titles = 'Notification';       break;
                    }

                    $notifyId  = $notification->notification_id;  
                    $notifyCat = Input::get('cat_id');
                    /*
                    ** Push notification for all apps in Android
                    ** by pankaj : 29th august 2019
                    */
                    $notificationMsgArray = array(
                        "msg"               => "New ".$titles." available : ".strip_tags(Input::get('title')),
                        "image"             => $imageName!=''? $imageName : "",
                        "notification_id"   => $notifyId,
                        "notification_for"  => 2
                    );

                    $notifyMsg = json_encode($notificationMsgArray);
                    $this->SendAndroidNotification($notifyCat,$notifyMsg);

                    /*
                    ** Push notification for all apps in IOS
                    ** by pankaj : 29th august 2019
                    */

                    $notifyTitle = "New ".$titles." available ";
                    $notifyDesc  = strip_tags(Input::get('title'));
                    $this->SendIphoneNotification($notifyCat,$notifyMsg,$notifyTitle,$notifyDesc);
                }


        	}
        	catch(\ Exception $e)
        	{
        		DB::rollback();
        		$error_msg = $e->getMessage();
        		return redirect()->back()->withErrors($error_msg);
        	}
        	DB::commit();
        }
        return redirect('admin-panel/notification/view-notification')->withSuccess($success_msg);
    }
    public function data(Request $request)
    {
    	$notify 	= [];
    	$notify 	= Notification::orderBy('notification_id','DESC')->where(function($query) use ($request){
            if ($request->get('title')!= null){
                $query->where('notification_title','LIKE','%'.$request->get('title').'%');
            }
            if($request->get('category_id')!=null){
                $query->where('notification_flag',$request->get('category_id'));
            }
            if($request->get('sub_category_id')!=null){
                $query->where('sub_category_id',$request->get('sub_category_id'));
            }
        })->get();
    	return Datatables::of($notify)
            ->addColumn('category',function($notify){
                return $notify['getCategory']->iq_category_name;
            })->addColumn('sub_category',function($notify){
                return $notify['getSubCat']->title!=''?$notify['getSubCat']->title:'---';
            })->addColumn('image', function ($notify)
            {
                if ($notify->notification_image!=''){
                    $img  = "<img src='".url('uploads/'.$notify->notification_image)."' height='50' width='50' />";    
                }else{
                    $img  = "<img src='".url('public/admin/nopreview-available.jpg')."' height='50' width='50' />";
                }
                return "<div class='bg-info text-center'>".$img."</div>";
               
            })->addColumn('action',function($notify){
			$encrypted_notification_id = get_encrypted_value($notify->notification_id, true);
			return "
                &nbsp;
                <a href='add-notification/".$encrypted_notification_id."'><i class='fas fa-pencil-alt'></i></a>
                &nbsp;&nbsp; 
                <a href='delete-notification/".$encrypted_notification_id."' onclick='return confirm(".'"Are you sure?"'.")'><i class='fas fa-trash'></i></a>";
		})->rawColumns(['sub_category'=>'sub_category','category'=>'category','image'=>'image','action'=>'action'])->addIndexColumn()->make(true);
    }
    public function status(Request $request)
    {
    	$id = $request['trick_id'];
        $status = $request['trick_status'];
        
        $trick    = Notification::find($id);
        if (!empty($trick))
        {
            $trick->tip_status  = $status;
            $trick->save();
            echo "Success";
        }
        else
        {
            echo "Fail";
        }
    }
    public function destroy($id)
    {
    	$trick_id = get_decrypted_value($id, true);
        $trick    = Notification::find($trick_id);
        if ($trick)
        {
            if (file_exists($trick->tip_image)){
                unlink($trick->tip_image);
            }
            $trick->delete();
            $success_msg = "Tip-Trick deleted successfully!";
            return redirect('admin-panel/tips-tricks/view-tricks')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Tip-Trick not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }
}
