<?php

namespace App\Http\Controllers\adminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
 use App\Model\Pages\Pages;
 
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use File;
use Image;
class PagesController extends Controller
{
    public function  __construct(){
        $this->middleware('subadmin');
    }
    public function index($id=null)
    {
     
        $loginInfo                  = get_loggedin_user_data();
        
        $pageid     = get_decrypted_value($id, true);


        $masterpagesdata            =masterpages($pageid);
    

        $save_url      = url('admin-panel/pages/'.$id);

        $data = array(
            'page_title'    => $masterpagesdata->pages_title,
            'redirect_url'  => url('admin-panel/pages/'),
            'login_info'    => $loginInfo,
            'save_url'      => $save_url,
            'masterPages'=>$masterpagesdata
            
        );

        
        return view('admin-panel.master-pages.add')->with($data);

    }

    public function updateData(Request $request,$id){

        
            
        $loginInfo = get_loggedin_user_data();
        $decrypted_pages_id = get_decrypted_value($id, true);
        if (!empty($id))
        {

        
     
            $Pages = Pages::find($decrypted_pages_id);
            if (!$Pages)
            {
                return redirect('admin-panel/pages/'.$id)->withError('Pages not found!');
            }
            $success_msg = 'Pages updated successfully!';
            $nameValidator = 'required|unique:master_pages,pages_title,'.$decrypted_pages_id.',master_pages_id';
            
        }
        else
        {
            $Pages     = New Pages;
            $success_msg = 'Pages saved successfully!';
            $nameValidator = 'required|unique:master_pages';
        }
        $validatior = Validator::make($request->all(), [
                'pages_title'        => $nameValidator,
                'master_pages_id'=>'required'
                
        ]);


        if ($validatior->fails())
        {


            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {
           
            DB::beginTransaction();
            try
            {
                
               

                $Pages->pages_title        = Input::get('pages_title');
                $Pages->pages_short_desc = Input::get('pages_short_desc');
                $Pages->pages_long_desc        = htmlentities(Input::get('pages_long_desc'));
                $Pages->pages_meta_title        = htmlentities(Input::get('pages_meta_title'));
                $Pages->pages_meta_description        = htmlentities(Input::get('pages_meta_description'));
                $Pages->pages_meta_keyword        = htmlentities(Input::get('pages_meta_keyword'));
                $image              = $request->file('image');

                    if ( !empty($image) ){
                        $ext                           = substr($image->getClientOriginalName(),-4);
                        $name                          = substr($image->getClientOriginalName(),0,-4);
                        $filename                      = $decrypted_pages_id.$ext;
                        $config_pages_images_path      = \Config::get('custom.pages_pic');
            $destinationPath               = public_path() . $config_pages_images_path['upload_path'];
     $destinationThumbPath          = public_path() . $config_pages_images_path['upload_thumb_path'];

                        $input['imagename'] = $filename;

                        $img = Image::make($image->getRealPath(),array(
                            'width' => 100,
                            'height' => 100,
                            'grayscale' => false
                        ));

                        if (!File::exists($destinationThumbPath)) {
                            File::makeDirectory($destinationThumbPath,0777,true);
                        }else{
                            unlink($destinationThumbPath.$Pages->image);
                            
                        }

                        if (!File::exists($destinationPath)) {
                            File::makeDirectory($destinationPath,0777,true);
                        }else{
                            unlink($destinationPath.$Pages->image);

                        }

                        $img->save($destinationThumbPath.$filename);
                        $image->move($destinationPath, $filename);
                        $Pages->image = $filename;
                    }
                $Pages->save();
            }
            catch (\Exception $e)
            {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }
        return redirect('admin-panel/pages/'.$id)->withSuccess('success', $success_msg);



    }




}