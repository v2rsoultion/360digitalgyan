<?php

namespace App\Http\Controllers\adminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Category\Category;
use App\Model\QuesAndAns\QuesAndAns;
use DB;
use App\Model\Tags\Tags;
use Validator;
use Illuminate\Support\Facades\Input;
use Yajra\Datatables\Datatables;
use App\Model\SeoUser\SeoUser;
use App\Model\UsersAnswers\UsersAnswers;

class PostedQueAnsController extends Controller
{
	public function posted_ques(Request $request)
	{
		$appends = array(
    		'question' => $request['title'],
    		'category_id' => $request['category_id'],
    	);
    	$query = QuesAndAns::Query();

    	if ($request['title']!=''){
    		$query->where('user_question','LIKE','%'.$request['title'].'%');
    	}

    	if ($request['category_id']!=''){
    		$query->where('ques_main_cat_id','LIKE','%'.$request['category_id'].'%');
    	}

    	$postedQues = $query->paginate(20)->appends($appends);

		$data = array(
			'postedQues'	=> $postedQues,
			'Categories'		=> Category::where('iq_category_status',1)->get(),
    		'page_title' 	=> trans('language.view_posted_ques'),
    		'redirect_url' 	=> url('admin-panel/posted-ques-ans/view-posted-ques'),
    	);
    	return view('admin-panel.posted-ques-ans.view_posted_ques')->with($data);
	}
	public function posted_ans(Request $request,$id=false)
	{		
		if ($id!="") 
		{
			$id 	= get_decrypted_value($id,true);
			$query = UsersAnswers::Query();
            $postedAns = $query->where('ans_ques_id',$id)->paginate(20); 
			$data = array(
				'postedAns'		=> $postedAns,
				'Categories'		=> Category::where('iq_category_status',1)->get(),
	    		'page_title' 	=> trans('language.posted_ans'),
	    		'redirect_url' 	=> url('admin-panel/posted-ques-ans/view-posted-ans'),
	    	);		
		}
		else
		{
			$appends = array(
    		'answer' => $request['answer'],
    		'category_id' => $request['category_id'],
	    	);
	    	$query = UsersAnswers::Query();

	    	if ($request['answer']!=''){
	    		$query->where('ques_answer','LIKE','%'.$request['answer'].'%');
	    	}

	    	if ($request['category_id']!=''){
	    		$query->where('main_ans_cat','LIKE','%'.$request['category_id'].'%');
	    	}

	    	$postedAns = $query->paginate(20)->appends($appends);
			$data = array(
				'postedAns'		=> $postedAns,
				'Categories'		=> Category::where('iq_category_status',1)->get(),
	    		'page_title' 	=> trans('language.posted_ans'),
	    		'redirect_url' 	=> url('admin-panel/posted-ques-ans/view-posted-ans'),
	    	);
    	}
    	return view('admin-panel.posted-ques-ans.view_posted_ans')->with($data);	
	}
     public function add($id=false)
     {
     	if (!empty($id)) 
    	{
    		$decrypted_id = get_decrypted_value($id, true);
            $ques              = QuesAndAns::Find($decrypted_id);
            if (!$ques)
            {
                return redirect('admin-panel/posted-ques-ans/add-posted-ques')->withError('Question not found!');
            }

            $ques 			= QuesAndAns::where('ques_id',$decrypted_id)->first();
            $page_title 	= trans('language.edit_posted_ques');
            $save_url 		= url('admin-panel/posted-ques-ans/save/'.$id);
            $submit_button 	= "update";
    	}
    	else
    	{
    		$ques      = [];
            $page_title    = trans('language.add_posted_ques');
        	$submit_button	="save";
            $save_url       = url('admin-panel/posted-ques-ans/save/');
    	}
    		$data=array(
        		'page_title'	=> $page_title,
        		'submit_button' 	=> $submit_button,
                'ques'     => $ques,
                'categories'		=> Category::where('iq_category_status',1)->get(),
                'save_url'      => $save_url,
                'redirect_url'      => url('admin-panel/posted-ques-ans/view-posted-ques'),
        	);
    	return view('admin-panel.posted-ques-ans.add')->with($data);
     }
     public function save(Request $request,$id=false)
     {
     	$alltags 	= Tags::pluck('tags')->toArray();
     	
     	$questags 	= explode(',',$request['tags']);

     
     	foreach ($questags as $tag) 
     	{
     		
     		if (!in_array($tag,$alltags)) 
     		{
     			$newTag = New Tags;
     			DB::beginTransaction();
	            try
	            {
	            	$newTag->tags_cat 	= 	Input::get('ques_cat_id');
	            	$newTag->tags  		= 	$tag;
	            	$newTag->save(); 
	            }
	            catch (\Exception $e)
		        {
		            DB::rollback();
		            $error_message = $e->getMessage();
		            return redirect()->back()->withErrors($error_message);
		        }
		        DB::commit();
     		}
     	}
        if(!empty($id))
        {
        	$decrypted_id 	= get_decrypted_value($id,true);
            $postedQues = QuesAndAns::find($decrypted_id);
            if (!$postedQues)
            {
                return redirect('admin-panel/posted-ques-ans/view-posted-ques')->withError('Question not found not found!');
            }
            $success_msg = 'Question updated successfully!';
            $nameValidator = 'required|unique:user_questions,user_question,' . $decrypted_trick_id . ',ques_id';
        }
        else
        {
            $postedQues     = New QuesAndAns;
            $success_msg = 'Question saved successfully!';
            $nameValidator = 'required|unique:user_questions,user_question';
        }
        $validator = Validator::make($request->all(), [
            'question'        => $nameValidator,
        ]);
        if ($validator->fails())
        {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        else
        {
            DB::beginTransaction();
            try
            {
            	$postedQues->ques_main_cat_id			= Input::get('ques_cat_id');
	            $postedQues->user_question 				= Input::get('question');
	            $postedQues->question_tags				= Input::get('tags');
	            $postedQues->view_question_count 		= 0;
	            $postedQues->ques_user_id				= 9764;
	            $postedQues->popular 					= 0;
	            $postedQues->save();
            }
            catch (\Exception $e)
            {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }

        return redirect('admin-panel/posted-ques-ans/view-posted-ques')->withSuccess($success_msg);
     }
     public function ques_data(Request $request)
     {
     	$postedQues = QuesAndAns::get();
     	return Datatables::of($postedQues)->addColumn('question', function ($postedQues){
                return "<div style='max-height:100px;overflow:scroll'>".getTextTransform($postedQues->user_question,0)."</div>";
            })->addColumn('category',function($postedQues){
            	return $postedQues['getCategory']->iq_category_name;
            })->addColumn('user',function($postedQues){
            	return $postedQues['getUser']->seo_users_name;
            })->addColumn('tags',function($postedQues){
            	return "<div style='max-width:100px;max-height:100px;overflow:scroll'>".getTextTransform($postedQues->question_tags,0)."</div>";
            })->addColumn('answer',function(){
            	return "&nbsp;
            		<a href=''>give answer</a>";
            })->addColumn('total',function($postedQues){
            	return count($postedQues['answers']);
            })->addColumn('date',function($postedQues){
            	return $postedQues->ques_time;
            })->addColumn('trending',function($postedQues){
            	if ($postedQues->trending==1) 
            	{
            		$trending = 0;
            		$radio_clss = 'radio-success';
            	}
            	else
            	{
            		$trending = 1;
            		$radio_clss = 'radio-danger';
            	}
            	return "<div class='radio $radio_clss custom_radiobox'><input type='radio' onClick='changetrend(".$postedQues['ques_id'].",".$trending.")' name='trendradio".$postedQues['ques_id']."' id='trendradio".$postedQues['ques_id']."' value='option4' checked=''></a>
                    <label for='trendradio".$postedQues['ques_id']."'> </label>
                	</div>";

            })->addColumn('action',function($postedQues){
			$encrypted_id = get_encrypted_value($postedQues->ques_id, true);
			return "
                &nbsp;
                <a href='add-posted-ques/".$encrypted_id."'><i class='fas fa-pencil-alt'></i></a>
                &nbsp;&nbsp; 
                <a href='posted-ques-delete/".$encrypted_id."' onclick='return confirm(".'"Are you sure?"'.")'><i class='fas fa-trash'></i></a>";
			})->addColumn('status',function($postedQues){
                if ($postedQues->question_status==1){
                    $status = 0;
                    $radio_cls = 'radio-success';
                }
                else
                {
                    $status = 1;
                    $radio_cls = 'radio-danger';
                }
                return "<div class='radio $radio_cls custom_radiobox'><input type='radio' onClick='changeStatus(".$postedQues['ques_id'].",".$status.")' name='vradio".$postedQues['ques_id']."' id='vradio".$postedQues['ques_id']."' value='option4' checked=''></a>
                    <label for='vradio".$postedQues['ques_id']."'> </label>
                </div>";
			})->rawColumns(['question'=>'question','category'=>'category','trending'=>'trending','date'=>'date','total'=>'total','user'=>'user','tags'=>'tags','answer'=>'answer','action'=>'action','status'=>'status'])->addIndexColumn()->make(true);
    }

    public function destroy($id)
    {
    	$postedQues_id = get_decrypted_value($id, true);
        $postedQues    = QuesAndAns::find($postedQues_id);
        if ($postedQues)
        {
            $postedQues->delete();
            $success_msg = "Question deleted successfully!";
            return redirect('admin-panel/posted-ques-ans/view-posted-ques')->withSuccess($success_msg);
        }else{

            $error_message = "Question not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    public function status(Request $request)
	{
	    $id 			= $request->get('ques_id');
	    $status 		= $request->get('ques_status');        
	    $postedQues  	= QuesAndAns::find($id);
	    if($postedQues)
	    {
	        $postedQues->question_status  = $status;           
	        $postedQues->save();
	        echo "Success";
	    }else{
	        echo "Fail";
	    }
	}

    public function trending(Request $request)
    {
     	$id 			= $request->get('trend_id');
	    $trend 			= $request->get('trend_status');        
	    $postedQues  	= QuesAndAns::find($id);
	    if($postedQues)
	    {
	        $postedQues->trending  = $trend;           
	        $postedQues->save();
	        echo "Success";
	    }else{
	        echo "Fail";
	    }
    }
    public function add_ans($id=false,$ans=false)
    {
    	$decrypted_id = get_decrypted_value($id, true);
    	if (!empty($ans)) 
    	{
    		$ans_id = get_decrypted_value($ans, true);
            $answer              = UsersAnswers::find($ans_id);
            if (!$answer)
            {
                return redirect('admin-panel/posted-ques-ans/view-posted-ans')->withError('Answer not found!');
            }
            echo $answer 		= UsersAnswers::where('answer_id',$ans_id)->first();
            $page_title 	= trans('language.edit_posted_ans');
            $save_url 		= url('admin-panel/posted-ques-ans/ans-save/'.$ans);
            $submit_button 	= "update";
    	}
    	else
    	{
    		$answer 		    = [];
            $page_title 	    = trans('language.add_posted_ans');
        	$submit_button		="save";
            $save_url      		 = url('admin-panel/posted-ques-ans/ans-save/');
    	}
    		$que 				= QuesAndAns::find($decrypted_id);
    		$data=array(
        		'page_title'	=> $page_title,
        		'submit_button' => $submit_button,
                'answer'     	=> $answer,
                'que'			=> $que,
                'save_url'      => $save_url,
                'redirect_url'  => url('admin-panel/posted-ques-ans/view-posted-ans'),
        	);
    	return view('admin-panel.posted-ques-ans.add_ans')->with($data);	
    }
    public function save_ans(Request $request,$id=false)
    {
    	 if(!empty($id))
        {
        	$decrypted_id 	= get_decrypted_value($id,true);
            $postedAns 	= UsersAnswers::find($decrypted_id);
            if (!$postedAns)
            {
                return redirect('admin-panel/posted-ques-ans/view-posted-ans')->withError('Answer not found not found!');
            }
            $success_msg = 'Answer updated successfully!';
            $nameValidator = 'required|unique:user_answers,ques_answer,' .$decrypted_id . ',answer_id';
        }
        else
        {
            $postedAns     = New UsersAnswers;
            $success_msg = 'Answer saved successfully!';
            $nameValidator = 'required|unique:user_answers,ques_answer';
        }
        $validator = Validator::make($request->all(), [
            'answer'        => $nameValidator,
        ]);
        if ($validator->fails())
        {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        else
        {
            DB::beginTransaction();
            try
            {
            	$postedAns->ans_ques_id				= Input::get('ques_id');
            	$postedAns->main_ans_cat			= Input::get('ans_cat');
	            $postedAns->ques_answer 			= Input::get('answer');
	            $postedAns->ans_user_id				= 25324;
	            $postedAns->answer_status 			= 1;
	            $postedAns->save();
            }
            catch (\Exception $e)
            {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }

        return redirect('admin-panel/posted-ques-ans/view-posted-ans')->withSuccess($success_msg);	
    }
    public function delete_ans($id)
    {
    	$decrypted_id  = get_decrypted_value($id, true);
        $postedAns    = UsersAnswers::find($decrypted_id);
        if ($postedAns)
        {
            $postedAns->delete();
            $success_msg = "Answer deleted successfully!";
            return redirect('admin-panel/posted-ques-ans/view-posted-ans')->withSuccess($success_msg);
        }else{

            $error_message = "Answer not found!";
            return redirect()->back()->withErrors($error_message);
        }	
    }
    public function status_ans(Request $request)
	{
	    $id 			= $request->get('answer_id');
	    $status 		= $request->get('answer_status');        
	    $postedAns  	= UsersAnswers::find($id);
	    if($postedAns)
	    {
	        $postedAns->answer_status  = $status;           
	        $postedAns->save();
	        echo "Success";
	    }else{
	        echo "Fail";
	    }
	}
}
