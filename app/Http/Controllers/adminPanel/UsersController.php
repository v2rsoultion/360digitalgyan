<?php

namespace App\Http\Controllers\adminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\SeoUser\SeoUser;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use Yajra\Datatables\Datatables;

class UsersController extends Controller
{
	/**
     *  Users for view page
     *  @pankaj on 12th March 2019
    **/
	public function index(Request $request)
    {   
    	// dd($request);
    	$appends = array(
    		'user_name' => $request['user_name'],
    		'user_mail' => $request['user_mail'],
    	);

    	$query = SeoUser::Query();

    	if ($request['user_name']!=''){
    		$query->where('seo_users_name','LIKE','%'.$request['user_name'].'%');
    	}

    	if ($request['user_mail']!=''){
    		$query->where('seo_users_email','LIKE','%'.$request['user_mail'].'%');
    	}

    	$users = $query->orderBy('user_points','DESC')->paginate(20)->appends($appends);
        
        $data = array(
            'page_title'    => trans('language.seo_users'),
            'redirect_url'  => url('admin-panel/view-seo-users'),
            'users' 		=> $users
        );
        
        return view('admin-panel.users.index')->with($data);
    }


    /**
     *  Get Users for view page(Datatables)
     *  @pankaj on 12th March 2019
    **/
    public function anyData(Request $request)
    {
        $users = [];
        
        
        return Datatables::of($users)->addColumn('user_status', function ($category)
            {
                if($category['iq_category_status'] == 0) {
                    $status = 1;
                    $statusValIq = '<div class="radio radio-danger custom_radiobox">
                    <input type="radio" name="iqradio'.$category['iq_category_id'].'" onClick="changeStatusIQCategory('.$category['iq_category_id'].','.$status.')" id="iqradio'.$category['iq_category_id'].'" value="option4" checked="">
                    <label for="iqradio'.$category['iq_category_id'].'"> </label>
                </div>';
                }
                else {
                    $status = 0;
                    $statusValIq = '<div class="radio radio-success custom_radiobox">
                        <input type="radio" name="iqradio'.$category['iq_category_id'].'" onClick="changeStatusIQCategory('.$category['iq_category_id'].','.$status.')" id="iqradio'.$category['iq_category_id'].'" value="option4" checked="">
                        <label for="iqradio'.$category['iq_category_id'].'"> </label>
                    </div>';    
                }
                return $statusValIq;
               
            })->rawColumns(['user_status'=>'user_status'])->addIndexColumn()->make(true);
    }

    /**
     *  Change users's status
     *  @pankaj on 8th March 2019
    **/
    public function changeStatus(Request $request)
    {
        $id = $request->get('user_id');
        $status = $request->get('user_status');
                
        $user    = SeoUser::find($id);
        if ($user)
        {
            $user->seo_users_status  = $status;
            $user->save();
            echo "Success";
        }
        else
        {
            echo "Fail";
        }
    }
}
