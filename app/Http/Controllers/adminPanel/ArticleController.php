<?php

namespace App\Http\Controllers\adminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\ArticleCategory\ArticleCategory;
use App\Model\Article\Article;

use Yajra\Datatables\Datatables;

class ArticleController extends Controller
{
    /**
     *  View page for category
     *  @Shree on 23 Aug 2018
    **/
    public function  __construct(){
        $this->middleware('subadmin');
    }
    public function index()
    {
     
        $loginInfo                  = get_loggedin_user_data();
        
        $article = [];
        $arr_articlecategory        = ArticleCategory::where('status', "=", 1)->get();


        $data = array(
            'page_title'    => trans('language.view_article'),
            // 'redirect_url'  => url('admin-panel/category/view-category'),
            'redirect_url'  => url('admin-panel/article/view-article'),
            'login_info'    => $loginInfo,
            'category'      => $arr_articlecategory
        );

        
        return view('admin-panel.article.index')->with($data);

    }

    /**
     *  Add page for Category
     *  @Pratyush on 20 July 2018
    **/
    public function add(Request $request, $id = NULL)
    {
     

        $data    		            = [];
        $article                   = [];
        $loginInfo 		            = get_loggedin_user_data();
        $arr_articlecategory        = ArticleCategory::where('status', "=", 1)->get();
        if (!empty($id))
        {
            $articles_id 	    = get_decrypted_value($id, true);
            $article      		= Article::Find($articles_id);
            if (!$article)
            {
                return redirect('admin-panel/article-category/add-article')->withError('category not found!');
            }


            $arr_Article        = Article::where('articles_id', "=", $articles_id)->first();
           
           
            $page_title             	= trans('language.edit_article');
            $encrypted_article_id= get_encrypted_value($article->articles_id, true);
            $save_url               	= url('admin-panel/article/save/' . $encrypted_article_id);
            $submit_button          	= 'Update';

        }
        else
        {
            $arr_Article  = [];
            $page_title    = trans('language.add_article');
            $save_url      = url('admin-panel/article/save/');
            $submit_button = 'Save';
        }


        // $article['arr_category']   = add_blank_option($arr_Article, 'Select Category');

        $data   = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'category' 		    => $arr_articlecategory,
            'login_info'    	=> $loginInfo,
            'article'=> $arr_Article,
            'redirect_url'  	=> url('admin-panel/article/view-article'),
        );
        return view('admin-panel.article.add')->with($data);
    }
    
    /**
     *  Save category data
     *  @Shree on 24 Aug 2018
    **/
    public function save(Request $request, $id = NULL)
    {
       

        $loginInfo = get_loggedin_user_data();
        $decrypted_article_id = get_decrypted_value($id, true);
        if (!empty($id))
        {

           
            $article = Article::find($decrypted_article_id);
            if (!$article)
            {
                return redirect('admin-panel/article/view-article/')->withError('Article not found!');
            }
            $success_msg = 'Article updated successfully!';
            $nameValidator = 'required|unique:article,title,' . $decrypted_article_id . ',articles_id';
            
        }
        else
        {
            $article     = New Article;
            $success_msg = 'Article saved successfully!';
            $nameValidator = 'required|unique:article';
        }
        $validatior = Validator::make($request->all(), [
                'title'        => $nameValidator,
                'article_category_id'=>'required'
                
        ]);


        if ($validatior->fails())
        {


            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {

            DB::beginTransaction();
            try
            {
               
                $article->title        = Input::get('title');
                $article->article_category_id = Input::get('article_category_id');
                $article->short_desc        = htmlentities(Input::get('short_desc'));
                $article->long_desc        = htmlentities(Input::get('long_desc'));
                
                $article->save();
            }
            catch (\Exception $e)
            {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }
        return redirect('admin-panel/article/view-article')->withSuccess('success', $success_msg);
    }

    /**
     *  Get Data for view page(Datatables)
     *  @Shree on 23 Aug 2018
    **/
    public function anyData(Request $request)
    {

        $article     = [];
      
        $article = Article::where(function($query) use ($request) 
        {
            if (!empty($request) && !empty($request->has('category_id')) && $request->get('category_id') != null)
            {

                $query->where('article_category_id', "=", $request->get('category_id'));
            }
            if (!empty($request) && !empty($request->has('title')) && $request->get('title') != null )
            {
                $query->where('title', 'like', '%' . $request->get('title'). '%');
            }
        })->with('getCategoryInfo')->get();
      
        $article = $article;

        return Datatables::of($article)
            ->addColumn('category_name', function ($article)
            {
               return $article['getCategoryInfo']->category_title;
            })
            ->addColumn('short_desc', function ($article)
            {
               return  htmlspecialchars_decode($article->short_desc);
            })
            ->addColumn('long_desc', function ($article)
            {
               return htmlspecialchars_decode($article->long_desc);
            })
            ->addColumn('action', function ($article)
            {
                 $encrypted_article_id = get_encrypted_value($article['articles_id'], true);
                 if($article['status'] == 0) {
                    $status = 1;
                    $statusVal = '<div class="radio radio-danger custom_radiobox">
                    <input type="radio" name="radio'.$article['articles_id'].'" onClick="changeStatus('.$article['articles_id'].','.$status.')" id="radio'.$article['articles_id'].'" value="option4" checked="">
                    <label for="radio'.$article['articles_id'].'"> </label>
                </div>';
                }
                else {
                    $status = 0;
                    $statusVal = '<div class="radio radio-success custom_radiobox">
                        <input type="radio" name="radio'.$article['articles_id'].'" onClick="changeStatus('.$article['articles_id'].','.$status.')" id="radio'.$article['articles_id'].'" value="option4" checked="">
                        <label for="radio'.$article['articles_id'].'"> </label>
                    </div>';
                }
                return '
                '.$statusVal.'
                &nbsp;
                <a href="add-article/' . $encrypted_article_id . '" "><i class="fas fa-pencil-alt"></i></a>
                &nbsp;&nbsp; 
                <a href="article-delete/' . $encrypted_article_id . '" onclick="return confirm('."'Are you sure?'".')" "><i class="fas fa-trash"></i></a> ';
               
            })->rawColumns(['action' => 'action','short_desc'=>'short_desc','long_desc'=>'long_desc'])->addIndexColumn()->make(true);
    }


    /**
     *  Change Category's status
     *  @Shree on 24 Aug 2018
    **/
    public function changeStatus(Request $request)
    {


        $id = $request->get('articles_id');
        $status = $request->get('status');
        $article    = Article::find($id);
        if ($article)
        {
            $article->status  = $status;
            $article->save();
            echo "Success";
        }
        else
        {
            echo "Fail";
        }
    }

     /**
     *  Destroy category data
     *  @Shree on 24 Aug 2018
    **/
    public function destroy($id)
    {
        $article_id = get_decrypted_value($id, true);
        $article    = Article::find($article_id);
        if ($article)
        {
            $article->delete();
            $success_msg = "Category deleted successfully!";
            return redirect('admin-panel/article/view-article')->withSuccess('success', $success_msg);
        }
        else
        {
            $error_message = "Article not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }
}
