<?php

namespace App\Http\Controllers\adminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\PracticeQuiz\PracticeQuiz;
use App\Model\Category\Category;
use App\Model\SubCategory\SubCategory;
use Yajra\Datatables\Datatables;

class PracticeQuizQuestionController extends Controller
{
    public function  __construct(){
        $this->middleware('subadmin');
    }

    public function index(Request $request)
    {
        $data = array(
            'page_title'    => trans('language.view_practice_questions'),
            'redirect_url'  => url('admin-panel/practice-quiz-questions/view-practice-questions'),
            'Categories'		=> Category::where('iq_category_status',1)->get(),
            'subCategories'		=> SubCategory::where('status',1)->get(),
        );
        return view('admin-panel.practice_question.index')->with($data);
    }

    /**
     *  Add page for quiz questions
     *  @Pankaj on 19th March 2019
    **/
    public function add(Request $request, $id = NULL)
    {
        if (!empty($id))
        {
            $decrypted_q_id 	= get_decrypted_value($id, true);
            $interviewQues      = PracticeQuiz::Find($decrypted_q_id);
            if(!$interviewQues)
            {
            	return redirect('admin-panel/practice-quiz-questions/add-practice-questions')->withError('Practice Question not found!');
            }

            $practiceQuiz 	     	= PracticeQuiz::where('qustion_id',$decrypted_q_id)->first();
            $page_title   		= trans('language.edit_practice_questions');
            $encrypted_q_id 	= get_encrypted_value($interviewQues->iq_ques_ans_id,true);
            $save_url          	= url('admin-panel/practice-quiz-questions/save/'.$id);
            $submit_button     	= 'Update';
        }
        else
        {
            $practiceQuiz  = [];
            $page_title    = trans('language.add_practice_questions');
            $save_url      = url('admin-panel/practice-quiz-questions/save/');
            $submit_button = 'Save';
        }

        $data   = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'quizQues' 	        => $practiceQuiz,
            'categories'		=> Category::where('iq_category_status',1)->get(),
            'subCategories'		=> SubCategory::where('status',1)->get(),
            'redirect_url'  	=> url('admin-panel/practice-quiz-questions/view-practice-question'),
        );
        return view('admin-panel.practice_question.add')->with($data);
    }
    
    /**
     *  Save Quiz Question data
     *  @pankaj on 19th March 2019
    **/

    // Save Quiz Question data
    // @hameed on 7th May 2019
    
    public function save(Request $request, $id = NULL)
    { 
        $decrypted_q_id = get_decrypted_value($id, true);
        if (!empty($id))
        {
            $practiceQuiz = PracticeQuiz::find($decrypted_q_id);
            if (!$practiceQuiz)
            {
                return redirect('admin-panel/practice-quiz-questions/view-practice-questions/')->withError('Quiz Question not found!');
            }
            foreach ($request['questionset'] as $q)
            {
                if($q['imgquestion']!="")
                {
                    if ($q['imgquestion']!=$practiceQuiz->qustion_qustion) 
                    {
                        unlink($practiceQuiz->qustion_qustion);
                    } 
                }
                if ($q['imgselect']!="") 
                {
                    if($q['imgselect']!=$practiceQuiz->question_section_part)
                    {
                        unlink($practiceQuiz->question_section_part);
                    }
                }
                if($q['option1']!="")
                {
                    if($q['option1']!=$practiceQuiz->qustion_option1)
                    {
                        unlink($practiceQuiz->qustion_option1);
                    }
                }
                if($q['option2']!="")
                {
                    if($q['option2']!=$practiceQuiz->qustion_option2)
                    {
                        unlink($practiceQuiz->qustion_option2);
                    }
                }
                if($q['option3']!="")
                {
                    if($q['option3']!=$practiceQuiz->qustion_option3)
                    {
                        unlink($practiceQuiz->qustion_option3);
                    }
                }
                if($q['option4']!="")
                {
                    if($q['option4']!=$practiceQuiz->qustion_option4)
                    {
                        unlink($practiceQuiz->qustion_option4);
                    }
                }
                if($q['option5']!="")
                {
                    if($q['option5']!=$practiceQuiz->qustion_option5)
                    {
                        unlink($practiceQuiz->qustion_option5);
                    }
                }
            }   
        }
        else
        {
                $practiceQuiz     = New PracticeQuiz;
        }
        DB::beginTransaction();
        try
        {
        foreach ($request['questionset'] as $question)
        {
            $practiceQuiz->qustion_sec_id       = Input::get('qustion_sec_id');
            $practiceQuiz->qustion_sub_sec_id   = Input::get('qustion_sub_sec_id');
            $practiceQuiz->code_flag            = Input::get('code_flag');
            if ($question['textimg']=='text')
            {
                $practiceQuiz->qustion_qustion = $question['textquestion'];
            }
            else
            {
                // dd($question['imgquestion']);
                $file   = $question['imgquestion'];
                if($file != "")
                {
                    if($file!=$practiceQuiz->qustion_qustion)
                    {
                        $destinationPath = 'uploads/Quiz_Question_image/';
                        $imageName = $destinationPath.rand().time().'.'.$file->getClientOriginalExtension();
                        $file->move($destinationPath,$imageName);
                        $practiceQuiz->qustion_qustion = $imageName;
                    }
                }
            }

            if ($question['detailtextimg']=='text') 
            {
                $practiceQuiz->question_section_part =  $question['textselect'];
            }
            else
            {
                $file1   = $question['imgselect'];
                if($file1 != "")
                {
                    if ($file1!=$practiceQuiz->question_section_part) 
                    {
                        $destinationPath = 'uploads/Quiz_Question_image/';
                        $imageName1 = $destinationPath.rand().time().'.'.$file1->getClientOriginalExtension();
                        $file1->move($destinationPath,$imageName1);
                        $practiceQuiz->question_section_part = $imageName1;
                    }
                }
            }
            if ($question['option1_radio']=='text') 
            {
                $practiceQuiz->qustion_option1 = $question['option1'];
            }
            else
            {
                $file2  = $question['option1'];
                if($file2 != "")
                {
                    if ($file2!=$practiceQuiz->qustion_option1) 
                    {
                        $destinationPath = 'uploads/Quiz_Question_image/';
                        $imageName2 = $destinationPath.rand().time().'.'.$file2->getClientOriginalExtension();
                        $file2->move($destinationPath,$imageName2);
                        $practiceQuiz->qustion_option2 = $imageName1;
                    }
                }
            }
            if ($question['option2_radio']!='img') 
            {
                $practiceQuiz->qustion_option2 = $question['option2'];
            }
            else
            {
                $file3  = $question['option2'];
                if($file3 != "")
                {
                    if ($file3!=$practiceQuiz->qustion_option1) 
                    {
                        $destinationPath = 'uploads/Quiz_Question_image/';
                        $imageName3 = $destinationPath.rand().time().'.'.$file3->getClientOriginalExtension();
                        $file3->move($destinationPath,$imageName3);
                        $practiceQuiz->qustion_option2 = $imageName3;
                    }
                }
            }
            if ($question['option3_radio']=='text') 
            {
                $practiceQuiz->qustion_option3 = $question['option3'];
            }
            else
            {
                $file4   = $question['option3'];
                if($file4 != "")
                {
                    if ($file4!=$practiceQuiz->qustion_option3) 
                    {
                        $destinationPath = 'uploads/Quiz_Question_image/';
                        $imageName4 = $destinationPath.rand().time().'.'.$file4->getClientOriginalExtension();
                        $file4->move($destinationPath,$imageName4);
                        $practiceQuiz->qustion_option3 = $imageName4;
                    }
                }
            }
            if ($question['option4_radio']=='text') 
            {
                $practiceQuiz->qustion_option4 = $question['option4'];
            }
            else
            {
                $file5   = $question['option4'];
                if($file5 != "")
                {
                    if ($file5!=$practiceQuiz->qustion_option4) 
                    {
                        $destinationPath = 'uploads/Quiz_Question_image/';
                        $imageName5 = $destinationPath.rand().time().'.'.$file5->getClientOriginalExtension();
                        $file5->move($destinationPath,$imageName5);
                        $practiceQuiz->qustion_option4 = $imageName5;
                    }
                }
            }
            if ($question['option5_radio']=='text') 
            {
                $practiceQuiz->qustion_option5 = $question['option5'];
            }
            else
            {
                $file6   = $question['option5'];
                if($file6 != "")
                {
                    if ($file6!=$practiceQuiz->qustion_option5) 
                    {
                        $destinationPath = 'uploads/Quiz_Question_image/';
                        $imageName6 = $destinationPath.rand().time().'.'.$file6->getClientOriginalExtension();
                        $file6->move($destinationPath,$imageName6);
                        $practiceQuiz->qustion_option5 = $imageName6;
                    }
                }
            }
            $practiceQuiz->qustion_correct_answer   = $question['correct'];
            $practiceQuiz->question_order           = 1 ;
            $practiceQuiz->is_new                   = Input::get('is_new')?Input::get('is_new'):0;
            $practiceQuiz->save();
        }
        }
        catch (\Exception $e)
        {
            DB::rollback();
            $error_message = $e->getMessage();
            return redirect()->back()->withErrors($error_message);
        }
        DB::commit();
        $success_msg = 'Quiz Question  saved successfully!';
        return redirect('admin-panel/practice-quiz-questions/view-practice-questions')->withSuccess($success_msg);
    }

    /**
     *  Get Data for view page(Datatables)
     *  @pankaj on 18th March 2019
    **/
    public function anyData(Request $request)
    {
        $practiceQuiz = [];
        $practiceQuiz = PracticeQuiz::orderBy('qustion_id','DESC')->where(function($query) use ($request){
            if ($request->get('category_id')!=''){
                $query->where('qustion_sec_id',$request->get('category_id'));
            }

            if ($request->get('sub_category_id')!=''){
                $query->where('qustion_sub_sec_id',$request->get('sub_category_id'));
            }

        })->with('getCategory')->with('getSubCategory')->get();
        return Datatables::of($practiceQuiz)
            ->addColumn('action', function ($practiceQuiz){
                 $encrypted_q_id = get_encrypted_value($practiceQuiz['qustion_id'], true);
                return '&nbsp;
                <a href="add-practice-questions/' . $encrypted_q_id . '" "><i class="fas fa-pencil-alt"></i></a>
                &nbsp;&nbsp; 
                <a href="practice-questions-delete/' . $encrypted_q_id . '" onclick="return confirm('."'Are you sure?'".')" "><i class="fas fa-trash"></i></a> ';
               
            })->addColumn('category',function($practiceQuiz){
                return $practiceQuiz['getCategory']->iq_category_name;
            })->addColumn('sub_category',function($practiceQuiz){
                return $practiceQuiz['getSubCategory']->title;
                
            })->addColumn('status', function ($practiceQuiz){
                if($practiceQuiz['question_trial']==0) {
                    $status = 1;
                    $radio_cls = 'radio-danger';
                }
                else {
                    $status = 0;
                    $radio_cls = 'radio-success';
                }
                return "<div class='radio $radio_cls custom_radiobox'>
                        <input type='radio' name='iqradio".$practiceQuiz['qustion_id']."' onClick='changeStatus(".$practiceQuiz["qustion_id"].",".$status.")' id='iqradio".$practiceQuiz["qustion_id"]."' value='option4' checked=''>
                        <label for='iqradio".$practiceQuiz["qustion_id"]."'> </label>
                    </div>";
               
            })->addColumn('new', function ($practiceQuiz){
            	return $practiceQuiz->is_new==1 ? "New" : "Old";
            })->addColumn('order', function ($practiceQuiz)
            {
                $order  = "<input class='nums' name='order[]' data-id='".$practiceQuiz->qustion_id."' onchange='changeOrder(this)' min='0' type='number' value='".$practiceQuiz->question_order."' type='number' /> ";
                return $order;
            })->addColumn('category',function($practiceQuiz){
            	return $practiceQuiz['getCategory']->iq_category_name;
            })->addColumn('subcategory',function($practiceQuiz){
            	return $practiceQuiz['getSubCategory']->title;
            })->addColumn('question',function($practiceQuiz){
            	return getTextTransform($practiceQuiz->qustion_qustion,0);
            })->addColumn('answer',function($practiceQuiz){
            	return "<div style='max-height:100px;overflow:scroll'>".getTextTransform($practiceQuiz->question_section_part,0)."</div>";
            })->rawColumns(['action' => 'action','status'=>'status','question'=>'question','order'=>'order','answer'=>'answer','category'=>'category','sub_category'=>'sub_category'])->addIndexColumn()->make(true);
            // addColumn('detail',function($practiceQuiz){
            //     return "<div style='max-height:100px;overflow:scroll'>".getTextTransform($practiceQuiz->qustion_option1,0)."</div>";
            // })->
    }

    public function detailData($id)
    {
        $detail = PracticeQuiz::where('qustion_id',$id)->first();

        return view('admin-panel.practice_question.detail', compact('detail'));
    }


    /**
     *  Change PracticeQuiz status
     *  @pankaj on 19th March 2019
    **/
    public function changeStatus(Request $request)
    {
        $id             = $request->get('interview_id');
        $status         = $request->get('interview_status');        
        $practice_quiz  = PracticeQuiz::find($id);
        if($practice_quiz)
        {
            $practice_quiz->question_trial  = $status;           
            $practice_quiz->save();
            echo "Success";
        }else{
            echo "Fail";
        }
    }

    /**
     *  Destroy quiz data
     *  @Pankaj on 19th March 2019
    **/
    public function destroy($id)
    {
        $practiceQuiz_id = get_decrypted_value($id, true);
        $practiceQuiz    = PracticeQuiz::find($practiceQuiz_id);
        if ($practiceQuiz)
        {
            $practiceQuiz->delete();
            $success_msg = "Quiz Question deleted successfully!";
            return redirect('admin-panel/practice-quiz-questions/view-practice-questions')->withSuccess($success_msg);
        }else{

            $error_message = "Quiz Question not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }

    public function getsubcats($id)
    {
    	$options = "<option value=''>Select Sub Category</option>";
    	$subCategories = SubCategory::where('category_id',$id)->get();

    	foreach ($subCategories as $cat) {
    		$options.= "<option value='".$cat->sub_category_id."'> $cat->title </option>";
    	}

    	return response()->json(['data'=>$options]);
    }


    /**
     *  Change inter's Order
     *  @pankaj on 8th March 2019
    **/
    public function changeOrder(Request $request)
    {
        $id     = $request->get('quiz_id');
        $order  = $request->get('quiz_order');
        $practiceQuiz   = PracticeQuiz::find($id);
        if($practiceQuiz)
        {
            $practiceQuiz->question_order  = $order;
            $practiceQuiz->save();
            echo "Success";
        }
        else
        {
            echo "Fail";
        }
    }
}
