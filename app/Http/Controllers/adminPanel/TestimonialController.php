<?php

namespace App\Http\Controllers\adminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Validator;
use Illuminate\Support\Facades\Input;
use App\Model\Testimonials\Testimonials;
use Yajra\Datatables\Datatables;

class TestimonialController extends Controller
{
    public function index()
    {
    	$data = array(
    		'page_title' 	=> trans('language.view_testimonial'),
    		'redirect_url' 	=> url('admin-panel/testimonils/view-testimonials'),
    	);
    	return view('admin-panel.testimonials.index')->with($data);
    }
    public function add(Request $request,$id=false)
    {
    	if (!empty($id)) 
    	{
    		$decrypted_testimonial_id = get_decrypted_value($id, true);
            $testimonial              = Testimonials::Find($decrypted_testimonial_id);
            
            if (!$testimonial)
            {
                return redirect('admin-panel/testimonials/add-testimonials')->withError('video not found!');
            }

            $testimonial = Testimonials::where('testimonial_id', "=", $decrypted_testimonial_id)->first();
            $page_title                 = trans('language.edit_testimonial');
            $encrypted_testimonial_id      = get_encrypted_value($testimonial->testimonial_id,true);
            $save_url                   = url('admin-panel/testimonials/save/'.$encrypted_testimonial_id);
            $submit_button              = 'Update';
    	}
    	else
    	{
    		$video      = [];
            $page_title    = trans('language.add_testimonial');
        	$submit_button	="save";
            $save_url       = url('admin-panel/testimonials/save/');
    	}
    		$data=array(
        		'page_title'	=> $page_title,
        		'submit_button' 	=> $submit_button,
                'testimonial'     => $testimonial,
                'save_url'      => $save_url,
                'redirect_url'      => url('admin-panel/testimonials/view-testimonials'),
        	);
    	return view('admin-panel.testimonials.add')->with($data);
    }
    public function data(Request $request)
    {
        $testimonial = [];
        $testimonial = Testimonials::orderBy('testimonial_id','DESC')->where(function($query) use ($request){
            if ($request->get('title')!= null){
                $query->where('testimonial_name','LIKE','%'.$request->get('title').'%');
            }
        })->get();
        return Datatables::of($testimonial)
            ->addColumn('description', function ($testimonial){
                return "<div>".getTextTransform($testimonial->testimonial_description,0)."</div>";

            })->addColumn('image', function ($testimonial)
            {
                if ($testimonial->testimonial_imgs!=''){
                    $img  = "<img src='".url('uploads/'.$testimonial->testimonial_imgs)."' height='50' width='50' />";    
                }else{
                    $img  = "<img src='".url('public/admin/nopreview-available.jpg')."' height='50' width='50' />";
                }
                return "<div class='text-center'>".$img."</div>";
               
            })->addColumn('action',function($testimonial){
                $encrypted_testimonial_id = get_encrypted_value($testimonial->testimonial_id, true);
                return "
                &nbsp;
                <a href='add-testimonials/".$encrypted_testimonial_id."'><i class='fas fa-pencil-alt'></i></a>
                &nbsp;&nbsp; 
                <a href='testimonials-delete/".$encrypted_testimonial_id."' onclick='return confirm(".'"Are you sure?"'.")'><i class='fas fa-trash'></i></a>";
               
            })->addColumn('status',function($testimonial){
                $encrypted_testimonial_id = get_encrypted_value($testimonial->testimonial_id, true);
                if ($testimonial->testimonial_status==1){
                    $status = 0;
                    $radio_cls = 'radio-success';
                }
                else
                {
                    $status = 1;
                    $radio_cls = 'radio-danger';
                }
                return "<div class='radio $radio_cls custom_radiobox'><input type='radio' onClick='changeStatus(".$testimonial['testimonial_id'].",".$status.")' name='vradio".$testimonial['testimonial_id']."' id='vradio".$testimonial['testimonial_id']."' value='option4' checked=''></a>
                    <label for='vradio".$testimonial['testimonial_id']."'> </label>
                </div>";
            })->rawColumns(['description'=>'description','image'=>'image','action' => 'action','status' => 'status'])->addIndexColumn()->make(true);
    }
    public function save(Request $request,$id = false)
    {
        $decrypted_testimonial_id = get_decrypted_value($id, true);
        if(!empty($id))
        {
            $testimonial = Testimonials::find($decrypted_testimonial_id);
            if (!$testimonial)
            {
                return redirect('admin-panel/testimonials/view-testimonials/')->withError('Testimonial not found!');
            }
            $success_msg = 'Testimonial updated successfully!';
            $nameValidator = 'required|unique:testimonial,testimonial_name,' . $decrypted_testimonial_id . ',testimonial_id';
        }
        else
        {
            $testimonial     = New Testimonials;
            $success_msg = 'Testimonial saved successfully!';
            $nameValidator = 'required|unique:testimonial,testimonial_name';
        }
        $validator = Validator::make($request->all(), [
            'testimonial_name'        => $nameValidator,
        ]);
        if ($validator->fails())
        {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        else
        {
            DB::beginTransaction();
            try
            {
                $file = $request->file('testimonial_image');
                if($file != "")
                {   
                    if(file_exists('uploads/'.$testimonial->testimonial_imgs))
                    {
                        unlink('uploads/'.$testimonial->testimonial_imgs);
                    }

                    $destinationPath = 'uploads/testimonial_image/';
                    $imageName = 'testimonial_image/'.rand().time().'.'.$file->getClientOriginalExtension();
                    $file->move($destinationPath,$imageName);
                    $testimonial->testimonial_imgs = $imageName;
                }
                $testimonial->testimonial_name              = Input::get('testimonial_name');
                $testimonial->testimonial_description        = Input::get('testimonial_des');
                $testimonial->save();
            }
            catch (\Exception $e)
            {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }

        return redirect('admin-panel/testimonials/view-testimonials')->withSuccess($success_msg);
    }
    public function status(Request $request)
    {
        $id = $request['testimonial_id'];
        $status = $request['testimonial_status'];
        
        $testimonial    = Testimonials::find($id);
        if (!empty($testimonial))
        {
            $testimonial->testimonial_status  = $status;
            $testimonial->save();
            echo "Success";
        }
        else
        {
            echo "Fail";
        }
    }
    public function destroy($id)
    {
        $testimonial_id = get_decrypted_value($id, true);
        $testimonial    = Testimonials::find($testimonial_id);
        if ($testimonial)
        {
            if (file_exists($testimonial->testimonial_imgs)){
                unlink($testimonial->testimonial_imgs);
            }
            $testimonial->delete();
            $success_msg = "Testimonial deleted successfully!";
            return redirect('admin-panel/testimonials/view-testimonials')->withSuccess($success_msg);
        }
        else
        {
            $error_message = "Testimonial not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }
}
