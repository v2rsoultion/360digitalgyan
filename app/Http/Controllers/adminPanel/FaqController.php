<?php

namespace App\Http\Controllers\adminPanel;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\Faq\Faq;

use Yajra\Datatables\Datatables;
class FaqController extends Controller
{
    //
    public function  __construct(){
        $this->middleware('subadmin');
    }

    public function index(){
     	 $loginInfo                  = get_loggedin_user_data();
        
       
        $data = array(
            'page_title'    => trans('language.view_article_categories'),
            // 'redirect_url'  => url('admin-panel/category/view-category'),
            'redirect_url'  => url('admin-panel/faq/view-faq'),
            'login_info'    => $loginInfo,
            
        );

        
        return view('admin-panel.faq.index')->with($data);
    }


    public function add(Request $request, $id = NULL)
    {


        $data    		            = [];
        
        $loginInfo 		            = get_loggedin_user_data();
        if (!empty($id))
        {
            $decrypted_faq_id 	    = get_decrypted_value($id, true);
            $faq      		= Faq::Find($decrypted_faq_id);
            if (!$decrypted_faq_id)
            {
                return redirect('admin-panel/faq/add-faq')->withError('category not found!');
            }


            $arr_faq       = Faq::where('faq_id', "=", $decrypted_faq_id)->first();
           
            $page_title             	= trans('language.edit_article_category');
            $encrypted_faq_id= get_encrypted_value($arr_faq->faq_id, true);
            $save_url               	= url('admin-panel/faq/save/' . $encrypted_faq_id);
         
            $submit_button          	= 'Update';

        }
        else
        {
            $arr_faq = [];
            $page_title    = trans('language.add_faq');
            $save_url      = url('admin-panel/faq/save/');
            $submit_button = 'Save';
        }
       

        // $faq['arr_category']   = add_blank_option($arr_Faq, 'Select Category');

        $data   = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'faq' 		    => $arr_faq,
            'login_info'    	=> $loginInfo,
            'redirect_url'  	=> url('admin-panel/faq/view-faq'),
        );
        return view('admin-panel.faq.add')->with($data);
    }


    public function save(Request $request, $id = NULL)
    {
      
        $loginInfo = get_loggedin_user_data();
        $faq_id = get_decrypted_value($id, true);
        if (!empty($id))
        {


            $faq = Faq::find($faq_id);
            if (!$faq)
            {
                return redirect('admin-panel/faq/view-faq/')->withError('Faq not found!');
            }
            $success_msg = 'Category updated successfully!';
            $nameValidator = 'required|unique:article_category,category_title,' . $faq_id . ',article_category_id';
        }
        else
        {
            $faq     = New Faq;
            $success_msg = 'Category saved successfully!';
            $nameValidator = 'required|unique:article_category';
        }
        
        $validatior = Validator::make($request->all(), [
             
                
        ]);


        if ($validatior->fails())
        {

            return redirect()->back()->withInput()->withErrors($validatior);
        }
        else
        {

            DB::beginTransaction();
            try
            {
               
                $faq->faq_question        = Input::get('faq_question');
                $faq->faq_answer        = Input::get('faq_answer');
                $faq->save();
            }
            catch (\Exception $e)
            {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }
        return redirect('admin-panel/faq/view-faq')->withSuccess('success', $success_msg);
    }


    public function anyData(Request $request)
    {

        $faq     = [];
      
        $faq = Faq::where(function($query) use ($request) 
        {
           
            if (!empty($request) && !empty($request->has('faq_question')) && $request->get('faq_question') != null )
            {
                $query->where('faq_question', 'like', '%' . $request->get('faq_question'). '%');
            }
        })->get();
      
        $faq = $faq;

        return Datatables::of($faq)
           
            ->addColumn('faq_question', function ($faq)
            {
               return  htmlspecialchars_decode($faq->faq_question);
            })
            ->addColumn('faq_answer', function ($faq)
            {
               return htmlspecialchars_decode($faq->faq_answer);
            })
            ->addColumn('action', function ($faq)
            {
                 $encrypted_faq_id = get_encrypted_value($faq['faq_id'], true);
                 if($faq['status'] == 0) {
                    $status = 1;
                    $statusVal = '<div class="radio radio-danger custom_radiobox">
                    <input type="radio" name="radio'.$faq['faq_id'].'" onClick="changeStatus('.$faq['faq_id'].','.$status.')" id="radio'.$faq['faq_id'].'" value="option4" checked="">
                    <label for="radio'.$faq['faq_id'].'"> </label>
                </div>';
                }
                else {
                    $status = 0;
                    $statusVal = '<div class="radio radio-success custom_radiobox">
                        <input type="radio" name="radio'.$faq['faq_id'].'" onClick="changeStatus('.$faq['faq_id'].','.$status.')" id="radio'.$faq['faq_id'].'" value="option4" checked="">
                        <label for="radio'.$faq['faq_id'].'"> </label>
                    </div>';
                }
                return '
                '.$statusVal.'
                &nbsp;
                <a href="add-faq/' . $encrypted_faq_id . '" "><i class="fas fa-pencil-alt"></i></a>
                &nbsp;&nbsp; 
                <a href="faq-delete/' . $encrypted_faq_id . '" onclick="return confirm('."'Are you sure?'".')" "><i class="fas fa-trash"></i></a> ';
               
            })->rawColumns(['action' => 'action','faq_question'=>'faq_question','faq_answer'=>'faq_answer'])->addIndexColumn()->make(true);
    }
    public function changeStatus(Request $request)
    {
        $id = $request->get('faq_id');
        $status = $request->get('status');
        $faq    = Faq::find($id);
        if ($faq)
        {
            $faq->status  = $status;
            $faq->save();
            echo "Success";
        }
        else
        {
            echo "Fail";
        }
    }

     /**
     *  Destroy category data
     *  @Shree on 24 Aug 2018
    **/
    public function destroy($id)
    {
        $faq_id = get_decrypted_value($id, true);
        $faq    = Faq::find($faq_id);
        if ($faq)
        {
            $faq->delete();
            $success_msg = "Faq deleted successfully!";
            return redirect('admin-panel/faq/view-faq')->withSuccess('success', $success_msg);
        }
        else
        {
            $error_message = "faq not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }



}
