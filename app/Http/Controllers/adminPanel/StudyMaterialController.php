<?php

namespace App\Http\Controllers\adminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\StudyMaterial\StudyMaterial;
use App\Model\Category\Category;
use DB;
use Validator;
use Illuminate\Support\Facades\Input;
use Yajra\Datatables\Datatables;
use App\Model\SubCategory\SubCategory;
use App\Model\AllQuesComments\AllQuesComments;

use App\Model\Notification\Notification;
use App\Traits\PushNotification;

class StudyMaterialController extends Controller
{
    use PushNotification;

    public function index()
    {
    	$data = array(
    		'page_title' 	=> trans('language.view_study_material'),
    		'redirect_url' 	=> url('admin-panel/study-material/view-study-material'),
    		'Categories'        => Category::where('iq_category_status',1)->get(),
            'subCategories'     => SubCategory::where('status',1)->get(),
    	);
    	return view('admin-panel.study_material.index')->with($data);
    }
    public function add($id=false)
    {
    	if (!empty($id)) 
    	{
    		$decrypted_id = get_decrypted_value($id, true);
        $study              = StudyMaterial::Find($decrypted_id);
        if (!$study)
        {
            return redirect('admin-panel/study-material/add-study-material')->withError('Study Material not found!');
        }

        $study = StudyMaterial::where('study_material_id', "=", $decrypted_id)->first();
        $page_title 	= trans('language.edit_study_material');
        $save_url 		= url('admin-panel/study-material/save/'.$id);
        $submit_button 	= "update";
    	}
    	else
    	{
    		$Study      = [];
            $page_title    = trans('language.add_study_material');
        	$submit_button	="save";
            $save_url       = url('admin-panel/study-material/save/');
    	}
    		$data=array(
        		'page_title'	=> $page_title,
        		'submit_button' => $submit_button,
                'study' 	    => $study,
                'categories'	=> Category::where('iq_category_status',1)->get(),
            	'subCategories'	=> SubCategory::where('status',1)->get(),
                'save_url'      => $save_url,
                'redirect_url'  => url('admin-panel/study-material/view-study-material'),
        	);
    	return view('admin-panel.study_material.add')->with($data);
    }
    public function data(Request $request)
    {
    	$study 	= [];
    	$study 	= StudyMaterial::where(function($query) use ($request){
            if ($request->get('title')!= null){
                $query->where('study_ques','LIKE','%'.$request->get('title').'%');
            }

            if ($request->get('category_id')!=null) {
            	$query->where('study_material_catid',$request->get('category_id'));
            }
            
            if ($request->get('sub_category_id')!=null) {
            	$query->where('sub_cat_id',$request->get('sub_category_id'));
            }
        })->orderBy('study_material_id','DSEC')->get();
    	return Datatables::of($study)->addColumn('question', function ($study){
                return "<div style='max-width:150px;max-height:100px;overflow:scroll'>".getTextTransform($study->study_ques,0)."</div>";
            })->addColumn('answer', function ($study){
                return "<div style='max-width:150px;max-height:100px;overflow:scroll'>".getTextTransform($study->study_ques_ans,0)."</div>";
            })->addColumn('new',function($study){
            	if($study->is_new==1){
            		$new = "New";
            	}
            	else
            	{
            		$new = "-";
            	}
            	return $new;
            })->addColumn('category',function($study){
                return $study['getCategory']->iq_category_name;
            })->addColumn('sub_category',function($study){
                return $study['getSubCategory']->title;
            })->addColumn('image', function ($study)
            {
                if ($study->study_image!=''){
                    $img  = "<img src='".url('uploads/'.$study->study_image)."' height='50' width='50' />";    
                }else{
                    $img  = "<img src='".url('public/admin/nopreview-available.jpg')."' height='50' width='50' />";
                }
                return "<div class='bg-info text-center'>".$img."</div>";
               
            })->addColumn('likes',function($study){
            	return count($study['getLikes']);
            })->addColumn('views',function($study){
            	return count($study['getViews']);
            })->addColumn('cmt',function($study){
            	$encrypted_study_id = get_encrypted_value($study->study_material_id, true);
            	return "<a href='".url('admin-panel/view-all-comments/study/'.$encrypted_study_id)."'>".count($study['getComments'])."<a>";
            })->addColumn('action',function($study){
			$encrypted_study_id = get_encrypted_value($study->study_material_id, true);
			return "
                &nbsp;
                <a href='add-study-material/".$encrypted_study_id."'><i class='fas fa-pencil-alt'></i></a>
                &nbsp;&nbsp; 
                <a href='study-material-delete/".$encrypted_study_id."' onclick='return confirm(".'"Are you sure?"'.")'><i class='fas fa-trash'></i></a>";
		})->addColumn('status',function($study){
                if ($study->study_ques_ans_status==1){
                    $status = 0;
                    $radio_cls = 'radio-success';
                }
                else
                {
                    $status = 1;
                    $radio_cls = 'radio-danger';
                }
                return "<div class='radio $radio_cls custom_radiobox'><input type='radio' onClick='changeStatus(".$study['study_material_id'].",".$status.")' name='vradio".$study['study_material_id']."' id='vradio".$study['study_material_id']."'  checked></a>
                    <label for='vradio".$study['tip_id']."'> </label>
                </div>";
		})->rawColumns(['question'=>'question','answer'=>'answer','views'=>'views','likes'=>'likes','cmt'=>'cmt','sub_category'=>'sub_category','category'=>'category','image'=>'image','action'=>'action','status'=>'status'])->addIndexColumn()->make(true);
    }
    public function save(Request $request,$id=false)
    {
    	$decrypted_id = get_decrypted_value($id, true);
    	if(!empty($id)) 
    	{
    		$study = StudyMaterial::find($decrypted_id);
        if (!$study)
        {
            return redirect('admin-panel/study-material/view-study-material/')->withError('Study Material not found!');
        }
        $success_msg = 'Study Material updated successfully!';
        $nameValidator = 'required|unique:study_material,study_ques,' . $decrypted_id . ',study_material_id';
        if ($request['study_image']!="") 
        {
        	unlink($study->study_image);	
        }
    	}
    	else
    	{
    		$study 	= New StudyMaterial;
    		$success_msg = 'Study Material saved successfully';
			$nameValidator = 'required|unique:study_material,study_ques';
    	}
    	$validator = Validator::make($request->all(), [
            'question'        => $nameValidator,
        ]);
    	if ($validator->fails())
        {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        else
        {
        	DB::beginTransaction();
        	try
        	{
        		$file = $request->file('image');
                if($file != "")
                {
                    if(file_exists('uploads/'.$study->study_image))
                    {
                        unlink('uploads/'.$study->study_image);
                    }
                    $destinationPath = 'uploads/study_material_image/';
                    $imageName = 'study_material_image/'.rand().time().'.'.$file->getClientOriginalExtension();
                    $file->move($destinationPath,$imageName);
                    $study->study_image = $imageName;
                }
                $study->study_ques 			= Input::get('question');
                $study->study_ques_ans 		= Input::get('answer');
         		$study->study_metatitle 	= Input::get('title');
        		$study->study_material_catid  = Input::get('cat_id');
        		$study->study_material_slug   = str_slug(Input::get('question'));
        		$study->sub_cat_id	= Input::get('sub_id');
        		$study->study_metadesc 	= Input::get('desc');
        		$study->study_metakey 	= Input::get('keyword');
        		$study->coding 		= Input::get('coding');
        		$study->is_new 		= Input::get('is_new')==""? "0" : "1";
        		$study->save();

                /* only in insert case */
                if(empty($id))
                {
                    $notification = new Notification;
                    $notification->notification_title   = strip_tags(Input::get('question'));
                    $notification->sub_category_id      = Input::get('sub_id');
                    $notification->notification_for     = 5; //5=study materials
                    $notification->notification_flag    = Input::get('cat_id');
                    $notification->save();
                    $notifyId     = $notification->notification_id;
                    
                    $notifyCat = Input::get('cat_id');
                    /*
                    ** Push notification for all apps in Android
                    ** by pankaj : 29th august 2019
                    */
                    $notificationMsgArray = array(
                        "msg"               => "New study material available : ".strip_tags(Input::get('question')),
                        "image"             => $imageName!=''? $imageName : "",
                        "notification_id"   => $notifyId,
                        "notification_for"  => 5
                    );

                    $notifyMsg = json_encode($notificationMsgArray);
                    $this->SendAndroidNotification($notifyCat,$notifyMsg);

                    /*
                    ** Push notification for all apps in IOS
                    ** by pankaj : 29th august 2019
                    */

                    $notifyTitle = "New study material available";
                    $notifyDesc  = strip_tags(Input::get('question'));
                    $this->SendIphoneNotification($notifyCat,$notifyMsg,$notifyTitle,$notifyDesc);
                }

        	}
        	catch(\ Exception $e)
        	{
        		DB::rollback();
        		$error_msg = $e->getMessage();
        		return redirect()->back()->withErrors($error_msg);
        	}
        	DB::commit();
        }
        return redirect('admin-panel/study-material/view-study-material')->withSuccess($success_msg);
    }
    public function destroy($id)
    {
  		$study_id = get_decrypted_value($id, true);
      $study    = StudyMaterial::find($study_id);
      if ($study)
      {
          if (file_exists($study->study_image)){
              unlink($study->study_image);
          }
          $study->delete();
          $success_msg = "Study Material deleted successfully!";
          return redirect('admin-panel/study-material/view-study-material')->withSuccess($success_msg);
      }
      else
      {
          $error_message = "Study Material not found!";
          return redirect()->back()->withErrors($error_message);
      }	
    }
    public function status(Request $request)
    {
    	$id = $request['study_id'];
      $status = $request['study_status'];
      
      $study    = StudyMaterial::find($id);
      if (!empty($study))
      {
          $study->study_ques_ans_status  = $status;
          $study->save();
          echo "Success";
      }
      else
      {
          echo "Fail";
      }
    }

    // public function comments()
    // {
    // 	$data = array(
    // 		'page_title' 	=> trans('language.view_study_comments'),
    // 		'redirect_url' 	=> url('admin-panel/study-material/view-study-comment'),
    // 	);
    // 	return view('admin-panel.study_material.view_comments')->with($data);
    // }
   //  public function cmt_data()
   //  {
   //  	$comment 	= [];
   //  	$comment 	= AllQuesComments::where('comment_flag',2)->get();
   //  	return Datatables::of($comment)->addColumn('user',function($comment){
   //  		return $comment['getUser']->seo_users_name;
   //  	})->addColumn('cmt-for',function($comment){
   //  		return "<div style='max-height:100px;overflow:scroll'>".getTextTransform($comment['getQuestion']->user_question,0)."</div>";
   //  	})->addColumn('date',function($comment){
   //  		return $comment->comment_created;
   //  	})->addColumn('cmt', function ($comment){
   //      return "<div style='max-width:150px;max-height:100px;overflow:scroll'>".getTextTransform($comment->comment_comment,0)."</div>";
			// })->rawColumns(['date'=>'date','cmt'=>'cmt','user'=>'user','cmt-for'=>'cmt-for'])->addIndexColumn()->make(true);
   //  }
}
