<?php

namespace App\Http\Controllers\adminPanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Model\NewsCategory\NewsCategory;
use App\Model\News\News;
use Yajra\Datatables\Datatables;

use App\Model\Notification\Notification;
use App\Traits\PushNotification;

class NewsController extends Controller
{
    use PushNotification;
    public function  __construct(){
        $this->middleware('subadmin');
    }

    public function index()
    {   
        $data = array(
            'page_title'    => trans('language.view_news'),
            'redirect_url'  => url('admin-panel/news/view-news'),
        );
        return view('admin-panel.news.index')->with($data);
    }

    /**
     *  Add page for News
     *  @Pankaj on 18th March 2019
    **/
    public function add(Request $request, $id = NULL)
    {
        if (!empty($id))
        {
            $decrypted_news_id 	= get_decrypted_value($id, true);
            $news      			= News::Find($decrypted_news_id);
            if (!$news)
            {
                return redirect('admin-panel/news/add-news')->withError('news not found!');
            }

            $news 				= News::where('news_id',$decrypted_news_id)->first();
            $page_title   		= trans('language.edit_news');
            $encrypted_news_id 	= get_encrypted_value($news->news_id,true);
            $save_url          	= url('admin-panel/news/save/'.$encrypted_news_id);
            $submit_button     	= 'Update';
        }
        else
        {
            $news  = [];
            $page_title    = trans('language.add_news');
            $save_url      = url('admin-panel/news/save/');
            $submit_button = 'Save';
        }

        $data   = array(
            'page_title'    	=> $page_title,
            'save_url'      	=> $save_url,
            'submit_button' 	=> $submit_button,
            'news' 		    	=> $news,
            'newsCategories'	=> NewsCategory::where('news_category_status',1)->get(),
            'redirect_url'  	=> url('admin-panel/news/view-news'),
        );
        return view('admin-panel.news.add')->with($data);
    }
    
    /**
     *  Save news data
     *  @pankaj on 18th March 2019
    **/
    public function save(Request $request, $id = NULL)
    {
        $decrypted_news_id = get_decrypted_value($id, true);
        if (!empty($id))
        {
            $news = News::find($decrypted_news_id);
            if (!$news)
            {
                return redirect('admin-panel/news/view-news/')->withError('News not found!');
            }
            $success_msg = 'News updated successfully!';
            $nameValidator = 'required';
        }
        else
        {
            $news     = New News;
            $success_msg = 'News  saved successfully!';
            $nameValidator = 'required';
        }
        
        $validator = Validator::make($request->all(), [
                'news_title'        => $nameValidator,
        ]);

        if ($validator->fails())
        {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        else
        {
            DB::beginTransaction();
            try
            {
            	$file = $request->file('news_image');
                if($file != ""){
                    if(file_exists('uploads/'.$news->news_image))
                    {
                        unlink('uploads/'.$news->news_image);
                    }
                    $destinationPath = 'uploads/news_images/';
                    $imageName = 'news_images/'.rand().time().'.'.$file->getClientOriginalExtension();
                    $file->move($destinationPath,$imageName);
                    $news->news_image = $imageName;
                }

                $fileThumb = $request->file('news_thumbnail');
                if($fileThumb != ""){
                    if(file_exists('uploads/'.$news->news_thumbnail))
                    {
                        unlink('uploads/'.$news->news_thumbnail);
                    }
                    $destinationPath = 'uploads/news_images/thumbs/';
                    $imageNameThumb = 'news_images/thumbs/thumbs'.rand().time().'.'.$fileThumb->getClientOriginalExtension();
                    $fileThumb->move($destinationPath,$imageNameThumb);
                    $news->news_thumbnail = $imageNameThumb;
                }

                $news->news_title         = Input::get('news_title');
                $news->news_category_id   = Input::get('news_category_id');
                $news->news_description   = Input::get('news_description');
                $news->save();

                /* only in insert case */
                if (empty($id))
                {
                    /*
                    ** Push notification for all apps in Android
                    ** by pankaj : 29th august 2019
                    */
                    $notificationMsgArray = array(
                        "msg"   => "360 Digital Gyan News Alert : ".Input::get('news_title'),
                        "image" => $imageNameThumb!=''? $imageNameThumb : "",
                        "notification_id"   => 0,
                        "notification_for"  => 10
                    );

                    $notifyMsg = json_encode($notificationMsgArray);
                    $this->SendAndroidNotification('all',$notifyMsg);

                    /*
                    ** Push notification for all apps in IOS
                    ** by pankaj : 29th august 2019
                    */

                    $notifyTitle = "360 Digital Gyan News Alert";
                    $notifyDesc   = Input::get('news_title');
                    $this->SendIphoneNotification('all',$notifyMsg,$notifyTitle,$notifyDesc);
                }

            }
            catch (\Exception $e)
            {
                DB::rollback();
                $error_message = $e->getMessage();
                return redirect()->back()->withErrors($error_message);
            }
            DB::commit();
        }
        return redirect('admin-panel/news/view-news')->withSuccess($success_msg);
    }

    /**
     *  Get Data for view page(Datatables)
     *  @pankaj on 18th March 2019
    **/
    public function anyData(Request $request)
    {

        $news     = [];
        $news = News::orderBy('news_id','DESC')->where(function($query) use ($request){
            if ($request->get('news_title')!= null){
                $query->where('news_title','LIKE','%'.$request->get('news_title').'%');
            }
        })->get();
        
        return Datatables::of($news)
            ->addColumn('action', function ($news){
                 $encrypted_news_id = get_encrypted_value($news['news_id'], true);
                return '
                &nbsp;
                <a href="add-news/' . $encrypted_news_id . '" "><i class="fas fa-pencil-alt"></i></a>
                &nbsp;&nbsp; 
                <a href="news-delete/' . $encrypted_news_id . '" onclick="return confirm('."'Are you sure?'".')" "><i class="fas fa-trash"></i></a> ';
               
            })->addColumn('news_status', function ($news){
                if($news['news_status'] == 0) {
                    $status = 1;
                    $statusValIq = '<div class="radio radio-danger custom_radiobox">
                    <input type="radio" name="iqradio'.$news['news_id'].'" onClick="changeStatusNews('.$news['news_id'].','.$status.')" id="iqradio'.$news['news_id'].'" value="option4" checked="">
                    <label for="iqradio'.$news['news_id'].'"> </label>
                </div>';
                }
                else {
                    $status = 0;
                    $statusValIq = '<div class="radio radio-success custom_radiobox">
                        <input type="radio" name="iqradio'.$news['news_id'].'" onClick="changeStatusNews('.$news['news_id'].','.$status.')" id="iqradio'.$news['news_id'].'" value="option4" checked="">
                        <label for="iqradio'.$news['news_id'].'"> </label>
                    </div>';    
                }
                return $statusValIq;
               
            })->addColumn('image', function ($news){

                if ($news->news_image!=''){
                    $img  = "<img src='".url('uploads/'.$news->news_image)."' height='50' width='50' />";    
                }else{
                    $img  = "<img src='".url('public/admin/nopreview-available.jpg')."' height='50' width='50' />";
                }
                return "<div class='text-center'>".$img."</div>";

            })->addColumn('description', function ($news){
                return "<div style='max-height:100px;overflow:scroll'>".getTextTransform($news->news_description,0)."</div>";

            })->rawColumns(['action' => 'action','news_status'=>'news_status','image'=>'image','description'=>'description'])->addIndexColumn()->make(true);
    }


    /**
     *  Change News status
     *  @pankaj on 18th March 2019
    **/
    public function changeStatus(Request $request)
    {
        $id 		= $request->get('news_id');
        $status 	= $request->get('news_status');        
        $news   = News::find($id);
        if ($news)
        {
            $news->news_status  = $status;           
            $news->save();
            echo "Success";
        }
        else
        {
            echo "Fail";
        }
    }

    /**
     *  Destroy news data
     *  @Pankaj on 8th March 2019
    **/
    public function destroy($id)
    {
        $news_id = get_decrypted_value($id, true);
        $news    = News::find($news_id);
        if ($news)
        {
            $news->delete();
            $success_msg = "News deleted successfully!";
            return redirect('admin-panel/news/view-news')->withSuccess($success_msg);
        }else{

            $error_message = "News not found!";
            return redirect()->back()->withErrors($error_message);
        }
    }
}
