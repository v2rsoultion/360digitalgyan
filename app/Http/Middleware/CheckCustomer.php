<?php

    namespace App\Http\Middleware;

    use Closure;
    use Auth;

    class CheckCustomer
    {

        /**
         * Handle an incoming request.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  \Closure  $next
         * @return mixed
         */
        public function handle($request, Closure $next)
        {
            if (Auth::guard('web')->user())
            {
                $customer = Auth::guard('web')->user();
                if ($customer->status == 1)
                {
                    return $next($request);
                }
                else
                {
                    auth()->guard('web')->logout();
                    return redirect('/')->withErrors('account_inactive');
                }
            }
            else
            {
                // it will redirect to login popup
                return redirect('/')->withErrors('Login');
            }
        }

    }
    