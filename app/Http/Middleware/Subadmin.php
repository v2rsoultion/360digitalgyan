<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
class Subadmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$guard='admin')
    {
        if (Auth::guard($guard)->check()) {
           $data = Auth::guard($guard)->user();
           if($data->type==0){
               return redirect('admin-panel/dashboard');
           }
        }
        return $next($request);
    }
}
