<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('payment/status', 'frontend\CustomerPaymentController@paymentCallback');
Route::middleware('auth:apis')->get('/user', function (Request $request) {
    return $request->user();
});
/* get sub categories */
Route::any('get-sub-categories','Api\ApiController@get_sub_category');

/* get questions according sub category id */
Route::any('get-questions','Api\ApiController@get_questions');