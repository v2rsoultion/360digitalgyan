<?php


ini_set('MAX_EXECUTION_TIME', '-1');
ini_set('memory_limit', '-1');
error_reporting(0);

// use App\Mail\digitalgyanMail;
/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */
// Route::get('/',function(){
//   echo session('digital_user_id');
//   die;
// });

Route::any('truecaller-callback',function(){
    echo "hello";
});

Route::any('call',function(){
    return view('frontend.truecaller');
});

Route::get('/clear-cache', function()
{
    Artisan::call('cache:clear');
    Artisan::call('config:cache');
    return redirect('/');
    // return wherever you want
});

Route::get('user-mail',function(){
  // $emailData = (object) array(
  //   'mail'    => 'kunwarpankajdna27@gmail.com',
  //   'name'    => 'pankaj kasotiya',
  //   'subject'   => "Welcome to 360digitalgyan",
  // );
  // $view = 'email.360.reset-password';
  // $emailData = (object) array(
  //   'mail'    => 'kunwarpankajdna27@gmail.com',
  //   'name'    => 'pankaj kasotiya',
  //   'subject' => "Welcome to 360digitalgyan",
  // );
  // $data = compact('emailData');
  // Mail::to($emailData->mail)->send(new digitalgyanMail($emailData,$view));
  return view('email.360.reset-password',$data);
});


/*page not found*/
Route::get('page-not-found','frontend\HomeController@notfound');
/* User related routes */
Route::get('user/email-check','frontend\UserController@email_check');
Route::post('user/sign-up','frontend\UserController@register');


/* social login link */
Route::any('callback/{driver}','frontend\UserController@callback');
Route::get('login/{driver}','frontend\UserController@social');

Route::post('user/login','frontend\UserController@login');
Route::get('user/my-profile','frontend\UserController@profile');
Route::post('user/update-profile','frontend\UserController@update_profile');
Route::post('user/change-password','frontend\UserController@change_password');
Route::post('user/forgot-password','frontend\UserController@forgot_password');
Route::get('user/reset-password/{id}','frontend\UserController@reset_password');
Route::post('user/save-reset-password/{id}','frontend\UserController@change_reset_password');


Route::get('user/log-out','frontend\UserController@logout');
Route::post('user/get-quiz-result','frontend\UserController@quiz_result');
Route::post('user/practice-quiz','frontend\UserController@practice_quiz');
Route::post('user/save-answer','frontend\UserController@save_answer');
Route::post('user/save-question','frontend\UserController@save_question');

/* like dislike */
Route::any('user/like-question','frontend\UserController@like_dislike_ques');
Route::any('user/like-answer','frontend\UserController@like_dislike_ans');

/* IQ Comments and Like */
Route::post('user/like-interview-question','frontend\UserController@iq_like');
Route::post('user/save-comment','frontend\UserController@save_iq_comment');
Route::post('user/like-comment','frontend\UserController@iq_commnt_like');
Route::get('/key-trends','frontend\KeyTrendsController@index');
Route::get('/all-competition','frontend\CompetitionController@index');
Route::get('/questions-answers','frontend\QuestionsAnswerController@questions_answers');
Route::get('/findingspotting-errors','frontend\CompetitionController@finderrors');


/* home page*/
Route::get('/','frontend\HomeController@index');

/* routes for website apps pages */
$apps = array(
  'digital-marketing',
  'web-development',
  'software-testing',
  'plc-scada',
  'python',
  'java',
  'android'
);

foreach($apps as $app){
  Route::group(['prefix'=> $app],function($ap) use ($app){
    // echo $app;
    // die;
      Route::get('/',function(){
        return redirect('digital-marketing/videos-tutorials');
      });
      Route::any('/videos-tutorials/{slug?}','frontend\VideosTutorialsController@index');

      Route::get('interview-questions/{subcat?}','frontend\InterviewQuestionsController@index');

      Route::any('technical-terms/{subcat?}','frontend\TechnicalTermsController@index');

      Route::any('/questions-answers','frontend\QuestionsAnswerController@questions_answers');
      
      Route::get('/answer/{id}','frontend\QuestionsAnswerController@answer');

      Route::any('key-terms/{char?}','frontend\KeyTrendsController@index');

      Route::get('study-materials/{slug?}','frontend\StudyMaterialsController@index');

      Route::get('get-study-materials','frontend\StudyMaterialsController@getstudy');
      
      Route::any('practice-quiz/{slug?}','frontend\PracticeQuizController@index');

      Route::any('/quiz/{slug?}','frontend\QuizController@index');
  });
}

Route::prefix('admin-panel')->middleware('admin.guest')->group(function ()
{
    Route::get('/', 'AdminAuth\LoginController@showLoginForm')->name('login');
    Route::post('/login', 'AdminAuth\LoginController@login');
    Route::post('/logout', 'AdminAuth\LoginController@logout')->name('logout');

    Route::post('/password/email', 'AdminAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
    Route::post('/password/reset', 'AdminAuth\ResetPasswordController@reset')->name('password.email');
    Route::get('/password/reset', 'AdminAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
    Route::get('/password/reset/{token}', 'AdminAuth\ResetPasswordController@showResetForm');
});

Route::group(['prefix' => 'admin-panel', 'middleware' => 'admin'], function ()
{

/* all */
 Route::any('/view-all-comments/{slug}/{id?}','adminPanel\DashboardController@comments');
 Route::any('/comment-data/{slug}/{id?}','adminPanel\DashboardController@comments_data');

Route::get('/add-sub-admin', 'AdminAuth\RegisterController@showRegistrationForm')->name('register');
Route::post('/register', 'AdminAuth\RegisterController@register');
Route::get('/view-sub-admin', 'adminPanel\DashboardController@showsubadmin')->name('showsubadmin');

// For logout
Route::get('/logout', 'AdminAuth\LoginController@logout')->name('logout');

// For Dashboard
Route::get('/dashboard', 'adminPanel\DashboardController@index');
// Admin Profile

Route::get('/profile', 'adminPanel\ProfileController@index');
Route::post('/profile', 'adminPanel\ProfileController@update_profile');
Route::get('/change-password', ['as' => 'admin-panel.profile.change-password', 'uses' => 'AdminAuth\ChangePasswordController@index']);
Route::post('change-password', ['uses' => 'AdminAuth\ChangePasswordController@changePassword']);

Route::get('/data', 'adminPanel\DashboardController@anyData');


Route::get('/subadmins-delete/{id?}', 'adminPanel\DashboardController@destroy');

Route::get('subadmins-status/{status?}/{id?}', 'adminPanel\DashboardController@changeStatus');


// For Category module 
// On 11 March 2019 
// By @Pankaj
Route::group(['prefix'=>'category'],function(){
  Route::get('/add-category/{id?}', 'adminPanel\CategoryController@add');
  Route::post('/save/{id?}', 'adminPanel\CategoryController@save');
  Route::get('/view-categories', 'adminPanel\CategoryController@index');
  Route::get('/data', 'adminPanel\CategoryController@anyData');
  Route::get('/category-delete/{id?}', 'adminPanel\CategoryController@destroy');
  Route::get('/category-status/{status?}/{id?}/{type?}', 'adminPanel\CategoryController@changeStatus');
  Route::get('/category-order/{order?}/{id?}', 'adminPanel\CategoryController@changeOrder');
});

// For Sub Category module 
// On 12 March 2019 
// By @Pankaj
Route::group(['prefix'=>'sub-category'],function(){
  Route::get('/add-sub-category/{id?}', 'adminPanel\SubCategoryController@add');
  Route::post('/save/{id?}', 'adminPanel\SubCategoryController@save');
  Route::get('/view-sub-categories', 'adminPanel\SubCategoryController@index');
  Route::get('/data', 'adminPanel\SubCategoryController@anyData');
  Route::get('/sub-category-delete/{id?}', 'adminPanel\SubCategoryController@destroy');
  Route::get('/sub-category-status/{status?}/{id?}', 'adminPanel\SubCategoryController@changeStatus');
  Route::get('/sub-category-order/{order?}/{id?}', 'adminPanel\SubCategoryController@changeOrder');
});


// For News Category module 
// On 18 March 2019 
// By @Pankaj
Route::group(['prefix'=>'news-category'],function(){
  Route::get('/add-news-category/{id?}', 'adminPanel\NewsCategoryController@add');
  Route::post('/save/{id?}', 'adminPanel\NewsCategoryController@save');
  Route::get('/view-news-categories', 'adminPanel\NewsCategoryController@index');
  Route::get('/data', 'adminPanel\NewsCategoryController@anyData');
  Route::get('/news-category-delete/{id?}', 'adminPanel\NewsCategoryController@destroy');
  Route::get('/news-category-status/{status?}/{id?}/{type?}', 'adminPanel\NewsCategoryController@changeStatus');
});


// For News module 
// On 18 March 2019 
// By @Pankaj
Route::group(['prefix'=>'news'],function(){
  Route::get('/add-news/{id?}', 'adminPanel\NewsController@add');
  Route::post('/save/{id?}', 'adminPanel\NewsController@save');
  Route::get('/view-news', 'adminPanel\NewsController@index');
  Route::get('/data', 'adminPanel\NewsController@anyData');
  Route::get('/news-delete/{id?}', 'adminPanel\NewsController@destroy');
  Route::get('/news-status/{status?}/{id?}', 'adminPanel\NewsController@changeStatus');
});


// For marketing blog module 
// On 18 March 2019 
// By @Pankaj
Route::group(['prefix'=>'marketing-blog'],function(){
  Route::get('/add-marketing-blog/{id?}', 'adminPanel\MarketingBlogController@add');
  Route::post('/save/{id?}', 'adminPanel\MarketingBlogController@save');
  Route::get('/view-marketing-blog', 'adminPanel\MarketingBlogController@index');
  Route::get('/data', 'adminPanel\MarketingBlogController@anyData');
  Route::get('/detail-data/{id}','adminPanel\MarketingBlogController@detailData');
  Route::get('/marketing-blog-delete/{id?}', 'adminPanel\MarketingBlogController@destroy');
  Route::get('/marketing-blog-status/{status?}/{id?}', 'adminPanel\MarketingBlogController@changeStatus');
});

// For Videos Module
// 9 April 2019
//By @Hameed

Route::group(['prefix'=>'videos'],function(){
 Route::get('/add-videos/{id?}','adminPanel\VideosController@add');
 Route::post('/save/{id?}','adminPanel\VideosController@save');
 Route::get('/view-videos','adminPanel\VideosController@index');
 Route::get('/data','adminPanel\VideosController@data');
 Route::get('/video-delete/{id}','adminPanel\VideosController@destroy');
 Route::get('/video-status/{id?}/{status?}','adminPanel\VideosController@changeStatus');

});

Route::group(['prefix'=>'posted-ques-ans'],function(){
  Route::get('/add-posted-ques/{id?}','adminPanel\PostedQueAnsController@add');
  Route::post('/save/{id?}','adminPanel\PostedQueAnsController@save');
  Route::any('view-posted-ques','adminPanel\PostedQueAnsController@posted_ques');
  Route::get('/ques-data','adminPanel\PostedQueAnsController@ques_data');
  Route::get('/posted-ques-delete/{id?}','adminPanel\PostedQueAnsController@destroy');
  Route::get('/posted-ques-trend','adminPanel\PostedQueAnsController@trending');
  Route::get('/posted-ques-status','adminPanel\PostedQueAnsController@status');

  Route::get('/add-posted-ans/{id?}/{ans?}','adminPanel\PostedQueAnsController@add_ans');
  Route::post('/ans-save/{id?}','adminPanel\PostedQueAnsController@save_ans');
  Route::any('view-posted-ans/{id?}','adminPanel\PostedQueAnsController@posted_ans');
  Route::get('posted-ans-delete/{id?}','adminPanel\PostedQueAnsController@delete_ans');
  Route::get('posted-ans-status','adminPanel\PostedQueAnsController@status_ans');
});

//For Study Material Module
// 08 June 2019
// By @Hameed

Route::group(['prefix'=>'study-material'],function(){
  Route::get('/add-study-material/{id?}','adminPanel\StudyMaterialController@add');
  Route::post('/save/{id?}','adminPanel\StudyMaterialController@save');
  Route::get('/view-study-material','adminPanel\StudyMaterialController@index');
  Route::get('/data','adminPanel\StudyMaterialController@data');
  Route::get('/study-material-delete/{id}','adminPanel\StudyMaterialController@destroy');
  Route::get('/study-material-status/{id?}/{status?}','adminPanel\StudyMaterialController@status');
 
  Route::get('/cmt-data','adminPanel\StudyMaterialController@cmt_data');
});

//For Share Banner
// 11 June 2019
// By @Hameed

Route::group(['prefix'=>'share-banner'],function(){
  Route::get('/add-banner/{id?}','adminPanel\shareBannerController@add');
  Route::get('/view-banner','adminPanel\shareBannerController@index');
  Route::post('/save/{id?}','adminPanel\shareBannerController@save');
  Route::get('/data','adminPanel\shareBannerController@data');
  Route::get('/banner-status/{id?}/{status?}','adminPanel\shareBannerController@status');
  Route::get('/banner-delete/{id}','adminPanel\shareBannerController@destroy');
});

Route::group(['prefix'=>'seo-results'],function(){
  Route::get('/view-seo-results','adminPanel\SeoResultController@index');
  Route::get('/data','adminPanel\SeoResultController@data');
  Route::get('/result-delete/{id}','adminPanel\SeoResultController@destroy');
});

Route::group(['prefix'=>'seo-suggestion'],function(){
  Route::get('/view-seo-suggestion','adminPanel\SuggestionController@index');
  Route::get('/data','adminPanel\SuggestionController@data');
  Route::get('/suggestion-delete/{id}','adminPanel\SuggestionController@destroy');
});

//For Testimonial Module
// 16 April 2019
// By @Hameed

Route::group(['prefix'=>'testimonials'],function(){
 Route::get('/add-testimonials/{id?}','adminPanel\TestimonialController@add');
 Route::post('/save/{id?}','adminPanel\TestimonialController@save');
 Route::get('/view-testimonials','adminPanel\TestimonialController@index');
 Route::get('/data','adminPanel\TestimonialController@data');
 Route::get('/testimonials-delete/{id}','adminPanel\TestimonialController@destroy');
 Route::get('/testimonials-status/{id?}/{status?}','adminPanel\TestimonialController@status');
});

// For Tags
// On 19 April 2019
// By @Hameed

Route::group(['prefix'=>'tags'],function(){

  Route::get('/add-tag/{id?}','adminPanel\TagsController@add');
  Route::get('/data','adminPanel\TagsController@data');
  Route::get('/view-tags','adminPanel\TagsController@index');
  Route::post('/save/{id?}','adminPanel\TagsController@save');
  Route::get('/tag-delete/{id?}','adminPanel\TagsController@destroy');
  Route::get('/tag-status','adminPanel\TagsController@status');
});

// For Tips & Tricks
// On 20 April 2019
// By @Hameed

Route::group(['prefix'=>'tips-tricks'],function(){

  Route::get('/add-tricks/{id?}','adminPanel\TipsTricksController@add');
  Route::get('/view-tricks','adminPanel\TipsTricksController@index');
  Route::post('/save/{id?}','adminPanel\TipsTricksController@save');
  Route::get('/data','adminPanel\TipsTricksController@data');
  Route::get('/tricks-delete/{id?}','adminPanel\TipsTricksController@destroy');
  Route::get('/tricks-status','adminPanel\TipsTricksController@status');
});

// For Terms 
// On 29 April 2019
// By @Hameed

Route::group(['prefix'=>'terms'],function(){
  Route::get('/add-terms/{id?}','adminPanel\TermsController@add');
  Route::get('/view-terms','adminPanel\TermsController@index');
  Route::post('/save/{id?}','adminPanel\TermsController@save');
  Route::get('/data','adminPanel\TermsController@data');
  Route::get('/terms-delete/{id}','adminPanel\TermsController@destroy');
  Route::get('terms-status','adminPanel\TermsController@status');
});

// For Greetings Image
// On 03 May 2019
// By @Hameed

Route::group(['prefix'=>'greetings'],function(){
  Route::get('/add-greetings/{id?}','adminPanel\GreetingsController@add');
  Route::get('/view-greetings','adminPanel\GreetingsController@index');
  Route::post('/save/{id?}','adminPanel\GreetingsController@save');
  Route::get('/data','adminPanel\GreetingsController@data');
  Route::get('/greetings-delete/{id}','adminPanel\GreetingsController@destroy');
  Route::get('/greetings-status','adminPanel\GreetingsController@status');

}); 

// For practice-quiz-questions
// On 14 May 2019
//By @Hameed

Route::group(['prefix'=>'practice-quiz-questions'],function(){
  Route::get('/add-practice-questions/{id?}', 'adminPanel\PracticeQuizQuestionController@add');
  Route::post('/save/{id?}', 'adminPanel\PracticeQuizQuestionController@save');
  Route::get('/view-practice-questions', 'adminPanel\PracticeQuizQuestionController@index');
  Route::get('/data', 'adminPanel\PracticeQuizQuestionController@anyData');
  Route::get('/detail-data/{id}', 'adminPanel\PracticeQuizQuestionController@detailData');
  Route::get('/practice-questions-delete/{id?}', 'adminPanel\PracticeQuizQuestionController@destroy');
  Route::get('/practice-questions-status/{status?}/{id?}', 'adminPanel\PracticeQuizQuestionController@changeStatus');
  Route::get('/practice-questions-order/{order?}/{id?}','adminPanel\PracticeQuizQuestionController@changeOrder');
});

// For interview Questions module 
// On 19 March 2019 
// By @Pankaj
Route::group(['prefix'=>'interview-questions'],function(){
  Route::get('/add-interview-questions/{id?}', 'adminPanel\InterviewQuestionsController@add');
  Route::post('/save/{id?}', 'adminPanel\InterviewQuestionsController@save');
  Route::get('/view-interview-questions', 'adminPanel\InterviewQuestionsController@index');
  Route::get('/data', 'adminPanel\InterviewQuestionsController@anyData');
  Route::get('detail-data/{id}','adminPanel\InterviewQuestionsController@detailData');
  Route::get('/interview-questions-delete/{id?}', 'adminPanel\InterviewQuestionsController@destroy');
  Route::get('/interview-questions-status/{status?}/{id?}', 'adminPanel\InterviewQuestionsController@changeStatus');
  Route::get('/interview-questions-order/{order?}/{id?}','adminPanel\InterviewQuestionsController@changeOrder');
});


// For Quiz Questions module 
// On 23 March 2019 
// By @Pankaj
Route::group(['prefix'=>'quiz-questions'],function(){
  Route::get('/add-quiz-questions/{id?}', 'adminPanel\QuizQuestionsController@add');
  Route::post('/save/{id?}', 'adminPanel\QuizQuestionsController@save');
  Route::get('/view-quiz-questions', 'adminPanel\QuizQuestionsController@index');
  Route::get('/data', 'adminPanel\QuizQuestionsController@anyData');
  Route::get('/detail-data/{id}', 'adminPanel\QuizQuestionsController@detailData');
  Route::get('/quiz-questions-delete/{id?}', 'adminPanel\QuizQuestionsController@destroy');
  Route::get('/quiz-questions-status/{status?}/{id?}', 'adminPanel\QuizQuestionsController@changeStatus');
  Route::get('/quiz-questions-order/{order?}/{id?}','adminPanel\QuizQuestionsController@changeOrder');
});


//Notification Module
// 2nd sept. 2019
// By @pankaj
Route::group(['prefix'=>'notification'],function(){
 Route::get('/add-notification/{id?}','adminPanel\NotificationController@add');
 Route::post('/save-notification/{id?}','adminPanel\NotificationController@save');
 Route::get('/view-notification','adminPanel\NotificationController@index');
 Route::get('/data-notification','adminPanel\NotificationController@data');
 Route::get('/delete-notification/{id}','adminPanel\NotificationController@destroy');
 Route::get('/status-notification/{id?}/{status?}','adminPanel\NotificationController@status');
});


Route::get('get-sub-categories/{id}','adminPanel\InterviewQuestionsController@getsubcats');


Route::any('/view-seo-users','adminPanel\UsersController@index');
// Route::get('/view-users/data','adminPanel\UsersController@anyData');
Route::get('/view-users/staus/','adminPanel\UsersController@changeStatus');

// On 13 Dec 2018 
// By @pradeep

Route::get('/faq/add-faq/{id?}', 'adminPanel\FaqController@add');
Route::post('/faq/save/{id?}', 'adminPanel\FaqController@save');
Route::get('/faq/view-faq', 'adminPanel\FaqController@index');
Route::get('/faq/data', 'adminPanel\FaqController@anyData');
Route::get('/faq/faq-delete/{id?}', 'adminPanel\FaqController@destroy');
Route::get('/faq/faq-status/{status?}/{id?}', 'adminPanel\FaqController@changeStatus');

});