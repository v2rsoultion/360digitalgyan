<?php

ini_set('MAX_EXECUTION_TIME', '-1');
ini_set('memory_limit', '-1');
error_reporting(0);
/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */
use App\Mail\digitalgyanMail;

Route::get('/clear-cache', function()
{
    Artisan::call('cache:clear');
    Artisan::call('config:cache');
    return redirect('/');
    // return wherever you want
});

Route::get('user-mail',function(){
  return view('email.360.welcome-user');
});

/* home page*/
Route::get('/','frontend\HomeController@index');

/* User related routes */
Route::get('user/email-check','website\userController@email_check');
Route::post('user/sign-up','website\userController@register');

/* social login link */
Route::any('callback/{driver}','website\userController@callback');
Route::get('login/{driver}','website\userController@social');

Route::post('user/login','website\userController@login');
Route::get('user/log-out','website\userController@logout');
Route::post('user/get-quiz-result','website\userController@quiz_result');
Route::post('user/practice-quiz','website\userController@practice_quiz');
Route::post('user/save-answer','website\userController@save_answer');
Route::post('user/save-question','website\userController@save_question');

Route::get('/key-trends','frontend\KeyTrendsController@index');
Route::get('/all-competition','frontend\CompetitionController@index');
Route::get('/questions-answers','frontend\QuestionsAnswerController@questions_answers');
Route::get('/findingspotting-errors','frontend\CompetitionController@finderrors');
Route::get('/study-materials','frontend\StudyMaterialsController@index');
Route::get('practice-quiz','frontend\QuizQuestionsController@practice_quiz');
Route::get('interview-questions/{cat?}/{subcat?}','frontend\InterviewQuestionsController@index');

// /* videos page*/
// Route::get('/videos-tutorials/{slug?}','frontend\VideosTutorialsController@index');


/*====================================================================================*/
                    // Digital Marketing                           
/*====================================================================================*/


Route::group(['prefix'=>'digital-marketing'],function(){

/* for detail page deigital marketing */
Route::get('/',function(){
  return redirect('digital-marketing/videos-tutorials');
});

/* videos page*/
Route::any('/videos-tutorials/{slug?}','website\digital\VideosTutorialsController@index');

/* interview questions */
Route::get('interview-questions/{subcat?}','website\digital\InterviewQuestionsController@index');

/* technical terms */
Route::get('technical-terms/{subcat?}','website\digital\TechnicalTermsController@index');

/* Questiosn Answers */
Route::any('/questions-answers','website\digital\QuestionsAnswerController@questions_answers');
Route::get('/answer/{id}','website\digital\QuestionsAnswerController@answer');

/* key terms and concepts questions */
Route::any('key-terms/{char?}','website\digital\KeyTrendsController@index');

/* study materials */
Route::get('study-materials/{slug?}','website\digital\StudyMaterialsController@index');

/* for ajax study materials*/
Route::get('get-study-materials','website\digital\StudyMaterialsController@getstudy');

/* practice quiz */

Route::any('practice-quiz/{slug?}','website\digital\PracticeQuizController@index');

/* Quiz */
Route::any('/quiz/{slug?}','website\digital\QuizController@index');

});


/*====================================================================================*/
                    // Web Development                          
/*====================================================================================*/


Route::group(['prefix'=>'web-development'],function(){

/* for detail page deigital marketing */
Route::get('/',function(){
  return redirect('web-development/videos-tutorials');
});

/* videos page*/
Route::any('/videos-tutorials/{slug?}','website\webdev\VideosTutorialsController@index');

/* interview questions */
Route::get('interview-questions/{subcat?}','website\webdev\InterviewQuestionsController@index');

/* technical terms */
Route::get('technical-terms/{subcat?}','website\webdev\TechnicalTermsController@index');

/* Questiosn Answers */
Route::any('/questions-answers','website\webdev\QuestionsAnswerController@questions_answers');
Route::get('/answer/{id}','website\webdev\QuestionsAnswerController@answer');

/* key terms and concepts questions */
Route::any('key-terms/{char?}','website\webdev\KeyTrendsController@index');

/* study materials */
Route::get('study-materials/{slug?}','website\webdev\StudyMaterialsController@index');

/* for ajax study materials*/
Route::get('get-study-materials','website\webdev\StudyMaterialsController@getstudy');


});

/*====================================================================================*/
                    // Software Testing                          
/*====================================================================================*/

Route::group(['prefix'=>'software-testing'],function(){

/* for detail page software testing */
Route::get('/',function(){
  return redirect('software-testing/videos-tutorials');
});

/* videos page*/
Route::any('/videos-tutorials/{slug?}','website\software\VideosTutorialsController@index');

/* interview questions */
Route::get('interview-questions/{subcat?}','website\software\InterviewQuestionsController@index');

/* technical terms */
Route::get('technical-terms/{subcat?}','website\software\TechnicalTermsController@index');

/* Questiosn Answers */
Route::any('/questions-answers','website\software\QuestionsAnswerController@questions_answers');
Route::get('/answer/{id}','website\software\QuestionsAnswerController@answer');

/* key terms and concepts questions */
Route::any('key-terms/{char?}','website\software\KeyTrendsController@index');

/* study materials */
Route::get('study-materials/{slug?}','website\software\StudyMaterialsController@index');

/* for ajax study materials*/
Route::get('get-study-materials','website\software\StudyMaterialsController@getstudy');

/* practice quiz */
Route::any('practice-quiz/{slug?}','website\software\PracticeQuizController@index');

});


/*====================================================================================*/
                    // PLC SCADA                          
/*====================================================================================*/

Route::group(['prefix'=>'plc-scada'],function(){

/* for detail page software testing */
Route::get('/',function(){
  return redirect('plc-scada/videos-tutorials');
});

/* videos page*/
Route::any('/videos-tutorials/{slug?}','website\plc\VideosTutorialsController@index');

/* interview questions */
Route::get('interview-questions/{subcat?}','website\plc\InterviewQuestionsController@index');

/* technical terms */
Route::get('technical-terms/{subcat?}','website\plc\TechnicalTermsController@index');

/* Questiosn Answers */
Route::any('/questions-answers','website\plc\QuestionsAnswerController@questions_answers');
Route::get('/answer/{id}','website\plc\QuestionsAnswerController@answer');

/* key terms and concepts questions */
Route::any('key-terms/{char?}','website\plc\KeyTrendsController@index');

/* study materials */
Route::get('study-materials/{slug?}','website\plc\StudyMaterialsController@index');

/* for ajax study materials*/
Route::get('get-study-materials','website\plc\StudyMaterialsController@getstudy');

/* practice quiz */
Route::any('practice-quiz/{slug?}','website\plc\PracticeQuizController@index');

});


/*====================================================================================*/
                    // Python                          
/*====================================================================================*/

Route::group(['prefix'=>'python'],function(){

/* for detail page software testing */
Route::get('/',function(){
  return redirect('python/videos-tutorials');
});

/* videos page*/
Route::any('/videos-tutorials/{slug?}','website\python\VideosTutorialsController@index');

/* interview questions */
Route::get('interview-questions/{subcat?}','website\python\InterviewQuestionsController@index');

/* technical terms */
Route::get('technical-terms/{subcat?}','website\python\TechnicalTermsController@index');

/* Questiosn Answers */
Route::any('/questions-answers','website\python\QuestionsAnswerController@questions_answers');
Route::get('/answer/{id}','website\python\QuestionsAnswerController@answer');

/* key terms and concepts questions */
Route::any('key-terms/{char?}','website\python\KeyTrendsController@index');

/* study materials */
Route::get('study-materials/{slug?}','website\python\StudyMaterialsController@index');

/* for ajax study materials*/
Route::get('get-study-materials','website\python\StudyMaterialsController@getstudy');

/* practice quiz */
Route::any('practice-quiz/{slug?}','website\python\PracticeQuizController@index');

});


/*====================================================================================*/
                    // Java                           
/*====================================================================================*/

Route::group(['prefix'=>'java'],function(){

/* for detail page software testing */
Route::get('/',function(){
  return redirect('java/videos-tutorials');
});

/* videos page*/
Route::any('/videos-tutorials/{slug?}','website\java\VideosTutorialsController@index');

/* interview questions */
Route::get('interview-questions/{subcat?}','website\java\InterviewQuestionsController@index');

/* technical terms */
Route::get('technical-terms/{subcat?}','website\java\TechnicalTermsController@index');

/* Questiosn Answers */
Route::any('/questions-answers','website\java\QuestionsAnswerController@questions_answers');
Route::get('/answer/{id}','website\java\QuestionsAnswerController@answer');

/* key terms and concepts questions */
Route::any('key-terms/{char?}','website\java\KeyTrendsController@index');

/* study materials */
Route::get('study-materials/{slug?}','website\java\StudyMaterialsController@index');

/* for ajax study materials*/
Route::get('get-study-materials','website\java\StudyMaterialsController@getstudy');

/* practice quiz */
Route::any('practice-quiz/{slug?}','website\java\PracticeQuizController@index');

});


/*====================================================================================*/
                    // Android                           
/*====================================================================================*/

Route::group(['prefix'=>'android'],function(){

/* for detail page software testing */
Route::get('/',function(){
  return redirect('android/videos-tutorials');
});

/* videos page*/
Route::any('/videos-tutorials/{slug?}','website\android\VideosTutorialsController@index');

/* interview questions */
Route::get('interview-questions/{subcat?}','website\android\InterviewQuestionsController@index');

/* technical terms */
Route::get('technical-terms/{subcat?}','website\android\TechnicalTermsController@index');

/* Questiosn Answers */
Route::any('/questions-answers','website\android\QuestionsAnswerController@questions_answers');
Route::get('/answer/{id}','website\android\QuestionsAnswerController@answer');

/* key terms and concepts questions */
Route::any('key-terms/{char?}','website\android\KeyTrendsController@index');

/* study materials */
Route::get('study-materials/{slug?}','website\android\StudyMaterialsController@index');

/* for ajax study materials*/
Route::get('get-study-materials','website\android\StudyMaterialsController@getstudy');

/* practice quiz */
Route::any('practice-quiz/{slug?}','website\android\PracticeQuizController@index');

});



Route::group(['prefix' => 'admin-panel', 'middleware' => 'admin.guest'], function ()
{
    Route::get('/', 'AdminAuth\LoginController@showLoginForm')->name('login');
    Route::post('/login', 'AdminAuth\LoginController@login');
    Route::post('/logout', 'AdminAuth\LoginController@logout')->name('logout');

    Route::post('/password/email', 'AdminAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
    Route::post('/password/reset', 'AdminAuth\ResetPasswordController@reset')->name('password.email');
    Route::get('/password/reset', 'AdminAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
    Route::get('/password/reset/{token}', 'AdminAuth\ResetPasswordController@showResetForm');
});

Route::group(['prefix' => 'admin-panel', 'middleware' => 'admin'], function ()
{


Route::get('/add-sub-admin', 'AdminAuth\RegisterController@showRegistrationForm')->name('register');
Route::post('/register', 'AdminAuth\RegisterController@register');
Route::get('/view-sub-admin', 'adminPanel\DashboardController@showsubadmin')->name('showsubadmin');

// For logout
Route::get('/logout', 'AdminAuth\LoginController@logout')->name('logout');

// For Dashboard
Route::get('/dashboard', 'adminPanel\DashboardController@index');
// Admin Profile

Route::get('/profile', 'adminPanel\ProfileController@index');
Route::post('/profile', 'adminPanel\ProfileController@update_profile');
Route::get('/change-password', ['as' => 'admin-panel.profile.change-password', 'uses' => 'AdminAuth\ChangePasswordController@index']);
Route::post('change-password', ['uses' => 'AdminAuth\ChangePasswordController@changePassword']);

Route::get('/data', 'adminPanel\DashboardController@anyData');


Route::get('/subadmins-delete/{id?}', 'adminPanel\DashboardController@destroy');

Route::get('subadmins-status/{status?}/{id?}', 'adminPanel\DashboardController@changeStatus');


// For Category module 
// On 11 March 2019 
// By @Pankaj
Route::group(['prefix'=>'category'],function(){
  Route::get('/add-category/{id?}', 'adminPanel\CategoryController@add');
  Route::post('/save/{id?}', 'adminPanel\CategoryController@save');
  Route::get('/view-categories', 'adminPanel\CategoryController@index');
  Route::get('/data', 'adminPanel\CategoryController@anyData');
  Route::get('/category-delete/{id?}', 'adminPanel\CategoryController@destroy');
  Route::get('/category-status/{status?}/{id?}/{type?}', 'adminPanel\CategoryController@changeStatus');
  Route::get('/category-order/{order?}/{id?}', 'adminPanel\CategoryController@changeOrder');
});

// For Sub Category module 
// On 12 March 2019 
// By @Pankaj
Route::group(['prefix'=>'sub-category'],function(){
  Route::get('/add-sub-category/{id?}', 'adminPanel\SubCategoryController@add');
  Route::post('/save/{id?}', 'adminPanel\SubCategoryController@save');
  Route::get('/view-sub-categories', 'adminPanel\SubCategoryController@index');
  Route::get('/data', 'adminPanel\SubCategoryController@anyData');
  Route::get('/sub-category-delete/{id?}', 'adminPanel\SubCategoryController@destroy');
  Route::get('/sub-category-status/{status?}/{id?}', 'adminPanel\SubCategoryController@changeStatus');
  Route::get('/sub-category-order/{order?}/{id?}', 'adminPanel\SubCategoryController@changeOrder');
});


// For News Category module 
// On 18 March 2019 
// By @Pankaj
Route::group(['prefix'=>'news-category'],function(){
  Route::get('/add-news-category/{id?}', 'adminPanel\NewsCategoryController@add');
  Route::post('/save/{id?}', 'adminPanel\NewsCategoryController@save');
  Route::get('/view-news-categories', 'adminPanel\NewsCategoryController@index');
  Route::get('/data', 'adminPanel\NewsCategoryController@anyData');
  Route::get('/news-category-delete/{id?}', 'adminPanel\NewsCategoryController@destroy');
  Route::get('/news-category-status/{status?}/{id?}/{type?}', 'adminPanel\NewsCategoryController@changeStatus');
});


// For News module 
// On 18 March 2019 
// By @Pankaj
Route::group(['prefix'=>'news'],function(){
  Route::get('/add-news/{id?}', 'adminPanel\NewsController@add');
  Route::post('/save/{id?}', 'adminPanel\NewsController@save');
  Route::get('/view-news', 'adminPanel\NewsController@index');
  Route::get('/data', 'adminPanel\NewsController@anyData');
  Route::get('/news-delete/{id?}', 'adminPanel\NewsController@destroy');
  Route::get('/news-status/{status?}/{id?}', 'adminPanel\NewsController@changeStatus');
});


// For marketing blog module 
// On 18 March 2019 
// By @Pankaj
Route::group(['prefix'=>'marketing-blog'],function(){
  Route::get('/add-marketing-blog/{id?}', 'adminPanel\MarketingBlogController@add');
  Route::post('/save/{id?}', 'adminPanel\MarketingBlogController@save');
  Route::get('/view-marketing-blog', 'adminPanel\MarketingBlogController@index');
  Route::get('/data', 'adminPanel\MarketingBlogController@anyData');
  Route::get('/marketing-blog-delete/{id?}', 'adminPanel\MarketingBlogController@destroy');
  Route::get('/marketing-blog-status/{status?}/{id?}', 'adminPanel\MarketingBlogController@changeStatus');
});

// For Videos Module
// 9 April 2019
//By @Hameed

Route::group(['prefix'=>'videos'],function(){
 Route::get('/add-videos/{id?}','adminPanel\VideosController@add');
 Route::post('/save/{id?}','adminPanel\VideosController@save');
 Route::get('/view-videos','adminPanel\VideosController@index');
 Route::get('/data','adminPanel\VideosController@data');
 Route::get('/video-delete/{id}','adminPanel\VideosController@destroy');
 Route::get('/video-status/{id?}/{status?}','adminPanel\VideosController@changeStatus');

});

Route::group(['prefix'=>'posted-ques-ans'],function(){
  Route::get('/add-posted-ques/{id?}','adminPanel\PostedQueAnsController@add');
  Route::post('/save/{id?}','adminPanel\PostedQueAnsController@save');
  Route::get('posted-ans','adminPanel\PostedQueAnsController@posted_ans');
  Route::get('view-posted-ques','adminPanel\PostedQueAnsController@posted_ques');
  Route::get('/ques-data','adminPanel\PostedQueAnsController@ques_data');
  Route::get('/posted-ques-delete/{id?}','adminPanel\PostedQueAnsController@destroy');
  Route::get('/posted-ques-trend','adminPanel\PostedQueAnsController@trending');
  Route::get('/posted-ques-status','adminPanel\PostedQueAnsController@status');
});

//For Testimonial Module
// 16 April 2019
// By @Hameed

Route::group(['prefix'=>'testimonials'],function(){
 Route::get('/add-testimonials/{id?}','adminPanel\TestimonialController@add');
 Route::post('/save/{id?}','adminPanel\TestimonialController@save');
 Route::get('/view-testimonials','adminPanel\TestimonialController@index');
 Route::get('/data','adminPanel\TestimonialController@data');
 Route::get('/testimonials-delete/{id}','adminPanel\TestimonialController@destroy');
 Route::get('/testimonials-status/{id?}/{status?}','adminPanel\TestimonialController@status');
});

// For Tags
// On 19 April 2019
// By @Hameed

Route::group(['prefix'=>'tags'],function(){

  Route::get('/add-tag/{id?}','adminPanel\TagsController@add');
  Route::get('/data','adminPanel\TagsController@data');
  Route::get('/view-tags','adminPanel\TagsController@index');
  Route::post('/save/{id?}','adminPanel\TagsController@save');
  Route::get('/tag-delete/{id?}','adminPanel\TagsController@destroy');
  Route::get('/tag-status','adminPanel\TagsController@status');
});

// For Tips & Tricks
// On 20 April 2019
// By @Hameed

Route::group(['prefix'=>'tips-tricks'],function(){

  Route::get('/add-tricks/{id?}','adminPanel\TipsTricksController@add');
  Route::get('/view-tricks','adminPanel\TipsTricksController@index');
  Route::post('/save/{id?}','adminPanel\TipsTricksController@save');
  Route::get('/data','adminPanel\TipsTricksController@data');
  Route::get('/tricks-delete/{id?}','adminPanel\TipsTricksController@destroy');
  Route::get('/tricks-status','adminPanel\TipsTricksController@status');
});

// For Terms 
// On 29 April 2019
// By @Hameed

Route::group(['prefix'=>'terms'],function(){
  Route::get('/add-terms/{id?}','adminPanel\TermsController@add');
  Route::get('/view-terms','adminPanel\TermsController@index');
  Route::post('/save/{id?}','adminPanel\TermsController@save');
  Route::get('/data','adminPanel\TermsController@data');
  Route::get('/terms-delete/{id}','adminPanel\TermsController@destroy');
  Route::get('terms-status','adminPanel\TermsController@status');
});

// For Greetings Image
// On 03 May 2019
// By @Hameed

Route::group(['prefix'=>'greetings'],function(){
  Route::get('/add-greetings/{id?}','adminPanel\GreetingsController@add');
  Route::get('/view-greetings','adminPanel\GreetingsController@index');
  Route::post('/save/{id?}','adminPanel\GreetingsController@save');
  Route::get('/data','adminPanel\GreetingsController@data');
  Route::get('/greetings-delete/{id}','adminPanel\GreetingsController@destroy');
  Route::get('/greetings-status','adminPanel\GreetingsController@status');

}); 

// For practice-quiz-questions
// On 14 May 2019
//By @Hameed

Route::group(['prefix'=>'practice-quiz-questions'],function(){
  Route::get('/add-practice-questions/{id?}', 'adminPanel\PracticeQuizQuestionController@add');
  Route::post('/save/{id?}', 'adminPanel\PracticeQuizQuestionController@save');
  Route::get('/view-practice-questions', 'adminPanel\PracticeQuizQuestionController@index');
  Route::get('/data', 'adminPanel\PracticeQuizQuestionController@anyData');
  Route::get('/detail-data/{id}', 'adminPanel\PracticeQuizQuestionController@detailData');
  Route::get('/practice-questions-delete/{id?}', 'adminPanel\PracticeQuizQuestionController@destroy');
  Route::get('/practice-questions-status/{status?}/{id?}', 'adminPanel\PracticeQuizQuestionController@changeStatus');
  Route::get('/practice-questions-order/{order?}/{id?}','adminPanel\PracticeQuizQuestionController@changeOrder');
});

// For interview Questions module 
// On 19 March 2019 
// By @Pankaj
Route::group(['prefix'=>'interview-questions'],function(){
  Route::get('/add-interview-questions/{id?}', 'adminPanel\InterviewQuestionsController@add');
  Route::post('/save/{id?}', 'adminPanel\InterviewQuestionsController@save');
  Route::get('/view-interview-questions', 'adminPanel\InterviewQuestionsController@index');
  Route::get('/data', 'adminPanel\InterviewQuestionsController@anyData');
  Route::get('/interview-questions-delete/{id?}', 'adminPanel\InterviewQuestionsController@destroy');
  Route::get('/interview-questions-status/{status?}/{id?}', 'adminPanel\InterviewQuestionsController@changeStatus');
  Route::get('/interview-questions-order/{order?}/{id?}','adminPanel\InterviewQuestionsController@changeOrder');
});


// For Quiz Questions module 
// On 23 March 2019 
// By @Pankaj
Route::group(['prefix'=>'quiz-questions'],function(){
  Route::get('/add-quiz-questions/{id?}', 'adminPanel\QuizQuestionsController@add');
  Route::post('/save/{id?}', 'adminPanel\QuizQuestionsController@save');
  Route::get('/view-quiz-questions', 'adminPanel\QuizQuestionsController@index');
  Route::get('/data', 'adminPanel\QuizQuestionsController@anyData');
  Route::get('/detail-data/{id}', 'adminPanel\QuizQuestionsController@detailData');
  Route::get('/quiz-questions-delete/{id?}', 'adminPanel\QuizQuestionsController@destroy');
  Route::get('/quiz-questions-status/{status?}/{id?}', 'adminPanel\QuizQuestionsController@changeStatus');
  Route::get('/quiz-questions-order/{order?}/{id?}','adminPanel\QuizQuestionsController@changeOrder');
});

Route::get('get-sub-categories/{id}','adminPanel\InterviewQuestionsController@getsubcats');


Route::any('/view-seo-users','adminPanel\UsersController@index');
// Route::get('/view-users/data','adminPanel\UsersController@anyData');
Route::get('/view-users/staus/','adminPanel\UsersController@changeStatus');

// On 13 Dec 2018 
// By @pradeep

Route::get('/faq/add-faq/{id?}', 'adminPanel\FaqController@add');
Route::post('/faq/save/{id?}', 'adminPanel\FaqController@save');
Route::get('/faq/view-faq', 'adminPanel\FaqController@index');
Route::get('/faq/data', 'adminPanel\FaqController@anyData');
Route::get('/faq/faq-delete/{id?}', 'adminPanel\FaqController@destroy');
Route::get('/faq/faq-status/{status?}/{id?}', 'adminPanel\FaqController@changeStatus');

});