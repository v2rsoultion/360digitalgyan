<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInterviewQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('interview_questions_answers')){
            Schema::create('interview_questions_answers', function (Blueprint $table) {
                $table->increments('iq_ques_ans_id');
                $table->integer('iq_ques_ans_catid');
                $table->integer('sub_cat_id');
                $table->tinyInteger('question_type')->comment = '1=interview ,2=technical';
                $table->text('iq_ques_ans_q');
                $table->text('question_coding')->nullable();
                $table->text('iq_ques_ans_a');
                $table->longText('coding')->nullable();
                $table->integer('iq_ques_ans_order')->nullable();
                $table->integer('iq_qus_ans_trial')->default(0);
                $table->tinyInteger('is_new')->comment = '0=Deactive,1=Active';
                $table->text('iq_ques_ans_metatitle')->nullable();
                $table->text('iq_ques_ans_metakey')->nullable();
                $table->text('iq_ques_ans_metadesc')->nullable();
                $table->tinyInteger('iq_ques_ans_status')->default(1)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('interview_questions_answers');
    }
}
