<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeoUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('seo_users')) {
            Schema::create('seo_users', function (Blueprint $table) {
                $table->increments('seo_users_id');
                $table->string('seo_users_name');
                $table->string('seo_users_email');
                $table->string('seo_users_image')->nullable();
                $table->string('seo_users_password')->nullable();
                $table->Integer('user_points')->nullable();
                $table->text('user_badges')->nullable();
                $table->tinyInteger('seo_user_type')->nullable()->comment = '0=Android ,1=ios';
                $table->tinyInteger('seo_users_status')->default(1)->comment = '0=Deactive,1=Active';
                $table->tinyInteger('app_user')->nullable()->comment = '1=SEO ,2=JAVA';
                $table->tinyInteger('flag')->nullable()->comment = '1=normal , 2=google , 1=facebook ,2=linkdin';
                $table->string('seo_users_fb_linked_in_id')->nullable();
                $table->integer('subscribe_status')->default(1);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seo_users');
    }
}
