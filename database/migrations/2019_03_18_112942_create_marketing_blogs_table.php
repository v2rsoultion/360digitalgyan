<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarketingBlogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('marketing_blogs')) {
            Schema::create('marketing_blogs', function (Blueprint $table) {
                $table->increments('marketing_id');
                $table->text('marketing_title');
                $table->text('marketing_slug');
                $table->longText('marketing_description')->nullable();
                $table->string('marketing_image');
                $table->text('marketing_slug_web')->nullable();
                $table->tinyInteger('cron_status')->default(0)->comment = '0=Cron not,1=cron done';
                $table->tinyInteger('marketing_status')->default(1)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('marketing_blogs');
    }
}
