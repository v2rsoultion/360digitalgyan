<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('iq_sub_categories')) {
            Schema::create('iq_sub_categories', function (Blueprint $table) {
                $table->increments('sub_category_id');
                $table->integer('category_id');
                /* set foregin key to category table */
                // $table->foreign('category_id')->references('iq_category_id')->on('iq_categories');
                $table->string('title');
                $table->string('image');
                $table->Integer('quiz_time')->nullable();
                $table->Integer('category_order')->nullable();
                $table->string('cat_slug');
                $table->tinyInteger('status')->default(1)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });

            Schema::table('iq_sub_categories', function($table) {
                $table->foreign('category_id')->references('iq_category_id')->on('iq_categories');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('iq_sub_categories');
    }
}
