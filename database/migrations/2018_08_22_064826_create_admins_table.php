<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateAdminsTable extends Migration
    {

        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            if (!Schema::hasTable('admins'))
            {
                Schema::create('admins', function (Blueprint $table)
                {
                    $table->increments('admin_id');
                    $table->string('admin_name')->nullable();
                    $table->string('email')->nullable();
                    $table->string('phone')->nullable();
                    $table->string('password');
                    $table->tinyInteger('admin_status')->default(1)->comment = '0=Deactive,1=Active';
                    $table->tinyInteger('type')->default(0)->comment         = '0=other,1=Admin';
                    $table->text('admin_profile_img')->nullable();
                    $table->rememberToken();
                    $table->timestamps();
                });
                // Insert some stuff
                DB::table('admins')->insert(
                    array(
                        'admin_id'       => 1,
                        'admin_name'     => 'Admin',
                        'email'          => 'admin@gmail.com',
                        'type'           => 1,
                        'password'       => '$2y$10$CRFA.bp9jE6bG9iYd0BcUugBhFSur3FEtfy7t2iHZbq2Uyp4dDgdW',
                        'remember_token' => 'oJJmPSevfhpZdKdRf2iSfnKLM8boFxrkWkYP1GjANsSkmcU6CMBNHkdMIfcx',
                        'created_at'     => date('Y-m-d H:i:s'),
                        'updated_at'     => date('Y-m-d H:i:s'),
                    )
                );
            }
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::drop('admins');
        }

    }
    