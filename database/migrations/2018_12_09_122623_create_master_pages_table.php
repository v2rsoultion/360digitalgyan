<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('master_pages')) {
            Schema::create('master_pages', function (Blueprint $table) {
                $table->increments('master_pages_id');
                $table->string('pages_title');
                $table->text('pages_short_desc');
                $table->longText('pages_long_desc');
                $table->string('image');
                $table->text('pages_meta_title');
                $table->text('pages_meta_description');
                $table->text('pages_meta_keyword');
                $table->tinyInteger('status')->default(1)->comment = '0=Deactive,1=Active';
                $table->text('pages_slug');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_pages');
    }
}
