<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Categories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        if (!Schema::hasTable('iq_categories')) {
            // Schema::create('iq_categories', function (Blueprint $table) {
            //     $table->increments('catgory_id');
            //     $table->string('catgory_name');
            //     $table->string('category_icon');
            //     $table->string('category_quiz_time');
            //     $table->Integer('category_order')->nullable();
            //     $table->text('category_meta_title');
            //     $table->text('category_meta_description');
            //     $table->text('category_meta_keyword');
            //     $table->text('category_quiz_meta_title');
            //     $table->text('category_quiz_meta_description');
            //     $table->text('category_quiz_meta_keyword');
            //     $table->tinyInteger('category_iq_status')->default(1)->comment = '0=Deactive,1=Active';
            //     $table->tinyInteger('category_quiz_status')->default(1)->comment = '0=Deactive,1=Active';
            //     $table->timestamps();
            // });

            Schema::create('iq_categories', function (Blueprint $table) {
                $table->increments('iq_category_id');
                $table->string('iq_category_name');
                $table->string('iq_category_icon');
                $table->string('quiz_time');
                $table->Integer('iq_category_order')->nullable();
                $table->text('iq_category_metatitle')->nullable();
                $table->text('iq_category_metadesc')->nullable();
                $table->text('iq_category_metakey')->nullable();
                $table->text('quiz_meta_title')->nullable();
                $table->text('quiz_meta_desp')->nullable();
                $table->text('quiz_meta_key')->nullable();
                $table->tinyInteger('iq_category_status')->default(1)->comment = '0=Deactive,1=Active';
                $table->tinyInteger('iq_quiz_status')->default(1)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('iq_categories');
    }
}
