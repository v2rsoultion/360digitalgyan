<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuizQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('quiz_questions')){
            Schema::create('quiz_questions', function (Blueprint $table) {
                $table->increments('qustion_id');
                $table->integer('qustion_sec_id')->comment = 'Category';
                $table->integer('qustion_sub_sec_id')->comment = 'Sub Category';
                $table->text('question_section_part')->comment = 'Detail';
                $table->tinyInteger('code_flag')->comment = '0 = no Code, 1 = Code';
                $table->longText('qustion_qustion');
                $table->longText('qustion_option1')->nullable();
                $table->longText('qustion_option2')->nullable();
                $table->longText('qustion_option3')->nullable();
                $table->longText('qustion_option4')->nullable();
                $table->longText('qustion_option5')->nullable();
                $table->tinyInteger('qustion_correct_answer');
                $table->integer('question_order')->nullable();
                $table->tinyInteger('is_new')->comment = '0=Old,1=New';
                $table->tinyInteger('question_trial')->default(1)->comment = '0=Deactive,1=Active';
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quiz_questions');
    }
}
