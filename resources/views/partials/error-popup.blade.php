@if (session('warning'))
        <div class="alert alert-warning">
            {{ session('warning') }}
        </div>
        @endif
        @if(count($errors))
        <div id="error-popup" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body text-center">
                        <div class="col-xl-12 col-12  sucessfull">
                            <div class="form-group">
                                <i class="far fa-times-circle"></i>
                                <span class="text-center">Oops!</span>
                                @foreach($errors->all() as $key=> $error) 
                                @if($key == 0)
                                @if($error =='Login')
                                <?php echo'<script type="text/javascript">$(document).ready(function (e) { $("#login-popup").modal("show") });</script>'; ?>
                                @else
                                <?php echo'<script type="text/javascript">$(document).ready(function (e) { $("#error-popup").modal("show") });</script>'; ?>
                                @endif
                                @endif
                                <p>{{ $error }}</p>
                                @endforeach 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif  