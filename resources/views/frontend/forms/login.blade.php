<div class="modal centered-modal in login-modal" id="login-popup">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <!--   -->
                <button type="button" class="close" title="Close" data-dismiss="modal">&times;</button>
                <div class="clearfix"></div>
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xl-6 col-md-6">
                            <h1>Hello</h1>
                            <img src="{{ url('public/frontend/images/border.png') }}" alt="border">
                            <h5>Sign in to your account</h5>
                            <div id="login-msg" class=""></div>
                            <form id="login-form" method="post" action="{{ url('user/login') }}">
                                {{ csrf_field() }}
                                <div class="col-md-12 col-xl-12 padding-remove">
                                    <div class="form-group">
                                        <input type="text" class="form-control form-fields" placeholder="Email Id" name="email">
                                    </div>
                                </div>
                                <div class="col-md-12 col-xl-12  padding-remove">
                                    <div class="form-group">
                                        <input id="password-field" type="password" placeholder="Password" class="form-control form-fields" name="password" value=""  minlength="8">
                                        <!-- pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" -->
                                        <span toggle="#password-field" class="far fa-eye field-icon toggle-password"></span>
                                    </div>
                                </div>
                                <label class="remember float-left">Remember me
                                    <input type="checkbox">
                                    <span class="checkmark"></span>
                                </label>
                                <div class="forgot float-right">
                                    <h3 id="forgotpassword">Forgot password?</h3>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-12 col-xs-12">
                                    <button type="submit" class="Submit-btn all-button" title="SIGN IN">SIGN IN</button>
                                </div>
                            </form>
                            <h6 class="text-center">or sign up with</h6>
                            <div class="social-icon">
                                <ul>
                                    <li>
                                        <a href="{{ url('login/facebook') }}" class="facebook" title="Facebook" >
                                            <i class="fab fa-facebook-f"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ url('login/linkedin') }}" title="LinkedIn" class="twitter" >
                                            <i class="fab fa-linkedin"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ url('login/google') }}" class="google-plus" title="Google Plus">
                                            <i class="fab fa-google-plus-g"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <u class="text-center">Don’t have an account? <a id="loagin_popup" title="Create">Create</a></u>
                        </div>
                        <div class="col-xl-6 col-md-6 d-none d-md-block">
                            <img src="{{ url('public/frontend/images/login-img.png') }}" alt="login-img" class="img-fluid login-side">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script type="text/javascript">
    
</script>