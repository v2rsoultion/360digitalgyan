<div class="modal  login-modal" id="signup-popup">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="close" title="Close" data-dismiss="modal">&times;</button>
            <div class="clearfix"></div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xl-6 col-md-6">
                        <h1>Hello</h1>
                        <img src="{{ url('public/frontend/images/border.png') }}" alt="border">
                        <h5>Sign up to your account</h5>
                        <div id="sign-msg"></div>
                        <form id="signup-form">
                            {{ csrf_field() }}
                            <div class="col-xl-12 col-12 padding-remove">
                                <div class="form-group">
                                    <input type="text" class="form-control form-fields" placeholder="Name" name="name" id="name">
                                </div>
                            </div>
                            <div class="col-md-12 col-xl-12 padding-remove">
                                <div class="form-group">
                                    <input type="text" class="form-control form-fields" placeholder="Email Id" name="email" id="email-field">
                                </div>
                            </div>
                            <!-- <div class="col-md-12 col-xl-12 padding-remove">
                                <div class="form-group">
                                    <input type="text" class="form-control numeric form-fields" placeholder="Mobile No" name="mobilenumber">
                                </div>
                            </div> -->
                            <div class="col-md-12 col-xl-12  padding-remove">
                                <div class="form-group">
                                    <input id="password-field" type="password" placeholder="Password" class="form-control form-fields" name="pwd" value="" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" minlength="8">
                                </div>
                            </div>
                            <div class="col-xl-12 col-12 padding-remove">
                                <div class="form-group">
                                    <input type="password" data-match="#password-field" data-match-error="Whoops, these don't match" class="form-control form-fields" placeholder="Confirm Password" name="cnfrmpwd" id="cnfrmpwd" required>
                                </div>
                            </div>
                            <label class="remember float-left">I agree to the <a href="">Terms & Conditions</a> and <a href="">Privacy Policy.</a>
                                <input type="checkbox" name="agree[]" value="1">
                                <span class="checkmark"></span>
                            </label>
                            <div class="clearfix"></div>
                            <div class="col-md-12 col-xs-12">
                                <button type="submit" id="sign-btn" class="Submit-btn all-button" title="SIGN UP">SIGN UP</button>
                            </div>
                        </form>
                        <h6 class="text-center" onclick="register">or sign up with</h6>
                        <div class="social-icon">
                            <ul>
                                <li>
                                    <a href="{{ url('login/facebook') }}" class="facebook" title="Facebook" >
                                        <i class="fab fa-facebook-f"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('login/linkedin') }}" title="LinkedIn" class="twitter" >
                                        <i class="fab fa-linkedin"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ url('login/google') }}" class="google-plus" title="Google Plus">
                                        <i class="fab fa-google-plus-g"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <u class="text-center">Already have an account?<a id="sign_popup" class="signs-pop hidemodel" title="Login">Login</a></u>
                    </div>
                    <div class="col-xl-6 col-md-6 d-none d-md-block">
                        <img src="{{ url('public/frontend/images/login-img.png') }}" alt="login-img" class="img-fluid login-side">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>