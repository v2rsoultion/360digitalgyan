<div class="modal  login-modal" id="forgot-password">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" title="Close" class="close closebtn" data-dismiss="modal">&times;</button>
            <div class="clearfix"></div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xl-6 col-md-6">
                        <h1>Forgot Password</h1>
                        <img src="{{ url('public/frontend/images/border.png') }}" alt="border" class="boder-img">
                        <div id="forgot-msg"></div>
                        <form id="forgot-popup" method="post" action="{{ url('user/forgot-password') }}">
                            @csrf
                            <div class="col-md-12 col-xl-12 padding-remove">
                                <div class="form-group">
                                    <input type="email" id="forgot-email" class="form-control form-fields" placeholder="Email Id" name="email" required>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-12 col-xs-12 padding-remove">
                                <button type="submit" id="forgot-btn" class="Submit-btn all-button" title="SUBMIT">SUBMIT</button>
                            </div>
                        </form>
                        <u class="text-center">Continue to <a id="forgot_login" title="Login"> Login</a></u>
                    </div>
                    <div class="col-xl-6 col-md-6 d-none d-md-block">
                        <img src="{{ url('public/frontend/images/login-img.png') }}" alt="login-img" class="img-fluid login-side">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>