@extends('frontend/layout/layout')
@section('content')
<section id="bredcum">
  <div class="container">
    <div class="row">
      <div class="col-xl-6 col-md-6 col-6">
        <h4 class="text-capitalize">Question & answer (forum)</h4>
      </div>
      <div class="col-xl-6 col-md-6 col-6">
        <span class="text-capitalize float-right"><a href="{{ url('/') }}" class="home-main">Home</a> <i class="fas fa-chevron-right"></i> <u class="mater">Question & answer (forum)</u></span>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
</section>
<section id="ques-ans">
  @if(session()->has('success'))
    <div class="alert alert-success alert-dismissible text-muted m-b-10 col-lg-6 col-md-6 col-sm-12 col-xs-12 font-13 mx-auto green fade show" role="alert">
      <?php echo getTextTransform(session()->get('success'),0) ?>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  @endif

  @if($errors->any())
      <div class="alert alert-danger alert-dismissible text-muted m-b-10 col-lg-6 col-md-6 col-sm-12 col-xs-12 font-13 mx-auto text-danger fade show" role="alert">
      <?php echo getTextTransform($errors->first(),0) ?>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  @endif
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-xl-9 col-12">
        <div class="vedio-form">
          <form name="myform" method="POST" action="{{ $questionUrl.'/questions-answers' }}">
            <div class="input-group" id="seacr_panel">
              {{ csrf_field() }}
              <div class="select-dropdown">
                <select class="form-control" onchange="location = this.value" id="withSelect">
                  <option {{ Request::segment(1) =='digital-marketing' ? 'selected' : '' }} value="{{ url('/digital-marketing/questions-answers') }}" >Digital Marketing</option>
                  <option {{ Request::segment(1) =='web-development' ? 'selected' : '' }} value="{{ url('/web-development/questions-answers') }}" >Web Development</option>
                  <option {{ Request::segment(1) =='software-testing' ? 'selected' : '' }} value="{{ url('/software-testing/questions-answers') }}" >Software Testing</option>
                  <option {{ Request::segment(1) =='plc-scada' ? 'selected' : '' }} value="{{ url('/plc-scada/questions-answers') }}" >PLC-SCADA</option>
                  <option {{ Request::segment(1) =='python' ? 'selected' : '' }} value="{{ url('/python/questions-answers') }}" >Python</option>
                  <option {{ Request::segment(1) =='java' ? 'selected' : '' }} value="{{ url('/java/questions-answers') }}" >Java</option>
                  <option {{ Request::segment(1) =='android' ? 'selected' : '' }} value="{{ url('/android/questions-answers') }}" >Android</option>
                </select>
              </div>
              <div class="select-dropdown">
                <select class="form-control" name="question_type" id="withSelect">
                  <option value="1" {{ request()->question_type ==1 ? 'selected' : '' }}>Trending</option>
                  <option value="2" {{ request()->question_type ==2 ? 'selected' : '' }}>Most Popular</option>
                  <option value="3" {{ request()->question_type ==3 ? 'selected' : '' }}>Most Recent</option>
                  <option value="4" {{ request()->question_type ==4 ? 'selected' : '' }}>Most Answered</option>
                  <option value="5" {{ request()->question_type ==5 ? 'selected' : '' }}>Unanswered</option>
                  <option value="6" {{ request()->question_type ==6 ? 'selected' : '' }}>My Questions</option>
                  <option value="7" {{ request()->question_type ==7 ? 'selected' : '' }}>My Answered</option>
                </select>
              </div>
              <input type="text" name="tag" class="form-control vedio-search" value="{{ request()->tag }}" placeholder="Search tag here">
              <span class="input-group-btn">
              <button type="submit" title="Search" class="btn btn-default header-submit">SEARCH</button>
              </span>
            </div>
          </form>
        </div>
        <div class="row ask-ques">
          <div class="col-xl-6 col-md-6 col-6">
            <h5>Lorem ipsum dolor sit amet, consectetur</h5>
          </div>
          <div class="col-xl-6 col-md-6 col-6">
            <div class="post-ques float-right">
              @if(session('digital_user_id')!='')
                <button title="Post Your Questions" data-toggle="modal" data-target="#ask-your-question" class="text-capitalize" data-backdrop="static" data-keyboard="false">Post Your Question</button>
              @else
                <button title="Post Your Questions" class="text-capitalize" onclick="preventPostQues()">Post Your Question</button>
              @endif
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
        <div id="ques-answer">
            @include('frontend.ques')
        </div>
      </div>
      <div class="col-xl-3 col-md-4 d-none d-md-block">
        <div class="ques-sidebar">
          <h4>Unanswered Questions</h4>
          <ul>
            @foreach($unAnswerd as $ques)
            <li>
              <a href="{{ $questionUrl.'/answer/'.$ques->ques_id }}" title="{{ $ques->user_question }}">{{ $ques->user_question }}</a>
              <div class="clearfix"></div>
              <span>asked on {{ date("d M,Y", strtotime($ques->ques_time)) }}</span>
            </li>
            @endforeach
            <form action="{{ $questionUrl.'/questions-answers' }}" method="post">
                @csrf
                <input type="hidden" name="question_type" value="5">
                <input type="submit" class="all-button" value="View All">
            </form>
          </ul>
        </div>
        <div class="ques-sidebar">
          <h4>Related Questions</h4>
          <ul>
            @foreach($relQuestions as $ques)
            <li>
              <a href="{{ $questionUrl.'/answer/'.$ques->ques_id }}" title="{{ $ques->user_question }}">{{ $ques->user_question }}</a>
              <div class="clearfix"></div>
              <span>asked on {{ date("d M,Y", strtotime($ques->ques_time)) }}</span>
            </li>
            @endforeach
            <form action="{{ $questionUrl.'/questions-answers' }}" method="post">
                @csrf
                <input type="submit" class="all-button" value="View All">
            </form>
          </ul>
        </div>
      </div>
    </div>
  </div>
</section>

<div class="modal ask-modal fade" id="ask-answer">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Give Answer</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <form method="post" id="form-save-answer" action="{{ url('user/save-answer') }}">
        {{ csrf_field() }}
        <div class="modal-body">
          <h5 id="model-question"></h5>
          <div class="form-group">
            <textarea class="form-control" name="user_answer" placeholder="Type something here..." rows="5" id="comment"></textarea>
          </div>
          <input type="hidden" name="category_name" value="{{ Request::segment(1) }}">
          <input type="hidden" name="quetion_name" id="question-id">
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn all-button" title="SAVE">SAVE</button>
          <button type="button" class="btn cancel-button" data-dismiss="modal" title="CANCEL">
            CANCEL
          </button>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- Modal for Ask Question -->
<div class="modal centered-modal in ask-modal ask-questions" id="ask-your-question">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Ask Question </h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <form id="form-save-question" method="post" action="{{ url('user/save-question') }}">
        {{ csrf_field() }}
        <div class="modal-body">
          <div class="form-group">
            <textarea class="form-control" name="user_question" placeholder="Type your question here..." rows="5" id="comment"></textarea>
            <input id="form-tags-3" class="digitalgyan-tags" name="questions_tags" type="text" value="">
            <input type="hidden" name="category_name" value="{{ Request::segment(1) }}">
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn all-button" title="SAVE">SAVE</button>
          <button type="button" class="btn cancel-button" data-dismiss="modal" title="CANCEL">CANCEL</button>
        </div>
      </form>
    </div>
  </div>
</div>

{!! Html::script('public/frontend/js/jquery.tagsinput-revisited.js') !!}
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

<script type="text/javascript">
    $(window).on('hashchange', function() {
        if (window.location.hash) {
            var page = window.location.hash.replace('#', '');
            if (page == Number.NaN || page <= 0) {
                return false;
            }else{
                getData(page);
            }
        }
    });

    

    $(document).ready(function()
    {
      /* for ajax agination */
      $(document).on('click', '.pagination a',function(event){
        event.preventDefault();
        $('li').removeClass('active');
        $(this).parent('li').addClass('active');
        var ajaxUrl = $(this).attr('href');

        $.ajax({
          url: ajaxUrl,
          type: "get",
          datatype: "html"
        }).done(function(data){
          $("#ques-answer").empty().html(data);
          // location.hash = page;
        }).fail(function(jqXHR, ajaxOptions, thrownError){
          alert('No response from server');
        });
      });
      /* for ajax agination close */


      /* for set question in model */
      $(document).on('click', '.like-dislike a',function(event){
        var txt = $(this).closest('div.questions-parent').find('h3').text();
        var id = $(this).closest('div.questions-parent').attr('data-ques')
        $("#model-question").html(txt);
        $("#question-id").val(id);
      });

      /* for like question */
      $(document).on('click', '.like-dislike-question',function(event){
        var id     = $(this).closest('div.questions-parent').attr('data-ques');
        var elment = $(this);
        if ($(this).find('i').hasClass('fa-thumbs-up')){
          liketype1 = 'like';
        }else{
          liketype1 = 'dislike';
        }
        $.ajax({
          type: "POST",
          url: "{{ url('user/like-question') }}",
          data: {
            "_token"  : "{{ csrf_token() }}",
            data_id   : id,
            liketype  : liketype1,
            ques_ans  : 'question'
          },
          success: function(res){
            if(res.status==1){
              elment.parent('span1').html(res.data);
            }
          },
          error: function(){
            alert(" :( Sorry something went wrong..!!");
          }
        });
        return false;
      });
      
    });

/*
**  for hide alert with time
*/
window.setTimeout(function(){
  $(".alert").fadeTo(500, 0).slideUp(500, function(){
      $(this).remove();
  });
},4000);
</script>
@endsection