@extends('frontend/layout/layout')
@section('content')
<section id="bredcum">
  <div class="container">
    <div class="row">
      <div class="col-xl-6 col-md-6 col-6">
        <h4 class="text-capitalize">{{ $page_title }}</h4>
      </div>
      <div class="col-xl-6 col-md-6 col-6">
        <span class="text-capitalize float-right">
            <a href="{{ url('/') }}" class="home-main">Home</a>
            <i class="fas fa-chevron-right"></i>
            <u class="mater">{{ $page_title }}</u></span>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
</section>
<section id="key-trends">
  <div class="container">
    <div class="row">
      <div class="col-xl-9 col-12 col-md-9">
        <div id="key-form">
          <form name="myform" action='{{ $keyUrl }}' method="post">
            {{ csrf_field() }}
            <div class="input-group" id="seacr_panel">
              <input type="text" name="tag" class="form-control vedio-search" value="{{ request()->tag }}" placeholder="Search tag here">
              <span class="input-group-btn">
                <button type="submit" title="Search" class="btn btn-default header-submit">SEARCH</button>
              </span>
            </div>
          </form>
          <div class="all-button">
            <ul>
              <li><a href="{{ $keyUrl }}" class="text-uppercase {{ Request::segment(3)=='' ? 'active' : '' }}" title="All">All</a></li>
              @foreach($alphabts as $key => $char)
                <li><a href='{{ $keyUrl."/$key" }}' class="text-uppercase {{ Request::segment(3)==$key ? 'active' : '' }}"  title="{{ $key }}">{{ $key }}</a></li>
              @endforeach
            </ul>
          </div>
        </div>
        @foreach($trends as $trend)
        <div class="col-xl-12 padding-remove">
          <div class="answer-panel">
            <div class="a-panel float-left">
              <div class="panel-in text-center text-uppercase">
                {{ $trend->term_title[0] }}
              </div>
            </div>
            <div class="answer-dis float-left">
              <h5>{{ $trend->term_title }}</h5>
              <p><?php echo getTextTransform($trend->term_description,0) ?></p>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
        @endforeach

        <div class="col-xl-12 padding-remove">
          {{ $trends->links() }}  
        </div>
      </div>
      <div class="col-xl-3 col-12 col-md-3 adds d-none d-md-block">
        @include('frontend/layout/right-sidebar')
      </div>
    </div>
  </div>
</section>
@endsection