@extends('frontend/layout/layout')
@section('content')
<section id="bredcum">
  <div class="container">
    <div class="row">
      <div class="col-xl-6 col-md-6 col-6">
        <h4 class="text-capitalize">All Competition Exams</h4>
      </div>
      <div class="col-xl-6 col-md-6 col-6">
        <span class="text-capitalize float-right"><a href="{{ url('/') }}" class="home-main">Home</a>  >  <u class="mater">  Topics</u></span>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
</section>
<section id="all-competition">
  <div class="container">
    <div class="row">
      <div class="sidemenu"> Side Bar
        <button class="toggle float-right"><i class="fas fa-align-justify"></i></button>
      </div>
      <div class="col-xl-3 col-12 col-md-4" id="target">
        <div class="left-side-panel">
          <h2 class=" text-left">Test Your Grammar
            Knowledge
          </h2>
          <ul>
            <li><a href="" title="Tenses"><i class="fas fa-chevron-right"></i>Tenses</a></li>
            <li><a href="" title="Modal Verbs"><i class="fas fa-chevron-right"></i>Modal Verbs</a></li>
            <li><a href="" title="Prepositions"><i class="fas fa-chevron-right"></i>Prepositions</a></li>
            <li><a href="" title="SSC Quick Skill Online Test"><i class="fas fa-chevron-right"></i>SSC Quick Skill Online Test</a></li>
            <li><a href="" title="Verbs"><i class="fas fa-chevron-right"></i>Verbs</a></li>
            <li><a href="" title="If Conditions"><i class="fas fa-chevron-right"></i>If Conditions </a></li>
            <li><a href="" title="Articles"><i class="fas fa-chevron-right"></i>Articles</a></li>
            <li><a href="" title="Active & Passive Voice"><i class="fas fa-chevron-right"></i>Active & Passive Voice</a></li>
          </ul>
        </div>
      </div>
      <div class="col-xl-9 col-md-8">
        <div class="all-exams">
          <h3>Complete Solutions For All Competition Exams</h3>
          <p>English App is a completely free online platform to check your ability, knowledge and preparation for your upcoming competition exams. Maybe you have been preparing for one of these competition exams <b>(SSC CGL, CHSL (10+2), CPO, BANK P.O, CLERK, LDC, NDA, CDS and Air Force Airman X & Y Groups)</b> for a long time. Or perhaps you have just started the preparation for one of them or planning to start as soon as possible! Here at EA () you will find all previous years papers and practice sets with complete solution. </p>
          <div class="row">
            <div class="col-xl-3 col-6 col-md-6">
              <a href="" title="SSC CGL TIER- I & II">
                <div class="all-topic">
                  <h4 class="text-uppercase text-center">SSC CGL TIER- I & II</h4>
                  <span class="text-center"> <i class="fas fa-chevron-right"></i> List of Topics</span>
                </div>
              </a>
            </div>
            <div class="col-xl-3 col-6 col-md-6">
              <a href="" title="NDA">
                <div class="all-topic nda">
                  <h4 class="text-uppercase text-center">NDA</h4>
                  <span class="text-center"> <i class="fas fa-chevron-right"></i> List of Topics</span>
                </div>
              </a>
            </div>
            <div class="col-xl-3 col-6 col-md-6">
              <a href="" title="BANK P.O">
                <div class="all-topic bank">
                  <h4 class="text-uppercase text-center">BANK P.O</h4>
                  <span class="text-center"> <i class="fas fa-chevron-right"></i> List of Topics</span>
                </div>
              </a>
            </div>
            <div class="col-xl-3 col-6 col-md-6">
              <a href="" title="BANK CLERK">
                <div class="all-topic clerk">
                  <h4 class="text-uppercase text-center">BANK CLERK</h4>
                  <span class="text-center"> <i class="fas fa-chevron-right"></i> List of Topics</span>
                </div>
              </a>
            </div>
            <div class="col-xl-3 col-6 col-md-6">
              <a href="" title="CDS">
                <div class="all-topic cds">
                  <h4 class="text-uppercase text-center">CDS</h4>
                  <span class="text-center"> <i class="fas fa-chevron-right"></i> List of Topics</span>
                </div>
              </a>
            </div>
            <div class="col-xl-3 col-6 col-md-6">
              <a href="" title="SSC CHSL (10+2)">
                <div class="all-topic chcl">
                  <h4 class="text-uppercase text-center">SSC CHSL (10+2)</h4>
                  <span class="text-center"> <i class="fas fa-chevron-right"></i> List of Topics</span>
                </div>
              </a>
            </div>
            <div class="col-xl-3 col-6 col-md-6">
              <a href="" title="X & Y GROUPS">
                <div class="all-topic groups">
                  <h4 class="text-uppercase text-center">X & Y GROUPS</h4>
                  <span class="text-center"> <i class="fas fa-chevron-right"></i> List of Topics</span>
                </div>
              </a>
            </div>
            <div class="col-xl-3 col-6 col-md-6">
              <a href="" title="LDC CLERK">
                <div class="all-topic ldc">
                  <h4 class="text-uppercase text-center">LDC CLERK</h4>
                  <span class="text-center"> <i class="fas fa-chevron-right"></i> List of Topics</span>
                </div>
              </a>
            </div>
          </div>
        </div>
        <a href="">
        <div class="tier">
          <h1 class="text-center text-uppercase">SSC 10+2 & CGL TIER - I</h1>
        </div>
      </a>
      </div>
    </div>
  </div>
  </div>
</section>
@endsection