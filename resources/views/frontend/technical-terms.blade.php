@extends('frontend/layout/layout')
@section('content')
<section id="bredcum">
    <div class="container">
        <div class="row">
            <div class="col-xl-6 col-md-6 col-6">
                <h4 class="text-capitalize">{{ $questions->title }}</h4>
            </div>
            <div class="col-xl-6 col-md-6 col-6">
                <span class="text-capitalize float-right">
                    <a href="{{ url('/') }}" class="home-main">Home</a>
                    <i class="fas fa-chevron-right"></i>
                    <a href="{{ $termUrl }}">Technical Terms</a>
                    <i class="fas fa-chevron-right"></i>
                    <u class="mater">{{ $questions->title }}</u>
                </span>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</section>
<section id="email-marketing">
    <div class="container">
        <div class="row">
            <div class="col-xl-3 col-12 col-md-5" id="email-sidepanel">
                <aside>
                    <div class="left-side-panel">
                        <h2 class="text-center">
                            {{ unSlugify(Request::segment(2)) }}        
                        </h2>
                        <ul>
                            @foreach($subcategories as $subcat)
                            <li>
                                <a href="{{ $termUrl.'/'.$subcat['cat_slug'] }}" class="{{ $questions->sub_category_id==$subcat['sub_category_id'] ? 'active' : '' }}" title="{{ $subcat['title'] }}">
                                    <i class="fas fa-chevron-right"></i>
                                    {{ $subcat['title'] }}
                                </a>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </aside>
            </div>
            <div class="col-xl-9 col-md-7">
                <div class="vedio-form">
                    <form name="myform" method="POST" action="{{ $termUrl }}">
                        {{ csrf_field() }}
                        <div class="input-group" id="seacr_panel">
                          <!-- <div class="select-dropdown">
                            <select class="form-control" name="category" id="withSelect">
                            @foreach($subcategories as $subcat)
                              <option {{ request()->category==$subcat->sub_category_id || Request::segment(3)==$subcat->cat_slug ? selected :''}} value="{{ $subcat->sub_category_id }}">{{ $subcat['title'] }}</option>
                            @endforeach
                            </select>
                          </div> -->
                          <input type="text" name="title" class="form-control vedio-search" value="{{ request()->title }}" placeholder="Search...">
                          <span class="input-group-btn">
                            <button type="submit" title="Search" class="btn btn-default header-submit">SEARCH</button>
                          </span>
                        </div>
                    </form>
                </div>
                <div class="clearfix"></div>
                <br>
                <div class="row">
                    @forelse($questions['technicalTerms'] as $ques)
                        <div class="col-xl-4 col-md-4 clearfix">
                            <div class="accordion_technical_head">
                                <p class="text-capitalize">
                                    <?php echo getTextTransform($ques->iq_ques_ans_q,0) ?>    
                                </p>
                                @if($ques->is_new==1)
                                <span class="badge-new">new</span>
                                @endif
                                <!-- <span class="plusminus">+</span> -->
                            </div>
                            <div class="accordion_tech_body">
                                <p><?php echo getTextTransform($ques->iq_ques_ans_a,0) ?></p>
                            </div>
                        </div>
                    @empty
                        <div class="col-xl-12 col-md-12">
                            @include('frontend.layout.search-not-found')
                        </div>
                    @endforelse
                </div>
            </div>
        </div>
    </div>
</section>
@endsection