@extends('frontend/layout/layout')
@section('content')
<section id="bredcum">
    <div class="container">
        <div class="row">
            <div class="col-xl-6 col-md-6 col-6">
                <h4 class="text-capitalize">
                  <?php echo nl2br($materialSingle->study_ques) ?>
                </h4>
            </div>
            <div class="col-xl-6 col-md-6 col-6">
                <span class="text-capitalize float-right">
                    <a href="{{ url('/') }}" class="home-main">Home</a>
                    <i class="fas fa-chevron-right"></i>
                    <a href="#">Study Material</a>
                    <i class="fas fa-chevron-right"></i>
                    <u class="mater" style="font-size: 15px !important" >
                      <?php echo nl2br($materialSingle->study_ques) ?>
                    </u>
                </span>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</section>

<section id="digital-marketing">
  <div class="container">
    <div class="row">
      <div class="sidemenu"> Side Bar
        <button class="toggle float-right"><i class="fas fa-align-justify"></i></button>
      </div>
      <div class="col-xl-3 col-12 col-md-3" id="target">
        <aside>
          @foreach($subcategories as $cat)
            <div class="left-side-panel">
                <h2 class="text-center">
                    <?php 
                      echo getTextTransform($cat->title,0);
                    ?>        
                </h2>
                <ul>
                    @foreach($cat['StudyMaterials'] as $material)
                    <li>
                        <a href="{{ url($material_url.'/'.$material['study_material_slug']) }}" class="{{ $materialSingle->study_material_id==$material->study_material_id ? 'active' : '' }}"  id="study-title-{{ $material->study_material_id }}" >
                            <i class="fas fa-chevron-right"></i>
                            <?php echo nl2br($material['study_ques']) ?>
                            <!-- title="{{ $material['study_ques'] }}" -->
                        </a>
                    </li>
                    @endforeach
                </ul>
            </div>
          @endforeach
        </aside>
      </div>

      <div class="col-xl-7 col-12 col-md-7" id="study_material_section">
        <div class="mid-section">
          <div class="digital-head">
            <h1 class="text-uppercase float-left">{{ unSlugify(Request::segment(1)) }}</h1>
          </div>
          <div class="digital-head-img">
            <img src="{{ url('public/frontend/images/sidedigital-img.png') }}" alt="sidedigital-img" class="float-right img-fluid">
          </div>
          <div class="clearfix"></div>
        </div>
        <div class="digital-intro">
          <div class="row">
            <div class="col-xl-2 col-3 col-md-2">
              <a href="{{ url($material_url.'/'.$previous->study_material_slug) }}" title="Previous">
                <button type="button" data-material="" class="btn btn-primary float-left prev-study">
                    < Previous
                </button>
              </a>
            </div>
            <div class="col-xl-8 col-6  col-md-8">
              <h5 class="text-capitalize text-center">
                  <?php echo nl2br($materialSingle->study_ques) ?>
              </h5>
            </div>
            <div class="col-xl-2 col-3 col-md-2">
              <a href="{{ url($material_url.'/'.$next->study_material_slug) }}" title="Next">
                <button type="button" data-material="{{ url($material_url.'/'.$next->study_material_slug) }}" class="btn btn-primary float-right next-study"> 
                  Next >
                </button>
              </a>
            </div>
            <div class="clearfix"></div>
          </div>
          <p>
            <?php echo nl2br($materialSingle->study_ques_ans) ?>
          </p>
          <div class=" prev-next">
            <a href="{{ url($material_url.'/'.$previous->study_material_slug) }}" title="Previous">
              <button type="button" data-material="" class="btn btn-primary float-left prev-study">
                < Previous
              </button>
            </a>

            <a href="{{ url($material_url.'/'.$next->study_material_slug) }}" title="Next">
              <button type="button" data-material="" class="btn btn-primary next-bt float-right next-study"> 
                Next >
              </button>
            </a>

            <div class="clearfix"></div>
          </div>
        </div>
      </div>
      <div class="col-xl-2 col-12 col-md-2 adds d-none d-md-block">
        @include('frontend/layout/right-sidebar')
      </div>
    </div>
  </div>
</section>
@endsection