@extends('frontend/layout/layout')
@section('content')
<section id="bredcum">
  <div class="container">
    <div class="row">
      <div class="col-xl-6 col-md-6 col-5">
        <h4 class="text-capitalize">{{ $video->title }}</h4>
      </div>
      <div class="col-xl-6 col-md-6 col-7">
        <span class="text-capitalize float-right">
          <a href="{{ url('/') }}" class="home-main">Home</a>
          <i class="fas fa-chevron-right"></i>
          <a href="{{ url($videos_url) }}" class="home-main">Videos</a>
          <i class="fas fa-chevron-right"></i>
          <u class="mater">{{ $video->title }}</u>
        </span>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
</section>
<section id="view-vedio">
  <div class="container">
    <div class="row">
      <!-- <div class="col-md-3 col-xl-3 col-12">
        @include('frontend/layout/left-sidebar')
      </div> -->
      <div class="col-md-9 col-xl-9 col-12">
        <iframe width="100%" height="100%" src="https://www.youtube.com/embed/{{ $video->link }}" allow="autoplay; encrypted-media" allowfullscreen="" class="my_class_name"></iframe>
        <div class="clearfix"></div>
        <div id="video-des">
          <h5>{{ $video->title }}</h5>
          <p>
            <?php echo nl2br($video->description); ?>
          </p>
        </div>
      </div>
      <div class="col-xl-3 col-12 col-md-3 adds d-none d-md-block">
        @include('frontend/layout/right-sidebar')
      </div>
    </div>
  </div>
</section>
<section>
  <div class="container">
   <div class="related-vedios">
    <h2 class="text-uppercase text-center">Related Videos</h2>
    <div class="col-xl-10 offset-xl-1">
     <div class="owl-carousel owl-theme owl-loaded" id="related-videos">
      @foreach($relVideos as $rVideo)
        <a href="{{ url($videos_url.'/'.$rVideo->video_slug) }}" title="{{ $rVideo->title }}">
          <div class="item">
            <div class="related-1">
              <img src="{{ url('uploads/'.$rVideo->image) }}">
                <h5>{{ $rVideo->title }}</h5>
            </div>
          </div>
        </a>
      @endforeach
    </div>
  </div>
</div>
</div>
</section>
@endsection