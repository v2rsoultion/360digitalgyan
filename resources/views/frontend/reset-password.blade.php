@extends('frontend/layout/layout')
@section('content')
<section id="bredcum">
  <div class="container">
    <div class="row">
      <div class="col-xl-6 col-md-6 col-5">
        <h4 class="text-capitalize">Reset password</h4>
      </div>
      <div class="col-xl-6 col-md-6 col-7">
        <span class="text-capitalize float-right">
          <a href="{{ url('/') }}" class="home-main">Home</a>
          <i class="fas fa-chevron-right"></i>
          <a href="{{ url($videos_url) }}" class="home-main">Videos</a>
          <i class="fas fa-chevron-right"></i>
          <u class="mater">Reset password</u>
        </span>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
</section>
<section id="view-vedio">
  <div class="container">
    <div class="row">
      <div class="col-xl-3 col-12 col-md-3 adds d-none d-md-block">
        <!-- @include('frontend/layout/right-sidebar') -->
      </div>
      <div class="col-md-6">

          @if(session()->has('success'))
            <div class="alert alert-success alert-dismissible text-muted m-b-10 col-lg-6 col-md-6 col-sm-12 col-xs-12 font-13 mx-auto green fade show" role="alert">
              <?php echo getTextTransform(session()->get('success'),0) ?>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
          @endif

          @if($errors->any())
              <div class="alert alert-danger alert-dismissible text-muted m-b-10 col-lg-6 col-md-6 col-sm-12 col-xs-12 font-13 mx-auto text-danger fade show" role="alert">
              <?php echo getTextTransform($errors->first(),0) ?>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
          @endif

            <div class="reset-password-section">
              @if($msg=='')
              <form id="reset-password" method="post" action="{{ url('user/save-reset-password/'.$uid) }}">
                  {{ csrf_field() }}
                  <h4 class="text-center">
                    Reset password
                    </h4>
                  <div class="col-md-12 col-xl-12 padding-remove">
                      <div class="form-group">
                          <input type="password" class="form-control form-fields" required name="new_password" placeholder="New password">
                      </div>
                  </div>
                  
                  <div class="col-md-12 col-xl-12 padding-remove">
                      <div class="form-group">
                          <input type="password" class="form-control form-fields" required name="cnf_password" placeholder="Confirm password">
                      </div>
                  </div>
                  
                  <div class="clearfix"></div>
                  
                  <div class="col-md-12 col-xs-12">
                      <button type="submit" class="Submit-btn all-button" title="Save">Save</button>
                  </div>
              </form>
              @else
                <h3>{{ $msg }}</h3>
              @endif          
            </div>
         </div>
      <div class="col-xl-3 col-12 col-md-3 adds d-none d-md-block">
        <!-- @include('frontend/layout/right-sidebar') -->
      </div>
    </div>
  </div>
</section>
<script>
$(document).ready(function(){

/*
** Change password
*/
$('#reset-password').bootstrapValidator({
  feedbackIcons: {
    validating: 'glyphicon glyphicon-refresh'
  },
  fields: {
    new_password: {
      validators: {
        notEmpty: {
          message: 'please enter new password'
        },
        stringLength: {
            min: 6,
            message: 'Please enter password greater than 5'
        },
        identical: {
            field: 'cnf_password',
            message: 'The password and its confirm are not the same'
        }
      }
    },
    cnf_password: {
      validators: {
        notEmpty: {
          message: 'confirm your new password'
        },
        stringLength: {
            min: 6,
            message: 'Please enter password greater than 5'
        },
        identical: {
            field: 'new_password',
            message: 'The password and its confirm are not the same'
        }
      }
    },
  }
});

});
</script>
@endsection