<div class="mid-section">
  <div class="digital-head">
    <h1 class="text-uppercase float-left">{{ unSlugify(Request::segment(1)) }}</h1>
  </div>
  <div class="digital-head-img">
    <img src="{{ url('public/frontend/images/sidedigital-img.png') }}" alt="sidedigital-img" class="float-right img-fluid">
  </div>
  <div class="clearfix"></div>
</div>
<div class="digital-intro">
  <div class="row">
    <div class="col-xl-2 col-3 col-md-2">
      <a href="" title="Previous">
        <button type="button" data-material="{{ $previous }}" class="btn btn-primary float-left prev-study">
            < Previous
        </button>
      </a>
    </div>
    <div class="col-xl-8 col-6  col-md-8">
      <h5 class="text-capitalize text-center">
          {{ $materialSingle->study_ques }}
      </h5>
    </div>
    <div class="col-xl-2 col-3 col-md-2">
      <a href="" title="Next">
        <button type="button" data-material="{{ $next }}" class="btn btn-primary float-right next-study"> 
          Next >
        </button>
      </a>
    </div>
    <div class="clearfix"></div>
  </div>
  <p>
    <?php echo getTextTransform($materialSingle->study_ques_ans,0) ?>
  </p>
  <div class=" prev-next">
    <a href="" title="Previous">
      <button type="button" data-material="{{ $previous }}" class="btn btn-primary float-left prev-study">
        < Previous
      </button>
    </a>

    <a href="" title="Next">
      <button type="button" data-material="{{ $next }}" class="btn btn-primary next-bt float-right next-study"> 
        Next >
      </button>
    </a>

    <div class="clearfix"></div>
  </div>
</div>

<script type="text/javascript">

  var next = $('.next-study');
  var prev = $('.prev-study');

  next.add(prev).click(function(e){
    e.preventDefault();
    var id = $(this).attr('data-material');
    var token = '{!!csrf_token()!!}';
    $.ajax({
      url: "{{ url('digital-marketing/get-study-materials') }}",
      type: 'GET',
      data: {
        'material_id':id
      },
      success: function (res)
      {
        $('#target aside li a').removeClass('active');
        $('#study-title-'+id).addClass('active');
        $('#study_material_section').html(res);
      }
    });
  });
</script>

<!-- <script type="text/javascript">

  var next = $('.next-study');
  var prev = $('.prev-study');

  next.add(prev).click(function(e){
    e.preventDefault();
    var id = $(this).attr('data-material');
    var token = '{!!csrf_token()!!}';
    $.ajax({
      url: "{{ url('digital-marketing/get-study-materials') }}",
      type: 'GET',
      data: {
        'material_id':id
      },
      success: function (res)
      {
        $('#target aside li a').removeClass('active');
        $('#study-title-'+id).addClass('active');
        $('#study_material_section').html(res);
      }
    });
  });
</script> -->