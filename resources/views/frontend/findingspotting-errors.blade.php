@extends('frontend/layout/layout')
@section('content')
<section id="bredcum">
  <div class="container">
    <div class="row">
      <div class="col-xl-6 col-md-6 col-6">
        <h4 class="text-capitalize">Finding / Spotting Errors</h4>
      </div>
      <div class="col-xl-6 col-md-6 col-6">
        <span class="text-capitalize float-right"><a href="{{ url('/') }}" class="home-main">Home</a> <i class="fas fa-chevron-right"></i> <u class="mater">  Finding / Spotting Errors</u></span>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
</section>
<section id="spotting-error">
  <div class="container">
    <div class="row">
      <div class="sidemenu"> Side Bar
        <button class="toggle float-right"><i class="fas fa-align-justify"></i></button>
      </div>
      <div class="col-xl-3 col-12 col-md-4" id="target">
        <div class="left-side-panel">
          <h2 class=" text-left">Test Your Grammar
            Knowledge
          </h2>
          <ul>
            <li><a href="" title="CSS Online Training - Home"><i class="fas fa-chevron-right"></i>Tenses</a></li>
            <li><a href="" title="CSS - Introduction"><i class="fas fa-chevron-right"></i>Modal Verbs</a></li>
            <li><a href="" title="CSS - Basic Syntax"><i class="fas fa-chevron-right"></i>Prepositions</a></li>
            <li><a href="" title="CSS - Selectors"><i class="fas fa-chevron-right"></i>SSC Quick Skill Online Test</a></li>
            <li><a href="" title="CSS - Inclusion in HTML"><i class="fas fa-chevron-right"></i>Verbs</a></li>
            <li><a href="" title="CSS - Colors"><i class="fas fa-chevron-right"></i>If Conditions </a></li>
            <li><a href="" title="CSS - Backgrounds"><i class="fas fa-chevron-right"></i>Articles</a></li>
            <li><a href="" title="CSS -Font"><i class="fas fa-chevron-right"></i>Active & Passive Voice</a></li>
          </ul>
        </div>
      </div>
      <div class="col-xl-9 col-12 col-md-8">
        <?php for ($i=0; $i <10 ; $i++) {  ?>
        <div class="all-quesshow">
          <i class="fas fa-circle"></i>
          <h5 class="float-left">Each one of you (A) / must make up their mind (B) / as I did. (C) / No error (D) SSC CGL TIER- 1  (21.04.2013)</h5>
          <div class="clearfix"></div>
          <ul>
            <li class="float-left">
              <u class="float-left">A.</u> <b class="float-left">Each one of you </b>
              <div class="clearfix"></div>
            </li>
            <li class="float-left">
              <u class="float-left">B.</u> <b class="float-left">Must make up their mind</b>
              <div class="clearfix"></div>
            </li>
            <li class="float-left">
              <u class="float-left">C.</u> <b class="float-left">As I did.</b>
              <div class="clearfix"></div>
            </li>
            <li class="float-left">
              <u class="float-left">D.</u> <b class="float-left">No error</b>
              <div class="clearfix"></div>
            </li>
            <div class="clearfix"></div>
          </ul>
          <div class="col-xl-12">
            <div class="show-answer" id="show-answer<?php echo $i ?>">
              <div class="ans-option float-left" id="ans-option<?php echo $i ?>" >
                <h4 class="float-left">Answer : </h4>
                <h4 class="float-left"> Option B</h4>
                <div class="clearfix"></div>
                <h4 class="float-left">Explanation  : </h4>
                <h4 class="float-left"> Your is a possessive determiner of you, so we must use</h4>
              </div>
              <button id="reveal<?php echo $i ?>" class="reveal float-right" onclick="hideQues(<?php echo $i ?>)">Hide Answer </button>
              <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
        <?php } ?>      
      </div>
    </div>
  </div>
</section>
@endsection