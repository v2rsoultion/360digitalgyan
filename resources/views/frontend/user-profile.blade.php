@extends('frontend/layout/layout')
@section('content')
<section id="bredcum">
  <div class="container">
    <div class="row">
      <div class="col-xl-6 col-md-6 col-5">
        <h4 class="text-capitalize">My profile</h4>
      </div>
      <div class="col-xl-6 col-md-6 col-7">
        <span class="text-capitalize float-right">
          <a href="{{ url('/') }}" class="home-main">Home</a>
          <i class="fas fa-chevron-right"></i>
          <a href="{{ url($videos_url) }}" class="home-main">Videos</a>
          <i class="fas fa-chevron-right"></i>
          <u class="mater">My profile</u>
        </span>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
</section>
<section id="view-vedio">
  <div class="container">
    <div class="row">
      <div class="col-md-9">

          @if(session()->has('success'))
            <div class="alert alert-success alert-dismissible text-muted m-b-10 col-lg-6 col-md-6 col-sm-12 col-xs-12 font-13 mx-auto green fade show" role="alert">
              <?php echo getTextTransform(session()->get('success'),0) ?>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
          @endif

          @if($errors->any())
              <div class="alert alert-danger alert-dismissible text-muted m-b-10 col-lg-6 col-md-6 col-sm-12 col-xs-12 font-13 mx-auto text-danger fade show" role="alert">
              <?php echo getTextTransform($errors->first(),0) ?>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
          @endif

          <div class="user-profile-section">
            <ul class="nav nav-pills" role="tablist">
              <li class="nav-item">
                <a class="nav-link active" data-toggle="pill" href="#profile">My profile</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" data-toggle="pill" href="#change-password">Change Password</a>
              </li>
              <li>
                
              </li>
            </ul>
            <div class="tab-content" >
                <div id="profile" class="container tab-pane active"><br>
                  <h3>Profile</h3>
                  <p>
                    <form id="update-profile" method="post" action="{{ url('user/update-profile') }}">
                        {{ csrf_field() }}
                        <div class="col-xl-12 col-12 padding-remove">
                            <div class="form-group">
                                <input type="text" class="form-control form-fields" placeholder="Name" name="name" required value="{{ $user->seo_users_name }}">
                            </div>
                        </div>
                        <div class="col-md-12 col-xl-12 padding-remove">
                            <div class="form-group">
                                <input type="text" class="form-control form-fields" readonly placeholder="Email" name="email" value="{{ $user->seo_users_email }}" >
                            </div>
                        </div>
                        <div class="col-md-12 col-xl-12 padding-remove">
                            <div class="form-group">
                                <input type="text" class="form-control numeric form-fields" placeholder="Mobile" name="mobilenumber" value="{{ $user->seo_user_phone }}"  minlength="10" >
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12 col-xs-12">
                            <button type="submit" class="Submit-btn all-button" title="Save">Save</button>
                        </div>
                    </form>
                  </p>
                </div>
                <div id="change-password" class="container tab-pane fade"><br>
                  <h3>Change Password</h3>
                  <p>
                    <form id="change-password-form" method="post" action="{{ url('user/change-password') }}">
                        {{ csrf_field() }}
                        <div class="col-xl-12 col-12 padding-remove">
                            <div class="form-group">
                                <input type="password" class="form-control form-fields" placeholder="New  password" name="new_password" required>
                            </div>
                        </div>
                        <div class="col-md-12 col-xl-12 padding-remove">
                            <div class="form-group">
                                <input type="password" class="form-control form-fields" placeholder="Confirm  password" name="cnf_password" required>
                            </div>
                        </div>
                        <div class="col-xl-12 col-12 padding-remove">
                            <div class="form-group">
                                <input type="password" class="form-control form-fields" placeholder="Current password" name="cur_password" required>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12 col-xs-12">
                            <button type="submit" class="Submit-btn all-button" title="Update Password">Update Password</button>
                        </div>
                    </form>
                  </p>
                </div>
            </div>
         </div>
      </div>
      <div class="col-xl-3 col-12 col-md-3 adds d-none d-md-block">
        @include('frontend/layout/right-sidebar')
      </div>
    </div>
  </div>
</section>

<script>
$(document).ready(function(){
/*
** update profile
*/
$('#update-profile').bootstrapValidator({
  feedbackIcons: {
    validating: 'glyphicon glyphicon-refresh'
  },
  fields: {
    name: {
      validators: {
        notEmpty: {
          message: 'Name is required and cannot be empty'
        }
      }
    },
    email: {
      validators: {
        notEmpty: {
          message: 'Email is required and cannot be empty'
        }
      }
    },
    mobilenumber: {
      validators: {
        stringLength: {
            min: 10,
            max: 10,
            message: 'Please enter 10 characters of number'
        }
      }
    },
  }
});


/*
** Change password
*/
$('#change-password-form').bootstrapValidator({
  feedbackIcons: {
    validating: 'glyphicon glyphicon-refresh'
  },
  fields: {
    cur_password: {
      validators: {
        notEmpty: {
          message: 'please enter old password'
        }
      }
    },
    new_password: {
      validators: {
        notEmpty: {
          message: 'please enter new password'
        },
        stringLength: {
            min: 6,
            message: 'Please enter password greater than 5'
        },
        identical: {
            field: 'cnf_password',
            message: 'The password and its confirm are not the same'
        }
      }
    },
    cnf_password: {
      validators: {
        notEmpty: {
          message: 'confirm your new password'
        },
        stringLength: {
            min: 6,
            message: 'Please enter password greater than 5'
        },
        identical: {
            field: 'new_password',
            message: 'The password and its confirm are not the same'
        }
      }
    },
  }
});


/*
**  for hide alert with time
*/
window.setTimeout(function(){
  $(".alert").fadeTo(500, 0).slideUp(500, function(){
      $(this).remove();
  });
},2000);

});
</script>

@endsection