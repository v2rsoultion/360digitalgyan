@extends('frontend/layout/layout')
@section('content')
<section id="bredcum">
  <div class="container">
    <div class="row">
      <div class="col-xl-6 col-md-6 col-6">
        <h4 class="text-capitalize">Question & answer</h4>
      </div>
      <div class="col-xl-6 col-md-6 col-6">
        <span class="text-capitalize float-right"><a href="{{ url('/') }}" class="home-main">Home</a>  >  <u class="mater">Question & answer</u></span>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
</section>
<section id="ques-ans" class="asks-ans">
  @if(session()->has('success'))
    <div class="alert alert-success alert-dismissible text-muted m-b-10 col-lg-6 col-md-6 col-sm-12 col-xs-12 font-13 mx-auto green fade show" role="alert">
      <?php echo getTextTransform(session()->get('success'),0) ?>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  @endif

  @if($errors->any())
      <div class="alert alert-danger alert-dismissible text-muted m-b-10 col-lg-6 col-md-6 col-sm-12 col-xs-12 font-13 mx-auto text-danger fade show" role="alert">
      <?php echo getTextTransform($errors->first(),0) ?>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  @endif
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-xl-9 col-12">
        <div class="question-type">
          <div class="col-xl-12 col-md-12 col-12">
            <div class="row">
              <div class="col-xl-8 col-md-8 col-12">
                <h3 class="float-left">Q.1 {{ $question->user_question }}</h3>
                <div class="clearfix"></div>
                <ul>
                  <?php
                    $tags = explode(',',$question->question_tags);
                  ?>
                  @foreach($tags as $tag)
                    <li><a href="" title="{{ $tag }}">{{ $tag }}</a> </li>
                  @endforeach
                </ul>
                <div class="ques-side">
                  <a href=""> <img src="{{ $question['getUser']->seo_users_image!='' ? url('uploads/'.$question['getUser']->seo_users_image) : url('public/frontend/images/boy.png') }}" class="float-left" alt="boy"></a>
                  <div class="ques-side2 float-left">
                    <span class="text-center">
                      <a href="" title="{{ $question['getUser']->seo_users_name }}">
                        <u>{{ $question['getUser']->seo_users_name }}</u>
                      </a>
                      <br>
                      <i class="fas fa-calendar-alt"></i>
                      {{ date("d M,Y", strtotime($question->ques_time)) }}
                    </span>
                  </div>
                  <div class="clearfix"></div>
                </div>
              </div>
              <div class="col-xl-4 col-md-4 col-12 side-men">
                <i class="fas fa-ellipsis-h"  data-toggle="dropdown"></i>
                <div class="dropdown-menu dropdown-menu-right">
                  <a class="dropdown-item" href="#" title="Edit Profile"> Edit Profile</a>
                  <a class="dropdown-item" href="#" title="My Inbox"> My Inbox</a>
                  <a class="dropdown-item" href="#" title="Task">Task</a>
                  <a class="dropdown-item" href="#" title="Chats"> Chats</a>
                </div>
                <div class="clearfix"></div>
                <div class="like-dislike float-right">
                  @if(session('digital_user_id')!='')
                    <a data-toggle="modal" data-target="#ask-answer" class="setQues" title="Answer">
                      <span>
                        <i class="fas fa-comment-alt"></i>Answer
                      </span>
                    </a>
                  @else
                    <a onclick="preventPostAns()" class="setQues" title="Answer">
                      <span>
                        <i class="fas fa-comment-alt"></i>Answer
                      </span>
                    </a>
                  @endif
                  
                    <span>
                      <i class="fas fa-eye"></i>
                      {{ $question->view_question_count }}
                    </span>
                    <span1>
                      <span class="{{ session('digital_user_id')!='' && !in_array($question->ques_id,$quesLikedByUser) ? 'like-dislike-question ' : '' }}{{ in_array($question->ques_id,$quesLikedByUser) ? 'like' : '' }}">
                        <i class="fas fa-thumbs-up"></i>
                          {{ $question->get_likes_count }}
                      </span>
                      <span class="{{ session('digital_user_id')!='' && !in_array($question->ques_id,$quesDislikedByUser) ? 'like-dislike-question ' : '' }}{{ in_array($question->ques_id,$quesDislikedByUser) ? 'dislike' : '' }}">
                        <i class="fas fa-thumbs-down"></i>
                        {{ $question->get_dis_likes_count }}
                      </span>
                    </span1>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row ask-ques">
          <div class="col-xl-6 col-6 col-md-6">
            <h5>Lorem ipsum dolor sit amet, consectetur</h5>
          </div>
          <div class="col-xl-6 col-6 col-md-6">
            <div class="post-ques float-right">
              @if(session('digital_user_id')!='')
                <button title="Post Your Answer" data-toggle="modal" data-target="#ask-answer" class="text-capitalize" data-backdrop="static" data-keyboard="false">
                Post Your Answer
              </button>
              @else
                <button title="Post your answer" class="text-capitalize" onclick="preventPostQues()">Post Your Answer</button>
              @endif
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
        <div class="answer-pannel">
          @foreach($question['answers'] as $ans)
          <div class="question-type answer-type" data-ans="{{ $ans->answer_id }}" >
            <div class="col-xl-12 col-md-12 col-12 answer-pading">
              <div class="row">
                <div class="col-xl-9 col-md-8 col-12">
                  <p>
                    <?php echo getTextTransform($ans->ques_answer,0); ?>
                  </p>
                </div>
                <div class="col-xl-3 col-md-4 col-12 side-men">
                  <i class="fas fa-ellipsis-h"  data-toggle="dropdown"></i>
                  <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="#" title="Edit Profile"> Edit Profile</a>
                    <a class="dropdown-item" href="#" title="My Inbox"> My Inbox</a>
                    <a class="dropdown-item" href="#" title="Task">Task</a>
                    <a class="dropdown-item" href="#" title="Chats"> Chats</a>
                  </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-xl-9 col-md-8 col-12">
                  <div class="ques-side">
                    <a href=""><img src="{{ $ans['getAnsUser']->seo_users_image!='' ? url('uploads/'.$ans['getAnsUser']->seo_users_image) : url('public/frontend/images/boy.png') }}" class="float-left" alt="boy"></a>
                    <div class="ques-side2 float-left">
                      <span class="text-center">
                        <a href="" title="{{ $ans['getAnsUser']->seo_users_name }}">
                          <u>{{ $ans['getAnsUser']->seo_users_name }}</u> 
                        </a>
                        <br>
                        <i class="fas fa-calendar-alt"></i>
                        {{ date("d M,Y", strtotime($ans->ans_time)) }}
                      </span>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                </div>
                <div class="col-xl-3 col-md-4 col-12">
                  <div class="like-dislike float-right">
                    <span class="{{ session('digital_user_id')!='' ? 'like-dislike-answer ' : '' }}{{ in_array($ans->answer_id,$ansLikedByUser) ? 'like' : '' }}">
                      <i class="fas fa-thumbs-up"></i>
                      {{ $ans->get_ans_likes_count }}
                    </span>
                    <span class="{{ session('digital_user_id')!='' ? 'like-dislike-answer ' : '' }}{{ in_array($ans->answer_id,$ansDislikedByUser) ? 'dislike' : '' }}">
                      <i class="fas fa-thumbs-down"></i>
                      {{ $ans->get_ans_dis_likes_count }}
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
          @endforeach
      </div>
    </div>
    <div class="col-xl-3 col-md-4 d-none d-md-block">
      <div class="ques-sidebar">
        <h4>Unanswered Questions</h4>
        <ul>
          @foreach($unAnswerd as $ques)
          <li>
            <a href="{{ $questionUrl.'/answer/'.$ques->ques_id }}" title="{{ $ques->user_question }}">{{ $ques->user_question }}</a>
            <div class="clearfix"></div>
            <span>asked on {{ date("d M,Y", strtotime($ques->ques_time)) }}</span>
          </li>
          @endforeach
          <a href="" class="all-button" title="View All">View All</a>
        </ul>
      </div>
      <div class="ques-sidebar">
        <h4>Related Questions</h4>
        <ul>
          @foreach($relQuestions as $ques)
          <li>
            <a href="{{ $questionUrl.'/answer/'.$ques->ques_id }}" title="{{ $ques->user_question }}">{{ $ques->user_question }}</a>
            <div class="clearfix"></div>
            <span>asked on {{ date("d M,Y", strtotime($ques->ques_time)) }}</span>
          </li>
          @endforeach
          <a href="" class="all-button" title="View All">View All</a>
        </ul>
      </div>
    </div>
  </div>
  </div>
</section>


<div class="modal ask-modal fade" id="ask-answer">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Give Answer</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <form method="post" id="form-save-answer" action="{{ url('user/save-answer') }}">
        {{ csrf_field() }}
        <div class="modal-body">
          <h5>Q.1 {{ $question->user_question }}</h5>
          <div class="form-group">
            <textarea class="form-control" name="user_answer" required placeholder="Type something here..." rows="5" id="comment"></textarea>
          </div>
          <input type="hidden" name="category_name" value="{{ Request::segment(1) }}">
          <input type="hidden" name="quetion_name"  value="{{ $question->ques_id }}">
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn all-button" title="SAVE">SAVE</button>
          <button type="button" class="btn cancel-button" data-dismiss="modal" title="CANCEL">
            CANCEL
          </button>
        </div>
      </form>
    </div>
  </div>
</div>

{!! Html::script('public/frontend/js/jquery.tagsinput-revisited.js') !!}
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

<script type="text/javascript">
$(document).ready(function(){

  /* for like dislike question */
  $(document).on('click', '.like-dislike-question',function(event){
    var id     = <?php echo $question->ques_id ?>;
    var elment = $(this);
    if ($(this).find('i').hasClass('fa-thumbs-up')){
      liketype1 = 'like';
    }else{
      liketype1 = 'dislike';
    }
    $.ajax({
      type: "POST",
      url: "{{ url('user/like-question') }}",
      data: {
        "_token"  : "{{ csrf_token() }}",
        data_id   : id,
        liketype  : liketype1,
        ques_ans  : 'question'
      },
      success: function(res){
        if(res.status==1){
          elment.parent('span1').html(res.data);
        }
      },
      error: function(){
        alert(" :( Sorry something went wrong..!!");
      }
    });
    return false;
  });


  /* for like dislike answer */
  $(document).on('click', '.like-dislike-answer',function(event){
    var id2      = $(this).closest('div.answer-type').attr('data-ans');
    var elment2  = $(this);
    if ($(this).find('i').hasClass('fa-thumbs-up')){
      liketype2 = 'like';
    }else{
      liketype2 = 'dislike';
    }

    $.ajax({
      type: "POST",
      url: "{{ url('user/like-answer') }}",
      data: {
        "_token"  : "{{ csrf_token() }}",
        data_id   : id2,
        liketype  : liketype2,
        ques_ans  : 'answer'
      },
      success: function(res){
        if(res.status==1){
          elment2.parent('div').html(res.data);
        }
      },
      error: function(){
        alert(" :( Sorry something went wrong..!!");
      }
    });
    return false;
  });

});

/*
**  for hide alert with time
*/
window.setTimeout(function(){
  $(".alert").fadeTo(500, 0).slideUp(500, function(){
      $(this).remove();
  });
},4000);

</script>
@endsection