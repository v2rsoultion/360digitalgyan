@foreach($questions->chunk(5) as $chunk)
  @foreach($chunk as $key => $ques)
  <div class="question-type">
    <div class="col-xl-12 col-md-12 col-12">
      <div class="row questions-parent" data-ques="{{ $ques->ques_id }}">
        <div class="col-xl-9 col-md-8 col-12">
          <h3> 
            <a href="{{ $questionUrl.'/answer/'.$ques->ques_id }}">
              Q.{{ $questions->firstItem() + $key }}&nbsp;&nbsp;{{ $ques->user_question }}</h3>
            </a>
          <ul>
            <?php
              $tags = explode(',',$ques->question_tags);
            ?>
            @foreach($tags as $tag)
              <li><a href="" title="{{ $tag }}">{{ $tag }}</a> </li>
            @endforeach
          </ul>
        </div>
        <div class="col-xl-3 col-md-4 col-12 side-men">
          <i class="fas fa-ellipsis-h" data-toggle="dropdown"></i>
          <div class="dropdown-menu dropdown-menu-right">
            <a class="dropdown-item" href="#" title="Edit Profile"> Edit Profile</a>
            <a class="dropdown-item" href="#" title="My Inbox"> My Inbox</a>
            <a class="dropdown-item" href="#" title="Task">Task</a>
            <a class="dropdown-item" href="#" title="Chats"> Chats</a>
          </div>
          <div class="clearfix"></div>
        </div>
        <div class="col-xl-9 col-md-7">
          <div class="ques-side">
            <a href=""> <img src="{{ $ques['getUser']->seo_users_image!='' ? url('uploads/'.$ques['getUser']->seo_users_image) : url('public/frontend/images/boy.png') }}" class="float-left" alt="{{ $ques['getUser']->seo_users_name }}"></a>
            <div class="ques-side2 float-left">
              <span class="text-center">
                <a href="" title="{{ $ques['getUser']->seo_users_name }}">
                  <u>{{ $ques['getUser']->seo_users_name }}</u>
                </a>
                <br>
                <i class="fas fa-calendar-alt"></i>
                  {{ date("d M,Y", strtotime($ques->ques_time)) }}
              </span>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
        <div class="col-xl-3 col-md-5">
          <div class="like-dislike float-right">
            @if(session('digital_user_id')!='')
              <a data-toggle="modal" data-target="#ask-answer" class="setQues" title="Answer">
                <span>
                  <i class="fas fa-comment-alt"></i>Answer
                </span>
              </a>
            @else
              <a onclick="preventPostAns()" class="setQues" title="Answer">
                <span>
                  <i class="fas fa-comment-alt"></i>Answer
                </span>
              </a>
            @endif
            <span1>
              <span class="{{ session('digital_user_id')!='' && !in_array($ques->ques_id,$likedByUser) ? 'like-dislike-question ' : '' }}{{ in_array($ques->ques_id,$likedByUser) ? 'like' : '' }}">
                <i class="fas fa-thumbs-up"></i>{{ $ques->get_likes_count }}
              </span>
              <span class="{{ session('digital_user_id')!='' && !in_array($ques->ques_id,$dislikedByUser) ? 'like-dislike-question ' : '' }} {{ in_array($ques->ques_id,$dislikedByUser) ? 'dislike' : '' }}">
                <i class="fas fa-thumbs-down"></i>{{ $ques->get_dis_likes_count }}
              </span>
            </span1>
          </div>
        </div>
      </div>
    </div>
  </div>
  @endforeach
@endforeach
{!! $questions->appends($appends)->render() !!}