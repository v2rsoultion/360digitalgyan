@extends('frontend/layout/layout')
@section('content')
<section id="bredcum">
    <div class="container">
        <div class="row">
            <div class="col-xl-6 col-md-6 col-6">
                <h4 class="text-capitalize">{{ $questions->title }}</h4>
            </div>
            <div class="col-xl-6 col-md-6 col-6">
                <span class="text-capitalize float-right">
                    <a href="{{ url('/') }}" class="home-main">Home</a>
                    <i class="fas fa-chevron-right"></i>
                    <a href="{{ $practiceUrl }}">Practice Quiz</a>
                    <i class="fas fa-chevron-right"></i>
                    <u class="mater">{{ $questions->title }}</u>
                </span>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</section>
<section id="practice-quiz">
  <div class="container">
    <div class="row">
      <div class="sidemenu"> Side Bar
        <button class="toggle float-right"><i class="fas fa-align-justify"></i></button>
      </div>
      <div class="col-xl-3 col-12 col-md-4" id="target">
        <aside>
          <div class="left-side-panel">
              <h2 class="text-center">
                  {{ unSlugify(Request::segment(1)) }}        
              </h2>
              <ul>
                  @foreach($subcategories as $subcat)
                  <li>
                      <a href="{{ $practiceUrl.'/'.$subcat['cat_slug'] }}" class="{{ $questions->sub_category_id==$subcat['sub_category_id'] ? 'active' : '' }}" title="{{ $subcat['title'] }}">
                          <i class="fas fa-chevron-right"></i>
                          {{ $subcat['title'] }}
                      </a>
                  </li>
                  @endforeach
              </ul>
          </div>
        </aside>
      </div>
      <div class="col-xl-6 col-12 col-md-8">
        <h3>Practice Quiz Test</h3>
        <p>Online Test :: ({{ $questions->title }}) Programming Test</p>
        <div class="digital-intro" id="result" hidden>
          <b><h5>Result:</h5></b>
          <div>
            <p class="res_ques">Total Number of Questions<span>{{ count($questions['PracticeQuiz']) }}</span></p><br>
            <p class="res_corr">Correct Answer<span id="correct_pq">0</span></p><br>
            <p class="res_dur">Incorrect Answer<span id="incorrect_pq">0</span></p><br>
            <p class="res_att">Question Attempted<span id="attemped_pq">0</span></p><br>
            <p class="res_sco">Net Score<span id="net_pq">0</span></p><br>
          </div>
          <a href=""> 
            <button class="btn btn-primary next-bt text-uppercase review_btn">
              Restart
            </button> 
          </a>
        </div>
        <div class="digital-intro" id="practice-quiz-block">
          <form id="practice-quiz-form" method="post" action="{{ url('user/practice-quiz') }}">
            {{ csrf_field() }}
            <input type="hidden" name="subcategory" value="{{ $questions->sub_category_id }}">
        @forelse($questions['PracticeQuiz'] as $key => $ques)
            <?php $key++ ?>
            <div class="radiomain" id="ques{{ $key }}" {{ $key==1 ? '' : 'hidden' }}>
              <h5>Q.{{ $key }} <?php echo getTextTransform($ques->qustion_qustion,0); ?></h5>
              <div class="radio">
                
                <input id="radio1{{ $ques->qustion_id }}" name="radio{{ $key }}" class="practice-quiz-radio" data-ans="{{ $ques->qustion_correct_answer }}" value="1" type="radio">
                <label for="radio1{{ $ques->qustion_id }}"  class="radio-label green-check">
                  <span class="radio-label"></span>
                  <p>
                    {{ $ques->qustion_option1 }}
                  </p>

                </label>
              </div>
              <div class="radio">
                
                <input id="radio2{{ $ques->qustion_id }}" name="radio{{ $key }}" class="practice-quiz-radio" data-ans="{{ $ques->qustion_correct_answer }}" value="2" type="radio">
                <label  for="radio2{{ $ques->qustion_id }}"  class="radio-label green-check">
                  <span class="radio-label"></span>
                  <p>
                    {{ $ques->qustion_option2 }}
                  </p>

                </label>
              </div>
              @if($ques->qustion_option3!='')
                <div class="radio">
                  
                  <input id="radio3{{ $ques->qustion_id }}" name="radio{{ $key }}" class="practice-quiz-radio" data-ans="{{ $ques->qustion_correct_answer }}" value="3" type="radio">
                  <label  for="radio3{{ $ques->qustion_id }}"  class="radio-label green-check">
                    <span class="radio-label"></span>
                    <p>
                      {{ $ques->qustion_option3 }}
                    </p>

                  </label>
                </div>
              @endif

              @if($ques->qustion_option4!='')
                <div class="radio">
                  
                  <input id="radio4{{ $ques->qustion_id }}" name="radio{{ $key }}" class="practice-quiz-radio" data-ans="{{ $ques->qustion_correct_answer }}" value="4" type="radio">
                  <label  for="radio4{{ $ques->qustion_id }}"  class="radio-label green-check">
                    <span class="radio-label"></span>
                    <p>
                      {{ $ques->qustion_option4 }}
                    </p>

                  </label>
                </div>
              @endif


              @if($ques->qustion_option5!='')
                <div class="radio">
                  
                  <input id="radio5{{ $ques->qustion_id }}" name="radio{{ $key }}" class="practice-quiz-radio" data-ans="{{ $ques->qustion_correct_answer }}" value="5" type="radio">
                  <label  for="radio5{{ $ques->qustion_id }}"  class="radio-label green-check">
                    <span class="radio-label"></span>
                    <p>
                      {{ $ques->qustion_option5 }}
                    </p>

                  </label>
                </div>
              @endif
            </div>
        @empty
          <div class="digital-intro" >
            <h2>Sorry , No data found!!</h2>
          </div>

        @endforelse
            <div class=" prev-next">
              <button type="button" onclick="showQuestion(2)" class="btn btn-primary float-left text-uppercase "> Previous</button>
              <button type="button" onclick="showQuestion(1)" class="btn btn-primary next-bt text-uppercase active "> Next </button>
              <button type="submit" id="practice-quiz-submit" class="btn btn-success next-bt text-uppercase active" hidden> Submit Quiz </button>
              <div class="clearfix"></div>
            </div>
            <input type="hidden" name="current" id="current_show_ques" value="1">
            <input type="hidden" name="all_questions" id="total_question" value="{{ count($questions['PracticeQuiz']) }}">
          </form>
        </div>
      </div>
      <div class="col-xl-3 col-12 col-md-3 adds d-none d-md-block">
        @include('frontend/layout/right-sidebar')
      </div>
    </div>
  </div>
</section>


<script type="text/javascript">
$(document).ready(function()
{
  // $('.green-check').click(function(){
  //   $('.green-check i').remove();
  //   $(this).append('<i class="fas fa-times red"></i>');
  // });

  $(document).on('click', '.practice-quiz-radio',function(event){
    var answer      = $(this).attr('data-ans');
    var clickAnswer = $(this).val();
    var quesName = $(this).attr('name');

    if($("input[name="+quesName+"]").hasClass('preventClick')){
      return false;
    }else{
      $("input[name="+quesName+"]").addClass('preventClick');
      if (answer==clickAnswer){
        /* for remove icons check or wrong */
        $(this).parents('div.radiomain').find('label.red').removeClass('red');
        $(this).parents('div.radiomain').find("input[name="+quesName+"]").next().find('i.fas').remove();
        /* for show correct check icon */
        $(this).next().append('<i class="fas fa-check green"></i>').addClass('green');;
        
      }else{
        
        $(this).parents('div.radiomain').find('label.red').removeClass('red');
        /* for remove icons check or wrong */
        $(this).parents('div.radiomain').find("input[name="+quesName+"]").next().find('i.fas').remove();
        /* for show cross icon */
        $(this).next().append('<i class="fas fa-times red"></i>').addClass('red');
        
        /* for show which is correct answer when choose wrong answer */
        $(this).parents('div.radiomain').find("input[name="+quesName+"]:nth("+(answer-1)+")").next().append('<i class="fas fa-check green"></i>').addClass('green');
      }
    }
  });

});


function showQuestion(type)
{
  current = $("#current_show_ques").val();
  total = $('#total_question').val();
  if(current > 0)
  {
    $("#ques"+current).attr('hidden','true');
  }

  if(type == 1)
  {
    //1 = next type
    current = parseInt(current)+1;
  }
  else
  {
    //2 = Previous type
    current = parseInt(current)-1;
  }
  if(current == 0)
  {
    current = 1;
  }

  if(current > total)
  {
    current = total;
  }

  if (current==total){
    $('#practice-quiz-submit').removeAttr('hidden');
  }else{
    // $('#practice-quiz-submit').attr('hidden','true');
  }

  $("#ques"+current).removeAttr('hidden');
  $("#current_show_ques").val(current);
}


// function save_practice()
// {
//   $.ajax({
//     type: "POST",
//     url: "{{ url('user/practice-quiz') }}",
//     data: $('#practice-quiz').serialize(),
//     success: function(res){
//         alert(res);
//     },
//     error: function(){
//       alert("We found an error in your data.  Please return to home page and try again.");
//     }
//   });
//   return false;
// }

// $(document).ready(function() {
  $('#practice-quiz-form').on('submit',function(e){
    $.ajax({
      type: "POST",
      url: "{{ url('user/practice-quiz') }}",
      data : $('#practice-quiz-form').serialize(),
      success: function(res){
        data = res.message;
        $('#correct_pq').html(data.correct);
        $('#incorrect_pq').html(data.incorrect);
        $('#attemped_pq').html(data.attemped);
        $('#net_pq').html(data.net);
        $('#practice-quiz-block').attr('hidden',true);
        $('#result').removeAttr('hidden');
      },
      error: function(){
        alert("We found an error in your data.  Please return to home page and try again.");
      }
    });
    e.preventDefault();
  });
// });

</script>

@endsection