@extends('frontend/layout/layout')
@section('content')
<section id="bredcum">
  <div class="container">
    <div class="row">
      <div class="col-xl-6 col-md-6 col-6">
        <h4 class="text-capitalize">Videos Tutorials</h4>
      </div>
      <div class="col-xl-6 col-md-6 col-6">
        <span class="text-capitalize float-right">
            <a href="{{ url('/') }}" class="home-main">Home</a>
            <i class="fas fa-chevron-right"></i>
            <u class="mater">Videos</u>
        </span>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
</section>
<section id="vedios-tutorial">
  <div class="container">
    <div class="row">
      <div class="col-md-9 col-12 col-xl-9">
        <div class="vedio-form">
          <form name="myform" method="POST" action="{{ $videos_url }}">
            {{ csrf_field() }}
            <div class="input-group" id="seacr_panel">
              <div class="select-dropdown">
                <select class="form-control" name="language" id="withSelect">
                  <option value="all">All</option>
                  <option value="1" {{ $language==1 ? 'selected' : '' }} > English </option>
                  <option value="2" {{ $language==2 ? 'selected' : '' }}> Hindi </option>
                </select>
              </div>
              <input type="text" name="category_title" class="form-control vedio-search" value="{{ request()->category_title }}" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" title="Search" class="btn btn-default header-submit">SEARCH</button>
              </span>
            </div>
          </form>
        </div>
        <div id="tutorials">
          <div class="col-xl-12 col-md-12 col-12">
            <h1 class="text-center">Videos Tutorial</h1>
          </div>
          <div class="tab-slider--nav">
            <ul class="tab-slider--tabs">
              <!-- <a href="{{ url($videos_url) }}">
                <li class="tab-slider--trigger {{ $language=='all' ? 'active' : '' }} text-uppercase" title="All Videos">
                    All Videos ({{ $AllVideoCounts }})
                </li>
              </a> -->
              <a href="{{ url($videos_url.'/hindi') }}">
                <li class="tab-slider--trigger {{ $language==2 || $language=='all'  ? 'active' : '' }} text-uppercase" title="Hindi" >
                    Hindi ({{ $hindiVideoCounts }})
                </li>
              </a>
              <a href="{{ url($videos_url.'/english') }}">
                <li class="tab-slider--trigger {{ $language==1 ? 'active' : '' }} text-uppercase" title="English">
                    English ({{ $engVideoCounts }})
                </li>
              </a>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="clearfix"></div>
          <div class="tab-slider--container container padding-remove">
            <div id="hindi" class="tab-slider--body">
              <div class="loader">
                  <div class="row english-tab" id="videosMain">
                    @forelse($categories as $cat)
                      <div class="col-xl-3 col-md-6 col-6 lagy">
                        <div class="vedio-categories">
                          <form action="{{ url($videos_url) }}" method="post">
                            @csrf
                            <input type="hidden" name="cat_slug" value="{{ $cat->cat_slug }}" >
                            <input type="hidden" name="language" value="{{ $language }}" >
                          </form>
                          <a href="{{ url($videos_url.'/'.$cat->cat_slug) }}" class="js-btn-submit" title="{{ $cat->title }}" style="background: #32aced;display: block;">
                            <img src="{{ url('uploads/'.$cat->image) }} " class="img-fluid" alt="related-img">
                          </a>
                          <a href="{{ url($videos_url.'/'.$cat->cat_slug) }}" class="js-btn-submit" title="{{ $cat->title }}">
                            <h5>
                              {{ $cat->title }}
                            </h5>
                          </a>
                          <div class="clearfix"></div>
                          <div class="cat-video-count">
                            <span><i class="fas fa-eye"></i>
                               {{ $cat->video_count }}</span>
                          </div>
                        </div>
                      </div>
                    @empty
                      <div class="col-xl-12 col-md-12 col-12 lagy">
                            @include('frontend.layout.search-not-found')
                      </div>
                    @endforelse
                  </div>
                  <center>
                    <button id="moreBtn" class="btn btn-default btn-loadmore">
                      Load More
                    </button>
                  </center>
              </div>
            </div>
          </div>
        </div>
      </div>  
      <div class="col-xl-3 col-12 col-md-3 adds d-none d-md-block">
        @include('frontend/layout/right-sidebar')
      </div>
    </div>
  </div>
</section>

{!! Html::script('public/frontend/js/loadMore.js') !!}
<script type="text/javascript">
  $("#videosMain").loadMore({
    selector: '.lagy',
    loadBtn: '#moreBtn',
    limit:12,
    load:9,
    animate: true,
    animateIn: 'fadeInUp'
  });
  
  $('.js-btn-submit').on('click', function(e) {
    e.preventDefault();
    $(this).parent().find('form').submit();
  });

</script>
@endsection