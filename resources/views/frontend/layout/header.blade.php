<!DOCTYPE html>
<html>

<head>
    <title> {!! $title !!} </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" type="image/png" href="{{ url('public/frontend/images/favicon.png') }}" />
    {!! Html::style('public/frontend/css/bootstrap.css') !!}
    {!! Html::style('public/frontend/css/animate.css') !!}
    {!! Html::style('public/frontend/css/owl.carousel.min.css') !!}
    {!! Html::style('public/frontend/css/webslidemenu.css') !!}
    {!! Html::style('public/frontend/css/white-gry.css') !!}
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <!--<link href="css/aos.css" rel="stylesheet">-->
    <!--Main Menu File-->

    <!-- <link id="theme" rel="stylesheet" type="text/css" media="all" href="css/white-gry.css" /> -->
    <link href="https://fonts.googleapis.com/css?family=Heebo:300,400,500,700,600&amp;subset=hebrew" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,600" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,500,700,600"" rel=" stylesheet">
    {!! Html::style('public/frontend/css/owl.theme.default.min.css') !!}
    {!! Html::style('public/frontend/css/sweetalert.css') !!}

    {!! Html::style('public/frontend/css/jquery.tagsinput-revisited.css') !!}
    {!! Html::style('public/frontend/css/style.css') !!}
    {!! Html::style('public/frontend/css/responsive.css') !!}
    {!! Html::script('public/frontend/js/jquery-2.1.4.min.js') !!}
    {!! Html::script('public/frontend/js/popper-1.14.7.min.js') !!}
</head>
<body>
@if($errors->any())
    <script>alert("{{ $errors->first() }}")</script>
@endif
    <div class="wsmobileheader stepprocess1 clearfix">
        <a id="wsnavtoggle" class="wsanimated-arrow"><span></span></a>
        <span class="smllogo"><a href="{{ url('/') }}"><img src="{{ url('public/frontend/images/logo.png') }}" width="190" class="mobile-logo" alt="logo" /></a></span>
        <div class="wss">
            @if(!empty(session('digital_user_id')))
                <a title="Register" class="scrool-top">
                    <i class="fas fa-user"></i>
                </a>
            @else
                <a title="Login" class="loginShow scrool-top" data-target="#login-popup"> <i class="fas fa-sign-in-alt"></i></a>
                <a title="Register" class="singShow scrool-top" data-target="#signup-popup"> <i class="fas fa-user"></i></a>
            @endif
        </div>
    </div>
    <div class="headerfull stepprocess1">
        <div class="wsmain clearfix">
            <div class="smllogo ">
                <a href="{{ url('/') }}" title="360DigitalGyan">
                    <img src="{{ url('public/frontend/images/main-logo.png') }}" alt="img_fullsize" class="scroollogo" />
                </a>
            </div>
            <nav class="wsmenu">
                <ul class="wsmenu-list menu-other ">
                    <?php
                      $headerMenus = headerMenus(true);
                    ?>
                    @foreach($headerMenus as $menu)
                    <li aria-haspopup="true">
                        <?php
                            $IQCates      = getSubCategories($menu['id'],1);
                            $QuizCates    = getSubCategories($menu['id'],2);
                        ?>
                        <a class="navtext active" href="{{ url($menu['slug'].'/videos-tutorials') }}" title="{{ $menu['title'] }}">
                            <span>{{ $menu['title'] }} <i class="fas fa-chevron-down"></i></span>
                        </a>
                        <div class="wsmegamenu clearfix  halfmenu halfmenu_mobile">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-lg-4 col-xl-4 col-md-12 videos">
                                        <ul class="wstliststy06 clearfix">
                                            <?php
                                                /* 1 = english , 2 = hindi */
                                                $engVideos      = getVideosCount($menu['id'],1);
                                                $hindiVideos    = getVideosCount($menu['id'],2);
                                            ?>
                                            @if($engVideos!=0)
                                            <li>
                                                <a href="{{ url($menu['slug'].'/videos-tutorials/english') }}" title="Video Tutorial (English)">
                                                    <i class="fas fa-chevron-right"></i>
                                                    Video Tutorial (English)
                                                </a>
                                            </li>
                                            @endif
                                            
                                            @if($hindiVideos!=0)
                                            <li>
                                                <a href="{{ url($menu['slug'].'/videos-tutorials/hindi') }}" title="Video Tutorial (Hindi)">
                                                    <i class="fas fa-chevron-right"></i>
                                                    Video Tutorial (Hindi)
                                                </a>
                                            </li>
                                            @endif
                                            <?php
                                                $techTerm       = getTechTerm($menu['id']);
                                            ?>
                                            @if($techTerm!=0)
                                            <li>
                                                <a href="{{ url($menu['slug'].'/technical-terms') }}" title="Technical Trems">
                                                    <i class="fas fa-chevron-right"></i>
                                                    Technical Terms
                                                </a>
                                            </li>
                                            @endif
                                            <?php
                                                $quesAns = getQuesAns($menu['id']);
                                            ?>
                                            @if($quesAns!=0)
                                            <li>
                                                <a href="{{ url($menu['slug'].'/questions-answers') }}" title="Questions Answer">
                                                    <i class="fas fa-chevron-right"></i>
                                                    Questions Answer
                                                </a>
                                            </li>
                                            @endif
                                            <?php
                                                $term = getTerm($menu['id']);
                                            ?>
                                            @if($term!=0)
                                            <li>
                                                <a href="{{ url($menu['slug'].'/key-terms') }}" title="Key Terms & Concepts">
                                                    <i class="fas fa-chevron-right"></i>
                                                    Key Terms & Concepts
                                                </a>
                                            </li>
                                            @endif
                                            <?php
                                                $praquiz = getPraQuiz($menu['id']);
                                            ?>
                                            @if($praquiz!=0)
                                            <li>
                                                <a href="{{ url($menu['slug'].'/practice-quiz') }}" title="Practice Quiz">
                                                    <i class="fas fa-chevron-right"></i>
                                                    Practice Quiz
                                                </a>
                                            </li>
                                            @endif
                                            <?php
                                                $study = getStudy($menu['id']);
                                            ?>
                                            @if($study!=0)
                                            <li>
                                                <a href="{{ url($menu['slug'].'/study-materials') }}" title="Study Material">
                                                    <i class="fas fa-chevron-right"></i>
                                                    Study Material
                                                </a>
                                            </li>
                                            @endif
                                        </ul>
                                    </div>
                                    <div class="col-lg-4 col-xl-4 col-md-12 interview-li interview-li2">
                                        <h4 class="wstheading clearfix  text-uppercase menu-head flip" title="Interview questions">
                                            Interview questions <i class="wsmenu-arrow fa fa-angle-down"></i>
                                        </h4>
                                        <ul class="wstliststy06 clearfix  interview_inner">
                                            @foreach($IQCates as $cat)
                                            <li>
                                                <a href="{{ url($menu['slug'].'/interview-questions/'.$cat['cat_slug']) }}" title="{{ $cat['title'] }}">
                                                    <i class="fas fa-chevron-right"></i>
                                                    {{ $cat['title'] }}
                                                </a>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <div class="col-lg-4 col-xl-4 col-md-12 interview-li padding-remove">
                                        <h4 class="wstheading clearfix  text-uppercase menu-head flip">
                                            Quiz Tests<i class="wsmenu-arrow fa fa-angle-down"></i>
                                        </h4>
                                        <ul class="wstliststy06 clearfix interview_inner">
                                            @foreach($QuizCates as $cat)
                                            <li>
                                                <a href="{{ url($menu['slug'].'/quiz/'.$cat['cat_slug']) }}" title="{{ $cat['title'] }}">
                                                    <i class="fas fa-chevron-right"></i>
                                                    {{ $cat['title'] }}
                                                </a>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    @endforeach

                    @if(!empty(session('digital_user_id')))
                        <li aria-haspopup="true" class="wsshopmyaccount  d-md-block">
                            <a href="#" class="register profile">
                                <span>
                                    <i class="fa fa-user"></i>
                                   {{ session('digital_user_name') }}
                                </span>
                            </a>
                            <ul class="sub-menu sub-menu2 profile-menu">
                                <li>
                                    <a href="{{ url('user/my-profile') }}">My profile</a>
                                </li>
                                <li>
                                    <a href="{{ url('user/log-out') }}">Logout</a>
                                </li>
                            </ul>
                        </li>
                    @else
                        <li aria-haspopup="true" class="wsshopmyaccount d-none d-md-block">
                            <a class="register scrool-top singShow" title="Register" data-controls-modal="#signup-popup" data-backdrop="static" data-keyboard="false">
                                Register
                            </a>
                        </li>
                        <li aria-haspopup="true" class="wsshopmyaccount d-none d-md-block" data-backdrop="static" data-keyboard="false">
                            <a class="loginShow scrool-top" title=" Login">
                                Log in
                            </a>
                        </li>
                    @endif
                </ul>
            </nav>
        </div>
    </div>
    <div class="clearfix"></div>
    <!-- login popup -->
        @include('frontend/forms/login')
    <!-- forgot password popup -->
        @include('frontend/forms/forget-password')
    <!-- create account popup -->
        @include('frontend/forms/register')