<div class="empty-search">
    <h3 class="empty-title">
        Sorry , There are no results that match your search .!!
    </h3>
    <p>
        Maybe your search was too specific, please try searching with another term
    </p>
    <img src="{{ url('public/frontend/images/no-results.svg') }}" alt="Nothing found" width="300">
</div>