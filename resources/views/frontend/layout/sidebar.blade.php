<aside>
    @php
    $segment1 =  Request::segment(1);
    @endphp
    <ul>
        <a href="{{url('my-profile')}}" title="My Profile"><li class=" <?php if($segment1 == "my-profile"){ echo 'resp-tab-active'; } ?>">My Profile</li> </a>
        <a href="{{url('tax-planning-tool')}}" title="Tax Planning Tool"><li class="<?php if($segment1 == "tax-planning-tool") { echo 'resp-tab-active'; }?>">Tax Planning Tool</li></a>
        <a href="{{url('my-itr-return')}}" title="My ITR Returns"> <li class="<?php if($segment1 == "my-itr-return"){ echo 'resp-tab-active';} ?>">My ITR Returns</li></a>
        <a href="{{url('my-documents')}}" title="My Documents"><li class="<?php if($segment1 == "my-documents"){  echo'resp-tab-active'; } ?>">My Documents</li></a>
        <a href="{{url('my-plan')}}" title="My Plan"><li class="<?php if($segment1 == "my-plan") {echo 'resp-tab-active'; }  ?>">My Plan</li></a>
        <a href="{{url('logout')}}" title="Sign Out"><li>Sign Out</li></a>
    </ul>
</aside>