<section id="bredcum">
  <div class="container">
    <div class="row">
      <?php 
      if(Request::segment(2)!=''):
        $page_title = ucwords(str_replace('-',' ',Request::segment(2)));
        $page_cumb  = ucwords(str_replace('-',' ',Request::segment(1)));
      else:
        $page_title = $page_title;
      endif
      ?>
      <div class="col-xl-6 col-md-12 col-6">
        <h4 class="text-capitalize"> {{ $page_title }}</h4>
      </div>
      <div class="col-xl-6 col-md-12 col-6">
        <span class="text-capitalize float-right">
          <a href="{{ url('/') }}" class="home-main">Home</a>
          @if($page_cumb!='')
            <i class="fas fa-chevron-right"></i>
            <a href="{{ url('/'.Request::segment(1)) }}" class="home-main">{{ $page_cumb }}</a>
              <i class="fas fa-chevron-right"></i>
              <u class="mater">{{ $page_title }}</u>
          @else
            <i class="fas fa-chevron-right"></i>
            <u class="mater">{{ $page_title }}</u>
          @endif
        </span>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
</section>