<!--  footer panel start -->
<!-- <footer id="footer">
  <div class="container">
    <div class="row">
      <div class="col-xl-12">
        <div class="footer">
          <a href="{{ url('/') }}" title="Digital Marketing">
          <img src="{{ url('public/frontend/images/footer-logo.png')}}" alt="footer-logo" class="img-fluid">
          </a>
        </div>
      </div>
      <ul>
        <li><a href="{{ url('/') }}" class="text-uppercase" title="Home">Home</a></li>
        <li><a href="videos-tutorials.php" class="text-uppercase" title="Video Tutorials">VIDEO TUTORIALS</a></li>
        <li> <a href="practice-quiz.php" class="text-uppercase" title="Quiz Tests">QUIZ TESTS</a></li>
        <li> <a href="" class="text-uppercase" title="Interview Questions">INTERVIEW QUESTIONS</a></li>
        <li> <a href="" class="text-uppercase" title="technical Terms">TECHNICAL TERMS</a></li>
      </ul>
    </div>
  </div>
</footer> -->
<?php

$videosCat = App\Model\VideoTutorials\VideoTutorials::where('category_id',30)
                                                ->inRandomOrder()
                                                ->get()
                                                ->take(10);

$IQCat = App\Model\SubCategory\SubCategory::whereHas('InterviewQuestions')
                                                ->where('category_id',30)
                                                ->inRandomOrder()
                                                ->get()
                                                ->take(10);
$QuizCat = App\Model\SubCategory\SubCategory::whereHas('Quiz')
                                                ->where('category_id',30)
                                                ->inRandomOrder()
                                                ->get()
                                                ->take(10);

$StudyCat = App\Model\StudyMaterial\StudyMaterial::where('study_material_catid',30)
                                                ->inRandomOrder()
                                                ->get()
                                                ->take(10);


?>
<!-- Footer -->
<footer id="footer-new">
  <div class="container">
    <div class="row">
      <div class="col-xl-3">
        <h4 class="" title="Interview questions">Video Tutorials</h4>
        <ul class="footer-menu">
          @foreach($videosCat as $vdo)
          <li>
            <a href="{{ url('/digital-marketing/videos-tutorials/'.$vdo->video_slug) }}" title="{{ $vdo['title'] }}">
              <i class="fas fa-chevron-right"></i>
              {{ $vdo['title'] }}
            </a>
          </li>
          @endforeach
        </ul>
      </div>

      <div class="col-xl-3">
        <h4 class="" title="Interview questions">Interview questions</h4>
        <ul class="footer-menu">
          @foreach($IQCat as $cat)
          <li>
            <a href="{{ url('/digital-marketing/interview-questions/'.$cat['cat_slug']) }}" title="{{ $cat['title'] }}">
              <i class="fas fa-chevron-right"></i>
              {{ $cat['title'] }}
            </a>
          </li>
          @endforeach
        </ul>
      </div>

      <div class="col-xl-3">
        <h4 class="" title="Interview questions">Study Materials</h4>
        <ul class="footer-menu">
          @foreach($StudyCat as $cat)
          <li>
            <a href="{{ url('/digital-marketing/study-materials/'.$cat['study_material_slug']) }}" title="">
              <i class="fas fa-chevron-right"></i>
              <?php echo getTextTransform($cat['study_ques'],0) ?>
            </a>
          </li>
          @endforeach
        </ul>
      </div>

      <div class="col-xl-3">
        <h4 class="" title="Interview questions">Quiz Tests</h4>
        <ul class="footer-menu">
          @foreach($QuizCat as $cat)
          <li>
            <a href="{{ url('/digital-marketing/quiz/'.$cat['cat_slug']) }}" title="{{ $cat['title'] }}">
              <i class="fas fa-chevron-right"></i>
              {{ $cat['title'] }}
            </a>
          </li>
          @endforeach
        </ul>
      </div>
    </div>
  </div>
</footer>

<section id="last-footer">
  <div class="container">
    <div class="row">
      <div class="col-xl-6 col-md-6 col-12">
        <ul>
          <li>Copyright &copy
            <a href="" title="360DigitalGyan">360DigitalGyan</a>
            All Rights Reserved
          </li>
        </ul>
      </div>
      <div class="col-xl-6  col-md-6 col-12 text-right">
        <ul>
          <li>Designed & Developed by 
            <a href="http://www.wscubetech.com/" target="blank" title="Affordable Webdesign Company, Website Development Company, Creative Webdesign Company, Website Services, Website Packages, Hosting" class="wstech" >WsCube Tech</a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</section>
<!--  footer panel close -->

</body>
{!! Html::script('public/frontend/js/jquery-2.1.4.min.js') !!}
{!! Html::script('public/frontend/js/bootstrap.js') !!}
{!! Html::script('public/frontend/js/profile/jquery.hsmenu.js') !!}
{!! Html::script('public/frontend/js/owl.carousel.min.js') !!}
{!! Html::script('public/frontend/js/wow.min.js') !!}
{!! Html::script('public/frontend/js/webslidemenu.js') !!}
{!! Html::script('public/frontend/js/sweetalert.js') !!}
{!! Html::script('public/frontend/js/bootstrapvalidator.min.js') !!}
{!! Html::script('public/frontend/js/countdownTimer.js') !!}
{!! Html::script('public/frontend/js/jquery.tagsinput-revisited.js') !!}
{!! Html::script('public/frontend/js/popper-1.14.7.min.js') !!}

<!-- <script src="public/js/aos.js"></script> -->
<!-- <script type="text/javascript">
  // email marketing accordian
  $(".accordion_head").click(function() {
    if($('.accordion_body').is(':visible')) {
        $(".accordion_body").slideUp(300);
        $(".plusminus").text('+');
    }
    if ($(this).next(".accordion_body").is(':visible')) {
        $(this).next(".accordion_body").slideUp(300);
        $(this).children(".plusminus").text('+');
    }else {
        $(this).next(".accordion_body").slideDown(300);
        $(this).children(".plusminus").text('-');
    }
  });
</script> -->

<script type="text/javascript">
  // email marketing accordian
  $(".accordion_head_iq").click(function() {
    if($('.accordion_body_iq').is(':visible')) {
        $(".accordion_body_iq").slideUp(300);
        // $(".plusminus").text('+');
    }
    if ($(this).parents('div[class^="inter-question"]').next(".accordion_body_iq").is(':visible')) {
        $(this).parents('div[class^="inter-question"]').next(".accordion_body_iq").slideUp(300);
        // $(this).children(".plusminus").text('+');
    }else {
        $(this).parents('div[class^="inter-question"]').next(".accordion_body_iq").slideDown(300);
        // $(this).children(".plusminus").text('-');
    }
  });
</script>

<script type="text/javascript">

$(document).ready(function()
{
  /* Main slider */
  var owl = $("#main-slider");
  owl.owlCarousel({
    navigation : true,
    autoplay:true,
    items:1,
    loop:true,
    margin:10,
    dots:false,
    nav:true,
    responsive:{
      0:{
        dots:true,
        nav:false,
        items:1
      },
      600:{
        nav:false,
        dots:true,
        items:1
      },
      1000:{
         items:1
      }
    }
  });

  /* video related slider */
  var owl = $("#related-videos");
    owl.owlCarousel({
    navigation : true,
    autoplay:true,
    items:4,
    loop:true,
    margin:10,
    dots:false,
    nav:true,
    responsive:{
      0:{
        dots:false,
        nav:false,
        items:2
      },
      600:{
        nav:false,
        dots:false,
        items:3
      },
      1000:{
        nav:true,
        loop:true,
        dots:false,
        items:4
      }
    }
  });
  
  /*
  ** header fix function
  */
  var id;
  if (navigator.userAgent.match(/(iPod|iPhone|iPad|Android)/)){
    id = "body";
  }else{
    id = window;
  }
  
  /*
  ** Scroll function
  */  
  $(id).bind('scroll', function () {
    if ($(id).scrollTop() > 200) {
      $(".stepprocess1").addClass("stepprocess1_scroll");
      $(".scroollogo").addClass("scroollogo2");
      $(".menu-other").addClass("menu-padding");
    } else {
      $(".stepprocess1").removeClass("stepprocess1_scroll");
      $(".scroollogo").removeClass("scroollogo2");
      $(".menu-other").removeClass("menu-padding");
    }
  });
});
  
  /*
  ** fuction toggle for submenu
  */
  if ($(window).width() <= 900) {
    $(".flip").click(function(){
      $(this).next().slideToggle();
    });
  }
  
  
  /*
  ** wow animation
  */ 
  wow = new WOW({
    animateClass: 'animated',
    offset:       100,
    callback:     function(box) {
    // console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
    }
  });
  wow.init();

</script>
<script type="text/javascript">
$('.toggle').click(function(){
  $('#target').toggle('slow');
});
  
/*
** courses tabbing
*/
$("document").ready(function(){
  $(".tab-slider--body").hide();
  $(".tab-slider--body:first").show();
});
  
  
$(".tab-slider--nav li").click(function(){
  $(".tab-slider--body").hide();
  var activeTab = $(this).attr("rel");
  $("#"+activeTab).fadeIn();
  if($(this).attr("rel") == "tab2"){
    $('.tab-slider--tabs').addClass('slide');
  }else{
    $('.tab-slider--tabs').removeClass('slide');
  }
  $(".tab-slider--nav li").removeClass("active");
  $(this).addClass("active");
});
  
  // lazy loader
  // $(".loader .vedio-1").slice(3).hide();
  
  //  var mincount = 1;
  //  var maxcount = 3;
  
  
  //  $(window).scroll(function () {
  //      if ($(window).scrollTop() + $(window).height() >= $(document).height() - 500) {
  //          $(".loader .student-names2").slice(mincount, maxcount).show();
  
  //          mincount = mincount + 1;
  //          maxcount = maxcount + 1
  
  //      }
  //  });
  
  
  //  $(".loader2 .student-names2").slice(3).hide();
  
  //  var mincount = 1;
  //  var maxcount = 3;
  
  
  //  $(window).scroll(function () {
  //      if ($(window).scrollTop() + $(window).height() >= $(document).height() - 500) {
  //          $(".loader .vedio-1").slice(mincount, maxcount).show();
  
  //          mincount = mincount + 1;
  //          maxcount = maxcount + 1
  
  //      }
  //  });
  
</script>

<script type="text/javascript">
$('#email-field').blur(function(){
  if($('#email-field').val()!=""){
    $.ajax({
      type: "GET",
      url: "{{ url('user/email-check') }}",
      data: {
        // "_token": "{{ csrf_token() }}",
        email: $('#email-field').val()
      },
      success: function(res){
        if (res.status=="false"){
          $('#sign-msg').show();
          $('#sign-msg').removeAttr('class');
          $('#sign-msg').addClass('text-danger');
          $('#sign-msg').html(res.message);
        }else{
          $('#sign-msg').hide();
        }
      },
      error: function(){
        alert("We found an error in your data.  Please return to home page and try again.");
      }
    });
    return false;
  }
});
</script>

<script type="text/javascript">
/*
** for prevent user to post questions
*/
function preventPostQues(){
  $('#ask-your-question').modal('hide');
  swal({
      title: " ",
      text: "Please login first to post your question..!!",
      type: "warning",
      confirmButtonClass: "btn all-button",
      confirmButtonText: "Log in!"
    },
    function(){
      $('.loginShow').trigger('click');
  });
}

/*
** for prevent user to post answer 
*/
function preventPostAns(){
  $('#ask-answer').modal('hide');
  swal({
      title: " ",
      text: "Please login first to post your answer..!!",
      type: "warning",
      confirmButtonClass: "btn all-button",
      confirmButtonText: "Log in!"
    },
    function(){
      $('.loginShow').trigger('click');
  });
}
</script>


<script type="text/javascript">
// var clicked = false;
// $('.google-plus').on("click",function(e){
//   if(clicked===false){
//     clicked=true;
//     $(this).removeAttr('disable');
//   }else{
//     $(this).attr('disable',true);
//     e.preventDefault();
//   }
// });

$(document).ready(function(){
  /*
  ** Login form validation
  */
  $('#login-form').bootstrapValidator({
    feedbackIcons: {
      validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
      email: {
        validators: {
          emailAddress: {
            message: 'Please enter your email address'
          },
          notEmpty: {
            message: 'The email address is required and cannot be empty'
          }
        }
      },
      password: {
        validators: {
          notEmpty: {
            message: 'Please enter your Password'
          },
          stringLength: {
            min: 6,
            message: 'The password must contain at least 6 characters!'
          }
        }
      },
    },
    submitHandler: function(validator, form, submitButton){
      var BASE_URL = "{{ url('/') }}";
      $.ajax({
        type: "POST",
        url: "{{ url('user/login') }}",
        data: $('#login-form').serialize(),
        success: function(res){
          if (res.status=="true"){
            $('#login-msg').removeAttr('class');
            $('#login-msg').addClass('text-success');
            $('#login-msg').html(res.message);
            window.location= BASE_URL+res.url;
          }else{
            $('#login-msg').removeAttr('class');
            $('#login-msg').addClass('text-danger');
            $('#login-msg').html(res.message);
          }
        },
        error: function(){
          alert("We found an error in your data.  Please return to home page and try again.");
        }
      });
        return false;
    }
  });


  /*
  ** Ask Question from
  */
  $('#form-save-question').bootstrapValidator({
    feedbackIcons: {
      validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
      user_question: {
        validators: {
          notEmpty: {
            message: 'The question is required and cannot be empty'
          }
        }
      },
    }
  });

  /*
  ** Ask Answer from
  */
  $('#form-save-answer').bootstrapValidator({
    feedbackIcons: {
      validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
      user_answer: {
        validators: {
          notEmpty: {
            message: 'The answer is required and cannot be empty'
          }
        }
      },
    }
  });


  /*
  ** Ask Answer from
  */
  $('#form-question-comment').bootstrapValidator({
    feedbackIcons: {
      validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
      user_comment: {
        validators: {
          notEmpty: {
            message: 'The comment is required and cannot be empty'
          }
        }
      },
    }
  });

  /*
  ** Register form validation
  */
  $('#signup-form').bootstrapValidator({
    feedbackIcons:{
      validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
        name: {
          validators: {
            notEmpty: {
              message: 'Please enter your Full name'
            },
            regexp: {
              regexp: /^[a-z\s]+$/i,
              message: 'The name can consist of alphabetical characters and spaces only'
            }
          }
        },
        'agree[]': {
            validators: {
                notEmpty: {
                    message: 'The size is required'
                }
            }
        },
        pwd: {
          validators: {
            notEmpty: {
              message: 'Please enter your password'
            },
            stringLength: {
              min: 6,
              message: 'The password must contain at least 6 characters!'
            }
          }
        },
        cnfrmpwd: {
          validators: {
            notEmpty: {
              message: 'Please enter your confirm password'
            },
            identical:{
              field: 'pwd',
              message: 'The password and its confirm are not the same' 
            }
          }
        },
        email: {
          validators: {
            emailAddress: {
              message: 'Please enter your email address'
            },
            notEmpty: {
              message: 'The email address is required and cannot be empty'
            }
          }
        },
        'agree[]': {
            validators: {
                notEmpty: {
                    message: 'The size is required'
                }
            }
        },
    },
    submitHandler: function(validator, form, submitButton){
      var BASE_URL = "{{ url('/') }}";
      $.ajax({
        type: "POST",
        url: "{{ url('user/sign-up') }}",
        data: $('#signup-form').serialize(),
        success: function(res){
            if (res.status=="true"){
              $('#sign-msg').removeAttr('class');
              $('#sign-msg').addClass('text-success');
              $('#sign-msg').html(res.message);
              window.location= BASE_URL+res.url;
            }else{
              $('#sign-btn').removeAttr('disabled');
              $('#sign-msg').removeAttr('class');
              $('#sign-msg').addClass('text-danger');
              $('#sign-msg').html(res.message);
            }
        },
        error: function(){
          alert("We found an error in your data.  Please return to home page and try again.");
        }
      });
      return false;
    }
  });


  /*
  ** for numeric validation
  */
  $(function(){
    $('.numeric').on('input', function(event) {
      this.value = this.value.replace(/[^0-9]/g, '');
    });
  });

  /*
  **  forget password form validation
  */
  $('#forgot-popup').bootstrapValidator({
    feedbackIcons: {
      validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
      email: {
        validators: {
          emailAddress: {
            message: 'Please enter your email address'
          },
          notEmpty: {
            message: 'The email address is required and cannot be empty'
          }
        }
      },
    },
    submitHandler: function(validator, form, submitButton){
      var BASE_URL = "{{ url('/') }}";
      $.ajax({
        type: "POST",
        url: "{{ url('user/forgot-password') }}",
        data: $('#forgot-popup').serialize(),
        success: function(res){
            if (res.status=="true"){
              $('#forgot-msg').removeAttr('class');
              $('#forgot-msg').addClass('text-success');
              $('#forgot-msg').html(res.message);
              // window.location= BASE_URL+res.url;
              $("#forgot-popup").trigger("reset");
            }else{
              $('#forgot-btn').removeAttr('disabled');
              $('#forgot-msg').removeAttr('class');
              $('#forgot-msg').addClass('text-danger');
              $('#forgot-msg').html(res.message);
            }
        },
        error: function(){
          alert("We found an error in your data.  Please return to home page and try again.");
        }
      });
      return false;
    }
  });
});


/*
**  Toggle password
*/
$(".toggle-password").click(function() {
  $(this).toggleClass("fa-eye fa-eye-slash");
  var input = $($(this).attr("toggle"));
  if(input.attr("type") == "password") {
    input.attr("type", "text");
  }else{
    input.attr("type", "password");
  }
});

/*
**  Hide questions
*/  
function hideQues(qs) {
  $('#ans-option'+qs).slideToggle('slow', function() {
    if ($('#ans-option'+qs).is(":visible")) {
       $("#reveal"+qs).html('Hide Answer');
       $("#show-answer"+qs).css('border-top','1px solid rgb(231, 231, 231)');
    } else {
      $("#reveal"+qs).html('View Answer');
       $("#show-answer"+qs).css('border-top','0px');
    } 
  });
}


// $("#createForm").click(function () {
  // $('#signup-popup').show();       
// });

/*
**  User login/Signup popup
*/
$("#loagin_popup").click(function () {
    $('#login-popup').hide();
    $('#signup-popup').show();
});

$("#sign_popup").click(function () {
    $('#login-popup').show();
    $('#signup-popup').hide();
});

$(".close").click(function () {
    $('#login-popup').hide();
    $('#signup-popup').hide();
});

$(".loginShow").click(function () {
    $('#login-popup').show();
});

$(".singShow").click(function () {
    $('#signup-popup').show();
});
$("#forgotpassword").click(function () {
    $('#forgot-password').show();
});

$("#forgotpassword").click(function () {
    $('#login-popup').hide();
});
$(".closebtn").click(function () {
    $('#forgot-password').hide();
});

$("#forgot_login").click(function () {
    $('#forgot-password').hide();
});

$("#forgot_login").click(function () {
    $('#login-popup').show();
});

/* 
**  tag autocomplete for post your question popup
*/

$(function(){
  $('.digitalgyan-tags').tagsInput({
    'unique': true,
    'minChars': 2,
    // 'maxChars': 10,
    // 'limit': 20,
    'validationPattern': new RegExp('^[a-zA-Z]+$')
  });
});

$(document).ready(function(){
  $('#key-form .all-button ul li a').click(function(){
    $('#key-form .all-button li a').removeClass("active");
    $(this).addClass("active");
  });
});

$(".scrool-top").click(function() {
  $("html, body").animate({ scrollTop: 0 }, "slow");
  return false;
});

$(".toggle-profile").click(function () {

  $(this).toggleClass("ripple-out");
  $reveal = $(this).attr('data-reveal');
  $allRevealable = ".grid-items, .user-penal, .user-info";
  if ($($reveal).hasClass("fadeInUp")) {
    $($reveal).removeClass("fadeInUp").fadeOut();
  } else {
    $($reveal).fadeIn().addClass("fadeInUp");
  }
  $($allRevealable).not($reveal).removeClass("fadeInUp ripple-out").fadeOut();
});
</script>
</html>