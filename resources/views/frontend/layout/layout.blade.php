<!-- Start : Header -->  
@include('frontend/layout/header')

<!-- End : Header -->	

<!-- Start: Menu -->

<!--@include('frontend/layout/sidebar')-->

<!-- End: Menu -->

@yield('content')

<!-- Start: Footer -->

@include('frontend/layout/footer')

<!-- End: Footer -->