<!DOCTYPE html>
<html>

<head>
    <title> {!! $title !!} </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" type="image/png" href="{{ url('public/frontend/images/favicon.png') }}" />
    {!! Html::style('public/frontend/css/bootstrap.css') !!}
    {!! Html::style('public/frontend/css/animate.css') !!}
    {!! Html::style('public/frontend/css/owl.carousel.min.css') !!}
    {!! Html::style('public/frontend/css/webslidemenu.css') !!}
    {!! Html::style('public/frontend/css/white-gry.css') !!}
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <!--<link href="css/aos.css" rel="stylesheet">-->
    <!--Main Menu File-->

    <!-- <link id="theme" rel="stylesheet" type="text/css" media="all" href="css/white-gry.css" /> -->
    <link href="https://fonts.googleapis.com/css?family=Heebo:300,400,500,700,600&amp;subset=hebrew" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,600" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,500,700,600"" rel=" stylesheet">
    {!! Html::style('public/frontend/css/owl.theme.default.min.css') !!}
    {!! Html::style('public/frontend/css/sweetalert.css') !!}

    {!! Html::style('public/frontend/css/jquery.tagsinput-revisited.css') !!}
    {!! Html::style('public/frontend/css/style.css') !!}
    {!! Html::style('public/frontend/css/responsive.css') !!}
    {!! Html::script('public/frontend/js/jquery-2.1.4.min.js') !!}
</head>
<body>
    <div class="wsmobileheader stepprocess1 clearfix">
        <a id="wsnavtoggle" class="wsanimated-arrow"><span></span></a>
        <span class="smllogo"><a href="{{ url('/') }}"><img src="{{ url('public/frontend/images/logo.png') }}" width="190" class="mobile-logo" alt="logo" /></a></span>
        <div class="wss">
            @if(!empty(session('digital_user_id')))
                <a title="Register" class="scrool-top">
                    <i class="fas fa-user"></i>
                </a>
            @else
                <a title="Login" class="loginShow scrool-top" data-target="#login-popup"> <i class="fas fa-sign-in-alt"></i></a>
                <a title="Register" class="singShow scrool-top" data-target="#signup-popup"> <i class="fas fa-user"></i></a>
            @endif
        </div>
    </div>
    <div class="headerfull stepprocess1">
        <div class="wsmain clearfix">
            <div class="smllogo ">
                <a href="{{ url('/') }}" title="360DigitalGyan">
                    <img src="{{ url('public/frontend/images/main-logo.png') }}" alt="img_fullsize" class="scroollogo" />
                </a>
            </div>
            <nav class="wsmenu">
                <ul class="wsmenu-list menu-other ">
                    <li aria-haspopup="true">
                        <?php
                            $DM_InterviewCates      = getSubCategories(30,1);
                            $DM_QuizCates           = getSubCategories(30,2);
                        ?>
                        <a class="navtext active" href="{{ url('digital-marketing/videos-tutorials') }}" title="Digital Marketing">
                            <span>Digital Marketing <i class="fas fa-chevron-down"></i></span>
                        </a>
                        <div class="wsmegamenu clearfix  halfmenu halfmenu_mobile">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-lg-4 col-xl-4 col-md-12 videos">
                                        <ul class="wstliststy06 clearfix">
                                            <?php
                                                /* 1 = english , 2 = hindi */
                                                $engVideos      = getVideosCount(30,1);
                                                $hindiVideos    = getVideosCount(30,2);
                                            ?>
                                            @if($engVideos!=0)
                                            <li>
                                                <a href="{{ url('digital-marketing/videos-tutorials/english') }}" title="Video Tutorial (English)">
                                                    <i class="fas fa-chevron-right"></i>
                                                    Video Tutorial (English)
                                                </a>
                                            </li>
                                            @endif
                                            
                                            @if($hindiVideos!=0)
                                            <li>
                                                <a href="{{ url('digital-marketing/videos-tutorials/hindi') }}" title="Video Tutorial (Hindi)">
                                                    <i class="fas fa-chevron-right"></i>
                                                    Video Tutorial (Hindi)
                                                </a>
                                            </li>
                                            @endif
                                            <?php
                                                $techTerm       = getTechTerm(30);
                                            ?>
                                            @if($techTerm!=0)
                                            <li>
                                                <a href="{{ url('digital-marketing/technical-terms') }}" title="Technical Trems">
                                                    <i class="fas fa-chevron-right"></i>
                                                    Technical Terms
                                                </a>
                                            </li>
                                            @endif
                                            <?php
                                                $quesAns = getQuesAns(30);
                                            ?>
                                            @if($quesAns!=0)
                                            <li>
                                                <a href="{{ url('digital-marketing/questions-answers') }}" title="Questions Answer">
                                                    <i class="fas fa-chevron-right"></i>
                                                    Questions Answer
                                                </a>
                                            </li>
                                            @endif
                                            <?php
                                                $term = getTerm(30);
                                            ?>
                                            @if($term!=0)
                                            <li>
                                                <a href="{{ url('digital-marketing/key-terms') }}" title="Key Terms & Concepts">
                                                    <i class="fas fa-chevron-right"></i>
                                                    Key Terms & Concepts
                                                </a>
                                            </li>
                                            @endif
                                            <?php
                                                $praquiz = getPraQuiz(30);
                                            ?>
                                            @if($praquiz!=0)
                                            <li>
                                                <a href="{{ url('digital-marketing/practice-quiz') }}" title="Practice Quiz">
                                                    <i class="fas fa-chevron-right"></i>
                                                    Practice Quiz
                                                </a>
                                            </li>
                                            @endif
                                            <?php
                                                $study = getStudy(30);
                                            ?>
                                            @if($study!=0)
                                            <li>
                                                <a href="{{ url('digital-marketing/study-materials') }}" title="Study Material">
                                                    <i class="fas fa-chevron-right"></i>
                                                    Study Material
                                                </a>
                                            </li>
                                            @endif
                                        </ul>
                                    </div>
                                    <div class="col-lg-4 col-xl-4 col-md-12 interview-li interview-li2">
                                        <h4 class="wstheading clearfix  text-uppercase menu-head flip" title="Interview questions">
                                            Interview questions <i class="wsmenu-arrow fa fa-angle-down"></i>
                                        </h4>
                                        <ul class="wstliststy06 clearfix  interview_inner">
                                            @foreach($DM_InterviewCates as $category)
                                            <li>
                                                <a href="{{ url('digital-marketing/interview-questions/'.$category['cat_slug']) }}" title="{{ $category['title'] }}">
                                                    <i class="fas fa-chevron-right"></i>
                                                    {{ $category['title'] }}
                                                </a>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <div class="col-lg-4 col-xl-4 col-md-12 interview-li padding-remove">
                                        <h4 class="wstheading clearfix  text-uppercase menu-head flip">
                                            Quiz Tests<i class="wsmenu-arrow fa fa-angle-down"></i>
                                        </h4>
                                        <ul class="wstliststy06 clearfix interview_inner">
                                            @foreach($DM_QuizCates as $category)
                                            <li>
                                                <a href="{{ url('digital-marketing/quiz/'.$category['cat_slug']) }}" title="Content Marketing">
                                                    <i class="fas fa-chevron-right"></i>
                                                    {{ $category['title'] }}
                                                </a>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>

                    <li aria-haspopup="true">
                        <?php
                            $WEB_InterviewCates      = getSubCategories(52,1);
                            $WEB_QuizCates           = getSubCategories(52,2);
                        ?>
                        <a class="navtext active" href="{{ url('web-development/videos-tutorials') }}" title="Web Development">
                            <span>Web Development <i class="fas fa-chevron-down"></i></span>
                        </a>
                        <div class="wsmegamenu clearfix  halfmenu halfmenu_mobile">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-lg-4 col-xl-4 col-md-12 videos">
                                        <ul class="wstliststy06 clearfix">
                                            <?php
                                                /* 1 = english , 2 = hindi */
                                                $engVideos      = getVideosCount(52,1);
                                                $hindiVideos    = getVideosCount(52,2);
                                            ?>
                                            @if($engVideos!=0)
                                            <li>
                                                <a href="{{ url('web-development/videos-tutorials/english') }}" title="Video Tutorial (English)">
                                                    <i class="fas fa-chevron-right"></i>
                                                    Video Tutorial (English)
                                                </a>
                                            </li>
                                            @endif
                                            
                                            @if($hindiVideos!=0)
                                            <li>
                                                <a href="{{ url('web-development/videos-tutorials/hindi') }}" title="Video Tutorial (Hindi)">
                                                    <i class="fas fa-chevron-right"></i>
                                                    Video Tutorial (Hindi)
                                                </a>
                                            </li>
                                            @endif
                                            <?php
                                                $techTerm       = getTechTerm(52);
                                            ?>
                                            @if($techTerm!=0)
                                            <li>
                                                <a href="{{ url('web-development/technical-terms') }}" title="Technical Treams">
                                                    <i class="fas fa-chevron-right"></i>
                                                    Technical Treams
                                                </a>
                                            </li>
                                            @endif
                                            <?php
                                                $quesAns        = getTechTerm(52);
                                            ?>
                                            @if($quesAns!=0)
                                            <li>
                                                <a href="{{ url('web-development/questions-answers') }}" title="Questions Answer">
                                                    <i class="fas fa-chevron-right"></i>
                                                    Questions Answer
                                                </a>
                                            </li>
                                            @endif
                                            <?php
                                                $term = getTerm(52);
                                            ?>
                                            @if($term!=0)
                                            <li>
                                                <a href="{{ url('web-development/key-terms') }}" title="Key Terms & Concepts">
                                                    <i class="fas fa-chevron-right"></i>
                                                    Key Terms & Concepts
                                                </a>
                                            </li>
                                            @endif
                                            <?php
                                                $praquiz = getPraQuiz(52);
                                            ?>
                                            @if($praquiz!=0)
                                            <li>
                                                <a href="{{ url('web-development/practice-quiz') }}" title="Practice Quiz">
                                                    <i class="fas fa-chevron-right"></i>
                                                    Practice Quiz
                                                </a>
                                            </li>
                                            @endif
                                            
                                            <?php
                                                $study = getStudy(52);
                                            ?>
                                            @if($study!=0)
                                            <li>
                                                <a href="{{ url('web-development/study-materials') }}" title="Study Material">
                                                    <i class="fas fa-chevron-right"></i>
                                                    Study Material
                                                </a>
                                            </li>
                                            @endif
                                        </ul>
                                    </div>
                                    <div class="col-lg-4 col-xl-4 col-md-12 interview-li interview-li2">
                                        <h4 class="wstheading clearfix  text-uppercase menu-head flip" title="Interview questions">
                                            Interview questions <i class="wsmenu-arrow fa fa-angle-down"></i>
                                        </h4>
                                        <ul class="wstliststy06 clearfix  interview_inner">
                                            @foreach($WEB_InterviewCates as $category)
                                            <li>
                                                <a href="{{ url('web-development/interview-questions/'.$category['cat_slug']) }}" title="{{ $category['title'] }}">
                                                    <i class="fas fa-chevron-right"></i>
                                                    {{ $category['title'] }}
                                                </a>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <div class="col-lg-4 col-xl-4 col-md-12 interview-li padding-remove">
                                        <h4 class="wstheading clearfix  text-uppercase menu-head flip">
                                            Quiz Tests<i class="wsmenu-arrow fa fa-angle-down"></i>
                                        </h4>
                                        <ul class="wstliststy06 clearfix interview_inner">
                                            @foreach($WEB_QuizCates as $category)
                                            <li>
                                                <a href="{{ url('web-development/quiz/'.$category['cat_slug']) }}" title="Content Marketing">
                                                    <i class="fas fa-chevron-right"></i>
                                                    {{ $category['title'] }}
                                                </a>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>

                     <li aria-haspopup="true">
                        <?php
                            $SW_InterviewCates      = getSubCategories(50,1);
                            $SW_QuizCates           = getSubCategories(50,2);
                        ?>
                        <a class="navtext active" href="{{ url('software-testing/videos-tutorials') }}" title="Software Testing"><span>Software Testing <i class="fas fa-chevron-down"></i></span></a>
                        <div class="wsmegamenu clearfix  halfmenu halfmenu_mobile">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-lg-4 col-xl-4 col-md-12 videos">
                                        <ul class="wstliststy06 clearfix">
                                            <?php
                                                /* 1 = english , 2 = hindi */
                                                $engVideos      = getVideosCount(50,1);
                                                $hindiVideos    = getVideosCount(50,2);
                                            ?>
                                            @if($engVideos!=0)
                                            <li>
                                                <a href="{{ url('software-testing/videos-tutorials/english') }}" title="Video Tutorial (English)">
                                                    <i class="fas fa-chevron-right"></i>
                                                    Video Tutorial (English)
                                                </a>
                                            </li>
                                            @endif
                                            
                                            @if($hindiVideos!=0)
                                            <li>
                                                <a href="{{ url('software-testing/videos-tutorials/hindi') }}" title="Video Tutorial (Hindi)">
                                                    <i class="fas fa-chevron-right"></i>
                                                    Video Tutorial (Hindi)
                                                </a>
                                            </li>
                                            @endif
                                            <?php
                                                $testTerms      = getTechTerm(50);
                                            ?>
                                            @if($testTerms!=0)
                                            <li>
                                                <a href="{{ url('software-testing/technical-terms') }}" title="Technical Treams">
                                                    <i class="fas fa-chevron-right"></i>
                                                    Technical Treams
                                                </a>
                                            </li>
                                            @endif
                                            <?php
                                                $quesAns        = getTechTerm(50);
                                            ?>
                                            @if($quesAns!=0)
                                            <li>
                                                <a href="{{ url('software-testing/questions-answers') }}" title="Questions Answer">
                                                    <i class="fas fa-chevron-right"></i>
                                                    Questions Answer
                                                </a>
                                            </li>
                                            @endif
                                            <?php
                                                $term = getTerm(50);
                                            ?>
                                            @if($term!=0)
                                            <li>
                                                <a href="{{ url('software-testing/key-terms') }}" title="Key Terms & Concepts">
                                                    <i class="fas fa-chevron-right"></i>
                                                    Key Terms & Concepts
                                                </a>
                                            </li>
                                            @endif
                                            <?php
                                                $praquiz = getPraQuiz(50);
                                            ?>
                                            @if($praquiz!=0)
                                            <li>
                                                <a href="{{ url('software-testing/practice-quiz') }}" title="Practice Quiz">
                                                    <i class="fas fa-chevron-right"></i>
                                                    Practice Quiz
                                                </a>
                                            </li>
                                            @endif
                                            <?php
                                                $study = getStudy(50);
                                            ?>
                                            @if($study!=0)
                                            <li>
                                                <a href="{{ url('software-testing/study-materials') }}" title="Study Material">
                                                    <i class="fas fa-chevron-right"></i>
                                                    Study Material
                                                </a>
                                            </li>
                                            @endif
                                        </ul>
                                    </div>
                                    <div class="col-lg-4 col-xl-4 col-md-12 interview-li interview-li2">
                                        <h4 class="wstheading clearfix  text-uppercase menu-head flip" title="Interview questions">
                                            Interview questions <i class="wsmenu-arrow fa fa-angle-down"></i>
                                        </h4>
                                        <ul class="wstliststy06 clearfix  interview_inner">
                                            @foreach($SW_InterviewCates as $category)
                                            <li>
                                                <a href="{{ url('software-testing/interview-questions/'.$category['cat_slug']) }}" title="{{ $category['title'] }}">
                                                    <i class="fas fa-chevron-right"></i>
                                                    {{ $category['title'] }}
                                                </a>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <div class="col-lg-4 col-xl-4 col-md-12 interview-li padding-remove">
                                        <h4 class="wstheading clearfix  text-uppercase menu-head flip">
                                            Quiz Tests<i class="wsmenu-arrow fa fa-angle-down"></i>
                                        </h4>
                                        <ul class="wstliststy06 clearfix interview_inner">
                                            @foreach($SW_QuizCates as $category)
                                            <li>
                                                <a href="{{ url('software-testing/quiz/'.$category['cat_slug']) }}" title="{{ $category['title'] }}">
                                                    <i class="fas fa-chevron-right"></i>
                                                    {{ $category['title'] }}
                                                </a>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    
                    <li aria-haspopup="true">
                        <?php
                            $PLC_InterviewCates      = getSubCategories(55,1);
                            $PLC_QuizCates           = getSubCategories(55,2);
                        ?>
                        <a class="navtext active" href="{{ url('plc-scada/videos-tutorials') }}" title="PLC-SCADA"><span>PLC-SCADA <i class="fas fa-chevron-down"></i></span></a>
                        <div class="wsmegamenu clearfix  halfmenu halfmenu_mobile">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-lg-4 col-xl-4 col-md-12 videos">
                                        <ul class="wstliststy06 clearfix">
                                            <?php
                                                /* 1 = english , 2 = hindi */
                                                $engVideos      = getVideosCount(55,1);
                                                $hindiVideos    = getVideosCount(55,2);
                                            ?>
                                            @if($engVideos!=0)
                                            <li>
                                                <a href="{{ url('plc-scada/videos-tutorials/english') }}" title="Video Tutorial (English)">
                                                    <i class="fas fa-chevron-right"></i>
                                                    Video Tutorial (English)
                                                </a>
                                            </li>
                                            @endif
                                            
                                            @if($hindiVideos!=0)
                                            <li>
                                                <a href="{{ url('plc-scada/videos-tutorials/hindi') }}" title="Video Tutorial (Hindi)">
                                                    <i class="fas fa-chevron-right"></i>
                                                    Video Tutorial (Hindi)
                                                </a>
                                            </li>
                                            @endif
                                            <?php
                                                $plcTerm        = getTechTerm(55); 
                                            ?>
                                            <li>
                                                <a href="{{ url('plc-scada/technical-terms') }}" title="Technical Treams">
                                                    <i class="fas fa-chevron-right"></i>
                                                    Technical Treams
                                                </a>
                                            </li>
                                            <?php
                                                $quesAns        = getTechTerm(55);
                                            ?>
                                            @if($quesAns!=0)
                                            <li>
                                                <a href="{{ url('plc-scada/questions-answers') }}" title="Questions Answer">
                                                    <i class="fas fa-chevron-right"></i>
                                                    Questions Answer
                                                </a>
                                            </li>
                                            @endif
                                            <?php
                                                $term = getTerm(55);
                                            ?>
                                            @if($term!=0)
                                            <li>
                                                <a href="{{ url('plc-scada/key-terms') }}" title="Key Terms & Concepts">
                                                    <i class="fas fa-chevron-right"></i>
                                                    Key Terms & Concepts
                                                </a>
                                            </li>
                                            @endif
                                            <?php
                                                $praquiz = getPraQuiz(55);
                                            ?>
                                            @if($praquiz!=0)
                                            <li>
                                                <a href="{{ url('plc-scada/practice-quiz') }}" title="Practice Quiz">
                                                    <i class="fas fa-chevron-right"></i>
                                                    Practice Quiz
                                                </a>
                                            </li>
                                            @endif
                                            <?php
                                                $study = getStudy(55);
                                            ?>
                                            @if($study!=0)
                                            <li>
                                                <a href="{{ url('plc-scada/study-materials') }}" title="Study Material">
                                                    <i class="fas fa-chevron-right"></i>
                                                    Study Material
                                                </a>
                                            </li>
                                            @endif
                                        </ul>
                                    </div>
                                    <div class="col-lg-4 col-xl-4 col-md-12 interview-li interview-li2">
                                        <h4 class="wstheading clearfix  text-uppercase menu-head flip" title="Interview questions">
                                            Interview questions <i class="wsmenu-arrow fa fa-angle-down"></i>
                                        </h4>
                                        <ul class="wstliststy06 clearfix  interview_inner">
                                            @foreach($PLC_InterviewCates as $category)
                                            <li>
                                                <a href="{{ url('plc-scada/interview-questions/'.$category['cat_slug']) }}" title="{{ $category['title'] }}">
                                                    <i class="fas fa-chevron-right"></i>
                                                    {{ $category['title'] }}
                                                </a>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <div class="col-lg-4 col-xl-4 col-md-12 interview-li padding-remove">
                                        <h4 class="wstheading clearfix  text-uppercase menu-head flip">
                                            Quiz Tests<i class="wsmenu-arrow fa fa-angle-down"></i>
                                        </h4>
                                        <ul class="wstliststy06 clearfix interview_inner">
                                            @foreach($PLC_QuizCates as $category)
                                            <li>
                                                <a href="{{ url('plc-scada/quiz/'.$category['cat_slug']) }}" title="{{ $category['title'] }}">
                                                    <i class="fas fa-chevron-right"></i>
                                                    {{ $category['title'] }}
                                                </a>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    
                    <li aria-haspopup="true">
                        <?php
                            $PY_InterviewCates      = getSubCategories(53,1);
                            $PY_QuizCates           = getSubCategories(53,2);
                        ?>
                        <a class="navtext active" href="{{ url('python/videos-tutorials') }}" title="Python"><span>Python <i class="fas fa-chevron-down"></i></span></a>
                        <div class="wsmegamenu clearfix  halfmenu halfmenu_mobile">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-lg-4 col-xl-4 col-md-12 videos">
                                        <ul class="wstliststy06 clearfix">
                                            <?php
                                                /* 1 = english , 2 = hindi */
                                                $engVideos      = getVideosCount(53,1);
                                                $hindiVideos    = getVideosCount(53,2);
                                            ?>
                                            @if($engVideos!=0)
                                            <li>
                                                <a href="{{ url('python/videos-tutorials/english') }}" title="Video Tutorial (English)">
                                                    <i class="fas fa-chevron-right"></i>
                                                    Video Tutorial (English)
                                                </a>
                                            </li>
                                            @endif
                                            
                                            @if($hindiVideos!=0)
                                            <li>
                                                <a href="{{ url('python/videos-tutorials/hindi') }}" title="Video Tutorial (Hindi)">
                                                    <i class="fas fa-chevron-right"></i>
                                                    Video Tutorial (Hindi)
                                                </a>
                                            </li>
                                            @endif
                                            <?php
                                                $techTerm       = getTechTerm(53);
                                            ?>
                                            @if($techTerm!=0)
                                            <li>
                                                <a href="{{ url('python/technical-terms') }}" title="Technical Treams">
                                                    <i class="fas fa-chevron-right"></i>
                                                    Technical Treams
                                                </a>
                                            </li>
                                            @endif
                                            <?php
                                                $quesAns        = getTechTerm(53);
                                            ?>
                                            @if($quesAns!=0)
                                            <li>
                                                <a href="{{ url('python/questions-answers') }}" title="Questions Answer">
                                                    <i class="fas fa-chevron-right"></i>
                                                    Questions Answer
                                                </a>
                                            </li>
                                            @endif
                                            <?php
                                                $term = getTerm(53);
                                            ?>
                                            @if($term!=0)
                                            <li>
                                                <a href="{{ url('python/key-terms') }}" title="Key Terms & Concepts">
                                                    <i class="fas fa-chevron-right"></i>
                                                    Key Terms & Concepts
                                                </a>
                                            </li>
                                            @endif
                                            <?php
                                                $praquiz = getPraQuiz(53);
                                            ?>
                                            @if($praquiz!=0)
                                            <li>
                                                <a href="{{ url('python/practice-quiz') }}" title="Practice Quiz">
                                                    <i class="fas fa-chevron-right"></i>
                                                    Practice Quiz
                                                </a>
                                            </li>
                                            @endif
                                            <?php
                                                $study = getStudy(53);
                                            ?>
                                            @if($study!=0)
                                            <li>
                                                <a href="{{ url('python/study-materials') }}" title="Study Material">
                                                    <i class="fas fa-chevron-right"></i>
                                                    Study Material
                                                </a>
                                            </li>
                                            @endif
                                        </ul>
                                    </div>
                                    <div class="col-lg-4 col-xl-4 col-md-12 interview-li interview-li2">
                                        <h4 class="wstheading clearfix  text-uppercase menu-head flip" title="Interview questions">
                                            Interview questions <i class="wsmenu-arrow fa fa-angle-down"></i>
                                        </h4>
                                        <ul class="wstliststy06 clearfix  interview_inner">
                                            @foreach($PY_InterviewCates as $category)
                                            <li>
                                                <a href="{{ url('python/interview-questions/'.$category['cat_slug']) }}" title="{{ $category['title'] }}">
                                                    <i class="fas fa-chevron-right"></i>
                                                    {{ $category['title'] }}
                                                </a>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <div class="col-lg-4 col-xl-4 col-md-12 interview-li padding-remove">
                                        <h4 class="wstheading clearfix  text-uppercase menu-head flip">
                                            Quiz Tests<i class="wsmenu-arrow fa fa-angle-down"></i>
                                        </h4>
                                        <ul class="wstliststy06 clearfix interview_inner">
                                            @foreach($PY_QuizCates as $category)
                                            <li>
                                                <a href="{{ url('python/quiz/'.$category['cat_slug']) }}" title="{{ $category['title'] }}">
                                                    <i class="fas fa-chevron-right"></i>
                                                    {{ $category['title'] }}
                                                </a>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>

                    <li aria-haspopup="true">
                        <?php
                            $JAVA_InterviewCates      = getSubCategories(49,1);
                            $JAVA_QuizCates           = getSubCategories(49,2);
                        ?>
                        <a class="navtext active" href="{{ url('java/videos-tutorials') }}" title="Java"><span>Java <i class="fas fa-chevron-down"></i></span></a>
                        <div class="wsmegamenu clearfix  halfmenu halfmenu_mobile">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-lg-4 col-xl-4 col-md-12 videos">
                                        <ul class="wstliststy06 clearfix">
                                            <?php
                                                /* 1 = english , 2 = hindi */
                                                $engVideos      = getVideosCount(49,1);
                                                $hindiVideos    = getVideosCount(49,2);
                                            ?>
                                            @if($engVideos!=0)
                                            <li>
                                                <a href="{{ url('java/videos-tutorials/english') }}" title="Video Tutorial (English)">
                                                    <i class="fas fa-chevron-right"></i>
                                                    Video Tutorial (English)
                                                </a>
                                            </li>
                                            @endif
                                            
                                            @if($hindiVideos!=0)
                                            <li>
                                                <a href="{{ url('java/videos-tutorials/hindi') }}" title="Video Tutorial (Hindi)">
                                                    <i class="fas fa-chevron-right"></i>
                                                    Video Tutorial (Hindi)
                                                </a>
                                            </li>
                                            @endif
                                            <?php
                                                $techTerm       = getTechTerm(49);
                                            ?>
                                            @if($techTerm!=0)
                                            <li>
                                                <a href="{{ url('java/technical-terms') }}" title="Technical Treams">
                                                    <i class="fas fa-chevron-right"></i>
                                                    Technical Treams
                                                </a>
                                            </li>
                                            @endif
                                            <?php
                                                $quesAns        = getTechTerm(49);
                                            ?>
                                            @if($quesAns!=0)
                                            <li>
                                                <a href="{{ url('java/questions-answers') }}" title="Questions Answer">
                                                    <i class="fas fa-chevron-right"></i>
                                                    Questions Answer
                                                </a>
                                            </li>
                                            @endif
                                            <?php
                                                $term = getTerm(49);
                                            ?>
                                            @if($term!=0)
                                            <li>
                                                <a href="{{ url('java/key-terms') }}" title="Key Terms & Concepts">
                                                    <i class="fas fa-chevron-right"></i>
                                                    Key Terms & Concepts
                                                </a>
                                            </li>
                                            @endif
                                            <?php
                                                $praquiz = getPraQuiz(49);
                                            ?>
                                            @if($praquiz!=0)
                                            <li>
                                                <a href="{{ url('java/practice-quiz') }}" title="Practice Quiz">
                                                    <i class="fas fa-chevron-right"></i>
                                                    Practice Quiz
                                                </a>
                                            </li>
                                            @endif
                                            <?php
                                                $study = getStudy(49);
                                            ?>
                                            @if($study!=0)
                                            <li>
                                                <a href="{{ url('java/study-materials') }}" title="Study Material">
                                                    <i class="fas fa-chevron-right"></i>
                                                    Study Material
                                                </a>
                                            </li>
                                            @endif
                                        </ul>
                                    </div>
                                    <div class="col-lg-4 col-xl-4 col-md-12 interview-li interview-li2">
                                        <h4 class="wstheading clearfix  text-uppercase menu-head flip" title="Interview questions">
                                            Interview questions <i class="wsmenu-arrow fa fa-angle-down"></i>
                                        </h4>
                                        <ul class="wstliststy06 clearfix  interview_inner">
                                            @foreach($JAVA_InterviewCates as $category)
                                            <li>
                                                <a href="{{ url('java/interview-questions/'.$category['cat_slug']) }}" title="{{ $category['title'] }}">
                                                    <i class="fas fa-chevron-right"></i>
                                                    {{ $category['title'] }}
                                                </a>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <div class="col-lg-4 col-xl-4 col-md-12 interview-li padding-remove">
                                        <h4 class="wstheading clearfix  text-uppercase menu-head flip">
                                            Quiz Tests<i class="wsmenu-arrow fa fa-angle-down"></i>
                                        </h4>
                                        <ul class="wstliststy06 clearfix interview_inner">
                                            @foreach($JAVA_QuizCates as $category)
                                            <li>
                                                <a href="{{ url('java/quiz/'.$category['cat_slug']) }}" title="{{ $category['title'] }}">
                                                    <i class="fas fa-chevron-right"></i>
                                                    {{ $category['title'] }}
                                                </a>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>

                    <li aria-haspopup="true">
                        <?php
                            $ANDROID_InterviewCates      = getSubCategories(10,1);
                            $ANDROID_QuizCates           = getSubCategories(10,2);
                        ?>
                        <a class="navtext active" href="{{ url('android/videos-tutorials') }}" title="Android"><span>Android <i class="fas fa-chevron-down"></i></span></a>
                        <div class="wsmegamenu clearfix  halfmenu halfmenu_mobile">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-lg-4 col-xl-4 col-md-12 videos">
                                        <ul class="wstliststy06 clearfix">
                                            <?php
                                                /* 1 = english , 2 = hindi */
                                                $engVideos      = getVideosCount(10,1);
                                                $hindiVideos    = getVideosCount(10,2);
                                            ?>
                                            @if($engVideos!=0)
                                            <li>
                                                <a href="{{ url('android/videos-tutorials/english') }}" title="Video Tutorial (English)">
                                                    <i class="fas fa-chevron-right"></i>
                                                    Video Tutorial (English)
                                                </a>
                                            </li>
                                            @endif
                                            
                                            @if($hindiVideos!=0)
                                            <li>
                                                <a href="{{ url('android/videos-tutorials/hindi') }}" title="Video Tutorial (Hindi)">
                                                    <i class="fas fa-chevron-right"></i>
                                                    Video Tutorial (Hindi)
                                                </a>
                                            </li>
                                            @endif
                                            <?php
                                                $techTerm       = getTechTerm(10);
                                            ?>
                                            @if($techTerm!=0)
                                            <li>
                                                <a href="{{ url('android/technical-terms') }}" title="Technical Treams">
                                                    <i class="fas fa-chevron-right"></i>
                                                    Technical Treams
                                                </a>
                                            </li>
                                            @endif
                                            <?php
                                                $quesAns        = getTechTerm(10);
                                            ?>
                                            @if($quesAns!=0)
                                            <li>
                                                <a href="{{ url('android/questions-answers') }}" title="Questions Answer">
                                                    <i class="fas fa-chevron-right"></i>
                                                    Questions Answer
                                                </a>
                                            </li>
                                            @endif
                                            <?php
                                                $term = getTerm(10);
                                            ?>
                                            @if($term!=0)
                                            <li>
                                                <a href="{{ url('android/key-terms') }}" title="Key Terms & Concepts">
                                                    <i class="fas fa-chevron-right"></i>
                                                    Key Terms & Concepts
                                                </a>
                                            </li>
                                            @endif
                                            <?php
                                                $praquiz = getPraQuiz(10);
                                            ?>
                                            @if($praquiz!=0)
                                            <li>
                                                <a href="{{ url('android/practice-quiz') }}" title="Practice Quiz">
                                                    <i class="fas fa-chevron-right"></i>
                                                    Practice Quiz
                                                </a>
                                            </li>
                                            @endif
                                            <?php
                                                $study = getStudy(10);
                                            ?>
                                            @if($study!=0)
                                            <li>
                                                <a href="{{ url('android/study-materials') }}" title="Study Material">
                                                    <i class="fas fa-chevron-right"></i>
                                                    Study Material
                                                </a>
                                            </li>
                                            @endif
                                        </ul>
                                    </div>
                                    <div class="col-lg-4 col-xl-4 col-md-12 interview-li interview-li2">
                                        <h4 class="wstheading clearfix  text-uppercase menu-head flip" title="Interview questions">
                                            Interview questions <i class="wsmenu-arrow fa fa-angle-down"></i>
                                        </h4>
                                        <ul class="wstliststy06 clearfix  interview_inner">
                                            @foreach($ANDROID_InterviewCates as $category)
                                            <li>
                                                <a href="{{ url('android/interview-questions/'.$category['cat_slug']) }}" title="{{ $category['title'] }}">
                                                    <i class="fas fa-chevron-right"></i>
                                                    {{ $category['title'] }}
                                                </a>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <div class="col-lg-4 col-xl-4 col-md-12 interview-li padding-remove">
                                        <h4 class="wstheading clearfix  text-uppercase menu-head flip">
                                            Quiz Tests<i class="wsmenu-arrow fa fa-angle-down"></i>
                                        </h4>
                                        <ul class="wstliststy06 clearfix interview_inner">
                                            @foreach($ANDROID_QuizCates as $category)
                                            <li>
                                                <a href="{{ url('android/quiz/'.$category['cat_slug']) }}" title="{{ $category['title'] }}">
                                                    <i class="fas fa-chevron-right"></i>
                                                    {{ $category['title'] }}
                                                </a>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>

                    @if(!empty(session('digital_user_id')))
                        <li aria-haspopup="true" class="wsshopmyaccount d-none d-md-block">
                            <a href="{{ url('user/log-out') }}">{{ session('digital_user_name') }} & logout</a>
                        </li>
                    @else
                        <li aria-haspopup="true" class="wsshopmyaccount d-none d-md-block">
                            <a class="register scrool-top singShow" title="Register" data-controls-modal="#signup-popup" data-backdrop="static" data-keyboard="false">
                                Register
                            </a>
                        </li>
                        <li aria-haspopup="true" class="wsshopmyaccount d-none d-md-block" data-backdrop="static" data-keyboard="false">
                            <a class="loginShow scrool-top" title=" Login">
                                Log in
                            </a>
                        </li>
                    @endif
                </ul>
            </nav>
        </div>
    </div>
    <div class="clearfix"></div>
    <!-- login popup -->
        @include('frontend/forms/login')
    <!-- forgot password popup -->
        @include('frontend/forms/forget-password')
    <!-- create account popup -->
        @include('frontend/forms/register')