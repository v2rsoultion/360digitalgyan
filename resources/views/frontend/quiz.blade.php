@extends('frontend/layout/layout')
@section('content')

<section id="bredcum">
    <div class="container">
        <div class="row">
            <div class="col-xl-6 col-md-6 col-6">
                <h4 class="text-capitalize">{{ $questions->title }}</h4>
            </div>
            <div class="col-xl-6 col-md-6 col-6">
                <span class="text-capitalize float-right">
                    <a href="{{ url('/') }}" class="home-main">Home</a>
                    <i class="fas fa-chevron-right"></i>
                    <a href="{{ $practiceUrl }}">Quiz</a>
                    <i class="fas fa-chevron-right"></i>
                    <u class="mater">{{ $questions->title }}</u>
                </span>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</section>
<section id="practice-quiz">
  <div class="container">
    <div class="row">
      <div class="sidemenu"> Side Bar
        <button class="toggle float-right"><i class="fas fa-align-justify"></i></button>
      </div>
      <div class="col-xl-3 col-12 col-md-4" id="target">
        <aside>
          <div class="left-side-panel">
              <h2 class="text-center">
                  {{ unSlugify(Request::segment(1)) }}
                  Quiz Topics   
              </h2>
              <ul>
                  @foreach($subcategories as $subcat)
                  <li>
                      <a href="{{ $practiceUrl.'/'.$subcat['cat_slug'] }}" class="{{ $questions->sub_category_id==$subcat['sub_category_id'] ? 'active' : '' }}" title="{{ $subcat['title'] }}">
                          <i class="fas fa-chevron-right"></i>
                          {{ $subcat['title'] }}
                      </a>
                  </li>
                  @endforeach
              </ul>
          </div>
        </aside>
      </div>
      <div class="col-xl-6 col-12 col-md-8">
        <h3>Quiz Test</h3>
        <span id="quiz_timer" class="float-right text-danger"></span>
        <button id="stopBtnhms" hidden></button>
        <p>Online Test :: ({{ $questions->title }}) Programming Test</p>
        

        <div class="digital-intro" id="intro">
          <b><h5>Instruction:</h5></b>
          <p>
            <?php
              $s_no = count($questions['Quiz']);
            ?>
            Total Number of Questions: {{ count($questions['Quiz']) }}<br>
            @if($questions->quiz_time!='')
            Time alloted: {{ $questions->quiz_time }} minutes<br>
            @endif
            Each question carry 1 mark each, no negative marking<br>
          </p>
          <p class="intro-p">
            NOTE:<br>
            Click the 'Submit Test' button given in the bottom of this page to Submit your answers.
            Test will be submitted after time is Expired.
            Do not refresh the Page.
          </p>
          @if(session('digital_user_id')!='')
            <button type="button" id="start-quiz" class="btn btn-primary next-bt start-quiz text-uppercase col-md-12"> 
              Start 
            </button>
          @else
            <button type="button" onclick="preventQuiz()" class="btn btn-primary start-quiz next-bt text-uppercase col-md-12"> 
              Start 
            </button>
          @endif

        </div>
        
        <div class="digital-intro" id="result" hidden>
          <div id="alert-msg" class="alert alert-info alert-dismissible text-muted m-b-10 col-lg-12 col-md-12 col-sm-12 col-xs-12 font-13 mx-auto info fade show" role="alert" hidden></div>
          <b><h5>Result:</h5></b>
          <div>
            <p class="res_ques">Total Number of Questions<span>{{ count($questions['Quiz']) }}</span></p><br>
            <p class="res_corr">Correct Answer<span id="correct_q">0</span></p><br>
            <p class="res_dur">Incorrect Answer<span id="incorrect_q">0</span></p><br>
            <p class="res_att">Question Attempted<span id="attemped_q">0</span></p><br>
            <p class="res_sco">Net Score<span id="net_q">0</span></p><br>
          </div>
          <a href=""> 
            <button class="btn btn-primary next-bt text-uppercase review_btn">
              Restart
            </button> 
          </a>
        </div>

        <div class="digital-intro" id="intro-field" hidden>
        <form id="quiz-form" method="post">
          @csrf
          <input type="hidden" name="subcategory" value="{{ $questions->sub_category_id }}">
          <input type="hidden" name="app-type" value="{{ Request::segment(1) }}">
          @foreach($questions['Quiz'] as $key => $ques)
            <?php $key++ ?>
            <div class="radiomain" id="ques{{ $key }}" {{ $key==1 ? '' : 'hidden' }}>
              <input type="text" name="q-{{ $key }}" value="{{ $ques->qustion_id }}" hidden>
              <h5>Q.{{ $key }} {{ getTextTransform($ques->qustion_qustion,0) }}</h5>
              <div class="radio">
                <input id="radio1{{ $ques->qustion_id }}" name="radio{{ $key }}" class="quiz-radio" value="1" type="radio">
                <label for="radio1{{ $ques->qustion_id }}"  class="radio-label green-check">
                  <span class="radio-label"></span>
                    <p>{{ getTextTransform($ques->qustion_option1,0) }}</p>
                </label>
              </div>
              
              <div class="radio">
                <input id="radio2{{ $ques->qustion_id }}" name="radio{{ $key }}" class="quiz-radio" value="2" type="radio">
                <label  for="radio2{{ $ques->qustion_id }}"  class="radio-label green-check">
                  <span class="radio-label"></span>
                  <p>{{ getTextTransform($ques->qustion_option2,0) }}</p>

                </label>
              </div>
              
              @if($ques->qustion_option3!='')
                <div class="radio">
                  <input id="radio3{{ $ques->qustion_id }}" name="radio{{ $key }}" class="quiz-radio" value="3" type="radio">
                  <label for="radio3{{ $ques->qustion_id }}"  class="radio-label green-check">
                    <span class="radio-label"></span>
                    <p>{{ getTextTransform($ques->qustion_option3,0) }}</p>
                  </label>
                </div>
              @endif

              @if($ques->qustion_option4!='')
                <div class="radio">
                  
                  <input id="radio4{{ $ques->qustion_id }}" name="radio{{ $key }}" class="quiz-radio" value="4" type="radio">
                  <label  for="radio4{{ $ques->qustion_id }}"  class="radio-label green-check">
                    <span class="radio-label"></span>
                    <p>{{ getTextTransform($ques->qustion_option4,0) }}</p>
                  </label>
                </div>
              @endif


              @if($ques->qustion_option5!='')
                <div class="radio">
                  
                  <input id="radio5{{ $ques->qustion_id }}" name="radio{{ $key }}" class="quiz-radio" value="5" type="radio">
                  <label  for="radio5{{ $ques->qustion_id }}"  class="radio-label green-check">
                    <span class="radio-label"></span>
                    <p>{{ getTextTransform($ques->qustion_option5,0) }}</p>
                  </label>
                </div>
              @endif
            
            </div>
          @endforeach
            <div class=" prev-next">
              <button type="button" onclick="showQuestion(2)" class="btn btn-primary float-left text-uppercase "> Previous</button>
              <button type="button" onclick="showQuestion(1)" class="btn btn-primary next-bt text-uppercase active "> Next </button>
              <button type="submit" id="quiz-submit" hidden class="btn btn-success next-bt text-uppercase active"> Submit Quiz </button>
              <div class="clearfix"></div>
            </div>
            <input type="hidden" id="current_show_ques" value="1">
            <input type="hidden" name="q-no" id="total_question" value="{{ count($questions['Quiz']) }}">
          </form>
        </div>
      </div>
      <div class="col-xl-3 col-12 col-md-3 adds d-none d-md-block">
        @include('frontend/layout/right-sidebar')
      </div>
    </div>
  </div>
</section>



<script type="text/javascript">
$(document).ready(function(){
  $(document).on('click', '.quiz-radio',function(event){
    $(this).parents('div.radiomain').find('label.quiz-blue').removeClass('quiz-blue');
    $(this).next().addClass('quiz-blue');
  });
});


/* show next questions */
function showQuestion(type){
  current = $("#current_show_ques").val();
  total = $('#total_question').val();
  if(current>0){
    $("#ques"+current).attr('hidden','true');
  }
  
  if(type==1){
    //1 = next type
    current = parseInt(current)+1;
  }else{
    //2 = Previous type
    current = parseInt(current)-1;
  }

  if(current == 0){
    current = 1;
  }

  if(current > total){
    current = total;
  }

  if (current==total){
    $('#quiz-submit').removeAttr('hidden');
  }else{
    $('#quiz-submit').attr('hidden','true');
  }

  $("#ques"+current).removeAttr('hidden');
  $("#current_show_ques").val(current);
}

/* Start Quiz Button */
$('#start-quiz').on('click',function(){
  $('#intro').hide();
  $('#intro-field').removeAttr('hidden');
});


/* get result for quiz */
$('#quiz-form').on('submit',function(e){
  $.ajax({
    type: "POST",
    url: "{{ url('user/get-quiz-result') }}",
    data : $('#quiz-form').serialize(),
    success: function(res){
      if (res.status==true){
        data = res.message;
        $('#correct_q').html(data.correct);
        $('#incorrect_q').html(data.incorrect);
        $('#attemped_q').html(data.attemped);
        $('#net_q').html(data.net);
        $('#intro-field').attr('hidden',true);
        $('#result').removeAttr('hidden');
        $('#stopBtnhms').trigger('click');
        $('#quiz_timer').remove();
      }else{
        alert("We found an error in your data.  Please return to home page and try again.");
      }
    },
    error: function(){
      alert("We found an error in your data.  Please return to home page and try again.");
    }
  });
  e.preventDefault();
});


/*
** for prevent user to post answer 
*/
function preventQuiz(){
  swal({
      title: " ",
      text: "Please login first to give quiz..!!",
      type: "warning",
      confirmButtonClass: "btn all-button",
      confirmButtonText: "Log in!"
    },
    function(){
      $('.loginShow').trigger('click');
  });
}

$('#start-quiz').on('click',function(){
  $('#quiz_timer').countdowntimer({
    minutes : <?php  echo $questions->quiz_time ?>,
    seconds : 0,
    size : "lg",
    regexpMatchFormat: "([0-9]{1,2}):([0-9]{1,2})",
    regexpReplaceWith: "$1 <b> Mins. </b> : $2 <b> Seconds</b>",
    timeUp : timeisUp,
    stopButton : "stopBtnhms"
  });


  function timeisUp(){
    $('#quiz_timer').hide();
    $('#alert-msg').html('<strong>Yeah..!!</strong> Your Quiz Test just submitted..!!<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
    $('#alert-msg').removeAttr('hidden');
    $('#quiz-submit').trigger('click');

      /*
      **  for hide alert with time
      */
      window.setTimeout(function(){
        $(".alert").fadeTo(500, 0).slideUp(500, function(){
            $(this).remove();
        });
      },6000);
  }
});



</script>
@endsection