@extends('frontend/layout/layout')
@section('content')
<section id="bredcum">
  <div class="container">
    <div class="row">
      <div class="col-xl-6 col-md-6 col-6">
        @if(Request::segment(3))
          <h4 class="text-capitalize">{{ unSlugify(Request::segment(3)) }}</h4>
        @else
          <h4 class="text-capitalize">Videos Tutorials</h4>
        @endif
        
      </div>
      <div class="col-xl-6 col-md-6 col-6">
        <span class="text-capitalize float-right">
            <a href="{{ url('/') }}" class="home-main">Home</a>
            <i class="fas fa-chevron-right"></i>
            <a class="#">Videos</a>
            @if(Request::segment(3))
            <i class="fas fa-chevron-right"></i>
            <u class="mater">{{ unSlugify(Request::segment(3)) }}</u>
            @endif
        </span>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
</section>
<section id="vedios-tutorial">
  <div class="container">
    <div class="row">
      <div class="col-md-9 col-12 col-xl-9">
        <div class="vedio-form">
          <form name="myform" method="POST" action="{{ $videos_url }}">
            {{ csrf_field() }}
            <div class="input-group" id="seacr_panel">
              <div class="select-dropdown">
                <select class="form-control" name="sort" id="withSelect">
                  <option value="1" {{ request()->sort==1 ? selected : '' }} >Newest First</option>
                  <option value="2" {{ request()->sort==2 ? selected : '' }}>Oldest First</option>
                  <option value="3" {{ request()->sort==3 ? selected : '' }}>Most Viewed</option>
                </select>
              </div>
              <input type="hidden" name="language" id="language" value="{{ $language }}" >
              <input type="hidden" name="categorytype" value="{{ $cat_slug }}">
              <input type="text" name="video_title" class="form-control vedio-search" value="{{ request()->video_title }}" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" title="Search" class="btn btn-default header-submit">SEARCH</button>
              </span>
            </div>
          </form>
        </div>
        <div id="tutorials">
          <div class="col-xl-12 col-md-12 col-12">
            <h1 class="text-center">Videos Tutorial</h1>
          </div>
          <div class="tab-slider--nav">
            <ul class="tab-slider--tabs">
              <!-- <a href="{{ url($videos_url) }}">
                <li class="tab-slider--trigger {{ $language=='all' ? 'active' : '' }} text-uppercase" title="All Videos">
                    All Videos ({{ $AllVideoCounts }})
                </li>
              </a> -->
              <a href="{{ url($videos_url.'/hindi') }}">
                <li class="tab-slider--trigger {{ $language==2 || $language=='all'  ? 'active' : '' }} text-uppercase" title="Hindi" >
                    Hindi ({{ $hindiVideoCounts }})
                </li>
              </a>
              <a href="{{ url($videos_url.'/english') }}">
                <li class="tab-slider--trigger {{ $language==1 ? 'active' : '' }} text-uppercase" title="English">
                    English ({{ $engVideoCounts }})
                </li>
              </a>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="clearfix"></div>
          <div class="tab-slider--container container padding-remove">
            <div id="hindi" class="tab-slider--body">
              <div class="loader">
                  <div class="row english-tab" id="videosMain">
                    @forelse($videos as $video)
                      <div class="col-xl-4 col-md-6 col-6 lagy">
                        <div class="vedio-1">
                          <a href="{{ url($videos_url.'/'.$video->video_slug) }}" title="{{ $video->title }}"> 
                            @if(file_exists('uploads/'.$video->image))
                              <img src="{{ url('uploads/'.$video->image) }} " class="img-fluid" alt="related-img">
                            @else
                              <img src="{{ url('public/frontend/images/nopreview-available.jpg') }}">
                            @endif
                          </a>
                          <a href="{{ url($videos_url.'/'.$video->video_slug) }}" title="{{ $video->title }}">
                            <h5>
                              {{ $video->title }}
                            </h5>
                          </a>
                          <span><i class="fas fa-eye"></i>{{ $video->video_views ?? 0 }}</span>
                        </div>
                      </div>
                    @empty
                      <div class="col-xl-12 col-md-12 col-12 lagy">
                            @include('frontend.layout.search-not-found')
                      </div>
                    @endforelse
                  </div>
                <center>
                  <button id="moreBtn" class="btn btn-default btn-loadmore">Load More</button>
                </center>
              </div>
            </div>
          </div>
        </div>
      </div>  
      <div class="col-xl-3 col-12 col-md-3 adds d-none d-md-block">
        @include('frontend/layout/right-sidebar')
      </div>
    </div>
  </div>
</section>

{!! Html::script('public/frontend/js/loadMore.js') !!}

<script type="text/javascript">
  $("#videosMain").loadMore({
    selector: '.lagy',
    loadBtn: '#moreBtn',
    limit:12,
    load:9,
    animate: true,
    animateIn: 'fadeInUp'
  });
</script>
@endsection
