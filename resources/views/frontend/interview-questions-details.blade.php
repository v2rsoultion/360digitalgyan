@extends('frontend/layout/layout')
@section('content')
<section id="bredcum">
    <div class="container">
        <div class="row">
            <div class="col-xl-6 col-md-6 col-6">
                <h4 class="text-capitalize">{{ $ques['getSubCategory']->title }}</h4>
            </div>
            <div class="col-xl-6 col-md-6 col-6">
                <span class="text-capitalize float-right">
                    <a href="{{ url('/') }}" class="home-main">Home</a>
                    <i class="fas fa-chevron-right"></i>
                    <a href="{{ $interUrl }}">Interview Questions</a>
                    <i class="fas fa-chevron-right"></i>
                    <u class="mater">{{ $ques['getSubCategory']->title }}</u>
                </span>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</section>
<section id="email-marketing">
    <div class="container">
        <div class="row">
            <div class="col-xl-3 col-12 col-md-5" id="email-sidepanel">
                <aside>
                    <div class="left-side-panel">
                        <h2 class="text-center">
                            {{ unSlugify(Request::segment(1)) }}        
                        </h2>
                        <ul>
                            @foreach($subcategories as $subcat)
                            <li>
                                <a href="{{ $interUrl.'/'.$subcat['cat_slug'] }}" class="{{ $ques['getSubCategory']->sub_category_id==$subcat['sub_category_id'] ? 'active' : '' }}" title="{{ $subcat['title'] }}">
                                    <i class="fas fa-chevron-right"></i>
                                    {{ $subcat['title'] }}
                                </a>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </aside>
            </div>
            <div class="col-xl-9 col-md-7">
              @if(session()->has('success'))
                  <div class="alert alert-success alert-dismissible text-muted m-b-10 col-lg-6 col-md-6 col-sm-12 col-xs-12 font-13 mx-auto green fade show" role="alert">
                    <?php echo getTextTransform(session()->get('success'),0) ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
              @endif

              @if($errors->any())
                  <div class="alert alert-danger alert-dismissible text-muted m-b-10 col-lg-6 col-md-6 col-sm-12 col-xs-12 font-13 mx-auto text-danger fade show" role="alert">
                    <?php echo getTextTransform($errors->first(),0) ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
              @endif
                <div class="email-question">
                    <div class="accordion_container">
                        <div class="inter-question">
                            <div class="col-xl-12 col-md-12 col-12">
                              <div class="row questions-parent" data-ques="{{ $ques->iq_ques_ans_id }}">
                                <div class="col-xl-12 col-md-12 col-12">
                                  <h3> 
                                    <a href="{{ $interUrl.'/'.$ques->iq_ques_ans_id }}">
                                      Q.1 &nbsp;&nbsp;<?php echo getTextTransform($ques->iq_ques_ans_q,0) ?>
                                    </a>
                                  </h3>  
                                </div>
                                <div class="col-xl-8 col-md-7">
                                    <div class="ques-side">
                                        <div class="ques-side2 float-left">
                                            <span class="text-center">
                                                <i class="fas fa-calendar-alt"></i>
                                                {{ date("d M,Y", strtotime($ques->created_at)) }}
                                                &nbsp;
                                            </span>
                                        </div>
                                    <div class="clearfix"></div>
                                  </div>
                                </div>
                                <div class="col-xl-4 col-md-5">
                                  <div class="like-dislike float-right">
                                    <a title="Views">
                                        <span>
                                          <i class="fas fa-eye"></i>{{ $ques->get_views_count ?? 0 }}
                                        </span>
                                    </a>
                                    @if(session('digital_user_id')!='')
                                      <a data-toggle="modal" data-target="#post-your-comment" class="setQues" title="Comment">
                                        <span>
                                          <i class="fas fa-comments"></i>
                                          {{ $ques->get_comment_count ?? 0 }} Comments
                                        </span>
                                      </a>
                                    @else
                                      <a onclick="preventPostAns()" class="setQues" title="Comment">
                                        <span>
                                          <i class="fas fa-comments"></i>
                                          {{ $ques->get_comment_count ?? 0 }} Comments
                                        </span>
                                      </a>
                                    @endif
                                    @if(session('digital_user_id')!='')
                                        <a class="heart-like" title="Comment">
                                            <span class="">
                                                <i class="fas fa-heart {{ in_array($ques->iq_ques_ans_id,$likedByUser) ? 'like-heart' : '' }}"></i> 
                                                <span1>
                                                    {{ $ques->get_likes_count ?? 0 }}
                                                </span1>
                                                Likes
                                            </span>
                                        </a>
                                    @else
                                        <a onclick="preventPostAns()" title="Comment">
                                            <span class="">
                                                <i class="fas fa-heart"></i>
                                                    {{ $ques->get_likes_count ?? 0 }} Likes
                                            </span>
                                        </a>
                                    @endif
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="accordion_body_iq detail" style="display: block;">
                            <p>
                                <?php  echo getTextTransform($ques->iq_ques_ans_a,0) ?>
                            </p>
                          </div>
                      <div class="answer-pannel">
                        @foreach($ques['getComment'] as $IqCmnt)
                        <div class="question-type answer-type" data-ans="{{ $IqCmnt->comment_id}}" >
                          <div class="col-xl-12 col-md-12 col-12 answer-pading">
                            <div class="row">
                              <div class="col-xl-11 col-md-8 col-12">
                                <p class="iq-detail">{{ $IqCmnt->comment_comment }}</p>
                              </div>
                              <div class="col-xl-1 col-md-4 col-12 side-men">
                                <i class="fas fa-ellipsis-h"  data-toggle="dropdown"></i>
                                <div class="dropdown-menu dropdown-menu-right">
                                  <a class="dropdown-item" href="#" title="Edit Profile"> Edit Profile</a>
                                  <a class="dropdown-item" href="#" title="My Inbox"> My Inbox</a>
                                  <a class="dropdown-item" href="#" title="Task">Task</a>
                                  <a class="dropdown-item" href="#" title="Chats"> Chats</a>
                                </div>
                              </div>
                              <div class="clearfix"></div>
                              <div class="col-xl-11 col-md-8 col-12">
                                <div class="ques-side">
                                  <a href=""><img src="{{ $IqCmnt['getUser']->seo_users_image!='' ? url('uploads/'.$IqCmnt['getUser']->seo_users_image) : url('public/frontend/images/boy.png') }}" class="float-left" alt="boy"></a>
                                  <div class="ques-side2 float-left">
                                    <span class="text-center">
                                      <a href="" title="{{ $IqCmnt['getUser']->seo_users_name }}">
                                        <u>{{ $IqCmnt['getUser']->seo_users_name }}</u> 
                                      </a>
                                      <br>
                                      <i class="fas fa-calendar-alt"></i>
                                      {{ date("d M,Y", strtotime($IqCmnt->comment_created)) }}
                                    </span>
                                  </div>
                                  <div class="clearfix"></div>
                                </div>
                              </div>
                              <div class="col-xl-1 col-md-4 col-12">
                                <div class="like-dislike float-right">
                                  <span class="{{ session('digital_user_id')!='' ? 'like-dislike-comment ' : '' }}{{ in_array($IqCmnt->comment_id,$cmntLikedByUser) ? 'like' : '' }}">
                                    <i class="fas fa-thumbs-up"></i>
                                    <span1>
                                      {{ $IqCmnt->likes_count ?? 0 }}
                                    </span1>
                                  </span>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        @endforeach
                    </div>           
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
/* for like question */
$(document).on('click', '.heart-like',function(event){
    var id     = $(this).closest('div.questions-parent').attr('data-ques');
    var elment = $(this);
    if ($(this).find('i').hasClass('like-heart')){
      $(this).find('i').removeClass('like-heart');
    }else{
      $(this).find('i').addClass('like-heart');
    }

    $.ajax({
      type: "POST",
      url: "{{ url('user/like-interview-question') }}",
      data: {
        "_token"  : "{{ csrf_token() }}",
        data_id   : id
      },
      success: function(res){
        if(res.status==1){
          elment.find('span1').html(res.likes);
        }
      },
      error: function(){
        alert(" :( Sorry something went wrong..!!");
      }
    });
    return false;
});


/* for like comment */
$(document).on('click', '.like-dislike-comment',function(event){
    var id     = $(this).closest('div.answer-type').attr('data-ans');
    var elment = $(this);
    if ($(this).hasClass('like')){
      $(this).removeClass('like');
    }else{
      $(this).addClass('like');
    }

    $.ajax({
        type: "POST",
      url: "{{ url('user/like-comment') }}",
      data: {
        "_token"  : "{{ csrf_token() }}",
        data_id   : id
      },
      success: function(res){
        if(res.status==1){
            elment.find('span1').html(res.likes);
        }
      },
      error: function(){
        alert(" :( Sorry something went wrong..!!");
      }
    });
    return false;
});


$(document).on('click', '.like-dislike a',function(event){
    var txt = $(this).closest('div.inter-question').find('h3').text();
    var id = $(this).closest('div.questions-parent').attr('data-ques');
    $("#model-question").html(txt);
    $("#question-id").val(id);
});

/*
**  for hide alert with time
*/
window.setTimeout(function(){
  $(".alert").fadeTo(500, 0).slideUp(500, function(){
      $(this).remove();
  });
},4000);
</script>

<div class="modal ask-modal fade" id="post-your-comment">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Post Your Comment</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <form method="post" id="form-question-comment" action="{{ url('user/save-comment') }}">
        {{ csrf_field() }}
        <div class="modal-body">
          <h5 id="model-question"></h5>
          <div class="form-group text-danger">
            <textarea class="form-control" name="user_comment" placeholder="Type your comment..." rows="5" id="comment"></textarea>
          </div>
          <input type="hidden" name="quetion_name" id="question-id">
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn all-button" title="SAVE">SAVE</button>
          <button type="button" class="btn cancel-button" data-dismiss="modal" title="CANCEL">
            CANCEL
          </button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection