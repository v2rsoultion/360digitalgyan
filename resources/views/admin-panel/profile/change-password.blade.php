@extends('admin-panel.layout.header')
@section('content')

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Change Password</h4> 
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="{{ url('/')}}">Dashboard</a></li>
                    <li class="active">Change Password</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->
        <!-- .row -->
        <div class="row">

            <div class="col-md-12">
                @if($errors->any())
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true" style="width: auto !important;">×</button>
                        <p> {{$errors->first()}} </p>
                    </div>
                @endif
    
                @if(session()->has('message'))
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true" style="width: auto !important;">×</button>
                        <p> {{ session()->get('message') }} </p>
                    </div>
                @endif
    
    
                @if(session()->has('notice'))
                    <div class="alert alert-info">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true" style="width: auto !important;">×</button>
                        <p> {{ session()->get('notice') }} </p>
                    </div>
                @endif
            </div>

            {!! Form::open(['id' => 'change_password_form', 'url' => $update_password_url, 'class' => 'form-horizontal']) !!}
            
            <div class="col-md-12 col-xs-12 " >
                <div class="white-box p-0">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default p-b-0">
                                <div class="panel-heading"><i class="ti-lock"></i> Change Password</div>
                                <div class="panel-wrapper collapse in">
                                    <div class="panel-body">
                                        <div class="">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="" for="{!! trans('language.current_password') !!}" >{!! trans('language.current_password') !!}</label>
                                                    {!! Form::password('current-password',  ['class' => 'form-control ', 'id' => 'current-password', 'autocomplete' => 'off']) !!}
                                                </div>
                                
                                                <div class="form-group">
                                                    <label class="" for="{!! trans('language.new_password') !!}" >{!! trans('language.new_password') !!}</label>
                                                    {!! Form::password('new-password',  ['class' => 'form-control ', 'id' => 'new-password', 'autocomplete' => 'off']) !!}
                                                </div>
                                
                                                <div class="form-group">
                                                    <label class="" for="{!! trans('language.confirm_new_password') !!}" >{!! trans('language.confirm_new_password') !!}</label>
                                                    {!! Form::password('new-password_confirmation', ['class' => 'form-control ', 'id' => 'new-password_confirmation', 'autocomplete' => 'off']) !!}
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
            
                        <div class="col-md-12">
                            <div class="white-box panel-footer p-20 m-b-0">
                                <button type="submit" class="btn btn-success">Update Password</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
        
    </div>
</div>
<!-- ============================================================== -->
<!-- / Page Content -->
<!-- ============================================================== -->

<script type="text/javascript">
$(document).ready(function() {
    $('#change_password_form').bootstrapValidator({
        fields: {
            'current-password': {
                validators: {
                    notEmpty: {
                        message: 'Please enter your current password'
                    }
                }
            },

            'new-password': {
                validators: {
                    notEmpty: {
                        message: 'Please enter your new password'
                    },
                    regexp: {
                        regexp:  /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^\w\s]).{8,}$/,
                        message: 'Password must contain minimum 6 characters including one uppercase, one lowercase, one special character and numeric value.'
                    }
                }
            },

            'new-password_confirmation': {
                validators: {
                    notEmpty: {
                        message: 'Please enter your confirm password'
                    },
                    identical: {
                        field: 'new-password',
                        message: 'The password and its confirm are not the same'
                    }
                }
            },
        }
    });

    $('#new-password').keypress(function( e ) {
        if(e.which === 32) 
            return false;
    });
    
    $('#new-password_confirmation').keypress(function( e ) {
        if(e.which === 32) 
            return false;
    });
});

</script>

@endsection

