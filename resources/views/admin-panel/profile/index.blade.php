@extends('admin-panel.layout.header')
@section('content')


<!-- ============================================================== -->
<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Profile</h4> 
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="{{ url('/')}}">Dashboard</a></li>
                    <li class="active">Profile page</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->
        <!-- .row -->
        <div class="row">

            <div class="col-md-12">
                @if($errors->any())
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true" style="width: auto !important;">×</button>
                        <p> {{$errors->first()}} </p>
                    </div>
                @endif
    
                @if(session()->has('message'))
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true" style="width: auto !important;">×</button>
                        <p> {{ session()->get('message') }} </p>
                    </div>
                @endif
    
    
                @if(session()->has('notice'))
                    <div class="alert alert-info">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true" style="width: auto !important;">×</button>
                        <p> {{ session()->get('notice') }} </p>
                    </div>
                @endif
            </div>

            {!! Form::open(['id' => 'profile_edit_form', 'url' => $edit_url, 'files' => 'true', 'class' => 'form-horizontal']) !!}
            <div class="col-md-4 col-xs-12">
                <div class="white-box">
                    <label for="input-file-disable-remove">Profile Picture</label>

                    @php
                        if( !empty($admin_data['admin_profile_img']) ){
                            $admin_pic = url("public/uploads/admin/profile-pic/".$admin_data['admin_profile_img']);
                        }  else {
                            $admin_pic = url("public/admin/plugins/images/placeholder-image.png");
                        } 
                    @endphp
                    
                    <input type="file" name="profile_picture" id="input-file-disable-remove" class="dropify" data-show-remove="false" data-default-file="{!! $admin_pic !!}"/> 
                </div>
            </div>
            <div class="col-md-8 col-xs-12">
                <div class="white-box">
                    <div class="tab-content m-t-0">
                        <div class="tab-pane active" id="settings">
                                <div class="form-group">
                                    <label class="col-md-12">{!! trans('language.username') !!}</label>
                                    <div class="col-md-12">
                                        {!! Form::text('admin_name', old('username',isset($admin_data['admin_name']) ? $admin_data['admin_name']: ''), ['class' => 'form-control ', 'id' => 'business_name', 'autocomplete' => 'off', 'placeholder' => 'Johnathan Doe']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="example-email" class="col-md-12">{!! trans('language.email') !!}</label>
                                    <div class="col-md-12">
                                        {!! Form::text('email', old('email',isset($admin_data['email']) ? $admin_data['email']: ''), ['class' => 'form-control readyonly-box', 'id' => 'email', 'autocomplete' => 'off', 'placeholder' => 'johnathan@admin.com']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">{!! trans('language.user_phone') !!}</label>
                                    <div class="col-md-12">
                                        {!! Form::text('phone', old('phone',isset($admin_data['phone']) ? $admin_data['phone']: ''), ['class' => 'form-control', 'id' => 'phone', 'autocomplete' => 'off', 'placeholder' => '123 456 7890']) !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button type="submit"  class="btn btn-success">Update Profile</button>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
        
    </div>
</div>
<!-- ============================================================== -->
<!-- / Page Content -->
<!-- ============================================================== -->



<script type="text/javascript">
$(document).ready(function() {
    $('#profile_edit_form').bootstrapValidator({
        fields: {
            username: {
                validators: {
                    notEmpty: {
                        message: 'Please enter username'
                    },
                    regexp: {
                        regexp: /^[A-Za-z0-9-_ ]+$/i,
                        message: 'Please enter valid username'
                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: 'Please enter email'
                    },
                    emailAddress: {
                        message: 'Please enter valid email address'
                    }
                }
            },
            phone: {
                validators: {
                    notEmpty: {
                        message: 'Please enter phone number'
                    },
                    regexp: {
                        regexp: /^[\+[1-9]{1}[0-9]{3,14}]*$/,
                        message: 'Please enter valid phone number'
                    }
                }
            }
        }
    });

    $('#phone').keypress(function( e ) {
        if(e.which === 32) 
            return false;
    });
});

</script>

<script type="text/javascript">
    $(document).ready(function() {
        // Basic
        $('.dropify').dropify();
        // Translated
        $('.dropify-fr').dropify({
            messages: {
                default: 'Drag and drop a file here or click',
                replace: 'Drag and drop or click to replace',
                remove: 'Remove',
                error: 'Ooops, something wrong happended.'
            }
        });
        // Used events
        var drEvent = $('#input-file-events').dropify();
        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });
        drEvent.on('dropify.afterClear', function(event, element) {
            alert('File deleted');
        });
        drEvent.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });
        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        })
    });
</script>
@endsection