<div class="row">
	<div>
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="white-box">
                <label for="input-file-disable-remove">
                    {{ trans('language.greetings_image') }}
                </label>
                @php
                    if(!empty($greetings['greetings_image'])){
                        $greetingsImage = url('uploads/'.$greetings['greetings_image']);
                    }else{
                        $greetingsImage = url("/public/admin/nopreview-available.jpg");
                    } 
                @endphp
                <input type="file" name="greetings_image" id="input-file-disable-remove" class="dropify" data-show-remove="false" data-default-file="{!! $greetingsImage !!}"/> 
            </div>
        </div>
	</div>
</div>
<div class="row">
	<div class=" col-sm-9">
	    <button type="submit" name="submit" class="btn btn-info waves-effect waves-light m-t-10">{{  $submit_button }}</button>
	</div>
</div>
<div class="clearfix"></div>