<div class="row">
    <div class="col-sm-8">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="input-group">
                 <label for="{!! trans('language.subcategory_name') !!}">
                    {!! trans('language.category_name') !!}
                </label>

                <select class="form-control select2 " id="category_id"  name="category_id" >
                    <option value="">Select Category</option>
                    @foreach ($categories as  $category)
                        <option value="{!! $category->iq_category_id !!}" {{ $subcategory->category_id==$category->iq_category_id ? 'selected' : '' }} >
                            {!! $category->iq_category_name !!}
                        </option>   
                    @endforeach
                </select>
            </div>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="input-group">
                <label for="{!! trans('language.subcategory_name') !!}">
                    {!! trans('language.subcategory_name') !!}
                </label>

                {!! Form::text('title', old('title',isset($subcategory['title']) ? $subcategory['title']: ''), ['class' => 'form-control','placeholder'=>trans('language.subcategory_name')]) !!}

                @if($errors->has('title'))
                    <p class="help-block">
                        {{ $errors->first('title') }}
                    </p>
                @endif
            </div>
        </div>

         <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="input-group">
                <label for="{!! trans('language.subcategory_quiz_time') !!}">
                    {!! trans('language.subcategory_quiz_time') !!} (minutes) 
                </label>

                {!! Form::number('quiz_time', old('quiz_time',isset($subcategory['quiz_time']) ? $subcategory['quiz_time']: ''), ['class' => 'form-control','placeholder'=>trans('language.subcategory_quiz_time')]) !!}

                @if($errors->has('quiz_time'))
                    <p class="help-block">
                        {{ $errors->first('quiz_time') }}
                    </p>
                @endif
            </div>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="input-group">
                <label for="{!! trans('language.subcategory_order') !!}">
                    {!! trans('language.subcategory_order') !!}
                </label>

                {!! Form::number('category_order', old('category_order',isset($subcategory['category_order']) ? $subcategory['category_order']: ''), ['class' => 'form-control','placeholder'=>trans('language.subcategory_order')]) !!}

                @if($errors->has('category_order'))
                    <p class="help-block">
                        {{ $errors->first('category_order') }}
                    </p>
                @endif
            </div>
        </div>

    </div>

    <div class="col-md-4 col-xs-12">
        <div class="white-box">
            <label for="input-file-disable-remove">Category App Icon</label>

            @php
                if( !empty($subcategory['image']) ){
                    $categoryIcon = url('uploads/'.$subcategory['image']);
                }  else {
                    $categoryIcon = url("/public/admin/nopreview-available.jpg");
                } 
            @endphp
            
            <input type="file" name="category_icon" id="input-file-disable-remove" class="dropify" data-show-remove="false" data-default-file="{{ $categoryIcon }}"/> 
        </div>
    </div>
</div>

<div class="row">
	<div class=" col-sm-9">
	    <button type="submit" name="submit" class="btn btn-info waves-effect waves-light m-t-10">{{  $submit_button }}</button>
	</div>
</div>
<div class="clearfix"></div>