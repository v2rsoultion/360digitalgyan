@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
    .nums{
        width: 50px;
    }
</style>

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">{!! $page_title !!}</h4> 
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="{!! URL::to('admin-panel/dashboard') !!}">Dashboard</a></li>
                    <li><a href="#">{!! trans('language.subcategory') !!}</a></li>
                    <li class="active">{!! $page_title !!}</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    @if(session()->has('success'))
                        <p class="alert alert-success text-muted m-b-10 col-lg-12 col-md-12 col-sm-12 col-xs-12 font-13 green">
                            {{ session()->get('success') }}
                        </p>
                    @endif
                    @if($errors->any())
                        <p class="text-muted m-b-10 col-lg-12 col-md-12 col-sm-12 col-xs-12 font-13 red">{{$errors->first()}}</p>
                    @endif
                    <div class="clearfix"></div>
                    {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3">
                                <div class="input-group">
                                    <select class="form-control select2 " id="category_id"  name="category_id" >
                                        <option value="">Select Category</option>
                                        @foreach ($categories as  $categoryData)
                                         <option value="{!! $categoryData->iq_category_id !!}">{{ $categoryData->iq_category_name }}</option>   
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3">
                                <div class="input-group">
                                    {!! Form::text('sub_category_name', old('sub_category_name',isset($category['sub_category_name']) ? $category['sub_category_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.category_title'), 'id' => 'sub_category_name']) !!}
                                </div>
                            </div>
                            <div class="col-lg-1 col-md-1">
                                {!! Form::submit('Search', ['class' => 'btn btn-info ','name'=>'Search']) !!}
                            </div>
                            <div class="col-lg-1 col-md-1">
                                {!! Form::button('Clear', ['class' => 'btn btn-info ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <div class="col-sm-12">
                <div class="white-box">
                    <div class="table-responsive">
                        <table class="table dataTable " id="category-table">
                            <thead>
                                <tr>
                                    <th>S.No</th>
                                    <th>Name</th>
                                    <th>App Icon</th>
                                    <th>Quiz Time</th>
                                    <th>Order</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

    $(document).ready(function () {
        var table = $('#category-table').DataTable({
            // dom: 'Blfrtip',
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            ajax: {
                url: '{{url('admin-panel/sub-category/data')}}',
                data: function (d) {
                    d.sub_category_name = $('input[name=sub_category_name]').val();
                    d.category_id       = $('select[name=category_id]').val();
                }
            },
            columns: [
                {data: 'DT_Row_Index', name: 'DT_Row_Index' },
                {data: 'title', name: 'title'},
                {data:'icon',name:'icon'},
                {data: 'quiz_time',name:'quiz_time'},   
                {data: 'category_order', name:'category_order'},
                {data: 'status',name:'status'},
                {data: 'action', name: 'action'},
            ],
             columnDefs: [
                {
                    "targets": 0, // your case first column
                    "width": "10%"
                },
                {
                    "targets": 1, // your case first column
                    "width": "30%"
                },
                
                {
                    "targets": 2, // your case first column
                    "width": "10%"
                },
                {
                    "targets": 5, // your case first column
                    "width": "10%"
                },
                
                {
                    targets: [ 0, 1, 2],
                    className: 'mdl-data-table__cell--non-numeric'
                }
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });

        $('#clearBtn').click(function(){
            location.reload();
        })
       
    });

    var elems = document.getElementsByClassName('confirmation');
    var confirmIt = function (e) {
        if (!confirm('Are you sure?')) e.preventDefault();
    };
    for (var i = 0, l = elems.length; i < l; i++) {
        elems[i].addEventListener('click', confirmIt, false);
    }
    

    /* for Quiz status change in category  */
    function changeStatusQuizCategory(id,status){
        var token = '{!!csrf_token()!!}';
        $.ajax(
        {
            url: "{{ url('admin-panel/category/category-status') }}",
            type: 'GET',
            data: {
                'category_id': id,
                'category_status': status,
                'category_type':'quiz',
            },
            success: function (res)
            {
                if (res == "Success")
                {
                    location.reload();
                } else
                {
                    location.reload();
                }
            }
        });
    }

    /* for Set Order of category  */
    function changeCategoryOrder(ele){
        var token = '{!!csrf_token()!!}';
        var id    = $(ele).attr('data-id');
        var order = $(ele).val();
        $.ajax(
        {
            url: "{{ url('admin-panel/sub-category/sub-category-order') }}",
            type: 'GET',
            data: {
                'category_id': id,
                'category_order': order,
            },
            success: function (res)
            {
                if (res == "Success")
                {
                    location.reload();
                } else
                {
                    location.reload();
                }
            }
        });
    }


    /* for IQ status change in category  */
    function changeStatusCategory(id,status){
        var token = '{!!csrf_token()!!}';
        $.ajax(
        {
            url: "{{ url('admin-panel/sub-category/sub-category-status') }}",
            type: 'GET',
            data: {
                'category_id': id,
                'category_status': status,
                'category_type':'iq',
            },
            success: function (res)
            {
                if (res == "Success")
                {
                    location.reload();
                } else
                {
                    location.reload();
                }
            }
        });
    }

$(function(){
    $(".select2").select2();
        $(".tch").TouchSpin({
        min: 0,
        max: 1000000000,
    });
});
</script>


@endsection




