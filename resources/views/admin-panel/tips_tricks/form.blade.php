<div class="row">
	<div>
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="input-group">
                <label for="{!! trans('language.category_name') !!}">
                    {!! trans('language.category_name') !!}
                </label>
                <select class="form-control select2 " id="trick_id" onchange="getSubcategories(this.value)" name="trick_cat_id" >
                    <option value="">Select Category</option>
                    @foreach ($categories as  $category)
                        <option value="{!! $category->iq_category_id !!}" {{ $tricks->tip_cat==$category->iq_category_id ? 'selected' : '' }} >
                            {!! $category->iq_category_name !!}
                        </option>   
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="input-group">
                <label for="{!! trans('language.subcategory_name') !!}">
                    {!! trans('language.subcategory_name') !!} 
                </label>
                <select class="form-control select2 " id="trick_sub_id"  name="trick_sub_id" >
                    <option value="">Select Sub Category</option>
                    @foreach ($subCategories as  $subCategory)
                        <option value="{!! $subCategory->sub_category_id !!}" {{ $tricks->tip_subcat==$subCategory->sub_category_id ? 'selected' : '' }} >
                            {!! $subCategory->title !!}
                        </option>   
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="input-group">
                <br>
                <div class="checkbox checkbox-info ">
                    <input id="is_new" type="checkbox" name="is_new" value="1" {{ $tricks->is_new==1 ? 'checked' : '' }} >
                    <label for="is_new">Check if your trick has new.</label>
                </div>
            </div>
        </div>
		<div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
			<div class="input-group">
				<label for="{!! trans('language.tips_tricks') !!}">
					{{ trans('language.tips_tricks') }}
				</label>
				{{ Form::text('title',old('title',isset($tricks['tip_title']) ? $tricks['tip_title']: ''),["class"=>"form-control","placeholder"=>trans('language.tricks_title')]) }}
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="white-box">
                <label for="input-file-disable-remove">
                    {{ trans('language.trick_img') }}
                </label>
                @php
                    if(!empty($tricks['tip_image'])){
                        $trickImage = url('uploads/'.$tricks['tip_image']);
                    }else{
                        $trickImage = url("/public/admin/nopreview-available.jpg");
                    } 
                @endphp
                <input type="file" name="trick_image" id="input-file-disable-remove" class="dropify" data-show-remove="false" data-default-file="{!! $trickImage !!}"/> 
            </div>
        </div>
        <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
            <div class="input-group">
                <label for="{!! trans('language.video_des') !!}">
                    {{ trans('language.trick_des') }}
                </label>
                {{ Form::textarea('trick_des',old('trick_des',isset($tricks['tip_desc']) ? $tricks['tip_desc']: ''),["class"=>"form-control","placeholder"=>trans('language.trick_des')]) }}
            </div>
        </div>
	</div>
</div>
<div class="row">
	<div class=" col-sm-9">
	    <button type="submit" name="submit" class="btn btn-info waves-effect waves-light m-t-10">{{  $submit_button }}</button>
	</div>
</div>
<div class="clearfix"></div>