<tr>
    <th>Views</th>
    <th>Likes</th>
    <th>Comments</th>
    </tr>
<tr>
    <?php
        $id = get_encrypted_value($detail->iq_ques_ans_id,true);
    ?>
    <td>{{ count($detail['getViews']) }}</td>
    <td>{{ count($detail['getLikes']) }}</td>
    <td><a href="{{ url('admin-panel/view-all-comments/interview/'.$id) }}">{{ count($detail['getComment']) }}</a></td>
</tr>