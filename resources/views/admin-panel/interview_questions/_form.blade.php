<div class="row">
    <div class="col-sm-12">
        
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="input-group">
                <br>
                <div class="radio radio-info ">
                    <input id="interview_question" type="radio" name="question_type" value="1" {{ isset($interviewQues->question_type) ? $interviewQues->question_type=='1' ? 'checked' : '' : 'checked' }} >
                    <label for="interview_question"> {!! trans('language.interview_title') !!}</label>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input id="quiz_question" type="radio" name="question_type" value="2" {{ $interviewQues->question_type==2 ? 'checked' : '' }} >
                    <label for="quiz_question"> {!! trans('language.quiz_title') !!}</label>
                </div>
            </div>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="input-group">
                <br>
               <div class="checkbox checkbox-info checkbox-circle">
                    
                    <input id="is_new" type="checkbox" name="is_new" value="1" {{ $interviewQues->is_new==1 ? 'checked' : '' }} >
                    <label for="is_new"> Question New or Not </label>
                </div>
            </div>
        </div>
<br><br><br>
        <div class="clearfix"></div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="input-group">
                <label for="{!! trans('language.category_name') !!}">
                    {!! trans('language.category_name') !!}
                </label>

                <select class="form-control select2 " id="category_id" onchange="getSubcategories(this.value)" name="category_id" >
                    <option value="">Select Category</option>
                    @foreach ($categories as  $category)
                        <option value="{!! $category->iq_category_id !!}" {{ $interviewQues->iq_ques_ans_catid==$category->iq_category_id ? 'selected' : '' }} >
                            {!! $category->iq_category_name !!}
                        </option>   
                    @endforeach
                </select>
            </div>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="input-group">
                <label for="{!! trans('language.subcategory_name') !!}">
                    {!! trans('language.subcategory_name') !!}
                </label>

                <select class="form-control select2 " id="sub_category_id"  name="sub_category_id" >
                    <option value="">Select Sub Category</option>
                    @foreach ($subCategories as  $subCategory)
                        <option value="{!! $subCategory->sub_category_id !!}" {{ $interviewQues->sub_cat_id==$subCategory->sub_category_id ? 'selected' : '' }} >
                            {!! $subCategory->title !!}
                        </option>   
                    @endforeach
                </select>
            </div>
        </div>
        <div class="clearfix"></div>


        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="input-group">
                <label for="{!! trans('language.question_title') !!}">
                    {!! trans('language.question_title') !!}
                </label>

                {!! Form::textarea('iq_ques_ans_q',isset($interviewQues['iq_ques_ans_q']) ? $interviewQues['iq_ques_ans_q']: '', ['class' => 'form-control','placeholder'=>trans('language.question_title'),'rows'=>'20','id'=>'iq_ques_ans_q']) !!}

                @if($errors->has('iq_ques_ans_q'))
                    <p class="help-block">
                        {{ $errors->first('iq_ques_ans_q') }}
                    </p>
                @endif
            </div>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="input-group">
                <label for="{!! trans('language.question_code') !!}">
                    {!! trans('language.question_code') !!}
                </label>

                {!! Form::textarea('question_coding',isset($interviewQues['question_coding']) ? $interviewQues['question_coding']: '', ['class' => 'form-control','placeholder'=>trans('language.question_code'),'rows'=>'20','id'=>'q_code']) !!}

                @if($errors->has('question_coding'))
                    <p class="help-block">
                        {{ $errors->first('question_coding') }}
                    </p>
                @endif
            </div>
        </div>


        <div class="clearfix"></div>


        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="input-group">
                <label for="{!! trans('language.answer_title') !!}">
                    {!! trans('language.answer_title') !!}
                </label>

                {!! Form::textarea('iq_ques_ans_a',isset($interviewQues['iq_ques_ans_a']) ? $interviewQues['iq_ques_ans_a']: '', ['class' => 'form-control','placeholder'=>trans('language.answer_title'),'rows'=>'20','id'=>'iq_ques_ans_a']) !!}

                @if($errors->has('iq_ques_ans_a'))
                    <p class="help-block">
                        {{ $errors->first('iq_ques_ans_a') }}
                    </p>
                @endif
            </div>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="input-group">
                <label for="{!! trans('language.answer_code') !!}">
                    {!! trans('language.answer_code') !!}
                </label>

                {!! Form::textarea('coding',isset($interviewQues['coding']) ? $interviewQues['coding']: '', ['class' => 'form-control ','placeholder'=>trans('language.answer_code'),'rows'=>'20','id'=>'a_code']) !!}

                @if($errors->has('coding'))
                    <p class="help-block">
                        {{ $errors->first('coding') }}
                    </p>
                @endif
            </div>
        </div>

        <div class="clearfix"></div>


        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <div class="input-group">
                <label for="{!! trans('language.pages_meta_title') !!}">
                    {!! trans('language.pages_meta_title') !!}
                </label>

                {!! Form::textarea('iq_ques_ans_metatitle',isset($interviewQues['iq_ques_ans_metatitle']) ? $interviewQues['iq_ques_ans_metatitle']: '', ['class' => 'form-control ','placeholder'=>trans('language.pages_meta_title'),'rows'=>'5','id'=>'answer']) !!}

                @if($errors->has('iq_ques_ans_metatitle'))
                    <p class="help-block">
                        {{ $errors->first('iq_ques_ans_metatitle') }}
                    </p>
                @endif
            </div>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <div class="input-group">
                <label for="{!! trans('language.pages_meta_keyword') !!}">
                    {!! trans('language.pages_meta_keyword') !!}
                </label>

                {!! Form::textarea('iq_ques_ans_metakey',isset($interviewQues['iq_ques_ans_metakey']) ? $interviewQues['iq_ques_ans_metakey']: '', ['class' => 'form-control ','placeholder'=>trans('language.pages_meta_keyword'),'rows'=>'5','id'=>'a_code']) !!}

                @if($errors->has('iq_ques_ans_metakey'))
                    <p class="help-block">
                        {{ $errors->first('iq_ques_ans_metakey') }}
                    </p>
                @endif
            </div>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <div class="input-group">
                <label for="{!! trans('language.pages_meta_description') !!}">
                    {!! trans('language.pages_meta_description') !!}
                </label>

                {!! Form::textarea('iq_ques_ans_metadesc',isset($interviewQues['iq_ques_ans_metadesc']) ? $interviewQues['iq_ques_ans_metadesc']: '', ['class' => 'form-control ','placeholder'=>trans('language.pages_meta_description'),'rows'=>'5','id'=>'a_code']) !!}

                @if($errors->has('iq_ques_ans_metadesc'))
                    <p class="help-block">
                        {{ $errors->first('iq_ques_ans_metadesc') }}
                    </p>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="row">
	<div class=" col-sm-9">
	    <button type="submit" name="submit" class="btn btn-info waves-effect waves-light m-t-10">{{  $submit_button }}</button>
	</div>
</div>
<div class="clearfix"></div>

