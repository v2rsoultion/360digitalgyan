<div class="row">
    <div class="col-sm-12">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="input-group">
                 <label for="{!! trans('language.subcategory_name') !!}">
                    {!! trans('language.category_name') !!}
                </label>

                <select class="form-control select2 " id="category_id"  name="news_category_id" >
                    <option value="">Select Category</option>
                    @foreach ($newsCategories as  $category)
                        <option value="{!! $category->news_category_id !!}" {{ $news->news_category_id==$category->news_category_id ? 'selected' : '' }} >
                            {!! $category->news_category_name !!}
                        </option>   
                    @endforeach
                </select>
            </div>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="input-group">
                <label for="{!! trans('language.news_title') !!}">
                    {!! trans('language.news_title') !!}
                </label>

                {!! Form::text('news_title',isset($news['news_title']) ? $news['news_title']: '', ['class' => 'form-control','placeholder'=>trans('language.news_title')]) !!}

                @if($errors->has('news_title'))
                    <p class="help-block">
                        {{ $errors->first('news_title') }}
                    </p>
                @endif
            </div>
        </div>

        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
            <div class="input-group">
                <label for="{!! trans('language.news_description') !!}">
                    {!! trans('language.news_description') !!}
                </label>

                {!! Form::textarea('news_description',isset($news['news_description']) ? $news['news_description']: '', ['class' => 'form-control tinytext','placeholder'=>trans('language.news_description'),'rows'=>'23']) !!}

                @if($errors->has('news_description'))
                    <p class="help-block">
                        {{ $errors->first('news_description') }}
                    </p>
                @endif
            </div>
        </div>

        <div class="col-md-4 col-xs-12">
            <div class="white-box">
                <label for="input-file-disable-remove">News Image</label>

                @php
                    if(!empty($news['news_image'])){
                        $newsImage = url('uploads/'.$news['news_image']);
                    }else{
                        $newsImage = url("/public/admin/nopreview-available.jpg");
                    } 
                @endphp
                
                <input type="file" name="news_image" id="input-file-disable-remove" class="dropify" data-show-remove="false" data-default-file="{!! $newsImage !!}"/> 
            </div>

            <div class="white-box">
                <label for="input-file-disable-remove">News Thumbnail</label>

                @php
                    if(!empty($news['news_thumbnail'])){
                        $newsImage = url('uploads/'.$news['news_thumbnail']);
                    }else{
                        $newsImage = url("/public/admin/nopreview-available.jpg");
                    } 
                @endphp
                
                <input type="file" name="news_thumbnail" id="input-file-disable-remove" class="dropify" data-show-remove="false" data-default-file="{!! $newsImage !!}"/> 
            </div>
        </div>


    </div>
</div>
<div class="row">
	<div class=" col-sm-9">
	    <button type="submit" name="submit" class="btn btn-info waves-effect waves-light m-t-10">{{  $submit_button }}</button>
	</div>
</div>
<div class="clearfix"></div>