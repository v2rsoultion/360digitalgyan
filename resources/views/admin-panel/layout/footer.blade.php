    <!-- /.container-fluid -->
    <footer class="footer text-center"> {{ now()->year }} &copy; {!!trans('language.project_tagline') !!} </footer>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
</div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <!-- Bootstrap Core JavaScript -->
    {!! Html::script('public/admin/bootstrap/dist/js/bootstrap.min.js') !!}
    <!-- Menu Plugin JavaScript -->
    {!! Html::script('public/admin/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js') !!}
    <!--slimscroll JavaScript -->
    {!! Html::script('public/admin/js/jquery.slimscroll.js') !!}
    <!--Wave Effects -->
    {!! Html::script('public/admin/js/waves.js') !!}

    {!! Html::script('public/admin/js/cbpFWTabs.js') !!}
    <!-- chartist chart -->
    {!! Html::script('public/admin/plugins/bower_components/chartist-js/dist/chartist.min.js') !!}
    {!! Html::script('public/admin/plugins/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js') !!}
    <!-- Sparkline chart JavaScript -->
    {!! Html::script('public/admin/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js') !!}
    <!-- Custom Theme JavaScript -->
    {!! Html::script('public/admin/js/custom.min.js') !!}
    {!! Html::script('public/admin/js/dashboard1.js') !!}
    {!! Html::script('public/admin/plugins/bower_components/toast-master/js/jquery.toast.js') !!}
    <!--Style Switcher -->
    {!! Html::script('public/admin/plugins/bower_components/styleswitcher/jQuery.style.switcher.js') !!}

    {!! Html::script('public/admin/plugins/bower_components/custom-select/dist/js/select2.full.min.js') !!}
    {!! Html::script('public/admin/plugins/bower_components/bootstrap-select/bootstrap-select.min.js') !!}
    {!! Html::script('public/admin/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js') !!}
    {!! Html::script('public/admin/plugins/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js') !!}
    {!! Html::script('public/admin/plugins/bower_components/multiselect/js/jquery.multi-select.js') !!}



    {!! Html::script('public/admin/download/dataTables.buttons.min.js') !!}
    {!! Html::script('public/admin/download/buttons.flash.min.js') !!}
    {!! Html::script('public/admin/download/jszip.min.js') !!}
    {!! Html::script('public/admin/download/pdfmake.min.js') !!}
    {!! Html::script('public/admin/download/vfs_fonts.js') !!}
    {!! Html::script('public/admin/download/buttons.html5.min.js') !!}
    {!! Html::script('public/admin/download/buttons.print.min.js') !!}

    
    {!! Html::script('public/admin/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.js') !!}
    
    {!! Html::script('public/admin/plugins/bower_components/moment/moment.js') !!}
    {!! Html::script('public/admin/plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') !!}
    {!! Html::script('public/admin/download/jquery.validate.min.js') !!}
    {!! Html::script('public/admin/plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') !!}
    {!! Html::script('public/admin/download/jquery.validate.min.js') !!}
    {!! Html::script('public/admin/plugins/bower_components/html5-editor/wysihtml5-0.3.0.js') !!}
    {!! Html::script('public/admin/plugins/bower_components/html5-editor/bootstrap-wysihtml5.js') !!}
    {!! Html::script('public/admin/plugins/bower_components/datatables/datatables.min.js') !!}
    {!! Html::script('public/admin/js/bootstrapvalidator.min.js') !!}
    {!! Html::script('public/admin/plugins/bower_components/dropify/dist/js/dropify.min.js') !!}
    {!! Html::script('public/admin/plugins/bower_components/sweetalert/sweetalert.min.js') !!}


    <script>
        $(document).ready(function(){
            $("#nava-toggle").click(function(){
                $("#side-menu li").hide();
            });
        });
    </script>

   <script>
  
    $(function() {
        $('#myTable').DataTable();
        $(document).ready(function() {
            var table = $('#example').DataTable({
               
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                
            });
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function() {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
        });
        
    });
    $(document).ready(function() {
        $('.textarea_editor').wysihtml5();
        $('.textarea_editor1').wysihtml5();
    });
    </script>
    <script type="text/javascript">
    (function() {
        [].slice.call(document.querySelectorAll('.sttabs')).forEach(function(el) {
            new CBPFWTabs(el);
        });
    })();
    </script>
<script src="https://cloud.tinymce.com/5/tinymce.min.js?apiKey=jym5lvhhm60hx7q9wllxshr5092e2h7exhe39hyrnb3k751c"></script>
<script type="text/javascript">

    var editor_config = {
        path_absolute : "/",
        selector: "textarea.tinytext",
        plugins: [
            "print preview fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern help "
        ],
        toolbar: "formatselect | bold italic strikethrough forecolor backcolor permanentpen | link image media | alignleft aligncenter alignright alignjustify | numlist bullist outdent indent | removeformat | addcomment",
        relative_urls: false,
        file_browser_callback : function(field_name, url, type, win) {
            var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
            var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

            var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
            if (type == 'image') {
                cmsURL = cmsURL + "&type=Images";
            } else {
                cmsURL = cmsURL + "&type=Files";
            }

            tinyMCE.activeEditor.windowManager.open({
                file : cmsURL,
                title : 'Filemanager',
                // width : x * 0.8,
                // height : y * 0.8,
                resizable : "yes",
                close_previous : "no"
            });
        }
    };
    tinymce.init(editor_config);
</script>
</body>
</html>