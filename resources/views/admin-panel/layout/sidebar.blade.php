<!-- ============================================================== -->
    <!-- Preloader -->
    <!-- ============================================================== -->
<?php $login_info = get_loggedin_user_data(); ?>

    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
        </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Wrapper -->
    <!-- ============================================================== -->
    <div id="wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <nav class="navbar navbar-default navbar-static-top m-b-0">
            <div class="navbar-header">
                <div class="top-left-part">
                    <!-- Logo -->
                    <a class="logo" href="{{ url('/admin-panel/dashboard') }}">
                        <b>
                            <img src="{!! URL::to('public/admin/plugins/images/email_logo.png') !!}" alt="home" class="dark-logo" />
                         </b>
                        <span class="hidden-xs">
                            <img src="{!! URL::to('public/admin/plugins/images/admin-text.png') !!}" alt="home" class="dark-logo" />
                            <img src="{!! URL::to('public/admin/plugins/images/email_logo.png') !!}" alt="home" class="light-logo" width="70%" />
                        </span>
                    </a>
                </div>
                <ul class="nav navbar-top-links navbar-right pull-right">
                    
                    <li class="dropdown">
                        @php
                                        
                            if(!empty($login_info['admin_profile_img']) ){
                                $admin_pic = url("public/uploads/admin/profile-pic/".$login_info['admin_profile_img']);
                            }  else {
                                $admin_pic = url("public/admin/plugins/images/default.jpg");
                            } 
                        @endphp


                        <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#"> <img src="{!!  $admin_pic !!}" alt="user-img" width="36" class="img-circle"><b class="hidden-xs">{!! $login_info['admin_name'] !!}</b><span class="caret"></span> </a>
                        <ul class="dropdown-menu dropdown-user animated flipInY">
                            <li>
                                <div class="dw-user-box">
                                    <div class="u-img"><img src="{{$admin_pic}}" alt="user" /></div>
                                    <div class="u-text">
                                        <h4>{!! $login_info['admin_name'] !!}</h4>
                                        <p class="text-muted">{!! $login_info['email'] !!}</p><a href="{{ url('/admin-panel/profile') }}" class="btn btn-rounded btn-danger btn-sm">View Profile</a></div>
                                </div>
                            </li>
                            <li role="separator" class="divider"></li>
                            <li><a href="{{ url('/admin-panel/profile') }}"><i class="ti-user"></i> My Profile</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="{{ url('/admin-panel/change-password') }}"><i class="ti-settings"></i> Change Password</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="{{ url('/admin-panel/logout') }}" title="Logout"><i class="fa fa-power-off"></i> Logout</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>

        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav slimscrollsidebar">
                <div class="sidebar-head">
                    <h3><span class="fa-fw open-close"><i class="ti-close ti-menu"></i></span> <span class="hide-menu">Navigation</span></h3> 
                </div>

                <ul class="nav" id="side-menu">
                    <li style="margin-top: 60px;">
                        <a href="#" class="waves-effect @if(Request::segment(2) == 'category')  active  @endif"><i class="fa fa-list-alt fa-fw" data-icon="v"></i> <span class="hide-menu">Category <span class="fa arrow"></span> </span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="{{ url('/admin-panel/category/add-category') }}"><span class="hide-menu">Add Category</span></a> </li>
                            <li> <a href="{{ url('/admin-panel/category/view-categories') }}"><span class="hide-menu">View Categories</span></a> </li>
                        </ul>
                    </li>

                    <li>
                        <a href="#" class="waves-effect @if(Request::segment(2) == 'sub-category')  active  @endif"><i class="fa fa-list-alt fa-fw" data-icon="v"></i> <span class="hide-menu">Sub Category <span class="fa arrow"></span> </span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="{{ url('/admin-panel/sub-category/add-sub-category') }}"><span class="hide-menu">Add Sub Category</span></a> </li>
                            <li> <a href="{{ url('/admin-panel/sub-category/view-sub-categories') }}"><span class="hide-menu">View Sub Categories</span></a> </li>
                        </ul>
                    </li>

                    <li>
                     	  <a href="{{ url('/admin-panel/view-seo-users') }}" class="waves-effect @if(Request::segment(2) == 'view-seo-users')  active  @endif"><i class="fa fa-user fa-fw" data-icon="v"></i> <span class="hide-menu"> View Seo Users </span></a>
                     </li>

                    <li>
                        <a href="#" class="waves-effect @if(Request::segment(2) == 'news-category')  active  @endif"><i class="fa fa-list-alt fa-fw" data-icon="v"></i> <span class="hide-menu">News Category <span class="fa arrow"></span> </span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="{{ url('/admin-panel/news-category/add-news-category') }}"><span class="hide-menu">Add News Category</span></a> </li>
                            <li> <a href="{{ url('/admin-panel/news-category/view-news-categories') }}"><span class="hide-menu">View News Categories</span></a> </li>
                        </ul>
                    </li>

                    <li>
                        <a href="#" class="waves-effect @if(Request::segment(2) == 'news')  active  @endif"><i class="fa fa-newspaper fa-fw" data-icon="v"></i> <span class="hide-menu">News <span class="fa arrow"></span> </span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="{{ url('/admin-panel/news/add-news') }}"><span class="hide-menu">Add News</span></a> </li>
                            <li> <a href="{{ url('/admin-panel/news/view-news') }}"><span class="hide-menu">View News</span></a> </li>
                        </ul>
                    </li>

                    <li>
                        <a href="#" class="waves-effect @if(Request::segment(2) == 'marketing-blog')  active  @endif"><i class="fa fa-newspaper fa-fw" data-icon="v"></i> <span class="hide-menu">Marketing Blogs <span class="fa arrow"></span> </span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="{{ url('/admin-panel/marketing-blog/add-marketing-blog') }}"><span class="hide-menu">Add Marketing Blogs</span></a> </li>
                            <li> <a href="{{ url('/admin-panel/marketing-blog/view-marketing-blog') }}"><span class="hide-menu">View Marketing Blogs</span></a> </li>
                            <li> <a href="{{ url('/admin-panel/view-all-comments/blog') }}"><span class="hide-menu">View Marketing Blogs Comments</span></a> </li>
                        </ul>
                    </li>


                    <li>
                        <a href="#" class="waves-effect @if(Request::segment(2) == 'interview-questions')  active  @endif"><i class="fa fa-newspaper fa-fw" data-icon="v"></i> <span class="hide-menu">Interview Questions <span class="fa arrow"></span> </span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="{{ url('/admin-panel/interview-questions/add-interview-questions') }}"><span class="hide-menu">Add Interview Questions</span></a> </li>
                            <li> <a href="{{ url('/admin-panel/interview-questions/view-interview-questions') }}"><span class="hide-menu">View Interview Questions</span></a> </li>
                            <li> <a href="{{ url('/admin-panel/view-all-comments/interview') }}"><span class="hide-menu">View Interview Comments</span></a> </li>
                        </ul>
                    </li>


                    <li>
                        <a href="#" class="waves-effect @if(Request::segment(2) == 'notification')  active  @endif"><i class="fa fa-newspaper fa-fw" data-icon="v"></i> <span class="hide-menu">Notifications <span class="fa arrow"></span> </span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="{{ url('/admin-panel/notification/add-notification') }}"><span class="hide-menu">Add Notification</span></a> </li>
                            <li> <a href="{{ url('/admin-panel/notification/view-notification') }}"><span class="hide-menu">View Notifications</span></a> </li>
                        </ul>
                    </li>

                    <li>
                        <a href="#" class="waves-effect @if(Request::segment(2) == 'quiz-questions')  active  @endif"><i class="fa fa-newspaper fa-fw" data-icon="v"></i> <span class="hide-menu">Quiz Questions <span class="fa arrow"></span> </span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="{{ url('/admin-panel/quiz-questions/add-quiz-questions') }}"><span class="hide-menu">Add Quiz Questions</span></a> </li>
                            <li> <a href="{{ url('/admin-panel/quiz-questions/view-quiz-questions') }}"><span class="hide-menu">View Quiz Questions</span></a> </li>
                        </ul>
                    </li>


                    <li>
                        <a href="#" class="waves-effect @if(Request::segment(2) == 'practice-quiz-questions')  active  @endif"><i class="fa fa-newspaper fa-fw" data-icon="v"></i> <span class="hide-menu">Practice Quiz Questions <span class="fa arrow"></span> </span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="{{ url('/admin-panel/practice-quiz-questions/add-practice-questions') }}"><span class="hide-menu">Add Practice Quiz Questions</span></a> </li>
                            <li> <a href="{{ url('/admin-panel/practice-quiz-questions/view-practice-questions') }}"><span class="hide-menu">View Practice Quiz Questions</span></a> </li>
                        </ul>
                    </li>

                    <li>
                        <a href="#" class="waves-effect @if(Request::segment(2) == 'videos')  active  @endif"><i class="fa fa-newspaper fa-fw" data-icon="v"></i> <span class="hide-menu">Videos <span class="fa arrow"></span> </span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="{{ url('/admin-panel/videos/add-videos') }}"><span class="hide-menu">Add video</span></a> </li>
                            <li> <a href="{{ url('/admin-panel/videos/view-videos') }}"><span class="hide-menu">View video</span></a> </li>
                        </ul>
                    </li>

                    <li>
                        <a href="#" class="waves-effect @if(Request::segment(2) == 'testimonials')  active  @endif"><i class="fa fa-newspaper fa-fw" data-icon="v"></i> <span class="hide-menu">Testimonials<span class="fa arrow"></span> </span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="{{ url('/admin-panel/testimonials/add-testimonials') }}"><span class="hide-menu">Add Testimonials</span></a> </li>
                            <li> <a href="{{ url('/admin-panel/testimonials/view-testimonials') }}"><span class="hide-menu">View Testimonials</span></a> </li>
                        </ul>
                    </li>

                    <li>
                        <a href="#" class="waves-effect @if(Request::segment(2) == 'tags')  active  @endif"><i class="fa fa-newspaper fa-fw" data-icon="v"></i> <span class="hide-menu">Tags<span class="fa arrow"></span> </span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="{{ url('/admin-panel/tags/add-tag') }}"><span class="hide-menu">Add Tag</span></a> </li>
                            <li> <a href="{{ url('/admin-panel/tags/view-tags') }}"><span class="hide-menu">View Tags</span></a> </li>
                        </ul>
                    </li>

                    <li>
                        <a href="#" class="waves-effect @if(Request::segment(2) == 'tips-tricks')  active  @endif"><i class="fa fa-newspaper fa-fw" data-icon="v"></i> <span class="hide-menu">Tips Tricks<span class="fa arrow"></span> </span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="{{ url('/admin-panel/tips-tricks/add-tricks') }}"><span class="hide-menu">Add Tricks</span></a> </li>
                            <li> <a href="{{ url('/admin-panel/tips-tricks/view-tricks') }}"><span class="hide-menu">View Tricks</span></a> </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" class="waves-effect @if(Request::segment(2) == 'terms')  active  @endif"><i class="fa fa-newspaper fa-fw" data-icon="v"></i> <span class="hide-menu">Terms<span class="fa arrow"></span> </span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="{{ url('/admin-panel/terms/add-terms') }}"><span class="hide-menu">Add Terms</span></a> </li>
                            <li> <a href="{{ url('/admin-panel/terms/view-terms') }}"><span class="hide-menu">View Terms</span></a> </li>
                        </ul>
                    </li>

                    <li>
                        <a href="#" class="waves-effect @if(Request::segment(2) == 'greetings')  active  @endif"><i class="fa fa-newspaper fa-fw" data-icon="v"></i> <span class="hide-menu">Greeting Image<span class="fa arrow"></span> </span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="{{ url('/admin-panel/greetings/add-greetings') }}"><span class="hide-menu">Add Greeting</span></a> </li>
                            <li> <a href="{{ url('/admin-panel/greetings/view-greetings') }}"><span class="hide-menu">View Greetings</span></a> </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" class="waves-effect @if(Request::segment(2) == 'posted-ques-ans')  active  @endif"><i class="fa fa-newspaper fa-fw" data-icon="v"></i> <span class="hide-menu">Posted Question/Answer<span class="fa arrow"></span> </span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="{{ url('/admin-panel/posted-ques-ans/add-posted-ques') }}"><span class="hide-menu">Add Posted Ques</span></a> </li>
                            <li> <a href="{{ url('/admin-panel/posted-ques-ans/view-posted-ques') }}"><span class="hide-menu">View Posted Ques</span></a> </li>
                            <li> <a href="{{ url('/admin-panel/posted-ques-ans/view-posted-ans') }}"><span class="hide-menu">View Posted Answer</span></a> </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" class="waves-effect @if(Request::segment(2) == 'study-material')  active  @endif"><i class="fa fa-newspaper fa-fw" data-icon="v"></i> <span class="hide-menu">Study Material<span class="fa arrow"></span> </span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="{{ url('/admin-panel/study-material/add-study-material') }}"><span class="hide-menu">Add Study Material</span></a> </li>
                            <li> <a href="{{ url('/admin-panel/study-material/view-study-material') }}"><span class="hide-menu">View Study Material</span></a> </li>
                            <li> <a href="{{ url('/admin-panel/view-all-comments/study') }}"><span class="hide-menu">View Study Comments</span></a> </li>
                        </ul>
                    </li>

                    <li>
                        <a href="#" class="waves-effect @if(Request::segment(2) == 'share-banner')  active  @endif"><i class="fa fa-newspaper fa-fw" data-icon="v"></i> <span class="hide-menu">Share Banner<span class="fa arrow"></span> </span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="{{ url('/admin-panel/share-banner/add-banner') }}"><span class="hide-menu">Add Share Banner</span></a> </li>
                            <li> <a href="{{ url('/admin-panel/share-banner/view-banner') }}"><span class="hide-menu">View Share Banner</span></a> </li>
                        </ul>
                    </li>

                    <li>
                        <a href="{{ url('/admin-panel/seo-results/view-seo-results') }}" class="waves-effect @if(Request::segment(2) == 'seo-results')  active  @endif"><i class="fa fa-newspaper fa-fw" data-icon="v"></i> <span class="hide-menu"> View Seo Results </span></a>
                     </li>

                     <li>
                        <a href="{{ url('/admin-panel/seo-suggestion/view-seo-suggestion') }}" class="waves-effect @if(Request::segment(2) == 'seo-suggestion')  active  @endif"><i class="fa fa-newspaper fa-fw" data-icon="v"></i> <span class="hide-menu"> View Seo Suggestion </span></a>
                     </li>

                    <!-- <li>

                        <a href="#" class="waves-effect @if(Request::segment(1) == 'pages')  active  @endif"><i class="fa fa-book " style="padding: 5px;"></i> <span class="hide-menu">Master Pages <span class="fa arrow"></span> </span></a>
                        <ul class="nav nav-second-level">
                        @php 
                           // $pages=masterpages();
                        @endphp
                            @foreach ($pages as $page)
                          
                            @php
                               // $pageen=get_encrypted_value($page->master_pages_id,true);
                            @endphp
                        

                            <li> <a href="{{ url('/admin-panel/pages/'.$pageen) }}"><span class="hide-menu">{{ $page->pages_title}}</span></a> </li>
                          @endforeach
                        </ul>
                    </li> -->
                     
                    


        </ul>
                <!-- Sidebar end -->
    </div>
</div>