<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="{!! URL::to('public/admin/plugins/images/favicon.png') !!}">
    <title>{!! $page_title !!} | {!!trans('language.project_title') !!}</title>
    <!-- Bootstrap Core CSS -->
    {!! Html::style('public/admin/bootstrap/dist/css/bootstrap.min.css') !!}

    <script src="https://cdn.ckeditor.com/4.11.3/standard/ckeditor.js"></script>
    <!-- Menu CSS -->
    {!! Html::style('public/admin/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css') !!}
    <!-- toast CSS -->
    {!! Html::style('public/admin/plugins/bower_components/toast-master/css/jquery.toast.css') !!}
    <!-- morris CSS -->
    {!! Html::style('public/admin/plugins/bower_components/morrisjs/morris.css') !!}
    <!-- chartist CSS -->
    {!! Html::style('public/admin/plugins/bower_components/chartist-js/dist/chartist.min.css') !!}
    {!! Html::style('public/admin/plugins/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css') !!}
    <!-- Calendar CSS -->
    {!! Html::style('public/admin/plugins/bower_components/calendar/dist/fullcalendar.css') !!}
    <!-- animation CSS -->
    {!! Html::style('public/admin/css/animate.css') !!}
    <!-- Custom CSS -->
    {!! Html::style('public/admin/css/style.css') !!}
    {!! Html::style('public/admin/css/custom.css') !!}
    <!-- color CSS -->
    {!! Html::style('public/admin/css/colors/default.css') !!}

    {!! Html::style('public/admin/plugins/bower_components/custom-select/dist/css/select2.min.css') !!}
    {!! Html::style('public/admin/plugins/bower_components/switchery/dist/switchery.min.css') !!}
    {!! Html::style('public/admin/plugins/bower_components/bootstrap-select/bootstrap-select.min.css') !!}
    {!! Html::style('public/admin/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') !!}
    {!! Html::style('public/admin/plugins/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css') !!}
    {!! Html::style('public/admin/plugins/bower_components/multiselect/css/multi-select.css') !!}


    {!! Html::style('public/admin/plugins/bower_components/html5-editor/bootstrap-wysihtml5.css') !!}

    {!! Html::style('public/admin/plugins/bower_components/sweetalert/sweetalert.css') !!}





    {!! Html::style('public/admin/plugins/bower_components/datatables/media/css/dataTables.bootstrap.css') !!}
    {!! Html::style('public/admin/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css') !!}
    {!! Html::style('public/admin/plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css') !!}
    {!! Html::style('public/admin/css/bootstrapValidator.min.css') !!}
    {!! Html::style('public/admin/plugins/bower_components/dropify/dist/css/dropify.min.css') !!}

    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->


    {!! Html::script('public/admin/plugins/bower_components/jquery/dist/jquery.min.js') !!}

    <script src="http://cloud.github.com/downloads/wycats/handlebars.js/handlebars-1.0.0.beta.6.js" type="text/javascript" charset="utf-8"></script>
    <script src="public/admin/test/app.js" type="text/javascript" charset="utf-8"></script>

</head>

<body class="fix-header">
@extends('admin-panel.layout.sidebar')
@yield('content')
@extends('admin-panel.layout.footer')