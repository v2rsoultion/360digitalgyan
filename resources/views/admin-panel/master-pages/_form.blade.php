

{!! Form::hidden('master_pages_id',old('master_pages_id',isset($masterPages['master_pages_id']) ? $masterPages['master_pages_id'] : ''),['class' => 'gui-input', 'id' => 'master_pages_id', 'readonly' => 'true']) !!}

@if($errors->any())
<p class="text-muted m-b-10 col-lg-12 col-md-12 col-sm-12 col-xs-12 font-13 red">{{$errors->first()}}</p>
@endif
<!-- <h3 class="box-title m-b-0 col-lg-12 col-md-12 col-sm-12 col-xs-12">Category</h3>
<p class="text-muted m-b-10 col-lg-12 col-md-12 col-sm-12 col-xs-12 font-13"> Enter new {!! trans('language.category') !!} </p> -->
<div class="row">
  <div class="col-md-8">
    <div class="row">
        <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
            <div class="input-group">
                <label for="{!! trans('language.pages_title') !!}">{!! trans('language.pages_title') !!}</label>
                {!! Form::text('pages_title', old('pages_title',isset($masterPages['pages_title']) ? $masterPages['pages_title']: ''), ['class' => 'form-control','placeholder'=>trans('language.page_title'), 'id' => 'pages_title','for'=>trans('language.pages_title')]) !!}

                @if($errors->has('title')) <p class="help-block">{{ $errors->first('pages_title') }}</p> @endif
            </div>
        </div>    

          <div class="col-xs-12">
            <div class="input-group">
                <label for="{!! trans('language.faq_question') !!}">{!! trans('language.pages_short_desc') !!}</label>

                {!! Form::textarea('pages_short_desc', old('title',isset($masterPages['pages_short_desc']) ? $masterPages['pages_short_desc']: ''), ['class' => 'textarea_editor form-control textareaheight1','placeholder'=>trans('language.pages_short_desc'), 'id' => 'title','for'=>trans('language.pages_short_desc')]) !!}

           </div>  
        </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="white-box" style="margin-top: 75px;margin-bottom: 0px;">
            <label for="input-file-disable-remove">Image</label>

                    @php
                        if( !empty($masterPages['image']) ){
                            $image = url("public/uploads/admin/pageImages/".$masterPages['image']);
                        }  else {
                            $image = url("public/admin/plugins/images/default.jpg");
                        } 
                    @endphp
                    
                    <input type="file" name="image" id="input-file-disable-remove" class="dropify" data-show-remove="false" data-default-file="{!! $image !!}"/> 
        </div>
    </div>
</div>
<div class="row" >
      <div class="col-xs-12">
         <div class="input-group">
            <label for="{!! trans('language.pages_long_desc') !!}">{!! trans('language.pages_long_desc') !!}</label>
             {!! Form::textarea('pages_long_desc', old('title',isset($masterPages['pages_long_desc']) ? $masterPages['pages_long_desc']: ''), ['class' => 'textarea_editor1 form-control textareaheight','placeholder'=>trans('language.pages_long_desc'), 'id' => 'title','for'=>trans('language.pages_long_desc')]) !!}

             
         </div>
    </div>
</div>

<div class="row" >
      <div class="col-xs-6">
         <div class="input-group">
            <label for="{!! trans('language.pages_meta_title') !!}">{!! trans('language.pages_meta_title') !!}</label>
             {!! Form::textarea('pages_meta_title', old('title',isset($masterPages['pages_meta_title']) ? $masterPages['pages_meta_title']: ''), ['class' => 'textarea_editor1 form-control textareaheight1','placeholder'=>trans('language.pages_meta_title'), 'id' => 'title','for'=>trans('language.pages_meta_title')]) !!}

             
         </div>
    </div>
      <div class="col-xs-6">
         <div class="input-group">
            <label for="{!! trans('language.pages_meta_description') !!}">{!! trans('language.pages_meta_description') !!}</label>
             {!! Form::textarea('pages_meta_description', old('title',isset($masterPages['pages_meta_description']) ? $masterPages['pages_meta_description']: ''), ['class' => 'textarea_editor1 form-control textareaheight1','placeholder'=>trans('language.pages_meta_description'), 'id' => 'title','for'=>trans('language.pages_meta_description')]) !!}

             
         </div>
    </div>
</div>
<div class="row" >
      <div class="col-xs-12">
             <div class="input-group">
            <label for="{!! trans('language.pages_meta_keyword') !!}">{!! trans('language.pages_meta_keyword') !!}</label>
             {!! Form::textarea('pages_meta_keyword', old('title',isset($masterPages['pages_meta_keyword']) ? $masterPages['pages_meta_keyword']: ''), ['class' => 'textarea_editor1 form-control textareaheight1','placeholder'=>trans('language.pages_long_desc'), 'id' => 'title','for'=>trans('language.pages_meta_keyword')]) !!}

             
         </div>
      </div>
</div>        
<div class="row" >
    <div class=" col-sm-9">
        <button type="submit" name="submit" class="btn btn-info waves-effect waves-light m-t-10">Submit</button>
    </div>
</div>

<div class="clearfix"></div>
<script type="text/javascript">

    
    jQuery(document).ready(function () {

        jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z\s]+$/i.test(value);
        }, "Only alphabetical characters");

        $("#master-page-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
           

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                
                page_title: {
                    required: true,
                }
               
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });
        
    });

    

    $(document).ready(function() {
        // Basic
        $('.dropify').dropify();
        // Translated
        $('.dropify-fr').dropify({
            messages: {
                default: 'Drag and drop a file here or click',
                replace: 'Drag and drop or click to replace',
                remove: 'Remove',
                error: 'Ooops, something wrong happended.'
            }
        });
        // Used events
        var drEvent = $('#input-file-events').dropify();
        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });
        drEvent.on('dropify.afterClear', function(event, element) {
            alert('File deleted');
        });
        drEvent.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });
        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        })
    });


</script>