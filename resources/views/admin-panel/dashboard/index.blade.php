@extends('admin-panel.layout.header')

@section('content')


        <!-- Page Content -->
        <!-- ============================================================== -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">{{trans('language.dashboard')}}</h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                       
                        <ol class="breadcrumb">
                            <li class="active">{{trans('language.dashboard')}}</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- Different data widgets -->
                <!-- ============================================================== -->
                <!-- .row -->
                <div class="row">
                    <!-- <div class="col-md-12"><h2>Today</h2></div> -->
                    <div class="col-lg-3 col-sm-6 col-xs-12">
                        <div class="white-box">
                            <h3 class="box-title">Seo Users</h3>
                            <ul class="list-inline m-t-30 p-t-10 two-part">
                                <li><i class="icon-people text-info"></i></li>
                                <li class="text-right"><h2 class="counter">25982</h2></li>
                            </ul>
                        </div>
                    </div>

<!--                     <div class="col-lg-3 col-sm-6 col-xs-12 text-center">
                        <a href="{{url('admin-panel/view-customer/1')}}"> <div class="white-box analytics-info">
                            <h3 class="box-title">Seo Users</h3>
                            <i class="fa fa-user fa-2x  fain" aria-hidden="true"></i>
                            <div class="fa-2x">
                                <h2 class="counter"></h2>
                            </div>    
                        </div></a>
                    </div> -->
                    <div class="col-lg-3 col-sm-6 col-xs-12 text-center">
                        <a href="{{url('#')}}">
                            <div class="white-box analytics-info">
                                <h3 class="box-title">Subscribers</h3>
                                <i class="fas fa-file fa-2x fain"></i>
                                <h2 class="counter ">25968</h2>
                               
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-3 col-sm-6 col-xs-12 text-center">
                        <a href="{{url('#')}}">
                            <div class="white-box analytics-info">
                                <h3 class="box-title">Blogs</h3>
                                <i class="fas fa-file fa-2x fain"></i>
                                 <h2 class="counter ">24</h2>
                           
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-3 col-sm-6 col-xs-12 text-center">
                        <a href="{{url('#')}}">
                            <div class="white-box analytics-info">
                                <h3 class="box-title">Downloads</h3>
                                <i class="fas fa-file fa-2x fain"></i>
                                <h2 class="counter ">126919</h2>
                            </div>
                        </a>
                    </div>
                </div>                
            </div>
@endsection

