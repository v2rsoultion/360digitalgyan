@extends('admin-panel.layout.header')

@section('content')

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">{!! $page_title !!}</h4> 
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                
                <ol class="breadcrumb">
                    <li><a href="{!! URL::to('admin-panel/dashboard') !!}">Dashboard</a></li>
                    <li><a href="#">{!! trans('language.videos') !!}</a></li>
                    <li class="active">{!! $page_title !!}</li> 
                </ol>
            </div>
            @if($errors->any())
                <p class="text-muted m-b-10 col-lg-12 col-md-12 col-sm-12 col-xs-12 font-13 red">{{$errors->first()}}</p>
            @endif
        </div>
        <!-- /row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box white-box-modify">
                    {!! Form::open(['files'=>TRUE,'id' => 'videos-form' , 'class'=>'form-horizontal','url' =>$save_url]) !!}
                    	@include('admin-panel.videos.form',['submit_button' => $submit_button])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
CKEDITOR.replace('video_des');

    $(function() {

        $(".select2").select2();

        $(".tch").TouchSpin({
            min: 0,
            max: 1000000000,
        });
    });

    jQuery(document).ready(function () {

        jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z\s]+$/i.test(value);
        }, "Only alphabetical characters");

        $("#videos-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */
            ignore: [],
            debug: false,
            rules: {
                videos_cat_id: {
                    required: true,
                },
                videos_sub_id: {
                    required: true,
                },
                videos_lang: {
                    required: true,
                },
                video_title: {
                    required: true,
                },
                video_des: {
                    required: true,
                },
                video_link: {
                    required: true,
                },
                video_dur: {
                    required: true,
                },

            },
            messages: {
                videos_cat_id: {
                    required: "Please select question category",
                },
                videos_sub_id: {
                    required: "Please select question sub category",
                },
                video_title: {
                    required: "Please enter video title",
                },
                video_des: {
                    required: "Please enter video description",
                },
                video_link: {
                    required: "Please enter video link",
                },
                video_dur: {
                    required: "Please enter video duration",
                },
                videos_lang: {
                    required: "Please select language",  
                }

               
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });
        
    });

    

</script>

<script type="text/javascript">
    $(document).ready(function() {
        // Basic
        $('.dropify').dropify();
        // Translated
        $('.dropify-fr').dropify({
            messages: {
                default: 'Drag and drop a file here or click',
                replace: 'Drag and drop or click to replace',
                remove: 'Remove',
                error: 'Ooops, something wrong happended.'
            }
        });
        // Used events
        var drEvent = $('#input-file-events').dropify();
        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });
        drEvent.on('dropify.afterClear', function(event, element) {
            alert('File deleted');
        });
        drEvent.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });
        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        })
    });
</script>

<script type="text/javascript">
    function getSubcategories(id)
   {
      BASE_URL = "{{ url('/') }}";
      $.ajax({
         url: BASE_URL+"/admin-panel/get-sub-categories/"+id,
         success: function(result){
            $('#videos_sub_id').html(result.data);
         }
      });
   }
</script>

@endsection