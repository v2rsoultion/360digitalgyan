<div class="row">
    <div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="input-group">
                <label for="{!! trans('language.category_name') !!}">
                    {!! trans('language.category_name') !!}
                </label>
                <select class="form-control select2 " id="videos_id" onchange="getSubcategories(this.value)" name="videos_cat_id" >
                    <option value="">Select Category</option>
                    @foreach ($categories as  $category)
                        <option value="{!! $category->iq_category_id !!}" {{ $video->category_id==$category->iq_category_id ? 'selected' : '' }} >
                            {!! $category->iq_category_name !!}
                        </option>   
                    @endforeach
                </select>
            </div>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="input-group">
                <label for="{!! trans('language.subcategory_name') !!}">
                    {!! trans('language.subcategory_name') !!} 
                </label>
                <select class="form-control select2 " id="videos_sub_id"  name="videos_sub_id" >
                    <option value="">Select Sub Category</option>
                    @foreach ($subCategories as  $subCategory)
                        <option value="{!! $subCategory->sub_category_id !!}" {{ $video->sub_category_id==$subCategory->sub_category_id ? 'selected' : '' }} >
                            {!! $subCategory->title !!}
                        </option>   
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="input-group">
                <label for="{!! trans('language.slt_lan') !!}">
                    {!! trans('language.slt_lan') !!}
                </label>
                <select class="form-control select2 " id="videos_lang" name="videos_lang" >
                    <option value="">Select Language</option>
                        <option value="1" {{ $video->language==1 ? 'selected' : '' }}>English</option>
                        <option value="2" {{ $video->language==2 ? 'selected' : '' }}>Hindi</option>
                </select>
            </div>
        </div>
        <div class="col-md-6 col-sm-12 col-lg-6 col-xs-12">
            <div class="input-group">
                <label for="{!! trans('language.slt_lan') !!}">
                    {!! trans('language.video_title') !!}
                </label>
                {{ Form::text('video_title',old('video_name',isset($video['title']) ? $video['title']: ''),["class"=>"form-control","placeholder"=>trans('language.video_title')]) }}
            </div>
        </div>
        <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
            <div class="input-group">
                <label for="{!! trans('language.video_des') !!}">
                    {{ trans('language.video_des') }}
                </label>
                {{ Form::textarea('video_des',old('video_des',isset($video['description']) ? $video['description']: ''),["class"=>"form-control","placeholder"=>trans('language.video_des')]) }}
            </div>
        </div>
        <div class="col-md-6 col-sm-12 col-xs-12 col-lg-6">
            <div class="input-group">
                <label for="{!! trans('language.slt_lan') !!}">
                    {{ trans('language.video_link') }}
                </label>
                {{ Form::text('video_link',old('video_link',isset($video['link']) ? $video['link']: ''),["class"=>"form-control","placeholder"=>trans('language.video_link')]) }}
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="white-box">
                <label for="input-file-disable-remove">
                    {{ trans('language.video_img') }}
                </label>
                @php
                    if(!empty($video['image'])){
                        $newsImage = url($video['image']);
                    }else{
                        $newsImage = url("/public/admin/nopreview-available.jpg");
                    } 
                @endphp
                <input type="file" name="video_image" id="input-file-disable-remove" class="dropify" data-show-remove="false" data-default-file="{!! $newsImage !!}"/> 
            </div>
        </div>
        <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
            <div class="input-group">
                <label for="{!! trans('language.video_duration') !!}">
                    {{ trans('language.video_duration') }}   
                </label>
                {{ Form::text('video_dur',old('video_dur',isset($video['duration']) ? $video['duration']: ''),["class"=>"form-control","placeholder"=>trans('language.video_duration')]) }}
            </div>
        </div>
    </div>
</div>

<div class="row">
	<div class=" col-sm-9">
	    <button type="submit" name="submit" class="btn btn-info waves-effect waves-light m-t-10">{{  $submit_button }}</button>
	</div>
</div>
<div class="clearfix"></div>