@extends('admin-panel.layout.header')
@section('content')

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">{!! $page_title !!}</h4>  
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                
                <ol class="breadcrumb">
                    <li><a href="{!! URL::to('admin-panel/dashboard') !!}">Dashboard</a></li>
                    <li><a href="#">{!! trans('language.category') !!}</a></li>
                    <li class="active">{!! $page_title !!}</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    @if(session()->has('success'))
                        <p class="alert alert-success text-muted m-b-10 col-lg-12 col-md-12 col-sm-12 col-xs-12 font-13 green">
                            {{ session()->get('success') }}
                        </p>
                    @endif
                    @if($errors->any())
                        <p class="text-muted m-b-10 col-lg-12 col-md-12 col-sm-12 col-xs-12 font-13 red">{{$errors->first()}}</p>
                    @endif
                    <div class="clearfix"></div>
                    {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3">
                                <div class="input-group">
                                    {!! Form::text('title', old('title',isset($video['title']) ? $video['title']: ''), ['class' => 'form-control','placeholder'=>trans('language.video_title'), 'id' => 'video_title','for'=>trans('language.video_title')]) !!}
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3">
                                <div class="input-group">
                                    <select class="form-control select2 " id="category_id" onchange="getSubcategories(this.value)" name="category_id" >
                                        <option value="">Select Category</option>
                                        @foreach ($Categories as  $category)
                                            <option value="{!! $category->iq_category_id !!}" {{ $video->category_id==$category->iq_category_id ? 'selected' : '' }} >
                                                {!! $category->iq_category_name !!}
                                            </option>   
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3">
                                <div class="input-group">
                                    <select class="form-control select2 " id="sub_category_id"  name="sub_category_id" >
                                        <option value="">Select Sub Category</option>
                                        @foreach ($subCategories as  $subCategory)
                                            <option value="{!! $subCategory->sub_category_id !!}" {{ $video->sub_category_id==$subCategory->sub_category_id ? 'selected' : '' }} >
                                                {!! $subCategory->title !!}
                                            </option>   
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-1 col-md-1">
                                {!! Form::submit('Search', ['class' => 'btn btn-info ','name'=>'Search']) !!}
                            </div>
                            <div class="col-lg-1 col-md-1">
                                {!! Form::button('Clear', ['class' => 'btn btn-info ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <div class="col-sm-12">
                <div class="white-box">
                    <div class="table-responsive">
                        <table class="table dataTable table-condensed" id="video-table">
                            <thead>
                                <tr>
                                    <th>S.No</th>
                                    <th>Title</th>
                                    <th>Category</th>
                                    <th>Sub-Category</th>
                                    <!-- <th>Link</th> -->
                                    <th>Discription</th>
                                    <!-- <th>Language</th> -->
                                    <th>Image</th>
                                    <th>Duration</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        var table = $('#video-table').DataTable({
            // dom: 'Blfrtip',
            pageLength: 10,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            ajax: {
                url: '{{url('admin-panel/videos/data')}}',
                data: function (d) {
                    d.title = $('input[name=title]').val();
                    d.category_id = $('select[name=category_id]').val();
                    d.sub_category_id = $('select[name=sub_category_id]').val();
                }
            },
            columns: [
                {data: 'DT_Row_Index', name: 'DT_Row_Index' },
                {data: 'title', name: 'title' },
                {data: 'category', name: 'category'},
                {data:'SubCategory',name:'SubCategory'},
                // {data: 'link', name: 'link' },
                {data: 'description', name: 'description' },
                // {data: 'language', name: 'language' },
                {data: 'image', name: 'image' },
                {data: 'duration', name: 'duration' },
                {data: 'status', name: 'status' },
                {data: 'action', name: 'action' },
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });

        $('#clearBtn').click(function(){
            location.reload();
        })
       
    });

    var elems = document.getElementsByClassName('confirmation');
    var confirmIt = function (e) {
        if (!confirm('Are you sure?')) e.preventDefault();
    };
    for (var i = 0, l = elems.length; i < l; i++) {
        elems[i].addEventListener('click', confirmIt, false);
    }


    function changeStatus(id,status){
        var token = '{!!csrf_token()!!}';
        $.ajax(
        {
            url: "{{ url('admin-panel/videos/video-status') }}",
            type: 'GET',
            data: {
                'video_id': id,
                'video_status': status,
            },
            success: function (res)
            {
                if (res == "Success")
                {
                    $('#video-table').DataTable().ajax.reload();
                } else
                {
                    ajax.reload();
                }
            }
        });
    }
   </script>
<script type="text/javascript">

function getSubcategories(id)
{
  BASE_URL = "{{ url('/') }}";
  $.ajax({
     url: BASE_URL+"/admin-panel/get-sub-categories/"+id,
     success: function(result){
        $('#sub_category_id').html(result.data);
     }
  });
}
</script>


@endsection