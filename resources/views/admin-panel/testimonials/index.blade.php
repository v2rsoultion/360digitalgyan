@extends('admin-panel.layout.header')
@section('content')

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">{!! $page_title !!}</h4>  
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                
                <ol class="breadcrumb">
                    <li><a href="{!! URL::to('admin-panel/dashboard') !!}">Dashboard</a></li>
                    <li><a href="#">{!! trans('language.testimonial') !!}</a></li>
                    <li class="active">{!! $page_title !!}</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    @if(session()->has('success'))
                        <p class="alert alert-success text-muted m-b-10 col-lg-12 col-md-12 col-sm-12 col-xs-12 font-13 green">
                            {{ session()->get('success') }}
                        </p>
                    @endif
                    @if($errors->any())
                        <p class="text-muted m-b-10 col-lg-12 col-md-12 col-sm-12 col-xs-12 font-13 red">{{$errors->first()}}</p>
                    @endif
                    <div class="clearfix"></div>
                    {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3">
                                <div class="input-group">
                                    {!! Form::text('title', old('title',isset($testimonial['title']) ? $testimonial['title']: ''), ['class' => 'form-control','placeholder'=>trans('language.testimonial_name'), 'id' => 'testimonial_title','for'=>trans('language.testmonial_name')]) !!}
                                </div>
                            </div>
                            <div class="col-lg-1 col-md-1">
                                {!! Form::submit('Search', ['class' => 'btn btn-info ','name'=>'Search']) !!}
                            </div>
                            <div class="col-lg-1 col-md-1">
                                {!! Form::button('Clear', ['class' => 'btn btn-info ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <div class="col-sm-12">
                <div class="white-box">
                    <div class="table-responsive">
                        <table class="table dataTable table-condensed" id="testimonials-table">
                            <thead>
                                <tr>
                                    <th>S.No</th>
                                    <th>Name</th>
                                    <th style="width: 50px">Image</th>
                                    <th>Description</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        var table = $('#testimonials-table').DataTable({
            // dom: 'Blfrtip',
            pageLength: 10,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            ajax: {
                url: '{{url('admin-panel/testimonials/data')}}',
                data: function (d) {
                    d.title = $('input[name=title]').val();
                }
            },
            columns: [
                {data: 'DT_Row_Index', name: 'DT_Row_Index' },
                {data: 'testimonial_name', name: 'testimonial_name' },
                {data: 'image', name: 'image'},
                {data:'description',name:'description'},
                {data: 'status', name: 'status' },
                {data: 'action', name: 'action' },
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });

        $('#clearBtn').click(function(){
            location.reload();
        })
    });
    function changeStatus(id,status){
        var token = '{!!csrf_token()!!}';
        $.ajax(
        {
            url: "{{ url('admin-panel/testimonials/testimonials-status') }}",
            type: 'GET',
            data: {
                'testimonial_id': id,
                'testimonial_status': status,
            },
            success: function (res)
            {
                if (res == "Success")
                {
                    $('#testimonials-table').DataTable().ajax.reload();
                } else
                {
                    ajax.reload();
                }
            }
        });
    }
   </script>


@endsection