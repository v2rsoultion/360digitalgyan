<div class="row">
    <div>
        <div class="col-md-6 col-sm-12 col-lg-6 col-xs-12">
            <div class="input-group">
                <label for="{!! trans('language.testimonial_name') !!}">
                    {!! trans('language.testimonial_name') !!}
                </label>
                {{ Form::text('testimonial_name',old('testimonial_name',isset($testimonial['testimonial_name']) ? $testimonial['testimonial_name']: ''),["class"=>"form-control","placeholder"=>trans('language.testimonial_name')]) }}
            </div>
            <div class="clearfix"></div>
            <div class="white-box">
                <label for="input-file-disable-remove">
                    {{ trans('language.testimonial_image') }}
                </label>
                @php
                    if(!empty($testimonial['testimonial_imgs'])){
                        $testimonialImage = url('uploads/'.$testimonial['testimonial_imgs']);
                    }else{
                        $testimonialImage = url("/public/admin/nopreview-available.jpg");
                    } 
                @endphp
                <input type="file" name="testimonial_image" id="input-file-disable-remove" class="dropify" data-show-remove="false" data-default-file="{!! $testimonialImage !!}"/> 
            </div>
        </div>
        <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
            <div class="input-group">
                <label for="{!! trans('language.video_des') !!}">
                    {{ trans('language.testimonial_des') }}
                </label>
                {{ Form::textarea('testimonial_des',old('description',isset($testimonial['testimonial_description']) ? $testimonial['testimonial_description']: ''),["class"=>"form-control","placeholder"=>trans('language.testimonial_des')]) }}
            </div>
        </div>
    </div>
</div>

<div class="row">
	<div class=" col-sm-9">
	    <button type="submit" name="submit" class="btn btn-info waves-effect waves-light m-t-10">{{  $submit_button }}</button>
	</div>
</div>
<div class="clearfix"></div>