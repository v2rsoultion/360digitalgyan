<div class="row">
    <div class="col-sm-8">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="input-group">
                <label for="{!! trans('language.category_name') !!}">
                    {!! trans('language.category_name') !!}
                </label>

                {!! Form::text('iq_category_name', old('iq_category_name',isset($category['iq_category_name']) ? $category['iq_category_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.category_name'), 'id' => 'category_name','for'=>trans('language.category_name')]) !!}

                @if($errors->has('iq_category_name'))
                    <p class="help-block">
                        {{ $errors->first('iq_category_name') }}
                    </p>
                @endif
            </div>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="input-group">
                <label for="{!! trans('language.category_order') !!}">
                    {!! trans('language.category_order') !!}
                </label>

                {!! Form::number('iq_category_order', old('category_order',isset($category['iq_category_order']) ? $category['iq_category_order']: ''), ['class' => 'form-control','placeholder'=>trans('language.category_order'), 'id' => 'category_order','for'=>trans('language.category_order')]) !!}

                @if($errors->has('iq_category_order'))
                    <p class="help-block">
                        {{ $errors->first('iq_category_order') }}
                    </p>
                @endif
            </div>
        </div>

         <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="input-group">
                <label for="{!! trans('language.category_quiz_time') !!}">
                    {!! trans('language.category_quiz_time') !!} (minutes) 
                </label>

                {!! Form::number('quiz_time', old('quiz_time',isset($category['quiz_time']) ? $category['quiz_time']: ''), ['class' => 'form-control','placeholder'=>trans('language.category_quiz_time'),'for'=>trans('language.category_quiz_time')]) !!}

                @if($errors->has('quiz_time'))
                    <p class="help-block">
                        {{ $errors->first('quiz_time') }}
                    </p>
                @endif
            </div>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="input-group">
                <label>
                     Show Video Category
                </label>
                <?php $arr = explode(',',$category->show_category_video); ?>
                <div class="checkbox checkbox-info ">
                    <input id="hindi" type="checkbox" name="video_lan[]" value="2" {{ in_array(2,$arr) ? 'checked' : '' }} >
                    <label for="hindi">Hindi </label>&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;
                    <input id="english" type="checkbox" name="video_lan[]" value="1" {{ in_array(1,$arr) ? 'checked' : '' }} >
                    <label for="english">English</label>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="input-group">
                <label for="{!! trans('language.category_icon') !!}">
                    <!-- {!! trans('language.category_icon') !!} -->
                </label>

<!--                 {!! Form::text('category_icon', old('category_name',isset($category['category_icon']) ? $category['category_icon']: ''), ['class' => 'form-control','placeholder'=>trans('language.category_icon'), 'id' => 'category_icon','for'=>trans('language.category_icon')]) !!}

                @if($errors->has('category_icon'))
                    <p class="help-block">
                        {{ $errors->first('category_icon') }}
                    </p>
                @endif -->
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="input-group">
                <label>
                    IQ {!! trans('language.pages_meta_title') !!} 
                </label>

                {!! Form::text('iq_category_metatitle', old('iq_category_metatitle',isset($category['iq_category_metatitle']) ? $category['iq_category_metatitle']: ''), ['class' => 'form-control','placeholder'=>trans('language.pages_meta_title')]) !!}

                @if($errors->has('iq_category_metatitle'))
                    <p class="help-block">
                        {{ $errors->first('iq_category_metatitle') }}
                    </p>
                @endif
            </div>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="input-group">
                <label>
                    Quiz {!! trans('language.pages_meta_title') !!} 
                </label>

                {!! Form::text('quiz_meta_title', old('quiz_meta_title',isset($category['quiz_meta_title']) ? $category['quiz_meta_title']: ''), ['class' => 'form-control','placeholder'=>trans('language.pages_meta_title')]) !!}

                @if($errors->has('quiz_meta_title'))
                    <p class="help-block">
                        {{ $errors->first('quiz_meta_title') }}
                    </p>
                @endif
            </div>
        </div>
    </div>

    <div class="col-md-4 col-xs-12">
        <div class="white-box">
            <label for="input-file-disable-remove">Category App Icon</label>

            @php
                if( !empty($category['iq_category_icon']) ){
                    $categoryIcon = url('uploads/'.$category['iq_category_icon']);
                }  else {
                    $categoryIcon = url("/public/admin/nopreview-available.jpg");
                } 
            @endphp
            
            <input type="file" name="category_icon" id="input-file-disable-remove" class="dropify" data-show-remove="false" data-default-file="{!! $categoryIcon !!}"/> 
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="input-group">
                <label>  
                    IQ {!! trans('language.pages_meta_keyword') !!} 
                </label>

                {!! Form::textarea('iq_category_metakey', old('iq_category_metakey',isset($category['iq_category_metakey']) ? $category['iq_category_metakey']: ''), ['class' => 'form-control','placeholder'=>trans('language.pages_meta_keyword')]) !!}

                @if($errors->has('iq_category_metakey'))
                    <p class="help-block">
                        {{ $errors->first('iq_category_metakey') }}
                    </p>
                @endif
            </div>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="input-group">
                <label>
                    Quiz {!! trans('language.pages_meta_keyword') !!} 
                </label>

                {!! Form::textarea('quiz_meta_key', old('quiz_meta_key',isset($category['quiz_meta_key']) ? $category['quiz_meta_key']: ''), ['class' => 'form-control','placeholder'=>trans('language.pages_meta_keyword')]) !!}

                @if($errors->has('quiz_meta_key'))
                    <p class="help-block">
                        {{ $errors->first('quiz_meta_key') }}
                    </p>
                @endif
            </div>
        </div>

        <div class="clearfix"></div>
        
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="input-group">
                <label>
                    IQ {!! trans('language.pages_meta_description') !!} 
                </label>

                {!! Form::textarea('iq_category_metadesc', old('iq_category_metadesc',isset($category['iq_category_metadesc']) ? $category['iq_category_metadesc']: ''), ['class' => 'form-control','placeholder'=>trans('language.pages_meta_description')]) !!}

                @if($errors->has('iq_category_metadesc'))
                    <p class="help-block">
                        {{ $errors->first('iq_category_metadesc') }}
                    </p>
                @endif
            </div>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="input-group">
                <label>
                    Quiz {!! trans('language.pages_meta_description') !!} 
                </label>

                {!! Form::textarea('quiz_meta_desp', old('quiz_meta_desp',isset($category['quiz_meta_desp']) ? $category['quiz_meta_desp']: ''), ['class' => 'form-control','placeholder'=>trans('language.pages_meta_description')]) !!}

                @if($errors->has('quiz_meta_desp'))
                    <p class="help-block">
                        {{ $errors->first('quiz_meta_desp') }}
                    </p>
                @endif
            </div>
        </div>
    </div>
</div>

<div class="row">
	<div class=" col-sm-9">
	    <button type="submit" name="submit" class="btn btn-info waves-effect waves-light m-t-10">{{  $submit_button }}</button>
	</div>
</div>
<div class="clearfix"></div>