@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
    .nums{
        width: 50px;
    }
</style>

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">{!! $page_title !!}</h4>
                  
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                
                <ol class="breadcrumb">
                    <li><a href="{!! URL::to('admin-panel/dashboard') !!}">Dashboard</a></li>
                    <li><a href="#">{!! trans('language.category') !!}</a></li>
                    <li class="active">{!! $page_title !!}</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    @if(session()->has('success'))
                        <p class="alert alert-success text-muted m-b-10 col-lg-12 col-md-12 col-sm-12 col-xs-12 font-13 green">
                            {{ session()->get('success') }}
                        </p>
                    @endif
                    @if($errors->any())
                        <p class="text-muted m-b-10 col-lg-12 col-md-12 col-sm-12 col-xs-12 font-13 red">{{$errors->first()}}</p>
                    @endif
                    <div class="clearfix"></div>
                    {!! Form::open(['id' => 'search-form','class'=>'form-horizontal','url'=>'admin-panel/view-seo-users']) !!}
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3">
                                <div class="input-group">
                                    {!! Form::text('user_name', old('user_name',isset($_REQUEST['user_name']) ? $_REQUEST['user_name']: ''), ['class' => 'form-control','placeholder'=>'User Name']) !!}
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3">
                                <div class="input-group">
                                    {!! Form::text('user_mail',old('user_mail',isset($_REQUEST['user_mail']) ? $_REQUEST['user_mail']: ''), ['class' => 'form-control','placeholder'=>'User Email']) !!}
                                </div>
                            </div>
                            <div class="col-lg-1 col-md-1">
                                {!! Form::submit('Search', ['class' => 'btn btn-info ','name'=>'Search']) !!}
                            </div>
                            <div class="col-lg-1 col-md-1">
                               <a href="{{ url('admin-panel/view-seo-users') }}" class="btn btn-info" >Clear</a>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <div class="col-sm-12">
                <div class="white-box">
                    <div class="table-responsive">
                        <table class="table dataTable " id="user-table">
                            <thead>
                                <tr>
                                    <th>S.No</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Login via</th>
                                    <th>Points</th>
                                    <th>Badge</th>
                                    <th>Rank</th>
                                    <th>Date</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $key => $user)
                                <tr>
                                    <?php 
                                        $ranks  = getUserRank($user->seo_users_id);
                                        $rank   = $ranks[0]->rank_overall;
                                    ?>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $user->seo_users_name }}</td>
                                    <td>{{ $user->seo_users_email }}</td>
                                    <td>{{ $user->flag }}</td>
                                    <td>{{ $user->user_points }}</td>
                                    <td>{{ $user->user_badges ? $user->user_badges : '-----' }}</td>
                                    <td>{{ $rank }}</td>
                                    <td>{{ $user->date_of_registration }}</td>
                                    <td>
                                        
                                        <div class="radio radio-{{ $user->seo_users_status==1 ? success : danger }} custom_radiobox">
                                            <input type="radio" name="iqradio{{ $user->seo_users_id }}" onClick="changeStatus({{ $user->seo_users_id }},{{ $user->seo_users_status==1 ? 0 : 1 }})" id="iqradio{{ $user->seo_users_id }}" value="option4" checked="">
                                            <label for="iqradio{{ $user->seo_users_id }}"> </label>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div>
                        {{ $users->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>


    var elems = document.getElementsByClassName('confirmation');
    var confirmIt = function (e) {
        if (!confirm('Are you sure?')) e.preventDefault();
    };
    for (var i = 0, l = elems.length; i < l; i++) {
        elems[i].addEventListener('click', confirmIt, false);
    }

    /* for Quiz status change in category  */
    function changeStatus(id,status){
        var token = '{!!csrf_token()!!}';
        $.ajax(
        {
            url : "{{ url('admin-panel/view-users/staus') }}",
            type: 'GET',
            data: {
                'user_id'       : id,
                'user_status'   : status,
            },
            success: function (res)
            {
                if (res == "Success")
                {
                    location.reload();
                } else
                {
                    location.reload();
                }
            }
        });
    }

    /* for Set Order of category  */
    function changeCategoryOrder(ele){
        var token = '{!!csrf_token()!!}';
        var id    = $(ele).attr('data-id');
        var order = $(ele).val();
        $.ajax(
        {
            url: "{{ url('admin-panel/category/category-order') }}",
            type: 'GET',
            data: {
                'category_id': id,
                'category_order': order,
            },
            success: function (res)
            {
                if (res == "Success")
                {
                    location.reload();
                } else
                {
                    location.reload();
                }
            }
        });
    }


    /* for IQ status change in category  */
    function changeStatusIQCategory(id,status){
        var token = '{!!csrf_token()!!}';
        $.ajax(
        {
            url: "{{ url('admin-panel/category/category-status') }}",
            type: 'GET',
            data: {
                'category_id': id,
                'category_status': status,
                'category_type':'iq',
            },
            success: function (res)
            {
                if (res == "Success")
                {
                    location.reload();
                } else
                {
                    location.reload();
                }
            }
        });
    }

</script>
@endsection




