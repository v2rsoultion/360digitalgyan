<div class="row">
	<div>
		<div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
			<div class="input-group">
				<label for="{!! trans('language.tags') !!}">
					{{ trans('language.tags') }}
				</label>
				{{ Form::text('tag_name',old('tag_name',isset($tag['tags']) ? $tag['tags']: ''),["class"=>"form-control","placeholder"=>trans('language.tag_name')]) }}
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class=" col-sm-9">
	    <button type="submit" name="submit" class="btn btn-info waves-effect waves-light m-t-10">{{  $submit_button }}</button>
	</div>
</div>
<div class="clearfix"></div>