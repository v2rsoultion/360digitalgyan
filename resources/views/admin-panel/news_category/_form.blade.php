<div class="row">
    <div class="col-sm-8">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="input-group">
                <label for="{!! trans('language.news_category_name') !!}">
                    {!! trans('language.news_category_name') !!}
                </label>

                {!! Form::text('news_category_name', old('news_category_name',isset($category['news_category_name']) ? $category['news_category_name']: ''), ['class' => 'form-control','placeholder'=>trans('language.news_category_name')]) !!}

                @if($errors->has('news_category_name'))
                    <p class="help-block">
                        {{ $errors->first('news_category_name') }}
                    </p>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="row">
	<div class=" col-sm-9">
	    <button type="submit" name="submit" class="btn btn-info waves-effect waves-light m-t-10">{{  $submit_button }}</button>
	</div>
</div>
<div class="clearfix"></div>