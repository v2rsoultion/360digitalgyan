<div class="row">
    <div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="input-group">
                <label for="{!! trans('language.category_name') !!}">
                    {!! trans('language.category_name') !!}
                </label>
                <select class="form-control select2 " id="study_cat_id" onchange="getSubcategories(this.value)" name="cat_id" >
                    <option value="">Select Category</option>
                    @foreach ($categories as  $category)
                        <option value="{!! $category->iq_category_id !!}" {{ $study->study_material_catid==$category->iq_category_id ? 'selected' : '' }} >
                            {!! $category->iq_category_name !!}
                        </option>   
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="input-group">
                <label for="{!! trans('language.subcategory_name') !!}">
                    {!! trans('language.subcategory_name') !!} 
                </label>
                <select class="form-control select2 " id="study_sub_id"  name="sub_id" >
                    <option value="">Select Sub Category</option>
                    @foreach ($subCategories as  $subCategory)
                        <option value="{!! $subCategory->sub_category_id !!}" {{ $study->sub_cat_id==$subCategory->sub_category_id ? 'selected' : '' }} >
                            {!! $subCategory->title !!}
                        </option>   
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="input-group">
                <br>
                <div class="checkbox checkbox-info ">
                    <input id="is_new" type="checkbox" name="is_new" value="1" {{ $study->is_new==1 ? 'checked' : '' }} >
                    <label for="is_new">Check if your study material has new.</label>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
            <div class="input-group">
                <label for="{!! trans('language.meta_title') !!}">
                    {{ trans('language.meta_title') }}
                </label>
                {{ Form::text('title',old('title',isset($study['study_metatitle']) ? $study['study_metatitle']: ''),["class"=>"form-control","placeholder"=>trans('language.meta_title')]) }}
            </div>
        </div>
        <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
            <div class="input-group">
                <label for="{!! trans('language.question') !!}">
                    {{ trans('language.question') }}
                </label>
                {{ Form::textarea('question',old('question',isset($study['study_ques']) ? $study['study_ques']: ''),["class"=>"form-control","placeholder"=>trans('language.question')]) }}
            </div>
        </div>
        <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
            <div class="input-group">
                <label for="{!! trans('language.answer') !!}">
                    {{ trans('language.answer') }}
                </label>
                {{ Form::textarea('answer',old('answer',isset($study['study_ques_ans']) ? $study['study_ques_ans']: ''),["class"=>"form-control","placeholder"=>trans('language.answer')]) }}
            </div>
        </div>
        <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
            <div class="input-group">
                <label for="{!! trans('language.coding') !!}">
                    {{ trans('language.coding') }}
                </label>
                {{ Form::textarea('coding',old('coding',isset($study['coding']) ? $study['coding']: ''),["class"=>"form-control","placeholder"=>trans('language.coding')]) }}
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="white-box">
                <label for="input-file-disable-remove">
                    {{ trans('language.study_material_img') }}
                </label>
                @php
                    if(!empty($tricks['tip_image'])){
                        $trickImage = url('uploads/'.$tricks['tip_image']);
                    }else{
                        $trickImage = url("/public/admin/nopreview-available.jpg");
                    } 
                @endphp
                <input type="file" name="image" id="input-file-disable-remove" class="dropify" data-show-remove="false" data-default-file="{!! $trickImage !!}"/> 
            </div>
        </div>
        <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
            <div class="input-group">
                <label for="{!! trans('language.meta_keyword') !!}">
                    {{ trans('language.meta_keyword') }}
                </label>
                {{ Form::textarea('keyword',old('keyword',isset($study['study_metakey']) ? $study['study_metakey']: ''),["class"=>"form-control","placeholder"=>trans('language.meta_keyword')]) }}
            </div>
        </div>
        <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
            <div class="input-group">
                <label for="{!! trans('language.meta_desc') !!}">
                    {{ trans('language.meta_desc') }}
                </label>
                {{ Form::textarea('desc',old('desc',isset($study['study_metadesc']) ? $study['study_metadesc']: ''),["class"=>"form-control","placeholder"=>trans('language.meta_desc')]) }}
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class=" col-sm-9">
        <button type="submit" name="submit" class="btn btn-info waves-effect waves-light m-t-10">{{  $submit_button }}</button>
    </div>
</div>
<div class="clearfix"></div>