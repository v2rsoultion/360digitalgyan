<div class="row">
	<div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="input-group">
                <br>
                <div class="checkbox checkbox-info ">
                    <input id="is_new" type="checkbox" name="is_new" value="1" {{ $terms->is_new==1 ? 'checked' : '' }} >
                    <label for="is_new">Check if your term has new.</label>
                </div>
            </div>
        </div>
		<div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
			<div class="input-group">
				<label for="{!! trans('language.terms_title') !!}">
					{{ trans('language.terms_title') }}
				</label>
				{{ Form::text('title',old('title',isset($terms['term_title']) ? $terms['term_title']: ''),["class"=>"form-control","placeholder"=>trans('language.terms_title')]) }}
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="white-box">
                <label for="input-file-disable-remove">
                    {{ trans('language.terms_img') }}
                </label>
                @php
                    if(!empty($terms['term_image'])){
                        $termImage = url('uploads/'.$terms['term_image']);
                    }else{
                        $termImage = url("/public/admin/nopreview-available.jpg");
                    } 
                @endphp
                <input type="file" name="term_image" id="input-file-disable-remove" class="dropify" data-show-remove="false" data-default-file="{!! $termImage !!}"/> 
            </div>
        </div>
        <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
            <div class="input-group">
                <label for="{!! trans('language.terms_des') !!}">
                    {{ trans('language.terms_des') }}
                </label>
                {{ Form::textarea('term_des',old('term_des',isset($terms['term_description']) ? $terms['term_description']: ''),["class"=>"form-control","placeholder"=>trans('language.terms_des')]) }}
            </div>
        </div>
	</div>
</div>
<div class="row">
	<div class=" col-sm-9">
	    <button type="submit" name="submit" class="btn btn-info waves-effect waves-light m-t-10">{{  $submit_button }}</button>
	</div>
</div>
<div class="clearfix"></div>