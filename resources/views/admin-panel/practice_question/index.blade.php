@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
    .nums{
        width: 50px;
    }
</style>

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">{!! $page_title !!}</h4>  
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                
                <ol class="breadcrumb">
                    <li><a href="{!! URL::to('admin-panel/dashboard') !!}">Dashboard</a></li>
                    <li><a href="#">{!! trans('language.practice_quiz_questions') !!}</a></li>
                    <li class="active">{!! $page_title !!}</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    @if(session()->has('success'))
                        <p class="alert alert-success text-muted m-b-10 col-lg-12 col-md-12 col-sm-12 col-xs-12 font-13 green">
                            {{ session()->get('success') }}
                        </p>
                    @endif
                    @if($errors->any())
                        <p class="text-muted m-b-10 col-lg-12 col-md-12 col-sm-12 col-xs-12 font-13 red">{{$errors->first()}}</p>
                    @endif
                    <div class="clearfix"></div>
                    {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3">
                                <div class="input-group">
                                    <select class="form-control select2 " id="category_id" onchange="getSubcategories(this.value)" name="category_id" >
                                        <option value="">Select Category</option>
                                        @foreach ($Categories as  $category)
                                            <option value="{!! $category->iq_category_id !!}" {{ $interviewQues->iq_ques_ans_catid==$category->iq_category_id ? 'selected' : '' }} >
                                                {!! $category->iq_category_name !!}
                                            </option>   
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3">
                                <div class="input-group">
                                    <select class="form-control select2 " id="sub_category_id"  name="sub_category_id" >
                                        <option value="">Select Sub Category</option>
                                        @foreach ($subCategories as  $subCategory)
                                            <option value="{!! $subCategory->sub_category_id !!}" {{ $interviewQues->sub_cat_id==$subCategory->sub_category_id ? 'selected' : '' }} >
                                                {!! $subCategory->title !!}
                                            </option>   
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-lg-1 col-md-1">
                                {!! Form::submit('Search', ['class' => 'btn btn-info ','name'=>'Search']) !!}
                            </div>
                            <div class="col-lg-1 col-md-1">
                                {!! Form::button('Clear', ['class' => 'btn btn-info ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <div class="col-sm-12">
                <div class="white-box">
                    <div class="table-responsive">
                        <table class="table table-condensed dataTable " id="quiz-table">
                            <thead>
                                <tr>
                                    <th class="fa fa-plus"></th>
                                    <th>S.No</th>
                                    <th>Category</th>
                                    <th>Sub Category</th>
                                    <!-- <th>Types</th> -->
                                    <th>Question</th>
                                    <th>Order</th>
                                    <th>New?</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.1.2/handlebars.js" type="text/javascript" charset="utf-8"></script>
<script id="details-template" type="text/x-handlebars-template">
    @verbatim
        <table class="table" id="detail" style="background-color:#f6f6f6;">
        </table>
    @endverbatim
</script>

<script>

    $(document).ready(function () {
        var table = $('#quiz-table').DataTable({
            processing: true,
            responsive: true,
            pageLength: 10,
            serverSide: true,
            bLengthChange: false,
            ajax: {
                    url: '{{url('admin-panel/practice-quiz-questions/data')}}',
                    data: function(d){
                            d.category_id = $('select[name=category_id]').val();
                            d.sub_category_id = $('select[name=sub_category_id]').val();
                    }
                },
                columns: [
                {
                    "className": 'details-control fa fa-plus-circle green',
                    "orderable": false,
                    "searchable": false,
                    "data": null,
                    "defaultContent": ''
                },
                {data: 'DT_Row_Index', name: 'DT_Row_Index' },
                {data: 'category', name: 'category' },
                {data: 'sub_category', name: 'sub_category'},
                {data:'question',name:'question'},
                {data: 'order', name: 'order' },
                {data: 'new', name: 'new' },
                {data: 'status', name: 'status' },
                {data: 'action', name: 'action' },
            ],

            // order: [[1, 'asc']]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });

        $('#clearBtn').click(function(){
            location.reload();
        });

        var template = Handlebars.compile($("#details-template").html());
        $('#quiz-table tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = table.row( tr );
            var id = row.data()['qustion_id'];
            var base_url = "{{ url('/') }}";
            $.ajax({
                url:base_url+"/admin-panel/practice-quiz-questions/detail-data/"+id,
                success:function(result) {
                    $('#detail').html(result);
                }
            });
            if ( row.child.isShown() ) {
            // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
                $(this).removeClass('fa fa-minus-circle red');
                $(this).addClass('fa fa-plus-circle green');
            }
            else {
            // Open this row
                row.child( template(row.data()) ).show();
                tr.addClass('shown');
                $(this).removeClass('fa fa-plus-circle green');
                $(this).addClass('fa fa-minus-circle red');
            }

        });
    });

    var elems = document.getElementsByClassName('confirmation');
    var confirmIt = function (e) {
        if (!confirm('Are you sure?')) e.preventDefault();
    };
    for (var i = 0, l = elems.length; i < l; i++) {
        elems[i].addEventListener('click', confirmIt, false);
    }
    

    /* for Quiz status change in category  */
    function changeStatus(id,status){
        var token = '{!!csrf_token()!!}';
        $.ajax(
        {
            url: "{{ url('admin-panel/practice-quiz-questions/practice-questions-status') }}",
            type: 'GET',
            data: {
                'interview_id': id,
                'interview_status': status,
            },
            success: function (res)
            {
                if (res == "Success")
                {
                    $('#quiz-table').DataTable().ajax.reload();
                } else
                {
                    ajax.reload();
                }
            }
        }); 
    }


    /* for Set Order of quiz  */
    function changeOrder(ele){
        var token = '{!!csrf_token()!!}';
        var id    = $(ele).attr('data-id');
        var order = $(ele).val();
        $.ajax(
        {
            url: "{{ url('admin-panel/practice-quiz-questions/practice-questions-order') }}",
            type: 'GET',
            data: {
                'quiz_id': id,
                'quiz_order': order,
            },
            success: function (res)
            {
                if (res == "Success")
                {
                    $('#quiz-table').DataTable().ajax.reload();
                } else
                {
                    ajax.reload();
                }
            }
        });
    }


$(function() {
    $(".select2").select2();
    $(".tch").TouchSpin({
        min: 0,
        max: 1000000000,
    });
});

</script>

<script type="text/javascript">
    function getSubcategories(id)
   {
      BASE_URL = "{{ url('/') }}";
      $.ajax({
         url: BASE_URL+"/admin-panel/get-sub-categories/"+id,
         success: function(result){
            $('#sub_category_id').html(result.data);
         }
      });
   }
</script>

@endsection




