<div class="row">
    <div class="col-sm-12">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

            <button class="btn btn-success pull-right" type="button" id="AddQuestionBtn">Add Question</button>
            <input type="hidden" name="num_questions" id="keyinput" value="1">
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="input-group">
                <br>
                <div class="checkbox checkbox-info ">
                    <input id="code_flag" type="checkbox" name="code_flag" value="1" {{ $quizQues->code_flag==1 ? 'checked' : '' }} >
                    <label for="code_flag">Check if your question has a code .</label>
                </div>
            </div>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="input-group">
                <br>
               <div class="checkbox checkbox-info">
                    
                    <input id="is_new" type="checkbox" name="is_new" value="1" {{ $quizQues->is_new==1 ? 'checked' : '' }} >
                    <label for="is_new"> Question New or Not </label>
                </div>
            </div>
        </div>
<br><br><br>
        <div class="clearfix"></div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="input-group">
                <label for="{!! trans('language.category_name') !!}">
                    {!! trans('language.category_name') !!}
                </label>

                <select class="form-control select2 " required id="qustion_sec_id" onchange="getSubcategories(this.value)" name="qustion_sec_id" >
                    <option value="">Select Category</option>
                    @foreach ($categories as  $category)
                        <option value="{!! $category->iq_category_id !!}" {{ $quizQues->qustion_sec_id==$category->iq_category_id ? 'selected' : '' }} >
                            {!! $category->iq_category_name !!}
                        </option>   
                    @endforeach
                </select>
            </div>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="input-group">
                <label for="{!! trans('language.subcategory_name') !!}">
                    {!! trans('language.subcategory_name') !!}
                </label>

                <select class="form-control select2 " required id="qustion_sub_sec_id"  name="qustion_sub_sec_id" >
                    <option value="">Select Sub Category</option>
                    @foreach ($subCategories as  $subCategory)
                        <option value="{!! $subCategory->sub_category_id !!}" {{ $quizQues->qustion_sub_sec_id==$subCategory->sub_category_id ? 'selected' : '' }} >
                            {!! $subCategory->title !!}
                        </option>   
                    @endforeach
                </select>
            </div>
        </div>
        <div class="clearfix"></div>
        <div id=Q-set-1>
        <div id="mainQuestionPanel">
            <input type="text" name="questionset[1]" hidden>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 toggle-text-image">
            <div class="input-group">
                <div class="radio radio-info ">
                    <?php
                        if (file_exists($quizQues['qustion_qustion'])){
                            $ques = 'img';
                        }else{
                            $ques = 'txt';
                        }

                    ?>
                    <input type="radio" onclick="typeChange(this)" {{ $ques=="txt" ? "checked" :  "" }} checked name="questionset[1][textimg]qustion_qustion_radio" value="text" >
                    <label>Text</label>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="radio" onclick="typeChange(this)" {{ $ques=='img' ? "checked" : ""  }} name="questionset[1][textimg]qustion_qustion_radio" value="image">
                    <label>Image</label>
                </div>
            </div>
            <div class="input-group">
                <label for="{!! trans('language.question_title') !!}">
                    {!! trans('language.question_title') !!}
                </label>
                <div class="toggle-text" {{ $ques=='txt' ? "" :  "hidden" }} >
                    {!! Form::textarea("questionset[1][textquestion]",isset($quizQues['qustion_qustion']) ? $quizQues['qustion_qustion']: '', ['class' => 'form-control cktext','placeholder'=>trans('language.question_title'),'rows'=>'20','id'=>'qustion_qustion','required']) !!}
                </div>
                @php
                    if(!empty($quizQues['qustion_qustion']) ){
                        $categoryIcon = url($quizQues['qustion_qustion']);
                    }else{
                        $categoryIcon = url("/public/admin/nopreview-available.jpg");
                    } 
                @endphp
                <div class="toggle-image" {{ $ques=='img' ? "" : "hidden"  }}>
                    <input type="file" name="questionset[1][imgquestion]" id="input-file-disable-remove" class="dropify" data-show-remove="false" data-default-file="{!! $categoryIcon !!}"/>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 toggle-text-image">
            <div class="input-group">
                <div class="radio radio-info ">
                    <?php
                        if (file_exists($quizQues['question_section_part'])){
                            $detail = 'img';
                        }else{
                            $detail = 'txt';
                        }

                    ?>
                    <input type="radio" onclick="typeChange(this)" {{ $detail=='txt' ? "checked" : "" }} checked name="questionset[1][detailtextimg]" value="text" >
                    <label for="qustion_optionDetails_text">Text</label>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="radio" onclick="typeChange(this)" {{ $detail=='img' ? "checked" : "" }} name="questionset[1][detailtextimg]" value="image">
                    <label for="qustion_optionDetails_img">Image</label>
                </div>
            </div>
            <div class="input-group">
                <label for="{!! trans('language.question_details') !!}">
                    {!! trans('language.question_details') !!}
                </label>
                <div class="toggle-text" {{ $detail=='img' ? "hidden" : "" }}>
                    {!! Form::textarea("questionset[1][textselect]",isset($quizQues['question_section_part']) ? $quizQues['question_section_part']: '', ['class' => 'form-control cktext','placeholder'=>trans('language.question_details'),'rows'=>'20']) !!}
                </div>
                @php
                    if( !empty($quizQues['question_section_part']) ){
                        $categoryIcon = url($quizQues['question_section_part']);
                    }  else {
                        $categoryIcon = url("/public/admin/nopreview-available.jpg");
                    } 
                @endphp
                <div class="toggle-image" {{ $detail=='txt' ? "hidden" : "" }}>
                    <input type="file" name="questionset[1][imgselect]" id="input-file-disable-remove" class="dropify" data-show-remove="false" data-default-file="{!! $categoryIcon !!}"/>
                </div>
            </div>
        </div>


        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 toggle-text-image">
            <div class="input-group">
                <div class="radio radio-info ">
                    <?php
                        if (file_exists($quizQues['qustion_option1'])){
                            $option1 = 'img';
                        }else{
                            $option1 = 'txt';
                        }

                    ?>
                    <input type="radio" onclick="typeChange(this)" {{ $option1=='txt' ? "checked" :"" }} name="questionset[1][option1_radio]qustion_option1_radio" value="text" >
                    <label for="qustion_option1_text">Text</label>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="radio" onclick="typeChange(this)" {{ $option1=='img' ? "checked" :"" }} id="qustion_option1_img" name="questionset[1][option1_radio]qustion_option1_radio" value="image">
                    <label for="qustion_option1_img">Image</label>
                </div>
            </div>
            <div class="input-group">
                <label for="{!! trans('language.answer_first') !!}">
                    {!! trans('language.answer_first') !!}
                </label>
                <div class="toggle-text" {{ $option1=='img' ? "hidden" : "" }}>
                    {!! Form::textarea("questionset[1][option1]",isset($quizQues['qustion_option1']) ? $quizQues['qustion_option1']: '', ['class' => 'form-control cktext','placeholder'=>trans('language.answer_first'),'rows'=>'20','id'=>'qustion_option1']) !!}
                </div>
                @php
                    if( !empty($quizQues['qustion_option1']) ){
                        $categoryIcon = url($quizQues['qustion_option1']);
                    }  else {
                        $categoryIcon = url("/public/admin/nopreview-available.jpg");
                    } 
                @endphp
                <div class="toggle-image" {{ $option1=='img' ? "" : "hidden" }}>
                    <input type="file" name="questionset[1][option1]" id="input-file-disable-remove" class="dropify" data-show-remove="false" data-default-file="{!! $categoryIcon !!}"/>
                </div>
            </div>
        </div>



        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 toggle-text-image">
            <div class="input-group">
                <div class="radio radio-info ">
                    <?php
                        if (file_exists($quizQues['qustion_option2'])){
                            $option2 = 'img';
                        }else{
                            $option2 = 'txt';
                        }

                    ?>
                    <input type="radio" onclick="typeChange(this)" {{ $option2=='txt' ? "checked" : ""}} name="questionset[1][option2_ratio]" value="text" >
                    <label for="qustion_option2_text">Text</label>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="radio" onclick="typeChange(this)" {{ $option2=='img' ? "checked" : ""}} name="questionset[1][option2_ratio]" value="image">
                    <label for="qustion_option2_img">Image</label>
                </div>
            </div>
            <div class="input-group">
                <label for="{!! trans('language.answer_second') !!}">
                    {!! trans('language.answer_second') !!}
                </label>
                <div class="toggle-text" {{ $option2=='txt' ? "" : "hidden"}}>
                    {!! Form::textarea("questionset[1][option2]",isset($quizQues['qustion_option2']) ? $quizQues['qustion_option2']: '', ['class' => 'form-control cktext','placeholder'=>trans('language.answer_second'),'rows'=>'20']) !!}
                </div>
                @php
                    if( !empty($quizQues['qustion_option2']) ){
                        $categoryIcon = url($quizQues['qustion_option2']);
                    }  else {
                        $categoryIcon = url("/public/admin/nopreview-available.jpg");
                    } 
                @endphp
                <div class="toggle-image" {{ $option2=='txt' ? "hidden" : ""}}>
                    <input type="file" name="questionset[1][option2]" id="input-file-disable-remove" class="dropify" data-show-remove="false" data-default-file="{!! $categoryIcon !!}"/>
                </div>
            </div>
        </div>


        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 toggle-text-image">
            <div class="input-group">
                <div class="radio radio-info ">
                    <?php
                        if (file_exists($quizQues['qustion_option3'])){
                            $option3 = 'img';
                        }else{
                            $option3 = 'txt';
                        }

                    ?>
                    <input type="radio" onclick="typeChange(this)" {{ $option3=='txt' ? "checked" : "" }} name="questionset[1][option3_radio]qustion_option3_radio" value="text" >
                    <label for="qustion_option3_text">Text</label>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="radio" onclick="typeChange(this)" {{ $option3=='img' ? "checked" : "" }} name="questionset[1][option3_radio]qustion_option3_radio" value="image">
                    <label for="qustion_option3_img">Image</label>
                </div>
            </div>
            <div class="input-group">
                <label for="{!! trans('language.answer_third') !!}">
                    {!! trans('language.answer_third') !!}
                </label>
                <div class="toggle-text" {{ $option3=='img' ? "hidden" : "" }}>
                    {!! Form::textarea("questionset[1][option3]",isset($quizQues['qustion_option3']) ? $quizQues['qustion_option3']: '', ['class' => 'form-control cktext','placeholder'=>trans('language.answer_third'),'rows'=>'20']) !!}
                </div>
                @php
                    if( !empty($quizQues['qustion_option3']) ){
                        $categoryIcon = url($quizQues['qustion_option3']);
                    }  else {
                        $categoryIcon = url("/public/admin/nopreview-available.jpg");
                    } 
                @endphp
                <div class="toggle-image" {{ $option3=='txt' ? "hidden" : "" }}>
                    <input type="file" name="questionset[1][option3]" id="input-file-disable-remove" class="dropify" data-show-remove="false" data-default-file="{!! $categoryIcon !!}"/>
                </div>
            </div>
        </div>


        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 toggle-text-image">
            <div class="input-group">
                <div class="radio radio-info ">
                    <?php
                        if (file_exists($quizQues['qustion_option4'])){
                            $option4 = 'img';
                        }else{
                            $option4 = 'txt';
                        }

                    ?>
                    <input type="radio" onclick="typeChange(this)" {{ $option4=='txt' ? "checked" : "" }} name="questionset[1][option4_radio]qustion_option4_radio" value="text" >
                    <label for="qustion_option4_text">Text</label>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="radio" onclick="typeChange(this)" {{ $option4=='img' ? "checked" : "" }} name="questionset[1][option4_radio]qustion_option4_radio" value="image">
                    <label for="qustion_option4_img">Image</label>
                </div>
            </div>
            <div class="input-group">
                <label for="{!! trans('language.answer_forth') !!}">
                    {!! trans('language.answer_forth') !!}
                </label>
                <div class="toggle-text" {{ $option4=='img' ? "hidden" : "" }}>
                    {!! Form::textarea("questionset[1][option4]",isset($quizQues['qustion_option4']) ? $quizQues['qustion_option4']: '', ['class' => 'form-control cktext','placeholder'=>trans('language.answer_forth'),'rows'=>'20']) !!}
                </div>
                @php
                    if( !empty($quizQues['qustion_option4']) ){
                        $categoryIcon = url($quizQues['qustion_option4']);
                    }  else {
                        $categoryIcon = url("/public/admin/nopreview-available.jpg");
                    } 
                @endphp
                <div class="toggle-image" {{ $option4=='txt' ? "hidden" : "" }}>
                    <input type="file" name="questionset[1][option4]" id="input-file-disable-remove" class="dropify" data-show-remove="false" data-default-file="{!! $categoryIcon !!}"/>
                </div>
            </div>
        </div>


        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 toggle-text-image">
            <div class="input-group">
                <div class="radio radio-info ">
                    <?php
                        if (file_exists($quizQues['qustion_option5'])){
                            $option5 = 'img';
                        }else{
                            $option5 = 'txt';
                        }

                    ?>
                    <input type="radio" onclick="typeChange(this)" {{ $option5=='txt' ? "checked" : "" }} checked name="questionset[1][option5_radio]qustion_option5_radio" value="text" >
                    <label for="qustion_option5_text">Text</label>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="radio" onclick="typeChange(this)" {{ $option5=='img' ? "checked" : "" }} name="questionset[1][option5_radio]qustion_option5_radio" value="image">
                    <label for="qustion_option5_img">Image</label>
                </div>
            </div>
            <div class="input-group">
                <label for="{!! trans('language.answer_fifth') !!}">
                    {!! trans('language.answer_fifth') !!}
                </label>
                <div class="toggle-text" {{ $option5=='img' ? "hidden" : "" }}>
                    {!! Form::textarea("questionset[1][option5]",isset($quizQues['qustion_option5']) ? $quizQues['qustion_option5']: '', ['class' => 'form-control cktext','placeholder'=>trans('language.answer_fifth'),'rows'=>'20']) !!}
                </div>
                @php
                    if( !empty($quizQues['qustion_option5']) ){
                        $categoryIcon = url($quizQues['qustion_option5']);
                    }  else {
                        $categoryIcon = url("/public/admin/nopreview-available.jpg");
                    } 
                @endphp
                <div class="toggle-image" {{ $option3=='txt' ? "hidden" : "" }}>
                    <input type="file" name="questionset[1][option5]" id="input-file-disable-remove" class="dropify" data-show-remove="false" data-default-file="{!! $categoryIcon !!}"/>
                </div>
            </div>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="input-group">
                <label for="{!! trans('language.correct_option') !!}">
                    {!! trans('language.correct_option') !!}
                </label>
                @php
                    $correct = $quizQues['qustion_correct_answer'];
                @endphp
                <div class="radio radio-info">
                    <input type="radio" name="questionset[1][correct]" {{ $correct==1 ? "checked" : "" }} value="1">
                    <label for="correct_option1">Option 1st</label>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="radio" name="questionset[1][correct]" {{ $correct==2 ? "checked" : "" }} value="2">
                    <label for="correct_option2">Option 2nd</label>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="radio" name="questionset[1][correct]" {{ $correct==3 ? "checked" : "" }} value="3">
                    <label for="correct_option3">Option 3rd</label>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="radio" name="questionset[1][correct]" {{ $correct==4 ? "checked" : "" }} value="4">
                    <label for="correct_option4">Option 4th</label>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="radio" name="questionset[1][correct]" {{ $correct==5 ? "checked" : "" }} value="5">
                    <label for="correct_option5">Option 5th</label>
                </div>
            </div>
        </div>


        </div>
        <!-- Main Question Panel Close -->
        </div>


    </div>
</div>
<div class="row">
	<div class=" col-sm-9">
	    <button type="submit" name="submit" class="btn btn-info waves-effect waves-light m-t-10">{{  $submit_button }}</button>
	</div>
</div>
<div class="clearfix"></div>
<script type="text/javascript">
    var key1 = $('#keyinput').val();
    key = parseInt(key1);
    $("#AddQuestionBtn").click(function()
    { 
      key = key+1;
      $("#keyinput").val(key);
      $("#mainQuestionPanel").append(`
        <div id="set-`+key+`">
        <input type="text" name="questionset[`+key+`]" hidden>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <button class="btn btn-danger pull-right" onclick="rmQuiz(`+key+`)" type="button" id="rmQuestionBtn[`+key+`]">Remove</button>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 toggle-text-image">
            <div class="input-group">
                <div class="radio radio-info ">
                    <input type="radio" onclick="typeChange(this)" id="quiz_question_text@!" checked name="questionset[`+key+`][textimg]qustion_qustion_radio" value="text" >
                    <label >Text</label>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="radio" onclick="typeChange(this)" id="quiz_question_img@!" name="questionset[`+key+`][textimg]qustion_qustion_radio" value="image">
                    <label>Image</label>
                </div>
            </div>
            <div class="input-group">
                <label for="{!! trans('language.question_title') !!}">
                    {!! trans('language.question_title') !!}
                </label>
                <div class="toggle-text">
                    {!! Form::textarea("questionset[`+key+`][textquestion]qustion_qustion",isset($quizQues['qustion_qustion']) ? $quizQues['qustion_qustion']: '', ['class' => 'form-control cktext','placeholder'=>trans('language.question_title'),'rows'=>'20']) !!}
                </div>
                @php
                    if(!empty($quizQues['qustion_qustion']) ){
                        $categoryIcon = url($quizQues['qustion_qustion']);
                    }else{
                        $categoryIcon = url("/public/admin/nopreview-available.jpg");
                    } 
                @endphp
                <div class="toggle-image" hidden>
                    <input type="file" name="questionset[`+key+`][imgquestion]qustion_qustion" id="input-file-disable-remove" class="dropify" data-show-remove="false" data-default-file="{!! $categoryIcon !!}"/>
                </div>
            </div>
        </div>


        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 toggle-text-image">
            <div class="input-group">
                <div class="radio radio-info ">
                    <input type="radio" onclick="typeChange(this)" checked name="questionset[`+key+`][detailtextimg]qustion_optionDetails_radio" value="text" >
                    <label for="qustion_optionDetails_text">Text</label>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="radio" onclick="typeChange(this)" name="questionset[`+key+`][detailtextimg]qustion_optionDetails_radio" value="image">
                    <label for="qustion_optionDetails_img">Image</label>
                </div>
            </div>
            <div class="input-group">
                <label for="{!! trans('language.question_details') !!}">
                    {!! trans('language.question_details') !!}
                </label>
                <div class="toggle-text">
                    <textarea name="questionset[`+key+`][textselect]question_section_part" value="{{ isset($quizQues['question_section_part']) ? $quizQues['question_section_part']: '' }}" class="form-control CKEDITOR.replaceClass = cktext" placeholder="{{ trans('language.question_details') }}" rows="20" id="question_section_part"></textarea>
                </div>
                @php
                    if( !empty($quizQues['question_section_part']) ){
                        $categoryIcon = url($quizQues['question_section_part']);
                    }  else {
                        $categoryIcon = url("/public/admin/nopreview-available.jpg");
                    } 
                @endphp
                <div class="toggle-image" hidden>
                    <input type="file" name="questionset[`+key+`][imgselect]question_section_part" id="input-file-disable-remove" class="dropify" data-show-remove="false" data-default-file="{!! $categoryIcon !!}"/>
                </div>
            </div>
        </div>


        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 toggle-text-image">
            <div class="input-group">
                <div class="radio radio-info ">
                    <input type="radio" onclick="typeChange(this)" checked name="questionset[`+key+`][option1_radio]qustion_option1_radio" value="text" >
                    <label for="qustion_option1_text">Text</label>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="radio" onclick="typeChange(this)" id="qustion_option1_img" name="questionset[`+key+`][option1_radio]qustion_option1_radio" value="image">
                    <label for="qustion_option1_img">Image</label>
                </div>
            </div>
            <div class="input-group">
                <label for="{!! trans('language.answer_first') !!}">
                    {!! trans('language.answer_first') !!}
                </label>
                <div class="toggle-text">
                    {!! Form::textarea("questionset[`+key+`][option1]qustion_option1",isset($quizQues['qustion_option1']) ? $quizQues['qustion_option1']: '', ['class' => 'form-control cktext','placeholder'=>trans('language.answer_first'),'rows'=>'20']) !!}
                </div>
                @php
                    if( !empty($quizQues['qustion_option1']) ){
                        $categoryIcon = url($quizQues['qustion_option1']);
                    }  else {
                        $categoryIcon = url("/public/admin/nopreview-available.jpg");
                    } 
                @endphp
                <div class="toggle-image" hidden>
                    <input type="file" name="questionset[`+key+`][option1]qustion_option1" id="input-file-disable-remove" class="dropify" data-show-remove="false" data-default-file="{!! $categoryIcon !!}"/>
                </div>
            </div>
        </div>



        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 toggle-text-image">
            <div class="input-group">
                <div class="radio radio-info ">
                    <input type="radio" onclick="typeChange(this)" checked name="questionset[`+key+`][option2_radio]qustion_option2_radio" value="text" >
                    <label for="qustion_option2_text">Text</label>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="radio" onclick="typeChange(this)" name="questionset[`+key+`][option2_radio]qustion_option2_radio" value="image">
                    <label for="qustion_option2_img">Image</label>
                </div>
            </div>
            <div class="input-group">
                <label for="{!! trans('language.answer_second') !!}">
                    {!! trans('language.answer_second') !!}
                </label>
                <div class="toggle-text">
                    {!! Form::textarea("questionset[`+key+`][option2]qustion_option2",isset($quizQues['qustion_option2']) ? $quizQues['qustion_option2']: '', ['class' => 'form-control cktext','placeholder'=>trans('language.answer_second'),'rows'=>'20']) !!}
                </div>
                @php
                    if( !empty($quizQues['qustion_option2']) ){
                        $categoryIcon = url($quizQues['qustion_option2']);
                    }  else {
                        $categoryIcon = url("/public/admin/nopreview-available.jpg");
                    } 
                @endphp
                <div class="toggle-image" hidden>
                    <input type="file" name="questionset[`+key+`][option2]qustion_option2" id="input-file-disable-remove" class="dropify" data-show-remove="false" data-default-file="{!! $categoryIcon !!}"/>
                </div>
            </div>
        </div>


        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 toggle-text-image">
            <div class="input-group">
                <div class="radio radio-info ">
                    <input type="radio" onclick="typeChange(this)" checked name="questionset[`+key+`][option3_radio]qustion_option3_radio" value="text" >
                    <label for="qustion_option3_text">Text</label>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="radio" onclick="typeChange(this)" name="questionset[`+key+`][option3_radio]qustion_option3_radio" value="image">
                    <label for="qustion_option3_img">Image</label>
                </div>
            </div>
            <div class="input-group">
                <label for="{!! trans('language.answer_third') !!}">
                    {!! trans('language.answer_third') !!}
                </label>
                <div class="toggle-text">
                    {!! Form::textarea("questionset[`+key+`][option3]qustion_option3",isset($quizQues['qustion_option3']) ? $quizQues['qustion_option3']: '', ['class' => 'form-control cktext','placeholder'=>trans('language.answer_third'),'rows'=>'20']) !!}
                </div>
                @php
                    if( !empty($quizQues['qustion_option3']) ){
                        $categoryIcon = url($quizQues['qustion_option3']);
                    }  else {
                        $categoryIcon = url("/public/admin/nopreview-available.jpg");
                    } 
                @endphp
                <div class="toggle-image" hidden>
                    <input type="file" name="questionset[`+key+`][option3]qustion_option3" id="input-file-disable-remove" class="dropify" data-show-remove="false" data-default-file="{!! $categoryIcon !!}"/>
                </div>
            </div>
        </div>


        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 toggle-text-image">
            <div class="input-group">
                <div class="radio radio-info ">
                    <input type="radio"  onclick="typeChange(this)" name="questionset[`+key+`][option4_radio]qustion_option4_radio" checked value="text" >
                    <label for="qustion_option4_text">Text</label>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="radio" onclick="typeChange(this)" name="questionset[`+key+`][option4_radio]qustion_option4_radio" value="image">
                    <label for="qustion_option4_img">Image</label>
                </div>
            </div>
            <div class="input-group">
                <label for="{!! trans('language.answer_forth') !!}">
                    {!! trans('language.answer_forth') !!}
                </label>
                <div class="toggle-text">
                    {!! Form::textarea("questionset[`+key+`][option4]qustion_option4",isset($quizQues['qustion_option4']) ? $quizQues['qustion_option4']: '', ['class' => 'form-control cktext','placeholder'=>trans('language.answer_forth'),'rows'=>'20']) !!}
                </div>
                @php
                    if( !empty($quizQues['qustion_option4']) ){
                        $categoryIcon = url($quizQues['qustion_option4']);
                    }  else {
                        $categoryIcon = url("/public/admin/nopreview-available.jpg");
                    } 
                @endphp
                <div class="toggle-image" hidden>
                    <input type="file" name="questionset[`+key+`][option4]qustion_option4" i class="dropifyd="input-file-disable-move"" data-show-remove="false" data-default-file="{!! $categoryIcon !!}"/>
                </div>
            </div>
        </div>


        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 toggle-text-image">
            <div class="input-group">
                <div class="radio radio-info ">
                    <input type="radio" onclick="typeChange(this)" checked name="questionset[`+key+`][option5_radio]qustion_option5_radio" value="text" >
                    <label for="qustion_option5_text">Text</label>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="radio" onclick="typeChange(this)" name="questionset[`+key+`][option5_radio]qustion_option5_radio" value="image">
                    <label for="qustion_option5_img">Image</label>
                </div>
            </div>
            <div class="input-group">
                <label for="{!! trans('language.answer_fifth') !!}">
                    {!! trans('language.answer_fifth') !!}
                </label>
                <div class="toggle-text">
                    {!! Form::textarea("questionset[`+key+`][option5]qustion_option5",isset($quizQues['qustion_option5']) ? $quizQues['qustion_option5']: '', ['class' => 'form-control cktext','placeholder'=>trans('language.answer_fifth'),'rows'=>'20']) !!}
                </div>
                @php
                    if( !empty($quizQues['qustion_option5']) ){
                        $categoryIcon = url($quizQues['qustion_option5']);
                    }  else {
                        $categoryIcon = url("/public/admin/nopreview-available.jpg");
                    } 
                @endphp
                <div class="toggle-image" hidden>
                    <input type="file" name="questionset[`+key+`][option5]qustion_option5" id="input-file-disable-remove" class="dropify" data-show-remove="false" data-default-file="{!! $categoryIcon !!}"/>
                </div>
            </div>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="input-group">
                <label for="{!! trans('language.correct_option') !!}">
                    {!! trans('language.correct_option') !!}
                </label>

                <div class="radio radio-info">
                    <input type="radio" name="questionset[`+key+`][correct]correct_option" value="1">
                    <label for="correct_option1">Option 1st</label>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="radio" name="questionset[`+key+`][correct]correct_option" value="2">
                    <label for="correct_option2">Option 2nd</label>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="radio" name="questionset[`+key+`][correct]correct_option" value="3">
                    <label for="correct_option3">Option 3rd</label>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="radio" name="questionset[`+key+`][correct]correct_option" value="4">
                    <label for="correct_option4">Option 4th</label>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="radio" name="questionset[`+key+`][correct]correct_option" value="5">
                    <label for="correct_option5">Option 5th</label>
                </div>
            </div>
        </div>
        </div>`);
            $('#keyinput').val(key);
    });

function remove_data(elemt)
{
    var key1 = $('#keyinput').val();
    key = parseInt(key1);
    $('#keyinput').val(key-1);
    $(elemt).parent().remove();
}
</script>
<script type="text/javascript">
    function typeChange(elem){
        var type    = $(elem).val();
        var div     = $(elem).parents("div.toggle-text-image");

        if (type=='text'){
            $(div).find('div.toggle-text').show();
            $(div).find('div.toggle-image').hide();
        }else{
            $(div).find('div.toggle-text').hide();
            $(div).find('div.toggle-image').show();
        }
    }

    function removeQuestion(elems) {

    }


// $('body').on('focus',".datetimepicker_class", function(){
    // $(this).
// });
</script>