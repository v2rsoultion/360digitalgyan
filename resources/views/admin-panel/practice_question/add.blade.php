@extends('admin-panel.layout.header')

@section('content')

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">{!! $page_title !!}</h4> 
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                
                <ol class="breadcrumb">
                    <li><a href="{!! URL::to('admin-panel/dashboard') !!}">Dashboard</a></li>
                    <li><a href="#">{!! trans('language.practice_quiz_questions') !!}</a></li>
                    <li class="active">{!! $page_title !!}</li> 
                </ol>
            </div>
            @if($errors->any())
                <p class="text-muted m-b-10 col-lg-12 col-md-12 col-sm-12 col-xs-12 font-13 red">{{$errors->first()}}</p>
            @endif
        </div>
        <!-- /row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box white-box-modify">
                    {!! Form::open(['files'=>TRUE,'id' => 'Quiz-questions-form' , 'class'=>'form-horizontal','url' =>$save_url]) !!}
                        @include('admin-panel.practice_question._form',['submit_button' => $submit_button])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

<script>

 $('.cktext').each(function(e){
        CKEDITOR.replace(this);
});


    $(function() {

        $(".select2").select2();

        $(".tch").TouchSpin({
            min: 0,
            max: 1000000000,
        });

    });

    jQuery(document).ready(function () {

        jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z\s]+$/i.test(value);
        }, "Only alphabetical characters");


        $("#Quiz-questions-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */
            ignore: [],
            debug: false,
            rules: {
                category_id: {
                    required: true,
                },
                sub_category_id: {
                    required: true,
                }
            },
            messages: {
                category_id: {
                    required: "Please select question category",
                },
                sub_category_id: {
                    required: "Please select question sub category",
                }              
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });


        $('.cktext').each(function() {
            $(this).rules('add', {
                required: true,
                minlength: 3,
                // number: true,
                messages: {
                    required:  "please fill filed",
                    // number:  "your custom message"
                }
            });
        });
        
    });

    

</script>

<script type="text/javascript">
    $(document).ready(function() {
        // Basic
        $('.dropify').dropify();
        // Translated
        $('.dropify-fr').dropify({
            messages: {
                default: 'Drag and drop a file here or click',
                replace: 'Drag and drop or click to replace',
                remove: 'Remove',
                error: 'Ooops, something wrong happended.'
            }
        });
        // Used events
        var drEvent = $('#input-file-events').dropify();
        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });
        drEvent.on('dropify.afterClear', function(event, element) {
            alert('File deleted');
        });
        drEvent.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });
        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        })
    });
</script>
<script>
    $('#AddQuestionBtn').click(function(){
        $('.cktext').focus(function(){
            CKEDITOR.replace(this);
        });
        $('.cktext').each(function() {
            $(this).rules('add', {
                required: true,
                minlength: 3,
                // number: true,
                messages: {
                    required:  "please fill filed",
                    // number:  "your custom message"
                }
            });
        });
    });
</script>

<script type="text/javascript">
    function getSubcategories(id)
   {
      BASE_URL = "{{ url('/') }}";
      $.ajax({
         url: BASE_URL+"/admin-panel/get-sub-categories/"+id,
         success: function(result){
            $('#qustion_sub_sec_id').html(result.data);
         }
      });
   }
</script>
<script type="text/javascript">
    function rmQuiz(id)
    {
        $("#set-"+id).remove();
    }
</script>
<script type="text/javascript">
   // CKEDITOR.replace('questionset[1][textselect]');
    </script>
@endsection