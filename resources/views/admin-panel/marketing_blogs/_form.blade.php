<div class="row">
    <div class="col-sm-12">
        
        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
            <div class="input-group">
                <label for="{!! trans('language.marketing_title') !!}">
                    {!! trans('language.marketing_title') !!}
                </label>

                {!! Form::text('marketing_title',isset($blog['marketing_title']) ? $blog['marketing_title']: '', ['class' => 'form-control','placeholder'=>trans('language.marketing_title')]) !!}

                @if($errors->has('marketing_title'))
                    <p class="help-block">
                        {{ $errors->first('marketing_title') }}
                    </p>
                @endif
            </div>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <div class="input-group">
                <br>
               <div class="checkbox checkbox-info checkbox-circle">
                    
                    <input id="checkbox2" type="checkbox" name="is_new" value="1" {{ $blog['is_new']==1 ? 'checked' : '' }} >
                    <label for="checkbox2"> {!! trans('language.marketing_blog') !!} New or Not </label>
                </div>
            </div>
        </div>

        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
            <div class="input-group">
                <label for="{!! trans('language.marketing_description') !!}">
                    {!! trans('language.marketing_description') !!}
                </label>

                {!! Form::textarea('marketing_description',isset($blog['marketing_description']) ? $blog['marketing_description']: '', ['class' => 'form-control tinytext','placeholder'=>trans('language.marketing_description'),'rows'=>'20','id'=>'marketing_description']) !!}

                @if($errors->has('marketing_description'))
                    <p class="help-block">
                        {{ $errors->first('marketing_description') }}
                    </p>
                @endif
            </div>
        </div>

        <div class="col-md-4 col-xs-12">
            <div class="white-box">
                <label for="input-file-disable-remove">{{ trans('language.marketing_blog') }} Image</label>

                @php
                    if(!empty($blog['marketing_image'])){
                        $newsImage = url('uploads/'.$blog['marketing_image']);
                    }else{
                        $newsImage = url("/public/admin/nopreview-available.jpg");
                    } 
                @endphp
                
                <input type="file" name="marketing_image" id="input-file-disable-remove" class="dropify" data-show-remove="false" data-default-file="{!! $newsImage !!}"/> 
            </div>
        </div>


    </div>
</div>
<div class="row">
	<div class=" col-sm-9">
	    <button type="submit" name="submit" class="btn btn-info waves-effect waves-light m-t-10">{{  $submit_button }}</button>
	</div>
</div>
<div class="clearfix"></div>