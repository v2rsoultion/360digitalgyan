@extends('admin-panel.layout.header')
@section('content')
<style type="text/css">
    .nums{
        width: 50px;
    }
</style>

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">{!! $page_title !!}</h4>  
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                
                <ol class="breadcrumb">
                    <li><a href="{!! URL::to('admin-panel/dashboard') !!}">Dashboard</a></li>
                    <li><a href="#">{!! trans('language.marketing_blog') !!}</a></li>
                    <li class="active">{!! $page_title !!}</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    @if(session()->has('success'))
                        <p class="alert alert-success text-muted m-b-10 col-lg-12 col-md-12 col-sm-12 col-xs-12 font-13 green">
                            {{ session()->get('success') }}
                        </p>
                    @endif
                    @if($errors->any())
                        <p class="text-muted m-b-10 col-lg-12 col-md-12 col-sm-12 col-xs-12 font-13 red">{{$errors->first()}}</p>
                    @endif
                    <div class="clearfix"></div>
                    {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3">
                                <div class="input-group">
                                    {!! Form::text('marketing_title',isset($category['marketing_title']) ? $category['marketing_title']: '', ['class' => 'form-control','placeholder'=>'marketing blog title']) !!}
                                </div>
                            </div>
                            <div class="col-lg-1 col-md-1">
                                {!! Form::submit('Search', ['class' => 'btn btn-info ','name'=>'Search']) !!}
                            </div>
                            <div class="col-lg-1 col-md-1">
                                {!! Form::button('Clear', ['class' => 'btn btn-info ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <div class="col-sm-12">
                <div class="white-box">
                    <div class="table-responsive">
                        <table class="table table-condensed dataTable " id="category-table">
                            <thead>
                                <tr>
                                    <th class="fa fa-plus"></th>
                                    <th>S.No</th>
                                    <th>Blog title</th>
                                    <th>Blog Image</th>
                                    <th>Blog Description</th>
                                    <th>Is New ?</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.1.2/handlebars.js" type="text/javascript" charset="utf-8"></script>
<script id="details-template" type="text/x-handlebars-template">
    @verbatim
        <table class="table" id="detail" style="background-color:#f6f6f6;">
        </table>
    @endverbatim
</script>


<script>

    $(document).ready(function () {
        var table = $('#category-table').DataTable({
            // dom: 'Blfrtip',
            pageLength: 10,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            ajax: {
                url: '{{url('admin-panel/marketing-blog/data')}}',
                data: function (d) {
                    d.marketing_title = $('input[name=marketing_title]').val();
                }
            },
            columns: [
                {
                    "className": 'details-control fa fa-plus-circle green',
                    "orderable": false,
                    "searchable": false,
                    "data": '',
                    "name": 'marketing_id[]',
                    "data-id": 'marketing_id',
                    "defaultContent": ''
                },
                {data: 'DT_Row_Index', name: 'DT_Row_Index' },
                {data: 'marketing_title', name: 'marketing_title'},
                {data: 'image', name: 'image'},
                {data: 'description', name: 'description'},
                {data:'new',name:'new'},
                {data:'status',name:'status'},
                {data: 'action', name: 'action'},
            ]
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });

        $('#clearBtn').click(function(){
            location.reload();
        })

        var template = Handlebars.compile($("#details-template").html());
        $('#category-table tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = table.row( tr );
            var id = row.data()['marketing_id'];
            
            
            var base_url = "{{ url('/') }}";
            $.ajax({
                url:base_url+"/admin-panel/marketing-blog/detail-data/"+id,
                success:function(result) {
                    $('#detail').html(result);
                }
            });
            if ( row.child.isShown() ) {
            // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
                $(this).removeClass('fa fa-minus-circle red');
                $(this).addClass('fa fa-plus-circle green');
            }
            else {
            // Open this row
                row.child( template(row.data()) ).show();
                tr.addClass('shown');
                $(this).removeClass('fa fa-plus-circle green');
                $(this).addClass('fa fa-minus-circle red');
            }
        });
       
    });

    var elems = document.getElementsByClassName('confirmation');
    var confirmIt = function (e) {
        if (!confirm('Are you sure?')) e.preventDefault();
    };
    for (var i = 0, l = elems.length; i < l; i++) {
        elems[i].addEventListener('click', confirmIt, false);
    }
    

    /* for Quiz status change in category  */
    function changeStatusBlog(id,status){
        var token = '{!!csrf_token()!!}';
        $.ajax(
        {
            url: "{{ url('admin-panel/marketing-blog/marketing-blog-status') }}",
            type: 'GET',
            data: {
                'blog_id': id,
                'blog_status': status,
            },
            success: function (res)
            {
                if (res == "Success")
                {
                    location.reload();
                } else
                {
                    location.reload();
                }
            }
        });
    }

</script>
@endsection




