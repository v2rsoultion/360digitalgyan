<div class="row">
	<div>
        <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
            <div class="input-group">
                <label for="{!! trans('language.banner_title') !!}">
                    {{ trans('language.banner_title') }}
                </label>
                {{ Form::text('banner_title',old('banner_title',isset($banner['banner_title']) ? $banner['banner_title']: ''),["class"=>"form-control","placeholder"=>trans('language.banner_title')]) }}
            </div>
            <div class="input-group">
                <label for="{!! trans('language.category_name') !!}">
                    {!! trans('language.category_name') !!}
                </label>
                <select class="form-control select2 " id="banner_cat_id" name="cat_id" >
                    <option value="">Select Category</option>
                    @foreach ($categories as  $category)
                        <option value="{!! $category->iq_category_id !!}" {{ $banner->banner_cat==$category->iq_category_id ? 'selected' : '' }} >
                            {!! $category->iq_category_name !!}
                        </option>   
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
            <div class="white-box">
                <label for="input-file-disable-remove">
                    {{ trans('language.banner_img') }}
                </label>
                @php
                    if(!empty($banner['banner_image'])){
                        $bannerImage = url('uploads/'.$banner['banner_image']);
                    }else{
                        $bannerImage = url("/public/admin/nopreview-available.jpg");
                    } 
                @endphp
                <input type="file" name="banner_image" id="input-file-disable-remove" class="dropify" data-show-remove="false" data-default-file="{!! $bannerImage !!}"/> 
            </div>
        </div>
	</div>
</div>
<div class="row">
	<div class=" col-sm-9">
	    <button type="submit" name="submit" class="btn btn-info waves-effect waves-light m-t-10">{{  $submit_button }}</button>
	</div>
</div>
<div class="clearfix"></div>