<div class="row">
	<div>
		<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
            <div class="input-group">
                <label for="{!! trans('language.question') !!}">
                    {{ trans('language.question') }}
                </label>
                {{ Form::text('ques_id',$que['ques_id'],['hidden']) }}
                {{ Form::text('ans_cat',$que['ques_main_cat_id'],['hidden']) }}
                <h5><b>{{ $que['user_question'] }}</b></h5>
                <!-- {{ Form::text('tags',old('tags',isset($ques['question_tags']) ? $ques['question_tags']: ''),["class"=>"form-control","placeholder"=>trans('language.ques_tag'),"id"=>"form-tags-3","required"]) }} -->
            </div>
        </div>
        <div class="col-md-9 col-lg-9 col-sm-12 col-xs-12">
            <div class="input-group">
                <label for="{!! trans('language.answer') !!}">
                    {{ trans('language.answer') }}
                </label>
                {{ Form::textarea('answer',old('answer',isset($answer['ques_answer']) ? $answer['ques_answer']: ''),["class"=>"form-control","placeholder"=>trans('language.answer')]) }}
            </div>
        </div>
	</div>
</div>
<div class="row">
	<div class=" col-sm-9">
	    <button type="submit" name="submit" class="btn btn-info waves-effect waves-light m-t-10">{{  $submit_button }}</button>
	</div>
</div>
<div class="clearfix"></div>