@extends('admin-panel.layout.header')
@section('content')



<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">{!! $page_title !!}</h4>  
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                
                <ol class="breadcrumb">
                    <li><a href="{!! URL::to('admin-panel/dashboard') !!}">Dashboard</a></li>
                    <li><a href="#">{!! trans('language.posted_ques_ans') !!}</a></li>
                    <li class="active">{!! $page_title !!}</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    @if(session()->has('success'))
                        <p class="alert alert-success text-muted m-b-10 col-lg-12 col-md-12 col-sm-12 col-xs-12 font-13 green">
                            {{ session()->get('success') }}
                        </p>
                    @endif
                    @if($errors->any())
                        <p class="text-muted m-b-10 col-lg-12 col-md-12 col-sm-12 col-xs-12 font-13 red">{{$errors->first()}}</p>
                    @endif
                    <div class="clearfix"></div>
                    {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal', url('admin-panel/posted-ques-ans/view-posted-ques')]) !!}
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3">
                                <div class="input-group">
                                    {!! Form::text('title', old('title',isset($postedQues['title']) ? $postedQues['title']: ''), ['class' => 'form-control','placeholder'=>trans('language.question'), 'id' => 'question_title','for'=>trans('language.question')]) !!}
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3">
                                <div class="input-group">
                                    <select class="form-control select2 " id="category_id" onchange="getSubcategories(this.value)" name="category_id" >
                                        <option value="">Select Category</option>
                                        @foreach ($Categories as  $category)
                                            <option value="{!! $category->iq_category_id !!}" {{ $postedQues->iq_ques_ans_catid==$category->iq_category_id ? 'selected' : '' }} >
                                                {!! $category->iq_category_name !!}
                                            </option>   
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-1 col-md-1">
                                {!! Form::submit('Search', ['class' => 'btn btn-info ','name'=>'Search']) !!}
                            </div>
                            <div class="col-lg-1 col-md-1">
                                <a href="{{ url('admin-panel/posted-ques-ans/view-posted-ques') }}" class="btn btn-info" >Clear</a>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <div class="col-sm-12">
                <div class="white-box">
                    <div class="table-responsive">
                        <table class="table display table-responsive table-hover DataTable table-condensed" id="posted-ques-table">
                            <thead>
                                <tr>
                                	<th class="fa fa-plus-circle"></th>
                                    <th>S.No</th>
                                    <th>Question</th>
                                    <th>User Name</th>
                                    <th>Category</th>
                                    <th>Date</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            	@foreach($postedQues as $key => $ques)
                            	<tr>
                            		<td class="clickable fa fa-plus-circle" data-toggle="collapse" data-target=".data{{ $key+1 }}" aria-expanded="false" aria-controls="data{{ $key+1 }}" style="cursor: pointer;"></td>
                                	<td>{{ $key+1 }}</td>
                                    <td style="max-height: 100px; overflow: scroll">{{ getTextTransform($ques->user_question,0) }}</td>
                                    <td >{{ $ques['getUser']->seo_users_name }}</td>
                                    <td >{{ $ques['getCategory']->iq_category_name }}</td>
                                    <td>{{ $ques->ques_time }}</td>
                                    <td>
                                    	<div class="radio radio-{{ $ques->question_status==1 ? success : danger }} custom_radiobox">
                                            <input type="radio" name="iqradio{{ $ques->ques_id }}" onClick="changeStatus({{ $ques->ques_id }},{{ $ques->question_status==1 ? 0 : 1 }})" id="iqradio{{ $ans->answer_id }}" value="option4" checked="">
                                            <label for="iqradio{{ $ans->answer_id }}"> </label>
                                        </div>
                                    </td>
                                    <td>
                                    	<?php
                                    		$encrypted_id = get_encrypted_value($ques->ques_id, true);
                                    	?>
                                    	<a href='add-posted-ques/{{ $encrypted_id }}'><i class='fas fa-pencil-alt'></i></a>
                						&nbsp;&nbsp; 
                						<a href='posted-ques-delete/{{ $encrypted_id }}' onclick="return confirm('Are you sure?')"><i class='fas fa-trash'></i></a>
                                    </td>
                                </tr>
                                 <tr class="collapse data{{ $key+1 }}" style="background-color: rgba(121, 121, 121, 0.12)">
                                    <th></th>
    								<th  style="padding-left:10px;">Answer</th>
								    <th>Populer</th>
								    <th>Tags</th>
								    <th>Trending</th>
								    <th>Total Answer</th>
								</tr>
								<tr class="collapse data{{ $key+1 }}" style="background-color: rgba(121, 121, 121, 0.12)">
                                    <td></td>
									<td><a href="{{ url('admin-panel/posted-ques-ans/add-posted-ans/'.$encrypted_id) }}">give answer</a></td>
								    <td>{{ $ques->popular }}</td>
								    <td style="max-height: 100px; overflow: scroll;">{{ $ques->question_tags }}</td>
								    <td>{{ count($ques->trending) }}</td>
								    <td><a href="{{ url('admin-panel/posted-ques-ans/view-posted-ans/'.$encrypted_id) }}">{{ count($ques['answers']) }}</a></td>
								</tr>
                                </tr>
                                @endforeach
                            </table>
                        </table>
                    </div>
                    <div>
                        {{ $postedQues->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    
    function changeStatus(id,status){
        var token = '{!!csrf_token()!!}';
        if (confirm('Are you sure you want change this !!')){
        $.ajax(
        {
            url: "{{ url('admin-panel/posted-ques-ans/posted-ques-status') }}",
            type: 'GET',
            data: {
                'ques_id': id,
                'ques_status': status,
            },
            success: function (res)
            {
                if (res == "Success")
                {
                    location.reload();
                } else
                {
                    location.reload();
                }
            }
        });
        }
    }

    var elems = document.getElementsByClassName('confirmation');
    var confirmIt = function (e) {
        if (!confirm('Are you sure?')) e.preventDefault();
    };
    for (var i = 0, l = elems.length; i < l; i++) {
        elems[i].addEventListener('click', confirmIt, false);
    }

    function changetrend(id,trending){
    	var token = '{!!csrf_token()!!}';
    	$.ajax(
    	{
    		url:"{{ url('admin-panel/posted-ques-ans/posted-ques-trend') }}",
    		type: 'GET',
    		data: {
    			'trend_id' : id,
    			'trend_status': trending,
    		},
    		success: function (res)
    		{
    			if (res == "Success") {
    				location.reload();
    			} else {
    				location.reload();
    			}
    		}
    	});
    }
   </script>


@endsection