@extends('admin-panel.layout.header')

@section('content')

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">{!! $page_title !!}</h4> 
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="{!! URL::to('admin-panel/dashboard') !!}">Dashboard</a></li>
                    <li><a href="#">{!! trans('language.posted_ques_ans') !!}</a></li>
                    <li class="active">{!! $page_title !!}</li> 
                </ol>
            </div>
            @if($errors->any())
                <p class="text-muted m-b-10 col-lg-12 col-md-12 col-sm-12 col-xs-12 font-13 red">{{$errors->first()}}</p>
            @endif
        </div>
        <!-- /row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box white-box-modify">
                    {!! Form::open(['files'=>TRUE,'id' => 'posted-question-form' , 'class'=>'form-horizontal','url' =>$save_url]) !!}
                    	@include('admin-panel.posted-ques-ans.form',['submit_button' => $submit_button])
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
CKEDITOR.replace('question');

    $(function() {

        $(".select2").select2();

        $(".tch").TouchSpin({
            min: 0,
            max: 1000000000,
        });
    });

    jQuery(document).ready(function () {

        jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z\s]+$/i.test(value);
        }, "Only alphabetical characters");

        $("#posted-question-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",

            /* @validation rules 
             ------------------------------------------ */
            ignore: [],
            debug: false,
            rules: {
                ques_id: {
                    required: true,
                },
                question: {
                    required: true,
                },
                tags: {
                    required: true,
                }

            },
            messages: {
                question: {
                    required: "Please enter Question",
                },
                tags: {
                    required: "Please enter Tag",
                }             
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });
        
    });

    

</script>

<script type="text/javascript">
    $(document).ready(function() {
        // Basic
        $('.dropify').dropify();
        // Translated
        $('.dropify-fr').dropify({
            messages: {
                default: 'Drag and drop a file here or click',
                replace: 'Drag and drop or click to replace',
                remove: 'Remove',
                error: 'Ooops, something wrong happended.'
            }
        });
        // Used events
        var drEvent = $('#input-file-events').dropify();
        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });
        drEvent.on('dropify.afterClear', function(event, element) {
            alert('File deleted');
        });
        drEvent.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });
        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        })
    });
</script>
{!! Html::script('public/frontend/js/jquery.tagsinput-revisited.js') !!}
<script type="text/javascript">
$(function()
{
  $('#form-tags-3').tagsInput({
    'unique': true,
    'minChars': 2,
    // 'maxChars': 10,
    // 'limit': 20,
    'validationPattern': new RegExp('^[a-zA-Z]+$')
  });
});
</script>

@endsection