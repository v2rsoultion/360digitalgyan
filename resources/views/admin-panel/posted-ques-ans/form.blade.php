<div class="row">
	<div>
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="input-group">
                <label for="{!! trans('language.category_name') !!}">
                    {!! trans('language.category_name') !!}
                </label>
                <select class="form-control select2 " id="trick_id" name="ques_cat_id" required="" >
                    <option value="">Select Category</option>
                    @foreach ($categories as  $category)
                        <option value="{!! $category->iq_category_id !!}" {{ $ques->ques_main_cat_id==$category->iq_category_id ? 'selected' : '' }} >
                            {!! $category->iq_category_name !!}
                        </option>   
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
            <div class="input-group">
                <label for="{!! trans('language.tag') !!}">
                    {{ trans('language.tag') }}
                </label>
                {{ Form::text('tags',old('tags',isset($ques['question_tags']) ? $ques['question_tags']: ''),["class"=>"form-control","placeholder"=>trans('language.ques_tag'),"id"=>"form-tags-3","required"]) }}
            </div>
        </div>
        <div class="col-md-10 col-lg-10 col-sm-12 col-xs-12">
            <div class="input-group">
                <label for="{!! trans('language.video_des') !!}">
                    {{ trans('language.question') }}
                </label>
                {{ Form::textarea('question',old('question',isset($ques['user_question']) ? $ques['user_question']: ''),["class"=>"form-control","placeholder"=>trans('language.question')]) }}
            </div>
        </div>
	</div>
</div>
<div class="row">
	<div class=" col-sm-9">
	    <button type="submit" name="submit" class="btn btn-info waves-effect waves-light m-t-10">{{  $submit_button }}</button>
	</div>
</div>
<div class="clearfix"></div>