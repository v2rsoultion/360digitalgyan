@extends('admin-panel.layout.header')
@section('content')

<!-- {{ Html::style('http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.0/css/jquery.dataTables.css') }}
{{ Html::style('http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.0/css/jquery.dataTables_themeroller.css') }} -->

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">{!! $page_title !!}</h4>  
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                
                <ol class="breadcrumb">
                    <li><a href="{!! URL::to('admin-panel/dashboard') !!}">Dashboard</a></li>
                    <li><a href="#">{!! trans('language.posted_ques_ans') !!}</a></li>
                    <li class="active">{!! $page_title !!}</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    @if(session()->has('success'))
                        <p class="alert alert-success text-muted m-b-10 col-lg-12 col-md-12 col-sm-12 col-xs-12 font-13 green">
                            {{ session()->get('success') }}
                        </p>
                    @endif
                    @if($errors->any())
                        <p class="text-muted m-b-10 col-lg-12 col-md-12 col-sm-12 col-xs-12 font-13 red">{{$errors->first()}}</p>
                    @endif
                    <div class="clearfix"></div>
                    {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal', url=>'admin-panel/posted-ques-ans/view-posted-ans']) !!}
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-3">
                                <div class="input-group">
                                    {!! Form::text('answer', old('answer',isset($postedAns['answer']) ? $postedAns['answer']: ''), ['class' => 'form-control','placeholder'=>trans('language.answer'), 'id' => 'ans_title','for'=>trans('language.answer')]) !!}
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3">
                                <div class="input-group">
                                    <select class="form-control select2 " id="category_id" name="category_id" >
                                        <option value="">Select Category</option>
                                        @foreach ($Categories as  $category)
                                            <option value="{!! $category->iq_category_id !!}" {{ $postedAns['main_ans_cat']==$category->iq_category_id ? 'selected' : '' }} >
                                                {!! $category->iq_category_name !!}
                                            </option>   
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-1 col-md-1">
                                {!! Form::submit('Search', ['class' => 'btn btn-info ','name'=>'Search']) !!}
                            </div>
                            <div class="col-lg-1 col-md-1">
                               <a href="{{ url('admin-panel/posted-ques-ans/view-posted-ans') }}" class="btn btn-info" >Clear</a>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <div class="col-sm-12">
                <div class="white-box">
                    <div class="table-responsive">
                        <table class="table DataTable" id="posted-ques-table">
                            <thead>
                                <tr>
                                    <th>S.No</th>
                                    <th>Question</th>
                                    <th>Answer</th>
                                    <th>User Name</th>
                                    <th>Category</th>
                                    <th>Date</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            	@foreach($postedAns as $key => $ans)
                            	<tr>
                                	<td>{{ $key+1 }}</td>
                                    <td style="max-width: 300px;max-height: 100px; overflow: scroll">{{ getTextTransform($ans['getAnsQues']->user_question,0) }}</td>
                                    <td style="max-width: 300px;max-height: 100px; overflow: scroll">{{ getTextTransform($ans->ques_answer,0) }}</td>
                                    <td >{{ $ans['getAnsUser']->seo_users_name }}</td>
                                    <td >{{ $ans['getAnsCat']->iq_category_name }}</td>
                                    <td>{{ $ans->ans_time }}</td>
                                    <td>
                                    	<div class="radio radio-{{ $ans->answer_status==1 ? success : danger }} custom_radiobox">
                                            <input type="radio" name="iqradio{{ $ans->answer_id }}" onClick="changeStatus({{ $ans->answer_id }},{{ $ans->answer_status==1 ? 0 : 1 }})" id="iqradio{{ $ans->answer_id }}" value="option4" checked="">
                                            <label for="iqradio{{ $ans->answer_id }}"> </label>
                                        </div>
                                    </td>
                                    <td>
                                    	<?php
                                         $ques_id      = get_encrypted_value($ans['getAnsQues']->ques_id,true);
                                     		$encrypted_id = get_encrypted_value($ans->answer_id, true);
                                    	?>
                                        <input type="text" name="answer_id" value="{{ $ans->answer_id }}" hidden>
                                        <input type="text" name="ques_id" value="{{ $ans['getAnsQues']->ques_id }}" hidden>
                                    	<a href='add-posted-ans/{{ $ques_id }}/{{ $encrypted_id }}'><i class='fas fa-pencil-alt'></i></a>
                						&nbsp;&nbsp; 
                						<a href='posted-ans-delete/{{ $encrypted_id }}' onclick="return confirm('Are you sure?')"><i class='fas fa-trash'></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            </table>
                        </table>
                    </div>
                    <div>
                        {{ $postedAns->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    // $(document).ready(function () {
    //     $('#posted-ques-table').DataTable();
    // });
</script>

{{ Html::script('https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js') }}
{{ Html::script('http://cdn.datatables.net/1.10.3/js/jquery.dataTables.min.js') }}
{{ Html::script('http://cdn.datatables.net/responsive/1.0.2/js/dataTables.responsive.js') }}
{{ Html::script('http://cdn.datatables.net/plug-ins/a5734b29083/integration/bootstrap/3/dataTables.bootstrap.js') }}

<script>

    var elems = document.getElementsByClassName('confirmation');
    var confirmIt = function (e) {
        if (!confirm('Are you sure?')) e.preventDefault();
    };
    for (var i = 0, l = elems.length; i < l; i++) {
        elems[i].addEventListener('click', confirmIt, false);
    }

    function changeStatus(id,status){
        var token = '{!!csrf_token()!!}';
        if (confirm('Are you sure you want change this !!')){
        $.ajax(
        {
            url : "{{ url('admin-panel/posted-ques-ans/posted-ans-status') }}",
            type: 'GET',
            data: {
                'answer_id'       : id,
                'answer_status'   : status,
            },
            success: function (res)
            {
                if (res == "Success")
                {
                    location.reload();
                } else
                {
                    location.reload();
                }
            }
        });
    }
    }
   </script>


@endsection