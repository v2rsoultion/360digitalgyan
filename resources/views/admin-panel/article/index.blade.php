@extends('admin-panel.layout.header')
@section('content')


<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">{!! $page_title !!}</h4> 
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                
                <ol class="breadcrumb">
                    <li><a href="{!! URL::to('admin-panel/dashboard') !!}">Dashboard</a></li>
                    <li><a href="#">{!! trans('language.article_category') !!}</a></li>
                    <li class="active">{!! $page_title !!}</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    @if(session()->has('success'))
                        <p class="text-muted m-b-10 col-lg-12 col-md-12 col-sm-12 col-xs-12 font-13 green">
                            {{ session()->get('success') }}
                        </p>
                    @endif
                    @if($errors->any())
                        <p class="text-muted m-b-10 col-lg-12 col-md-12 col-sm-12 col-xs-12 font-13 red">{{$errors->first()}}</p>
                    @endif
                    <div class="clearfix"></div>
                    {!! Form::open(['files'=>TRUE,'id' => 'search-form' , 'class'=>'form-horizontal']) !!}
                        <div class="row clearfix">
                          
                            <div class="col-lg-3 col-md-3">
                                <div class="input-group">
                                    {!! Form::text('title', '',['class' => 'form-control','placeholder'=>trans('language.article_title'), 'id' => 'title','for'=>trans('language.article_title')]) !!}
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3">
                                <div class="input-group">
                                <select class="form-control select2 " id="category_id"  name="category_id" >
                                    <option value="">Select Article Category</option>
                                    @foreach ($category as  $category_data)
                                     <option value="{!! $category_data->article_category_id !!}">{!! $category_data->category_title !!}</option>   
                                    @endforeach
                                </select>
                                </div>
                            </div>
                            <div class="col-lg-1 col-md-1">
                                {!! Form::submit('Search', ['class' => 'btn btn-info ','name'=>'Search']) !!}
                            </div>
                            <div class="col-lg-1 col-md-1">
                                {!! Form::button('Clear', ['class' => 'btn btn-info ','name'=>'Clear', 'id' => "clearBtn"]) !!}
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <div class="col-sm-12">
                <div class="white-box">
                    <div class="table-responsive">
                        <table class="table dataTable " id="article-table">
                            <thead>
                                <tr>
                                    <th>S.No</th>
                                    <th>Title</th>
                                    <th>Article Category</th>                       
                                    <th>Short Description</th>
                                    <th>Long Description</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
            
        </div>
        <!-- /.row -->
        
    </div>
    <!-- /.container-fluid -->
    <footer class="footer text-center"> 2017 &copy; Ample Admin brought to you by themedesigner.in </footer>
</div>

<textarea class="textarea_editor form-control"></textarea>
<script>

    $(document).ready(function () {
        var table = $('#article-table').DataTable({
            // dom: 'Blfrtip',
            pageLength: 20,
            processing: true,
            serverSide: true,
            bLengthChange: false,
            // buttons: [
            //     { 	
            //         "extend"	: 'excelHtml5', 
            //         "text"		: '<i class="fas fa-download"></i> &nbsp; Export Category',
            //         "title"		: 'Category',
            //         "filename"	: 'category',
            //         exportOptions: {
            //             columns: [0, 1, 2, 3,6]
            //         },
            //     }
            // ],
            ajax: {
                url: '{{url('admin-panel/article/data')}}',
                data: function (d) {
                    d.title = $('input[name=title]').val();
                    d.category_id = $('select[name="category_id"]').val();
                    // d.category_level = $('select[name="category_level"]').val();
                }
            },
            columns: [
                {data: 'DT_Row_Index', name: 'DT_Row_Index' },
                {data: 'title', name: 'title'},
                {data: 'category_name', name: 'category_name'},
                {data: 'short_desc', name: 'short_desc'},
                {data: 'long_desc', name: 'long_desc'},
                {data: 'action', name: 'action'},
            ],
             
        });
        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
        });

        $('#clearBtn').click(function(){
            location.reload();
        })
       
    });

    var elems = document.getElementsByClassName('confirmation');
    var confirmIt = function (e) {
        if (!confirm('Are you sure?')) e.preventDefault();
    };
    for (var i = 0, l = elems.length; i < l; i++) {
        elems[i].addEventListener('click', confirmIt, false);
    }
    
    function changeStatus(id,status){
        var token = '{!!csrf_token()!!}';
        $.ajax(
        {
            url: "{{ url('admin-panel/article/article-status') }}",
            type: 'GET',
            data: {
                'articles_id': id,
                'status': status,
            },
            success: function (res)
            {
                if (res == "Success")
                {
                    location.reload();
                } else
                {
                    location.reload();
                }
            }
        });
    }
   $(function() {
            
        // For select 2
        $(".select2").select2();

        $(".tch").TouchSpin({
            min: 0,
            max: 1000000000,
        });
    });

</script>
@endsection




