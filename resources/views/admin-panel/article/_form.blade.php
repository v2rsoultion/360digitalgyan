

{!! Form::hidden('articles_id',old('articles_id',isset($category['articles_id']) ? $category['articles_id'] : ''),['class' => 'gui-input', 'id' => 'articles_id', 'readonly' => 'true']) !!}

@if($errors->any())
<p class="text-muted m-b-10 col-lg-12 col-md-12 col-sm-12 col-xs-12 font-13 red">{{$errors->first()}}</p>
@endif
<!-- <h3 class="box-title m-b-0 col-lg-12 col-md-12 col-sm-12 col-xs-12">Category</h3>
<p class="text-muted m-b-10 col-lg-12 col-md-12 col-sm-12 col-xs-12 font-13"> Enter new {!! trans('language.category') !!} </p> -->
<div class="row">
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="input-group">
            <label for="{!! trans('language.article_title') !!}">{!! trans('language.article_title') !!}</label>
            {!! Form::text('title', old('title',isset($article['title']) ? $article['title']: ''), ['class' => 'form-control','placeholder'=>trans('language.article_title'), 'id' => 'title','for'=>trans('language.article_title')]) !!}

            @if($errors->has('title')) <p class="help-block">{{ $errors->first('title') }}</p> @endif
        </div>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="input-group">
            <label for="{!! trans('language.article_category_name') !!}">{!! trans('language.article_category_name') !!}</label>
             <select class="form-control select2" id="article_category_id"  name="article_category_id" >
                    <option value="">Select Article Category</option>
                    @foreach ($category as  $category_data)
                    <option  value="{!! $category_data->article_category_id !!}"     @if(isset($article['article_category_id']) && $article['article_category_id'] == $category_data->article_category_id) selected  @endif>{!! $category_data->category_title !!}</option>   
                     @endforeach
             </select>

            @if($errors->has('article_category_id')) <p class="help-block">{{ $errors->first('article_category_id') }}</p> @endif
        </div>
    </div>

  
</div>        

<div class="row" >
      <div class="col-xs-12">
        <div class="input-group">
            <label for="{!! trans('language.category_short_desc') !!}">{!! trans('language.category_short_desc') !!}</label>
             <textarea class="textarea_editor form-control textareaheight" name="short_desc" rows="15" required>{!!isset($article['short_desc']) ? $article['short_desc']:''!!}</textarea>
       </div>  
    </div>
</div>
<div class="row" >
      <div class="col-xs-12">
         <div class="input-group">
            <label for="{!! trans('language.category_long_desc') !!}">{!! trans('language.category_long_desc') !!}</label>
             <textarea class="textarea_editor1 form-control textareaheight" rows="15" placeholder="Enter text ..." name="long_desc" required>{!! isset($article['long_desc']) ? $article['long_desc']:'' !!}</textarea>
         </div>
    </div>
</div>
<div class=" col-sm-9">
    <button type="submit" name="submit" class="btn btn-info waves-effect waves-light m-t-10">Submit</button>
</div>

<div class="clearfix"></div>
<script type="text/javascript">

    $(function() {
            
        // For select 2
        $(".select2").select2();

        $(".tch").TouchSpin({
            min: 0,
            max: 1000000000,
        });
    });

    jQuery(document).ready(function () {

        jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z\s]+$/i.test(value);
        }, "Only alphabetical characters");

        $("#article-form").validate({

            /* @validation states + elements 
             ------------------------------------------- */

            errorClass: "state-error",
            validClass: "state-success",
            errorElement: "em",
            ignore: ":hidden:not(textarea)",

            /* @validation rules 
             ------------------------------------------ */

            rules: {
                title: {
                    required: true,
                },
                article_category_id: {
                    required: true,
                },
                 WysiHtmlField: "required"
               
            },

            /* @validation highlighting + error placement  
             ---------------------------------------------------- */
            highlight: function (element, errorClass, validClass) {
                $(element).closest('.field').addClass(errorClass).removeClass(validClass);
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).closest('.field').removeClass(errorClass).addClass(validClass);
            },

            errorPlacement: function (error, element) {
                if (element.is(":radio") || element.is(":checkbox")) {
                    element.closest('.option-group').after(error);
                } else {
                    error.insertAfter(element.parent());
                }
            }
        });
        
    });

    

</script>