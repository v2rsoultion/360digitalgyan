<!DOCTYPE html>  
<html lang="en">
    <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="{!! URL::to('public/admin/plugins/images/favicon.png') !!}">
    <title>{!! trans('language.forgot_password') !!} | {!! trans('language.project_title') !!}</title>
    <!-- Bootstrap Core CSS -->
    {!! Html::style('public/admin/bootstrap/dist/css/bootstrap.min.css') !!}
    <!-- animation CSS -->
    {!! Html::style('public/admin/css/animate.css') !!}
    <!-- Custom CSS -->
    {!! Html::style('public/admin/css/style.css') !!}
    {!! Html::style('public/admin/css/custom.css') !!}
    <!-- color CSS -->
    {!! Html::style('public/admin/css/colors/default.css') !!}
    <!-- <link href="public/admin/css/colors/default.css" id="theme"  rel="stylesheet"> -->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    </head>
    <body>
        <!-- Preloader -->
        <div class="preloader">
        <div class="cssload-speeding-wheel"></div>
        </div>
        <section id="wrapper" class="new-login-register">
            <div class="lg-info-panel">
                    <div class="inner-panel">
                        <a href="javascript:void(0)" class="p-20 di"> <img src="{!! URL::to('public/admin/plugins/images/email_logo.png') !!}" alt=""></a>
                        <div class="lg-content">
                            <h2>{!! trans('language.project_title') !!}</h2>
                            <p class="text-muted">{!! trans('language.project_tagline') !!}</p>
                            
                        </div>
                    </div>
            </div>
            <div class="new-login-box">
                @if (session('status'))
                    <div class="alert alert-success ">
                        {{ session('status') }}
                    </div>
                @endif
                <div class="white-box padding-top-less" >
                    {!! Form::open(array('url' => '/admin-panel/password/email', 'files' => true, 'class' => "form-horizontal new-lg-form")) !!}
                
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <h3>Forgot Password</h3>
                            <p class="text-muted">Enter your Email and instructions will be sent to you! </p>
                        </div>
                    </div>
                    <div class="form-group  m-t-20">
                        <div class="col-xs-12">
                            <label>Email Address</label>
                            <input type="email" name="email" id="email" class="form-control {{ $errors->has('email') ? ' has-error' : '' }}" placeholder="{{ trans('language.login_email') }}">
                            
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    {{ $errors->first('email') }}
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group text-center m-t-20">
                    <div class="col-xs-12">
                    <button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Send Password Reset Link</button>
                    <a href="{{ url('/admin-panel/') }}" class="text-dark pull-right">Login ?</a> </div>
                    </div>
                    </div>

                {!! Form::close() !!}
                </div>
            </div>            
        
        
        </section>
        <!-- jQuery -->
        {!! Html::script('public/admin/plugins/bower_components/jquery/dist/jquery.min.js') !!}
        <!-- Bootstrap Core JavaScript -->
        {!! Html::script('public/admin/bootstrap/dist/js/bootstrap.min.js') !!}
        <!-- Menu Plugin JavaScript -->
        {!! Html::script('public/admin/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js') !!}
        <!--slimscroll JavaScript -->
        {!! Html::script('public/admin/js/jquery.slimscroll.js') !!}
        <!--Wave Effects -->
        {!! Html::script('public/admin/js/waves.js') !!}
        <!-- Custom Theme JavaScript -->
        {!! Html::script('public/admin/js/custom.min.js') !!}
        <!--Style Switcher -->
        {!! Html::script('public/admin/plugins/bower_components/styleswitcher/jQuery.style.switcher.js') !!}
    </body>
</html>

