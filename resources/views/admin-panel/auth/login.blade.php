
<!-- <div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Login</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/admin/login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember"> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>

                                <a class="btn btn-link" href="{{ url('/admin/password/reset') }}">
                                    Forgot Your Password?
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> -->

<!DOCTYPE html>  
<html lang="en">
    <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="{!! URL::to('public/admin/plugins/images/favicon.png') !!}">
    <title>{!! trans('language.project_title') !!}</title>
    <!-- Bootstrap Core CSS -->
    {!! Html::style('public/admin/bootstrap/dist/css/bootstrap.min.css') !!}
    <!-- animation CSS -->
    {!! Html::style('public/admin/css/animate.css') !!}
    <!-- Custom CSS -->
    {!! Html::style('public/admin/css/style.css') !!}
    {!! Html::style('public/admin/css/custom.css') !!}
    <!-- color CSS -->
    {!! Html::style('public/admin/css/colors/default.css') !!}
    <!-- <link href="public/admin/css/colors/default.css" id="theme"  rel="stylesheet"> -->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    </head>
    <body>
        <!-- Preloader -->
        <div class="preloader">
        <div class="cssload-speeding-wheel"></div>
        </div>
        <section id="wrapper" class="new-login-register">
            <div class="lg-info-panel">
                    <div class="inner-panel">
                       <img src="{!! URL::to('public/admin/plugins/images/email_logo.png') !!}" alt="" width="70%">
                        <div class="lg-content">
                          
                            
                        </div>
                    </div>
            </div>
            <div class="new-login-box">
                        <div class="white-box">
                            <h3 class="box-title m-b-0">Sign In to Admin</h3>
                            <small>Enter your details below</small>
                            {!! Form::open(array('url' => '/admin-panel/login', 'files' => true, 'class' => "form-horizontal new-lg-form")) !!}     
                            <div class="form-group  m-t-20">
                                <div class="col-xs-12">
                                    <label>Email Address</label>
                                    <input type="email" name="email" id="email" class="form-control {{ $errors->has('email') ? ' has-error' : '' }}" placeholder="{{ trans('language.login_email') }}">
                                   
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            {{ $errors->first('email') }}
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-12">
                                    <label>Password</label>
                                    <input type="password" name="password" id="password" placeholder="{{ trans('language.login_password') }}" class="form-control {{ $errors->has('password') ? ' has-error' : '' }}" />
                                   
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            {{ $errors->first('password') }}
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                            <div class="col-md-12">
                                <div class="checkbox checkbox-info pull-left p-t-0">
                                <input id="checkbox-signup" type="checkbox">
                                <label for="checkbox-signup"> Remember me </label>
                                </div>
                                <a href="{{ url('/admin-panel/password/reset') }}" class="text-dark pull-right"><i class="fa fa-lock m-r-5"></i> Forgot password?</a> </div>
                            </div>
                            <div class="form-group text-center m-t-20">
                            <div class="col-xs-12">
                                <button class="btn btn-info btn-lg btn-block btn-rounded text-uppercase waves-effect waves-light" type="submit">Log In</button>
                            </div>
                            </div>

                        {!! Form::close() !!}
                        </div>
            </div>            
        
        
        </section>
        <!-- jQuery -->
        {!! Html::script('public/admin/plugins/bower_components/jquery/dist/jquery.min.js') !!}
        <!-- Bootstrap Core JavaScript -->
        {!! Html::script('public/admin/bootstrap/dist/js/bootstrap.min.js') !!}
        <!-- Menu Plugin JavaScript -->
        {!! Html::script('public/admin/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js') !!}
        <!--slimscroll JavaScript -->
        {!! Html::script('public/admin/js/jquery.slimscroll.js') !!}
        <!--Wave Effects -->
        {!! Html::script('public/admin/js/waves.js') !!}
        <!-- Custom Theme JavaScript -->
        {!! Html::script('public/admin/js/custom.min.js') !!}
        <!--Style Switcher -->
        {!! Html::script('public/admin/plugins/bower_components/styleswitcher/jQuery.style.switcher.js') !!}
    </body>
</html>

