@extends('admin-panel.layout.header')
@section('content')
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">{!! $page_title !!}</h4> 
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                
                <ol class="breadcrumb">
                    <li><a href="{!! URL::to('admin-panel/dashboard') !!}">Dashboard</a></li>
                    <li><a href="#">Admin</a></li>
                    <li class="active">{!! $page_title !!}</li> 
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /row -->
          <div class="row">
            <div class="col-sm-12">
                <div class="white-box white-box-modify">
                
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/admin-panel/register') }}" id=register>
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                           

                            <div class="col-md-6">
                                 <label for="name" class=" control-label">Name</label>
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                          

                            <div class="col-md-6">
                                  <label for="email" class="control-label">E-Mail Address</label>
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                           

                            <div class="col-md-6">
                                 <label for="password" class="control-label">Password</label>
                                <input id="password" type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                           
                            <div class="col-md-6">
                                 <label for="password-confirm" class=" control-label">Confirm Password</label>

                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation">

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
$(document).ready(function() {
    $('#register').bootstrapValidator({
        fields: {
            name: {
                validators: {
                    notEmpty: {
                        message: 'Please enter Name'
                    },
                    regexp: {
                        regexp: /^[A-Za-z0-9-_ ]+$/i,
                        message: 'Please enter valid Name'
                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: 'Please enter email'
                    },
                    emailAddress: {
                        message: 'Please enter valid email address'
                    }
                }
            },
            'password': {
                validators: {
                    notEmpty: {
                        message: 'Please enter your new password'
                    },
                    regexp: {
                        regexp:  /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^\w\s]).{8,}$/,
                        message: 'Password must contain minimum 6 characters including one uppercase, one lowercase, one special character and numeric value.'
                    }
                }
            },
            
            'password_confirmation': {
                validators: {
                    notEmpty: {
                        message: 'Please enter your confirm password'
                    },
                    identical: {
                        field: 'password',
                        message: 'The password and its confirm are not the same'
                    }
                }
            },
        }
    });

    $('#phone').keypress(function( e ) {
        if(e.which === 32) 
            return false;
    });
    
    $('#password').keypress(function( e ) {
        if(e.which === 32) 
            return false;
    });
    
    $('#password-confirm').keypress(function( e ) {
        if(e.which === 32) 
            return false;
    });
    
});

</script>
@endsection
