<div class="row">
	<div>
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="input-group">
                <label for="{!! trans('language.category_name') !!}">
                    {!! trans('language.category_name') !!}
                </label>
                <select class="form-control select2 " id="trick_id" onchange="getSubcategories(this.value)" name="cat_id" >
                    <option value="">Select Category</option>
                    @foreach ($categories as  $category)
                        <option value="{!! $category->iq_category_id !!}" {{ $notification->notification_flag==$category->iq_category_id ? 'selected' : '' }} >
                            {!! $category->iq_category_name !!}
                        </option>   
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="input-group">
                <label for="{!! trans('language.subcategory_name') !!}">
                    {!! trans('language.subcategory_name') !!} 
                </label>
                <select class="form-control select2 " id="sub_cat_id"  name="sub_cat_id" >
                    <option value="">Select Sub Category</option>
                    @foreach ($subCategories as  $subCategory)
                        <option value="{!! $subCategory->sub_category_id !!}" {{ $notification->sub_category_id==$subCategory->sub_category_id ? 'selected' : '' }} >
                            {!! $subCategory->title !!}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>
        
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="input-group">
                <label for="">
                    Notification for 
                </label>
                <select class="form-control select2 " id="notification_for"  name="notification_for" >
                    <option value="">Select Notification for</option>
                    @foreach ($notificationsFor as $key => $notify)
                        <option value="{{ $key }}" {{ $notification->notification_for==$key ? 'selected' : '' }} >
                            {!! $notify !!}
                        </option>   
                    @endforeach
                </select>
            </div>
        </div>

		<div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
			<div class="input-group">
				<label for="{!! trans('language.notification_title') !!}">
					{{ trans('language.notification_title') }}
				</label>
				{{ Form::text('title',old('title',isset($notification['notification_title']) ? $notification['notification_title']: ''),["class"=>"form-control","placeholder"=>trans('language.notification_title')]) }}
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="white-box" style="margin: 0 -15px">
                <label for="input-file-disable-remove">
                    Notification Image
                </label>
                @php
                    if(!empty($notification['notification_image'])){
                        $notifyImage = url('uploads/'.$notification['notification_image']);
                    }else{
                        $notifyImage = url("/public/admin/nopreview-available.jpg");
                    } 
                @endphp
                <input type="file" name="image" id="input-file-disable-remove" class="dropify" data-show-remove="false" data-default-file="{!! $notifyImage !!}"/> 
            </div>
        </div>
	</div>
</div>
<div class="row">
	<div class=" col-sm-9">
	    <button type="submit" name="submit" class="btn btn-info waves-effect waves-light m-t-10">{{  $submit_button }}</button>
	</div>
</div>
<div class="clearfix"></div>