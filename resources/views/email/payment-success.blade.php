<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="UTF-8"/>
        <meta http-equiv="Content-Type" content="text/plain; charset=utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
        <meta name="MobileOptimized" content="320" />
        <meta name="HandHeldFriendly" content="true" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,600,700" rel="stylesheet">
            <title></title>
            <style type="text/css">
                table {
                    border-collapse:collapse !important 
                }
            </style>
    </head>
    <body bgcolor="#54565e" style="margin:0px auto; padding:65px 0px; padding-bottom: 30px; background:url('<?php echo url('public/frontend/images/email-template/bg.jpg') ?>') center top no-repeat">
        <!--   <body style="margin:0; padding:65px 0px;"> -->
        <div class="widtfull" style="width: 800px; margin: 0px auto;  padding: 25px;  background-color: rgb( 255, 255, 255 ); box-shadow: 0px 0px 7px 0px #ccc" >
            <center >
                <table width="100%" cellspacing="0" cellpadding="0" border="0" style="padding: 25px;">
                    <tr>
                        <td align="center" >
                            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                <tr>
                                    <td style="text-align: center;">
                                        <img src="{{url('public/frontend/images/logo.png')}}"  alt="img" width="175">
                                            <table width="100%" border="0">
                                                <tr>
                                                    <td>
                                                        <hr style="border-bottom: 5px solid #f0f0f0 !important;border: 0px; margin-top: 20px; margin-bottom: 40px;">
                                                    </td>
                                                </tr>
                                            </table>
                                            <table width="100%" border="0">
                                                <tr>
                                                    <td>
                                                        <h3 style="color: #ff8a0c; padding: 0px; margin: 0px; font-size: 20px; font-family: 'Poppins', sans-serif; font-weight: 500;">Hi <span style='text-transform:capitalize'>{!! $customer->fname.' '.$customer->lname !!}</span>,</h3>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table width="100%" border="0">
                                                <tr>
                                                    <td>
                                                        <h3 style="color: #1c1c1c; padding: 10px 0px; margin: 0px; font-size: 30px; font-family: 'Poppins', sans-serif; font-weight: 500;">Payment done successfully.</h3>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table width="100%" border="0">
                                                <tr>
                                                    <td>
                                                        <p style="color: #797d86; text-align: center; padding: 10px 10%; line-height: 25px; margin: 0px; font-size: 16px; font-family: 'Poppins', sans-serif; font-weight: 500;">We have received your payment.
                                                        </p>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table width="100%" border="0">
                                                <tr>
                                                    <td>
                                                        <h2 style="color: #1f3161; text-align: left; padding: 10px 15%; line-height: 25px; margin: 0px; font-size: 16px; font-family: 'Poppins', sans-serif; font-weight: 500;">Please find the details as follows:
                                                        </h2>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table class="table table-striped" width=80%; style="margin:0px auto; border:1px solid #e1e1e1;">
                                                <thead>
                                                    <tr style="color: #1f3161; font-weight:400; padding: 10px 0%; line-height: 25px; margin: 0px; font-size: 14px; font-family: 'Poppins';">
                                                        <th style="padding:6px 6px; border:1px solid #e1e1e1;">Plan Name</th>
                                                        <th style="padding:6px 6px; border:1px solid #e1e1e1;">Plan Amount</th>
                                                        <th style="padding:6px 6px; border:1px solid #e1e1e1;">Transaction Id</th>
                                                        <th style="padding:6px 6px; border:1px solid #e1e1e1;">Plan Duration (FY)</th>
                                                    </tr>
                                                </thead>
                                                <tr style="color:#909090; font-weight:400; padding: 10px 0%; line-height: 25px; margin: 0px; font-size: 14px; font-family: 'Poppins';">
                                                    <td style="padding:6px 19px; border:1px solid #e1e1e1;">{!! $customer->plan_name !!}</td>
                                                    <td style="padding:6px 28px; border:1px solid #e1e1e1;">Rs. {!! $customer->plan_amount !!}/-</td>
                                                    <td style="padding:6px 17px; border:1px solid #e1e1e1;">{!! $customer->transaction_id !!}</td>
                                                    <td style="padding:6px 6px; border:1px solid #e1e1e1;">
                                                        {!! $customer->financial_year !!} 
                                                    </td>
                                                </tr>
                                            </table>
                                            <table width="100%">
                                                <tr>
                                                    <td style="text-align: center; padding: 41px 20px 0px 80px; color: #797d86; font-size: 14px; font-family: 'Poppins', sans-serif; font-weight: 600; line-height: 50px;">
                                                        <p style="padding: 0px; margin: 0px; line-height: 22px;">Yours sincerely,</p>
                                                    </td>
                                                    <td style="font-size: 15px; font-family: 'Poppins', sans-serif; font-weight: 600; padding-right: 20px; color: #1c1c1c; text-align: right;" >  </td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: center; padding: 0px 20px 0px 80px; color: #797d86; font-size: 14px; font-family: 'Poppins', sans-serif; font-weight: 600; line-height: 28px;">
                                                        <p style="padding: 0px; margin: 0px; line-height: 28px;">{!! get_app_name() !!} Team</p>
                                                    </td>
                                                </tr>
                                            </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </center>
        </div>
        <div style="width: 850px; margin: 0px auto;background: url('<?php echo url('public/frontend/images/email-template/b1.png') ?>') center top no-repeat; background-size: cover; height: 170px;">
            <table width="100%" >
                <tr>
                    <td style="padding-left: 25px; padding-top: 30px;">
                        <p style="font-family: 'Poppins', sans-serif; font-weight: 600; color: #ffffff; font-size: 27px; padding: 0px; margin: 0px; " >{!! trans('language.punch_line1') !!}</p>
                        <p style="font-family: 'Poppins', sans-serif; font-weight: 600; color: #ffffff; font-size: 22px; padding: 0px; margin: 0px; ">{!! trans('language.punch_line2') !!}</p>
                        <p style="font-family: 'Poppins', sans-serif; font-weight: 600; color: #ffffff; font-size: 16px; padding: 0px; margin: 0px; line-height: 30px; ">Team {!!  get_app_name() !!}</p>
                    </td>
                    <td align="right" style="padding-top: 140px; padding-right: 25px; "> 
                        <img src="{{url('public/frontend/images/email-template/web.png')}}" style="vertical-align: middle;">  <a style="font-family: 'Muli', sans-serif; font-weight: 600; color: #ffffff; font-size: 16px; text-decoration: none;"  href="{!! url('/') !!}" title="" target="_blank">{!! url('/') !!}</a>
                    </td>
                </tr>
            </table>
        </div>
        <div style="width: 800px; margin: 0px auto;">
            <table width="100%">
                <tr align="center">
                    <td>
                        @foreach(get_social_account() as $social)
                        <a href="{{$social['url']}}" title="Google" target="_blank"><img src="{{url('public/frontend/images/email-template/'.$social['img'])}}" style="margin: 30px 4px 0px 4px;"></a>
                        @endforeach
                        <p style="font-family: 'Muli', sans-serif; font-weight: 600; color: #666666; font-size: 14px;" >{!! get_reserved_text() !!}</p>
                    </td>
                </tr>
            </table>
        </div>
    </body>
</html>
