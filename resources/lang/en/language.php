<?php

    return [
        "authorization_error_msg"    => "You don\"t have authorization !",
        "email_sender"               => "360 Digitalgyan Team",
        "project_title"              => "360 Digitalgyan",
        "project_tagline"            => "360 Digitalgyan.",
        "login_email"                => "Email Address",
        "login_password"             => "Password",
        "forgot_password"            => "Forgot Password",
        "reset_password"             => "Reset Password",
        "dashboard"                  => "Dashboard",
        "subadmin"                   => "Subadmin",
        "add_subadmin"               => "Add Subadmin",
        "username"                   => "Name",
        "view_subadmin"              => "View Subadmin",
        "edit_subadmin"              => "Edit Subadmin",
        /* added by hameed */
        "videos"            => "Videos",
        "view_videos"       => "View Videos",
        "add_video"        => "Add Video",
        "edit_video"        => "Edit Video",
        "video_title"       => "Video Title",
        "video_des"         => "Video Description",
        "video_img"         => "Video Image",
        "video_link"        => "Video Link",
        "slt_lan"           => "Select Language",
        "video_duration"    => "Video Duration",
        /* added by hameed */
        "testimonial"       => "Testimonials",
        "add_testimonial"   => "Add Testimonials",
        "view_testimonial"  => "View Testimonials",
        "testimonial_name"  => "Testimonials Name",
        "testimonial_des"   => "Testimonials Description",
        "testimonial_image" => "Testimonials Image",
        "edit_testimonial"  => "Edit Testimonials",
        /* added by hameed */
        "tags"              => "Tags",
        "add_tag"           => "Add Tag",
        "view_tags"         => "View Tags",
        "edit_tag"          => "Edit Tag",
        "tag_name"          => "Tag Name",
        /* added by hameed */
        "tips_tricks"       => "Trips & Tricks",
        "add_tricks"        => "Add Tricks",
        "view_tricks"       => "View Tricks",
        "edit_tricks"       => "Edit Tircks",
        "tricks_title"      => "Tricks Title",
        "trick_img"         => "Trick Image",
        "trick_des"         => "Trick Description",
        /* added by hameed */
        "terms"             => "Terms",
        "add_terms"         => "Add Terms",
        "view_terms"        => "View Terms",
        "edit_terms"        => "Edit Terms",
        "terms_img"         => "Terms Image",
        "terms_des"         => "Terms Description",
        "terms_title"       => "Terms Title",
        "terms_new"         => "Terms New",
        /* added by hameed */
        "greetings"         => "Greetings",
        "add_greetings"     => "Add Greetings",
        "view_greetings"    => "View Greetings",
        "edit_greetings"    => "Edit Greetings",
        "greetings_image"   => "Greeting Image",
        /* add by hameed */
        "posted_ques_ans"    => "Posted Question Answer",
        "add_posted_ques"    => "Add Posted Question",
        "edit_posted_ques"   => "Edit Posted Question",
        "view_posted_ques"   => "View Posted Question",
        "posted_ans"         => "Posted Answer",
        "question"           => "Question",
        "add_posted_ans"     => "Add Posted Answer",
        "edit_posted_ans"    => "Edit Posted Answer",
        "answer"             => "Answer",
        "ques_tag"           => "Question Tag",
        "tag"                => "Tag",
        /* added by hameed*/
        "share_banner"       => "Share Banner",
        "add_share_banner"   => "Add Share Banner",
        "view_share_banner"  => "View Share Banner",
        "edit_share_banner"  => "Edit Share Banner",
        "banner_img"         => "Banner Image",
        "banner_title"       => "Banner Title",
        /* added by hameed */
        "meta_keyword"       => "Meta Keyword",
        "meta_desc"          => "Meta Description",
        "study_material_img" => "Study Material Image",
        "coding"             => "Coding",
        "meta_title"              => "Meta Title",
        "study_material"     => "Study Material",
        "add_study_material" => "Add Study Material",
        "view_study_material"=> "View Study Material",
        "edit_study_material" => "Edit Study Material",
        "view_study_comments" => "View Study Comments",
        "comment"             => "Comment",
        /* added by hameed */

        "seo_results"          => "Seo Results",
        "view_seo_result"      => "View Seo Results",
        "paper_name"           => "Paper Name",
        "user_email"            => "User Email",

        /* added by hameed */

        "seo_suggestion"        => "Seo Suggestion",
        "view_seo_suggestion"   => "View Seo Suggestion",
        "user_name"             => "User Name",

        /* added by pankaj */
        "category"           => "Category", // For category module
        "add_category"       => "Add Category",
        "view_categories"    => "View Categories",
        "edit_category"      => "Edit Category",
        "category_name"      => "Category Name",
        "category_order"     => "Category Order",
        "category_quiz_time" => "Quiz Time",
        "category_icon"      => "Category Icon",
        /* added by hameed */
        "practice_quiz_questions"          => "Practice Quiz Questions", // For Practice quiz Question module
        "add_practice_questions"      => "Add Practice Questions",
        "view_practice_questions"     => "View Practice Questions",
        "edit_practice_questions"     => "Edit Practice Questions",

        "subcategory"           => "Sub Category", // For sub category module
        "add_subcategory"       => "Add Sub Category",
        "view_subcategories"    => "View Sub Categories",
        "edit_subcategory"      => "Edit Sub Category",
        "subcategory_name"      => "Sub Category Name",
        "subcategory_order"     => "Sub Category Order",
        "subcategory_quiz_time" => "Quiz Time",
        "subcategory_icon"      => "Sub Category Icon",

        "seo_users"             => "Seo Users",

        "news_category"           => "News Category", // For news category module
        "add_news_category"       => "Add News Category",
        "view_news_categories"    => "View News Categories",
        "edit_news_category"      => "Edit News Category",
        "news_category_name"      => "News Category Name",
        "news_category_icon"      => "News Category Icon", 

        "news"                    => "News", // For News module
        "add_news"                => "Add News",
        "view_news"               => "View News",
        "edit_news"               => "Edit News",
        "news_title"              => "News Title",
        "news_description"        => "News Description", 


        "marketing_blog"          => "Marketing Blogs", // For Marketing module
        "add_marketing_blog"      => "Add Marketing Blogs",
        "view_marketing_blog"     => "View Marketing Blogs",
        "edit_marketing_blog"     => "Edit Marketing Blogs",
        "marketing_title"         => "Marketing Title",
        "marketing_description"   => "Marketing Description",
        "view_blog_comments"      => "View Blog Comments",


        "interview_questions"          => "Interview Questions", // For InterView Question module
        "add_interview_questions"      => "Add Interview Questions",
        "view_interview_questions"     => "View Interview Questions",
        "edit_interview_questions"     => "Edit Interview Questions",
        "interview_title"              => "Interview Question",
        "quiz_title"                   => "Quiz Question",
        "view_interview_comments"      => "View Interview Comments",
        
        "question_title"               => "Question",
        "question_code"                => "Question Code",
        "answer_title"               => "Answer",
        "answer_code"                => "Answer Code",   
        "answer_first"            => "Answer 1st",
        "answer_second"            => "Answer 2nd",
        "answer_third"            => "Answer 3rd",
        "answer_forth"            => "Answer 4th",
        "answer_fifth"            => "Answer 5th",
        "answer_correct"          => "Correct Answer",
        "question_details"        => "Question Details",
        "correct_option"          => "Correct Option",       

        "quiz_questions"          => "Quiz Questions", // For quiz Question module
        "add_quiz_questions"      => "Add Quiz Questions",
        "view_quiz_questions"     => "View Quiz Questions",
        "edit_quiz_questions"     => "Edit Quiz Questions",

        /*notification*/
        "notification"           => "Notification",
        "notification_title"           => "Notification Title",
        "add_notification"        => "Add Notification",
        "view_notification"       => "View Notifications",
        "edit_notification"       => "Edit Notification",
        /* close */
        'category_title'             => 'Name',
        "article"                    => "Article", // For category module
        'article_title'              => "Article Name",
        "article_category_name"      => "Select Article Category",
        "add_article"                => "Add Article",
        "view_articles"              => "View Articles",
        "view_article"               => "View Articles",
        "edit_article"               => "Edit Article",
        "category_article"           => "Name",
        "category_short_desc"        => "Short Description",
        "category_long_desc"         => "Long Description",
        "faq"                        => "FAQ", // For category module
        "add_faq"                    => "Add FAQ",
        "view_faq"                   => "View FAQ",
        "edit_faq"                   => "Edit FAQ",
        "faq_question"               => "Add Question",
        "faq_answer"                 => "Add Answer",
        "itr"                        => "ITR", // For category module
        "add_itr"                    => "ADD ITR",
        "view_itr"                   => "View ITR",
        "edit_itr"                   => "Edit ITR",
        "faq_question"               => "Add Question",
        "faq_answer"                 => "Add Answer",
        'view_customer'              => 'View Customer',
        'email'                      => 'User Email',
        'view_itr'                   => 'View Customer ITR',
        'user_phone'                 => 'Phone number',
        // ------------ Change Password ------------
        'current_password'           => 'Current Password',
        'new_password'               => 'New Password',
        'confirm_new_password'       => 'Confirm New Password',
        //Master pages
        'pages_title'                => 'Title',
        'page_title'                 => 'Title',
        'pages_short_desc'           => 'Short description',
        'pages_long_desc'            => 'Long Description',
        'pages_meta_title'           => 'Meta Title',
        'pages_meta_description'     => 'Meta Description',
        'pages_meta_keyword'         => 'Meta Keyword',
        //Service page
        'services_title'             => 'Title',
        'services_title'             => 'Title',
        'services_short_desc'        => 'Short description',
        'services_long_desc'         => 'Long Description',
        'services_meta_title'        => 'Meta Title',
        'services_meta_description'  => 'Meta Description',
        'services_meta_keyword'      => 'Meta Keyword',
        'payment_success_subject'    => 'Payment Successful',
        'invalid_reset_request'      => 'Invalid password reset request',
        'account_inactive'           => "You cann't login because your account has been Blocked",
        'account_verification_link'  => 'Please check your email id and click on the verification link to activate your account.',
        'reset_password_message'     => 'Please check your email and click on the link to reset password.',
        'invalid_verification_token' => "Sorry your email cann't be identified with current token.",
        'reset_token_expire'         => "Your password reset token has been expired, Please try with new request",
        'reset_password_success'     => "Your password reset successfully.",
        'invalid_reset_request'      => "Invalid reset password request.",
        'punch_line1'                => "Get ITR ",
        'punch_line2'                => "Filled filed by experts in minutes.",
    ];

    