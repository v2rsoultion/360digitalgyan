<?php

return [
    'admin_pic'                   => array(
        'upload_path'        => '/uploads/admin/profile-pic/',
        'upload_thumb_path'  => '/uploads/admin/profile-pic/thumb/',
        'display_path'       => 'public/uploads/admin/profile-pic/',
        'display_thumb_path' => 'public/uploads/admin/profile-pic/thumb/',
    ),
    'social_account' => [
        'google_plus' => array(
            'url'   => 'https://plus.google.com/106421900076739124383',
            'img'   => 'google.png',
            'title' => 'Google plus',
        ),
        'facebook' => array(
            'url' => 'https://www.facebook.com/digitalgyan360/',
            'img' => 'fb.png',
            'title' => 'Facebook',
        ),
        'twitter' => array(
            'url' => 'https://twitter.com/360digitalgyan',
            'img' => 'twitter.png',
            'title' => 'Twitter',
        ),
        'linkedin' => array(
            'url' => 'https://www.linkedin.com/in/360digitalgyan/',
            'img' => 'in.png',
            'title' => 'Linkedin',
        ),
        'instagram' => array(
            'url' => 'https://www.instagram.com/360digitalgyan//',
            'img' => 'instagram.png',
            'title' => 'Instagram',
        ),
    ],
    'points'    => [
        'question_post'         => 5, // on question posted by user 
        'answer_post'           => 10, // on answer posted by user
        'like_question'         => 1, // on ques liked by user1 (point give to user1)
        'like_answer'           => 1, // on ans liked by user1 (point give to user1)
        'user_like_question'    => 2, // point give to user who post question
        'user_like_answer'      => 2, // point give to user who post answer
        'quiz_points_80'        => 30, // for quiz when get 80% marks
        'quiz_points_60'        => 20, // for quiz when get 60% marks
        'quiz_points_50'        => 10, // for quiz when get 50% marks
    ],
    'notification_topics_android' =>[
/* seo */        '30'   => '/topics/360DigitalGyan-Dev',
/* plc */        '55'   => '/topics/LearnPlcScada-Dev',
/* software */   '50'   => '/topics/SoftwareTesting-Dev',
/* web_dev */    '52'   => '',
/* python */     '53'   => '/topics/LearnPython-AllUsers-Dev',
/* java */       '49'   => '',
/* android */    '10'   => ''
    ],
    'notification_topics_ios' =>[
/* seo */        '30'   => '/topics/360Digital_Debug_Iphone',
/* plc */        '55'   => '/topics/PLCSCADA-Debug_Iphone',
/* software */   '50'   => '',
/* web_dev */    '52'   => '',
/* python */     '53'   => '',
/* java */       '49'   => '',
/* android */    '10'   => ''
    ],
/*  
** Live Topics for apps
*/
    'notification_topics_android_live' =>[
/* seo */        '30'   => '/topics/360DigitalGyan-Dev',
/* plc */        '55'   => '/topics/LearnPlcScada-Dev',
/* software */   '50'   => '/topics/SoftwareTesting-Dev',
/* web_dev */    '52'   => '',
/* python */     '53'   => '/topics/LearnPython-AllUsers-Dev',
/* java */       '49'   => '',
/* android */    '10'   => ''
    ],
    'notification_topics_ios_live' =>[
/* seo */        '30'   => '/topics/360Digital_Debug_Iphone',
/* plc */        '55'   => '/topics/PLCSCADA-Debug_Iphone',
/* software */   '50'   => '',
/* web_dev */    '52'   => '',
/* python */     '53'   => '',
/* java */       '49'   => '',
/* android */    '10'   => ''
    ],
    'notifications_for' => [
        '1' => 'Videos',
        '2' => 'Interview Question',
        '3' => 'Quiz Question',
        '4' => 'Technical Terms',
        '5' => 'Study Material',
        '6' => 'Question Answer',
        '7' => 'Posted Question'
    ]
];